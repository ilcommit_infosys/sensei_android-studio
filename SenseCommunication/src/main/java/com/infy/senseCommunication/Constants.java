/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.senseCommunication;

/**
 * @author Shruti_Bansal01@Infosys.com
 * @description Constants used in the package
 *
 */
public class Constants {
    public static String sensor_groupId = "23110";
    // public static String grpAccessPwd = "96162119D7509CA4B8D9165F0483BFF3";
    public static String grpAccessPwd = "ytlDQv1rO9n56tx";
    public static String file = "file";
    public static String name = "first_name";

    public static String last_name = "last_name";
    public static String email = "email";
    public static String gender = "gender";
    public static String height = "height";
    public static String weight = "weight";
    public static String password = "password";
    public static String age = "age";
    public static String userData = "user_data";

}
