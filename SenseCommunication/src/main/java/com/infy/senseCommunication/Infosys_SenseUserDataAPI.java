/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.senseCommunication;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import nl.sense_os.service.commonsense.SenseApi;
import nl.sense_os.userdataapi.SenseResponseException;
import nl.sense_os.userdataapi.SenseUserDataAPI;

/**
 * @author Shruti Bansal <shruti_bansal01@infosys.com>
 * @description Class to send and receive given user data
 */
public class Infosys_SenseUserDataAPI {
    String TAG = "Infosys_SenseUserDataAPI";

    /**
     * @param context
     * @param name
     * @param last_name
     * @param email
     * @param height
     * @param weight
     * @param password
     * @param age
     * @param gender
     * @throws SenseResponseException
     * @throws IOException
     * @throws JSONException
     * @description Sends given details of user to SenseUserAPI for cloud storage
     */
    public void sendUserData(Context context, String name, String last_name, String email, String height, String weight, String password, String age, String gender) throws SenseResponseException, IOException, JSONException {
        boolean useLive = true;
        // Specifying whether you want to use live server or stagint server
        SenseUserDataAPI userDataAPI = new SenseUserDataAPI(useLive);
        try {
            String sessionId = SenseApi.getSessionId(context);
            userDataAPI.setSessionId(sessionId);
        } catch (IllegalAccessException e) {
            Log.e(TAG, e.getMessage());
        }


        JSONObject userData = new JSONObject();
        userData.put(Constants.name, name);

        userData.put(Constants.last_name, last_name);
        userData.put(Constants.email, email);
        userData.put(Constants.gender, gender);
        userData.put(Constants.height, height);
        userData.put(Constants.weight, weight);
        userData.put(Constants.password, password);
        userData.put(Constants.age, age);


        int userId = getUserId(context);
        // Sending PUT request with the user data
        if (userId != 0)
            userDataAPI.putUserData(userId, userData);
        else
            throw new RuntimeException("user not found");

    }

    /**
     * @description retrieve userId of current logged user
     * @param context
     * @return int userId
     * @throws IOException
     * @throws JSONException
     */
    private int getUserId(Context context) throws IOException, JSONException {
        int userId = 0;
        JSONObject user = SenseApi.getUser(context);
        userId = Integer.parseInt(user.get("id").toString());
        Log.i(TAG, "userId:" + userId);
        return userId;

    }

    /**
     * @description method to retrieve profile data of current logged user
     * @param context
     * @return userData response JSONObject
     * @throws IOException
     * @throws JSONException
     * @throws SenseResponseException
     */
    public JSONObject getUserData(Context context) throws IOException, JSONException, SenseResponseException {
        boolean useLive = true;
        // Specifying whether you want to use live server or stagint server
        SenseUserDataAPI userDataAPI = new SenseUserDataAPI(useLive);
        try {
            String sessionId = SenseApi.getSessionId(context);
            userDataAPI.setSessionId(sessionId);
        } catch (IllegalAccessException e) {
            Log.e(TAG, e.getMessage());
        }

        int userId = getUserId(context);
        if (userId != 0) {
            JSONObject response = userDataAPI.getUserData(userId);
            Log.i(TAG, response.toString());
            return response;

        } else
            throw new RuntimeException("user not found");
    }
}
