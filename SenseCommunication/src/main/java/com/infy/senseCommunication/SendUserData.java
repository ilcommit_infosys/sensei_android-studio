/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.senseCommunication;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import nl.sense_os.service.commonsense.SenseApi;
import nl.sense_os.userdataapi.SenseResponseException;
import nl.sense_os.userdataapi.SenseUserDataAPI;

/**
 * @author Shruti Bansal <shruti_bansal01@infosys.com>
 * @description Class to send given user data
 */
public class SendUserData {

    /**
     * @description Sends given details of user to SenseUserAPI for cloud storage
     * @param context
     * @param name
     * @param last_name
     * @param email
     * @param height
     * @param weight
     * @param password
     * @param dob
     * @param gender
     * @throws SenseResponseException
     * @throws IOException
     * @throws JSONException
     */
    public void sendUserData(Context context, String name, String last_name, String email, String height, String weight, String password, String dob, String gender) throws SenseResponseException, IOException, JSONException {
        boolean useLive = true;
        // Specifying whether you want to use live server or stagint server
        SenseUserDataAPI userDataAPI = new SenseUserDataAPI(useLive);
        try {
            String sessionId = SenseApi.getSessionId(context);
            userDataAPI.setSessionId(sessionId);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        JSONObject userData = new JSONObject();
        userData.put("first_name", name);

        userData.put("last_name", last_name);
        userData.put("email", email);
        userData.put("gender", gender);
        userData.put("height", height);
        userData.put("weight", weight);
        userData.put("password", password);
        userData.put("date_of_birth", dob);


        int userId = 0;
        JSONObject user = SenseApi.getUser(context);
        userId = Integer.parseInt(user.get("id").toString());

        // Sending PUT request with the user data
        if (userId != 0)
            userDataAPI.putUserData(userId, userData);

    }
}
