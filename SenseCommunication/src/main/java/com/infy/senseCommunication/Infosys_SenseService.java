/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/

package com.infy.senseCommunication;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import nl.sense_os.service.ISenseServiceCallback;
import nl.sense_os.service.commonsense.SenseApi;
import nl.sense_os.service.constants.SenseDataTypes;
import nl.sense_os.service.constants.SensePrefs;
import nl.sense_os.service.constants.SensePrefs.Main.Advanced;
import nl.sense_os.service.constants.SenseUrls;

/**
 * @author Shruti_Bansal01@Infosys.com
 * @description Service class to register/login users. send datapoints for
 *              location, accelerometer, distance, step frequency and run
 *              summary sensors share sensors with sensei_group join group
 */

public class Infosys_SenseService {
    public static final String RESPONSE_CODE = "http response code";
    public static final String RESPONSE_CONTENT = "content";
    private static final String TAG = "SenseService";
    private static TelephonyManager sTelManager;
    private static SharedPreferences sAuthPrefs;
    private static SharedPreferences sMainPrefs;
    Context context;
    int result = -1;

    public Infosys_SenseService(Context context) {

        this.context = context;
        // plat = mApplication.getSensePlatform();
        // startSense();

    }

    /**
     * Joins a group
     *
     * @param context
     *            Context for getting preferences
     * @param groupId
     *            Id of the group to join
     * @return true if joined successfully, false otherwise
     * @throws JSONException
     *             In case of unparseable response from CommonSense
     * @throws IOException
     *             In case of communication failure to CommonSense
     */
    private static boolean joinGroup(Context context, String groupId)
            throws JSONException, IOException {
        if (null == sAuthPrefs) {
            sAuthPrefs = context.getSharedPreferences(SensePrefs.AUTH_PREFS,
                    Context.MODE_PRIVATE);
        }
        if (null == sMainPrefs) {
            sMainPrefs = context.getSharedPreferences(SensePrefs.MAIN_PREFS,
                    Context.MODE_PRIVATE);
        }

        String cookie;
        try {
            cookie = SenseApi.getCookie(context);
        } catch (IllegalAccessException e) {
            cookie = null;
        }
        boolean devMode = sMainPrefs.getBoolean(Advanced.DEV_MODE, false);

        // get userId
        String userId = SenseApi.getUser(context).getString("id");

        String url = devMode ? SenseUrls.GROUP_USERS_DEV
                : SenseUrls.GROUP_USERS;
        url = url.replaceFirst("%1", groupId);

        // create JSON object to POST
        final JSONObject data = new JSONObject();
        final JSONArray users = new JSONArray();
        final JSONObject item = new JSONObject();
        final JSONObject user = new JSONObject();
        user.put("id", userId);
        // user.put("access_password", "96162119D7509CA4B8D9165F0483BFF3");

        item.put("user", user);
        String pwd = SenseApi.hashPassword(Constants.grpAccessPwd);
        // item.put("access_password", pwd.toLowerCase());
        item.put("access_password", pwd.toLowerCase());
        users.put(item);
        data.put("users", users);
        // perform actual request
        Map<String, String> response = SenseApi.request(context, url, data,
                cookie);

        String responseCode = response.get(RESPONSE_CODE);
        boolean result = false;
        if ("201".equalsIgnoreCase(responseCode)) {
            result = true;
            Log.w(TAG, "Joined group: " + responseCode + "Response: "
                    + response);
        } else {
            Log.w(TAG, "Failed to join group! Response code: " + responseCode
                    + "Response: " + response);
            result = false;
        }

        return result;
    }

    public static String getDefaultDeviceUuid(Context context) {
        if (null == sTelManager) {
            sTelManager = ((TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE));
        }
        String uuid = sTelManager.getDeviceId();
        if (null == uuid) {
            // device has no IMEI, try using the Android serial code
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                uuid = Build.SERIAL;

            } else {
                Log.w(TAG, "Cannot get reliable device UUID!");
            }
        }
        Log.i(TAG, "Device uuid inside SendData.getDefaultDeviceUuid:" + uuid);
        return uuid;
    }

    public int attemptRegistrationAtCloud(final String username,
                                          final String password, final String email, String address,
                                          String zipCode, String country, final String firstName,
                                          final String surname, final String mobileNumber,
                                          final ISenseServiceCallback callback) throws IllegalStateException,
            RemoteException {
        /*
		 * // startSense(); int result = -1; Log.v(TAG,
		 * "Attempting registration"); try { result =
		 * SenseApi.registerUser(context, username,
		 * SenseApi.hashPassword(password), firstName, surname, email,
		 * mobileNumber); } catch (JSONException e) { Log.e(TAG, e.toString());
		 * } catch (IOException e) { Log.e(TAG, e.toString()); } return result;
		 */
        new Thread() {

            @Override
            public void run() {
                int result = -1;
                try {
                    result = SenseApi.registerUser(context, username,
                            SenseApi.hashPassword(password), firstName,
                            surname, email, mobileNumber);
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                try {
                    callback.onRegisterResult(result);
                } catch (RemoteException e) {
                    Log.e(TAG,
                            "Failed to call back to bound activity after registration: "
                                    + e);
                }
            }
        }.start();
        return -1;
    }

    public void afterLoginSuccess() throws JSONException, IOException {
        // startSense();
        // join group sensei_group

        boolean flag = joinGroup(context, Constants.sensor_groupId);

        if (flag)
            Log.i(TAG, "Joined Sensei_group");
        else
            Log.i(TAG, "Failed to join Sensei_group");

    }

    public void attemptLogin(final String user, final String password,
                             final ISenseServiceCallback callback) throws IllegalStateException,
            RemoteException {
        // startSense();

        Log.v(TAG, "Attempting login");
        new Thread() {

            @Override
            public void run() {
                try {
                    result = SenseApi.login(context, user,
                            SenseApi.hashPassword(password));
                } catch (JSONException e) {
                    Log.e(TAG, e.toString());
                } catch (IOException e) {
                    Log.e(TAG, e.toString());
                }
                try {
                    Log.d(TAG, "trying onChangeLoginResult");
                    callback.onChangeLoginResult(result);

                } catch (RemoteException e) {
                    // TODO Auto-generated catch block
                    Log.d(TAG, "exception in onChangeLoginResult");
                    e.printStackTrace();
                }

            }
        }.start();
    }

    public boolean isUserLoggedIn() {
        boolean flag = false;
        try {
            JSONObject json = SenseApi.getUser(context);
            if (json != null) {
                flag = true;
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException while getUser");
        } catch (JSONException e) {
            Log.e(TAG, "JSONException while getUser");
        }

        return flag;
    }

    public boolean checkIfWifiWorking() {
        ConnectivityManager connManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            Log.i(TAG, "wi-fi service connected");
            return true;
        }
        Log.w(TAG, "wi-fi service not connected");
        return false;

    }

    public boolean addDataPoint(String name, String displayName,
                                String description, String dataType, JSONObject data, long timestamp) {
        Log.d(TAG, "Inside addDatapoint ");
        boolean flag = false;

        String cookie = null;
        String urlString = null;
        String SensorId = null;
        String deviceUuid = null;
        JSONObject sensorData = null, value = null;
        // JSONObject jsonObj = null;
        // making sample JSON data to upload
        try {

            // create sensor data JSON object with only 1 data point
            sensorData = new JSONObject();
            JSONArray dataArray = new JSONArray();
            value = new JSONObject();
            JSONArray xArray = new JSONArray();
            dataArray.put(data);
            value.put("value", dataArray);
            sensorData.put("data", value);
            cookie = SenseApi.getCookie(context);
        } catch (IllegalAccessException e) {
            Log.e(TAG, "addDataPoint cookie Exception", e);
            cookie = null;
        } catch (Exception e) {
            Log.e(TAG, "addDataPoint cookie Exception", e);

        }
        try {
            deviceUuid = getDefaultDeviceUuid(context);
            // Log.i(TAG,"Device uuid inside SendData.addDataPoint before calling getSensorId:"+deviceUuid);
            SensorId = SenseApi.getSensorId(context, name, description,
                    SenseDataTypes.JSON, deviceUuid);
            // Log.i("SendData","Obtained sensorId inside SendData after calling SenseApi.getSensorId:"+SensorId);
            Log.i("SendData", "Sensor details:" + name + " id:" + SensorId);

            urlString = SenseApi.getSensorUrl(context,
                    // Constants.accelerometerSensorName,
                    name, description, SenseDataTypes.JSON, deviceUuid);

        } catch (Exception e1) {
            Log.e(TAG, "Exception while getting sensor", e1);

        }

        if (SensorId == null) {
            try {
                SenseApi.registerSensor(context, name, name, description,
                        SenseDataTypes.JSON, data.toString(), Build.MODEL, deviceUuid);
                Log.d(TAG, " New Sensor registered");
                urlString = SenseApi.getSensorUrl(context, name, description,
                        SenseDataTypes.JSON, deviceUuid);
            } catch (Exception e) {
                Log.e(TAG, "Exception while registering sensor", e);
            }

        }
        try {
            Log.i("SendData", "urlString:" + urlString);
            Map<String, String> response = SenseApi.request(context, urlString,
                    sensorData, cookie);

            String code = response.get(SenseApi.RESPONSE_CODE);

            if (!"201".equals(code)) {
                flag = false;
                Log.w(TAG,
                        "Failed to Send sensor data to CommonSense! Response code: "
                                + code);
                throw new IOException("Incorrect response from CommonSense: "
                        + code);
            } else {
                flag = true;
            }

        } catch (Exception e) {

            Log.e(TAG, "Exception while sending data", e);

        }

        return flag;
    }


}
