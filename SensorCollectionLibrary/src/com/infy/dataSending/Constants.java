/**Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this 
 * Infosys proprietary software program ("Program"), this Program is protected 
 * by copyright laws, international treaties and other pending or existing 
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation 
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and 
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.dataSending;

/**
 * @author Shruti_Bansal01@Infosys.com
 * @description Constants used in the package
 * 
 */
public class Constants {
	public static String sensor_groupId = "23110";
	public static String personality = "personality";
	public static String injuries = "injuries";
	public static String workoutSensorName = "sRunWorkout";
	public static String accelerometerSensorName = "sAccelerometer";
	public static String ExertionSensorName = "sExertion";
	public static String stepFrequencySensorName = "sStepFrequency";
	public static String locationSensorName = "sLocation";
	public static String quesSensorName = "sQuestionnaire";
	public static String distanceSensorName = "sDistance";
	public static String accelerometerX = "x-axis";
	public static String accelerometerY = "y-axis";
	public static String accelerometerZ = "z-axis";
	public static String timestamp = "timestamp";
	public static String datapoint="dataPoint";
	public static String stepFreq = "stepFrequency";
	public static String exertion = "exertionColor";
	public static String file = "file";
	public static String locationLat = "latitude";
	public static String locationlong = "longitude";
	public static String locationSpeed = "speed";
	public static String locationAccuracy = "accuracy";
	public static String locationBearing = "bearing";
	public static String distance = "distance";
	public static String quesType = "quesType";
	public static String quesResult = "quesResult";

	public static String locationAltitude = "altitude";
	public static String averageHeartRate = "averageHeartRate";
	public static String averageSpeed = "averageSpeed";
	public static String averageStepFrequency = "averageStepFrequency";
	public static String totalDistance = "totalDistance";
	public static String totalduration = "totalDuration";
	public static String startTime = "startTimestamp";
	public static String stopTime = "stopTimestamp";
	public static String affect = "Affect";
	public static String pleasure = "Pleasure";
	public static String dominance = "Dominance";
	public static String arousal = "Arousal";

}
