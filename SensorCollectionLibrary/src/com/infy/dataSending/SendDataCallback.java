/**Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this 
 * Infosys proprietary software program ("Program"), this Program is protected 
 * by copyright laws, international treaties and other pending or existing 
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation 
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and 
 * will be prosecuted to the maximum extent possible under the law
 **/

package com.infy.dataSending;

public interface SendDataCallback {
	public void onDistanceUpload(boolean result);

	public void onAccelerometerUpload(boolean flag);

	public void onLocationUpload(boolean flag);

	public void onStepFrequencyUpload(boolean flag);
	public void onExertionUpload(boolean flag);
	public void onQuesUpload(boolean flag);
	public void onRunStatisticsUpload(boolean flag);
}
