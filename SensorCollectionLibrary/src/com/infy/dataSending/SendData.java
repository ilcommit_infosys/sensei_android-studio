/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/

package com.infy.dataSending;

/**
 * @author shruti_bansal01@Infosys.com, kushagra_kapoor01@Infosys.com
 * @description Service for Sending data to server every 2 minutes
 *
 */

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.infy.senseCommunication.Infosys_SenseService;

import org.hva.createit.scl.data.AccelerometerData;
import org.hva.createit.scl.data.AffectData;
import org.hva.createit.scl.data.DistanceData;
import org.hva.createit.scl.data.ExertionData;
import org.hva.createit.scl.data.LocationData;
import org.hva.createit.scl.data.QuestionnaireData;
import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.scl.data.StepfrequencyData;
import org.hva.createit.scl.dataaccess.AccelerometerDAO;
import org.hva.createit.scl.dataaccess.DistanceDAO;
import org.hva.createit.scl.dataaccess.ExertionDAO;
import org.hva.createit.scl.dataaccess.LocationDAO;
import org.hva.createit.scl.dataaccess.QuestionnaireDAO;
import org.hva.createit.scl.dataaccess.RunStatisticsDAO;
import org.hva.createit.scl.dataaccess.StepfrequencyDAO;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import nl.sense_os.service.commonsense.SenseApi;
import nl.sense_os.service.constants.SenseDataTypes;
import nl.sense_os.service.constants.SensePrefs;
import nl.sense_os.service.constants.SensePrefs.Main.Advanced;
import nl.sense_os.service.constants.SenseUrls;

/* Class To Send Data to server in background */

public class SendData extends Service {
    public static final String RESPONSE_CODE = "http response code";
    public Infosys_SenseService infosys_senseService=new Infosys_SenseService(this);
    public static final String RESPONSE_CONTENT = "content";
    private static final String TAG = "SendData";
    static SendThread thread = null;
    //JSONObject dataValue = null;
    private static volatile AtomicBoolean running;
    // private Handler mHandler;
    private static SharedPreferences sAuthPrefs;
    private static SharedPreferences sMainPrefs;
    private static volatile AtomicBoolean firstTime;
    AccelerometerDAO ad;
    boolean flag = false;
    JSONObject accDataValue = null;
    JSONObject workoutDataValue = null;
    JSONObject locDataValue = null;
    JSONObject distDataValue = null;
    JSONObject sfDataValue = null;

	/*
     * protected void onHandleIntent(Intent intent) { mHandler.post(new
	 * Runnable() {
	 * 
	 * @Override public void run() { Toast.makeText(getApplicationContext(),
	 * "Hello Toast!", Toast.LENGTH_LONG).show(); } }); }
	 */
    JSONObject quesDataValue = null;
    JSONObject exDataValue = null;

	/*
	 * protected void onProgressUpdate(String... text) {
	 * //yfinalResult.setText(text[0]); // Things to be done while execution of
	 * long running operation is in // progress. For example updating
	 * ProgessDialog
	 * 
	 * Toast.makeText(context, text[0], Toast.LENGTH_SHORT).show(); Log.d(TAG,
	 * "text[0]"); }
	 */

	/*
	 * @Override protected String doInBackground(String... params) { String
	 * result = ""; result = sendUnuploadedData(); //publishProgress(result);
	 * return result; }
	 */
    private Context context = this;
    private int sleepTime = 120000;
    private Infosys_SenseService senseService;
    private SendDataCallback callback = new SendDataCallback() {

        @Override
        public void onDistanceUpload(boolean flag) {
            if (flag) {

                DistanceDAO lao = new DistanceDAO(context);
                List<DistanceData> slist = new ArrayList<DistanceData>();
                slist = lao.getUnuploadedItems();
                for (DistanceData loc : slist) {
                    loc.setUploaded(true);
                    lao.update(loc);
                }
                Log.i(TAG, "Distance datapoint uploaded successfully");

            } else {
                Log.w(TAG, "Couldn't upload Distance datapoint");
            }

        }

        @Override
        public void onAccelerometerUpload(boolean flag) {

            if (flag) {

                AccelerometerDAO lao = new AccelerometerDAO(context);

                List<AccelerometerData> slist = new ArrayList<AccelerometerData>();
                slist = lao.getUnuploadedItems();

                for (AccelerometerData acc : slist) {

                    acc.getMeasurementX().setUploaded(true);
                    acc.getMeasurementY().setUploaded(true);
                    acc.getMeasurementZ().setUploaded(true);
                    lao.update(acc);
                }
                Log.i(TAG, "Accelerometer datapoint uploaded successfully");

            } else {
                Log.w(TAG, "Couldn't upload Accelerometer datapoint");

            }
        }

        @Override
        public void onLocationUpload(boolean flag) {

            if (flag) {
                LocationDAO lao = new LocationDAO(context);
                List<LocationData> slist = new ArrayList<LocationData>();
                slist = lao.getUnuploadedItems();
                for (LocationData loc : slist) {

                    loc.getLatitude().setUploaded(true);
                    loc.getLongitude().setUploaded(true);
                    loc.getAltitude().setUploaded(true);
                    loc.getAccuracy().setUploaded(true);
                    loc.getSpeed().setUploaded(true);
                    loc.getBearing().setUploaded(true);
                    lao.update(loc);
                }
                Log.i(TAG, "Location datapoint uploaded successfully");
            } else {
                Log.w(TAG, "Couldn't upload Location datapoint");

            }
        }

        @Override
        public void onRunStatisticsUpload(boolean flag) {

            if (flag) {
                // System.out.println("Inside callback:"+flag);
                RunStatisticsDAO lao = new RunStatisticsDAO(context);
                List<RunOverviewData> slist = new ArrayList<RunOverviewData>();
                //slist.add(lao.getLastRunOverview());
                slist = lao.getUnuploadedItems();

                for (RunOverviewData acc : slist) {
                    acc.setUploaded(true);
                    acc.getAverageHeartRateMeasurementData().setUploaded(true);
                    acc.getAverageSpeedMeasurementData().setUploaded(true);
                    acc.getAverageStepFrequencyMeasurementData().setUploaded(
                            true);
                    acc.getDurationMeasurementData().setUploaded(true);
                    acc.getDistanceMeasurementData().setUploaded(true);
                    lao.update(acc);
                }
                Log.i(TAG, "Runstats datapoint uploaded successfully");
                // publishProgress("RunStats uploaded successfully");
            } else {
                Log.w(TAG, "Couldn't upload Runstats datapoint");
            }
        }

        @Override
        public void onStepFrequencyUpload(boolean flag) {

            if (flag) {
                StepfrequencyDAO lao = new StepfrequencyDAO(context);
                List<StepfrequencyData> slist = new ArrayList<StepfrequencyData>();
                slist = lao.getUnuploadedItems();

                for (StepfrequencyData sd : slist) {

                    sd.setUploaded(true);
                    lao.update(sd);
                }
                // Toast.makeText(context,
                // "StepFrequency uploaded successfully",
                // Toast.LENGTH_SHORT).show();
                Log.i(TAG, "StepFrequency datapoint uploaded successfully");
                // publishProgress("StepFrequency datapoint uploaded successfully");

            } else {
                // Toast.makeText(context,
                // "Couldn't upload StepFrequency datapoint!!",
                // Toast.LENGTH_SHORT).show();
                Log.w(TAG, "Couldn't upload StepFrequency datapoint");
                // publishProgress("Couldn't upload StepFrequency datapoint");
            }
        }

        @Override
        public void onExertionUpload(boolean flag) {

            if (flag) {
                ExertionDAO lao = new ExertionDAO(context);
                List<ExertionData> slist = new ArrayList<ExertionData>();
                slist = lao.getUnuploadedItems();

                for (ExertionData sd : slist) {

                    sd.setUploaded(true);
                    lao.update(sd);
                }
                // Toast.makeText(context,
                // "StepFrequency uploaded successfully",
                // Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Exertion datapoint uploaded successfully");
                // publishProgress("StepFrequency datapoint uploaded successfully");

            } else {
                // Toast.makeText(context,
                // "Couldn't upload StepFrequency datapoint!!",
                // Toast.LENGTH_SHORT).show();
                Log.w(TAG, "Couldn't upload Exertion datapoint");
                // publishProgress("Couldn't upload StepFrequency datapoint");
            }
        }

        @Override
        public void onQuesUpload(boolean flag) {

            if (flag) {
                QuestionnaireDAO lao = new QuestionnaireDAO(context);
                List<QuestionnaireData> slist = new ArrayList<QuestionnaireData>();
                slist = lao.getUnuploadedItems();

                for (QuestionnaireData sd : slist) {

                    sd.setUploaded(true);
                    lao.update(sd);
                }
                // Toast.makeText(context,
                // "StepFrequency uploaded successfully",
                // Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Ques datapoint uploaded successfully");
                // publishProgress("StepFrequency datapoint uploaded successfully");

            } else {
                // Toast.makeText(context,
                // "Couldn't upload StepFrequency datapoint!!",
                // Toast.LENGTH_SHORT).show();
                Log.w(TAG, "Couldn't upload Ques datapoint");
                // publishProgress("Couldn't upload StepFrequency datapoint");
            }
        }

    };
    private Handler mHandler;

    public SendData() {
        super();
    }

    public SendData(Context context) {
        this.context = context;
    }



    @Override
    public void onCreate() {
        // mHandler = new Handler(context.getMainLooper());
        firstTime = new AtomicBoolean(true);
        running = new AtomicBoolean(true);
        senseService = new Infosys_SenseService(this);
        if (thread == null) {
            thread = new SendThread(sleepTime);
            Thread thread1 = new Thread(thread);
            thread1.start();
            // serviceRunning();
        }
        mHandler = new Handler(context.getMainLooper());
    }

    // Read all unuploaded data and send datapoints to Sense cloud
    private boolean sendUnuploadedDataToCloud(String sensorName,
                                              JSONObject dataValue) {

        // Description of the sensor
        final String name = sensorName;
        final String displayName = sensorName + " data";
        final String dataType = SenseDataTypes.JSON;
        final String description = name;
        final JSONObject value = dataValue;
        final long timestamp = System.currentTimeMillis();
        boolean result = false;
        if (value != null) {

            result = infosys_senseService.addDataPoint(name, displayName, description, dataType,
                    value, timestamp);
        }
        // Share given sensor with group
        //if (name.equals(Constants.workoutSensorName)) {

        String uuid = SenseApi.getDefaultDeviceUuid(context);
        try {
            String id = SenseApi.getSensorId(context, sensorName,
                    sensorName, dataType, uuid);
            if (id != null) {
                boolean flag = shareSensor(context, id,
                        Constants.sensor_groupId);

            }
        } catch (IOException e) {
            Log.e(TAG, "IOException while adding Datapoint", e);
        } catch (JSONException e) {
            Log.e(TAG, "JSONException while adding Datapoint", e);
        }
        //}
        return result;
    }


    private String sendUnuploadedData() {
        String resultToDisplay = "";
        // SenseServiceStub senseService=mApplication.getSenseService();
        resultToDisplay = "Starting to send unuploaded data";
        try {
            UploadAccelerometerData();

            UploadDistanceData();
            UploadLocationData();
            UploadStepFrequencyData();
            UploadWorkoutStatsData();
            UploadExertionData();
            UploadQuesData();
        } catch (JSONException e) {
            resultToDisplay = "JSON parsing exception";
        }
        resultToDisplay = "Sent all unuploaded data";
        return resultToDisplay;
    }

    private boolean UploadAccelerometerData() throws JSONException {

        ad = new AccelerometerDAO(context);
        Log.d(TAG,
                "Unuploaded Measurements (AccelerometerData) count: "
                        + ad.getUnuploadedItemCount());
        List<AccelerometerData> slist = new ArrayList<AccelerometerData>();
        slist = ad.getUnuploadedItems();
        if (slist.size() != 0) {
            accDataValue = returnAccelerometerDataPoint(slist);

            flag = false;
            new Thread() {
                @Override
                public void run() {
                    if (accDataValue != null)
                        callback.onAccelerometerUpload(sendUnuploadedDataToCloud(

                                Constants.accelerometerSensorName, accDataValue));

                }
            }.start();

        } else {
            Log.w(TAG, "No unuploaded accelerometer data");
        }
        return false;
    }

    private boolean UploadWorkoutStatsData() throws JSONException {

        RunStatisticsDAO rao = new RunStatisticsDAO(context);
        Log.d(TAG,
                "Unuploaded Measurements (WorkoutData) count:"
                        + rao.getUnuploadedItemCount());
        List<RunOverviewData> slist = new ArrayList<RunOverviewData>();

        slist = rao.getUnuploadedItems();
        if (slist.size() != 0) {
            workoutDataValue = returnWorkoutDataPoint(slist);
            flag = false;
            new Thread() {
                @Override
                public void run() {
                    if (workoutDataValue != null)
                        callback.onRunStatisticsUpload(sendUnuploadedDataToCloud(

                                Constants.workoutSensorName, workoutDataValue));
                }
            }.start();

        } else {
            // Toast.makeText(context,
            // "No unuploaded workout data",Toast.LENGTH_SHORT);
            // publishProgress("No unuploaded workout data");
            Log.w(TAG, "No unuploaded workout data");
        }
        return false;
    }

    private boolean UploadLocationData() throws JSONException {

        LocationDAO lao = new LocationDAO(context);
        List<LocationData> slist = new ArrayList<LocationData>();
        slist = lao.getUnuploadedItems();
        if (slist.size() != 0) {
            locDataValue = returnLocationDataPoint(slist);

            flag = false;
            new Thread() {
                @Override
                public void run() {
                    if (locDataValue != null)
                        callback.onLocationUpload(sendUnuploadedDataToCloud(

                                Constants.locationSensorName, locDataValue));
                }
            }.start();

        } else {
            Log.w(TAG, "No unuploaded location data");
            // Toast.makeText(context,
            // "No unuploaded location data",Toast.LENGTH_SHORT);
            // publishProgress("No unuploaded location data");
        }
        return false;
    }

    private boolean UploadDistanceData() throws JSONException {

        DistanceDAO lao = new DistanceDAO(context);
        List<DistanceData> slist = new ArrayList<DistanceData>();
        slist = lao.getUnuploadedItems();
        Log.d("SendData", "UploadDistanceData count:" + slist.size());
        if (slist.size() != 0) {
            distDataValue = returnDistanceDataPoint(slist);

            flag = false;
            new Thread() {
                @Override
                public void run() {
                    if (distDataValue != null)
                        callback.onDistanceUpload(sendUnuploadedDataToCloud(
                                Constants.distanceSensorName, distDataValue));

                }
            }.start();

        } else {
            // Toast.makeText(context,
            // "No unuploaded distance data",Toast.LENGTH_SHORT);
            // publishProgress("No unuploaded distance data");

            Log.w(TAG, "No unuploaded distance data");
        }
        return false;
    }

    private boolean UploadStepFrequencyData() throws JSONException {

        StepfrequencyDAO sdao = new StepfrequencyDAO(context);
        List<StepfrequencyData> slist = new ArrayList<StepfrequencyData>();
        slist = sdao.getUnuploadedItems();
        Log.d(TAG, "UploadStepFrequencyData count" + slist.size());
        if (slist.size() != 0) {
            sfDataValue = returnStepFrequencyDataPoint(slist);
            flag = false;
            new Thread() {
                @Override
                public void run() {
                    if (sfDataValue != null)
                        callback.onStepFrequencyUpload(sendUnuploadedDataToCloud(

                                Constants.stepFrequencySensorName, sfDataValue));
                }
            }.start();

        } else {
            // Toast.makeText(context,
            // "No unuploaded Stepfrequency data",Toast.LENGTH_SHORT);
            // publishProgress("No unuploaded Stepfrequency data");
            Log.w(TAG, "No unuploaded Stepfrequency data");
        }
        return false;
    }

    private boolean UploadQuesData() throws JSONException {

        QuestionnaireDAO sdao = new QuestionnaireDAO(context);
        List<QuestionnaireData> slist = new ArrayList<QuestionnaireData>();
        slist = sdao.getUnuploadedItems();
        Log.d(TAG, "UploadQuesData count" + slist.size());
        if (slist.size() != 0) {

            quesDataValue = returnQuesDataPoint(slist);
            flag = false;
            new Thread() {
                @Override
                public void run() {
                    if (quesDataValue != null)
                        callback.onQuesUpload(sendUnuploadedDataToCloud(

                                Constants.quesSensorName, quesDataValue));
                }
            }.start();

        } else {
            // Toast.makeText(context,
            // "No unuploaded Stepfrequency data",Toast.LENGTH_SHORT);
            // publishProgress("No unuploaded Stepfrequency data");
            Log.w(TAG, "No unuploaded Ques data");
        }
        return false;
    }

    private boolean UploadExertionData() throws JSONException {

        ExertionDAO sdao = new ExertionDAO(context);
        List<ExertionData> slist = new ArrayList<ExertionData>();
        slist = sdao.getUnuploadedItems();
        Log.d(TAG, "UploadExertionData count" + slist.size());
        if (slist.size() != 0) {
            exDataValue = returnExertionDataPoint(slist);
            flag = false;
            new Thread() {
                @Override
                public void run() {
                    if (exDataValue != null)
                        callback.onExertionUpload(sendUnuploadedDataToCloud(

                                Constants.ExertionSensorName, exDataValue));
                }
            }.start();

        } else {
            // Toast.makeText(context,
            // "No unuploaded Stepfrequency data",Toast.LENGTH_SHORT);
            // publishProgress("No unuploaded Stepfrequency data");
            Log.w(TAG, "No unuploaded Exertion data");
        }
        return false;
    }

    private JSONObject returnQuesDataPoint(List<QuestionnaireData> accL)
            throws JSONException {
        JSONObject dataVal = new JSONObject();
        JSONArray dArray = new JSONArray();
        if (accL.size() != 0) {
            for (QuestionnaireData acc : accL) {
                JSONObject distance = new JSONObject();
                distance.put(Constants.quesType, acc.getQuestionnaire_type());
                dArray.put(distance);
                JSONObject result = new JSONObject();
                result.put(Constants.quesResult, acc.getResult());
                dArray.put(result);
                JSONObject timestamp = new JSONObject();
                timestamp.put(Constants.timestamp, acc.getTimestamp());
                dArray.put(timestamp);
            }
        }

        dataVal.put(Constants.datapoint, dArray);
        Log.d(TAG, "Ques dataPoint:" + dataVal);
        return dataVal;

    }

    private JSONObject returnAccelerometerDataPoint(List<AccelerometerData> accL)
            throws JSONException {

        JSONObject dataVal = new JSONObject();
        JSONArray dArray = new JSONArray();

        if (accL.size() != 0) {
            for (AccelerometerData acc : accL) {
                JSONObject xObject = new JSONObject();
                JSONObject yObject = new JSONObject();
                JSONObject zObject = new JSONObject();
                JSONObject tObject = new JSONObject();
                xObject.put(Constants.accelerometerX, acc.getX());
                yObject.put(Constants.accelerometerY, acc.getY());
                zObject.put(Constants.accelerometerZ, acc.getZ());
                tObject.put(Constants.timestamp, acc.getTimestamp());
                dArray.put(xObject);
                dArray.put(yObject);
                dArray.put(zObject);
                dArray.put(tObject);
            }
        }
        dataVal.put(Constants.datapoint, dArray);
        Log.d(TAG, "Accelerometer" + dataVal);
        return dataVal;
    }

    private JSONObject returnDistanceDataPoint(List<DistanceData> accL)
            throws JSONException {
        JSONObject dataVal = new JSONObject();
        JSONArray dArray = new JSONArray();
        if (accL.size() != 0) {
            for (DistanceData acc : accL) {
                JSONObject distance = new JSONObject();
                distance.put(Constants.distance, acc.getDistance());
                dArray.put(distance);
                JSONObject timestamp = new JSONObject();
                timestamp.put(Constants.timestamp, acc.getTimestamp());
                dArray.put(timestamp);
            }
        }

        dataVal.put(Constants.datapoint, dArray);
        Log.d(TAG, "Distance dataPoint" + dataVal);
        return dataVal;

    }

    private JSONObject returnStepFrequencyDataPoint(List<StepfrequencyData> accL)
            throws JSONException {
        JSONObject dataVal = new JSONObject();
        JSONArray dArray = new JSONArray();

        if (accL.size() != 0) {
            for (StepfrequencyData acc : accL) {
                JSONObject sObject = new JSONObject();
                JSONObject tObject = new JSONObject();
                sObject.put(Constants.stepFreq, acc.getStepFrequency());
                tObject.put(Constants.timestamp, acc.getTimestamp());
                dArray.put(sObject);
                dArray.put(tObject);
            }
        }
        dataVal.put(Constants.datapoint, dArray);

        Log.d(TAG, "StepFreq" + dataVal);
        return dataVal;
    }

    private JSONObject returnExertionDataPoint(List<ExertionData> accL)
            throws JSONException {
        JSONObject dataVal = new JSONObject();
        JSONArray dArray = new JSONArray();

//        if (accL.size() != 0) {
//            for (ExertionLevelData acc : accL) {
//                JSONObject sObject = new JSONObject();
//                JSONObject tObject = new JSONObject();
//                sObject.put(Constants.exertion, acc.getValueAsInt());
//                tObject.put(Constants.timestamp, acc.getTimestamp());
//                dArray.put(sObject);
//                dArray.put(tObject);
//            }
//        }
        dataVal.put(Constants.datapoint, dArray);

        Log.d(TAG, "Exertion" + dataVal);
        return dataVal;
    }

	/*
	 * protected void onPostExecute(String result) { Toast.makeText(context,
	 * "Data Uploaded", 10);
	 * 
	 * }
	 */

    private JSONObject returnWorkoutDataPoint(List<RunOverviewData> accL)
            throws JSONException {
        JSONObject dataVal = new JSONObject();
        JSONArray dArray = new JSONArray();

        if (accL.size() != 0) {
            for (RunOverviewData acc : accL) {
                JSONObject startObject = new JSONObject();
                JSONObject stopObject = new JSONObject();
                JSONObject distObject = new JSONObject();
                JSONObject durObject = new JSONObject();
                JSONObject avgStepObject = new JSONObject();
                JSONObject avgHRObject = new JSONObject();
                JSONObject avgSpeedObject = new JSONObject();
                JSONObject affectObject = new JSONObject();
                startObject.put(Constants.startTime, acc.getStart_timestamp());
                stopObject.put(Constants.stopTime, acc.getStop_timestamp());
                distObject.put(Constants.totalDistance, acc.getDistance());
                durObject.put(Constants.totalduration, acc.getDuration());
                avgStepObject.put(Constants.averageHeartRate, acc.getAverageStepFrequency());
                avgHRObject.put(Constants.averageSpeed, acc.getAverageHeartRate());
                avgSpeedObject.put(Constants.averageStepFrequency, acc.getAverageSpeed());
                List<AffectData> list = acc.getAffect();
                JSONArray runAffectArray = new JSONArray();
                for (AffectData affect : list) {
                    JSONArray affectArray = new JSONArray();
                    JSONObject p = new JSONObject();
                    p.put(Constants.pleasure, affect.getPleasure());
                    affectArray.put(p);
                    JSONObject d = new JSONObject();
                    d.put(Constants.dominance, affect.getDominance());
                    affectArray.put(d);
                    JSONObject a = new JSONObject();
                    a.put(Constants.arousal, affect.getArousal());
                    affectArray.put(a);
                    JSONObject t = new JSONObject();
                    t.put(Constants.timestamp, affect.getTimestamp());
                    affectArray.put(t);
                    runAffectArray.put(affectArray);
                }
                affectObject.put(Constants.affect, runAffectArray);
                dArray.put(startObject);
                dArray.put(stopObject);
                dArray.put(distObject);
                dArray.put(durObject);
                dArray.put(avgHRObject);
                dArray.put(avgSpeedObject);
                dArray.put(avgStepObject);
                dArray.put(affectObject);
            }
        }

        dataVal.put(Constants.datapoint, dArray);
        Log.d(TAG, "RunStats:" + dataVal);
        return dataVal;
    }

    private JSONObject returnLocationDataPoint(List<LocationData> accL)
            throws JSONException {
        JSONArray dArray = new JSONArray();
        JSONObject dataVal = new JSONObject();

//loc.getBearing().getValueAsFloat();		loc.getAccuracy().getValueAsFloat();		loc.getSpeed().getValueAsFloat();

        if (accL.size() != 0) {
            for (LocationData acc : accL) {
                JSONObject longObject = new JSONObject();
                JSONObject latObject = new JSONObject();
                JSONObject altObject = new JSONObject();
                JSONObject bearingObject = new JSONObject();
                JSONObject AccuracyObject = new JSONObject();
                JSONObject SpeedObject = new JSONObject();
                JSONObject tObject = new JSONObject();

                longObject.put(Constants.locationLat, acc.getLongitude().getValueAsDouble());
                latObject.put(Constants.locationlong, acc.getLatitude().getValueAsDouble());
                altObject.put(Constants.locationAltitude, acc.getAltitude().getValueAsDouble());
                bearingObject.put(Constants.locationAccuracy, acc.getBearing().getValueAsFloat());
                AccuracyObject.put(Constants.locationSpeed, acc.getAccuracy().getValueAsFloat());
                SpeedObject.put(Constants.locationBearing, acc.getSpeed().getValueAsFloat());
                tObject.put(Constants.timestamp, acc.getTimestamp());
                dArray.put(latObject);
                dArray.put(longObject);
                dArray.put(altObject);
                dArray.put(AccuracyObject);
                dArray.put(SpeedObject);
                dArray.put(bearingObject);
                dArray.put(tObject);

            }
        }

        dataVal.put(Constants.datapoint, dArray);

        Log.d(TAG, "Locn" + dataVal);
        return dataVal;
    }

    /**
     * Shares a sensor with a user or group
     *
     * @param context
     *            Context for getting preferences
     * @param sensorId
     *            Id of the sensor to share
     * @return true if shared successfully, false otherwise
     * @throws JSONException
     *             In case of unparseable response from CommonSense
     * @throws IOException
     *             In case of communication failure to CommonSense
     */
    private boolean shareSensor(Context context, String sensorId, String groupId)
            throws JSONException, IOException {

        if (null == sAuthPrefs) {
            sAuthPrefs = context.getSharedPreferences(SensePrefs.AUTH_PREFS,
                    Context.MODE_PRIVATE);
        }
        if (null == sMainPrefs) {
            sMainPrefs = context.getSharedPreferences(SensePrefs.MAIN_PREFS,
                    Context.MODE_PRIVATE);
        }

        String cookie;
        try {
            cookie = SenseApi.getCookie(context);
        } catch (IllegalAccessException e) {
            cookie = null;
        }
        boolean devMode = sMainPrefs.getBoolean(Advanced.DEV_MODE, false);
        String url = devMode ? SenseUrls.API_DEV : SenseUrls.API;
        url += "groups/" + groupId + "/sensors ";

        // https://api.sense-os.nl/groups/23110/sensors

        // create JSON object to POST
        final JSONObject data = new JSONObject();
        final JSONObject id = new JSONObject();
        id.put("id", sensorId);
        final JSONArray sensors = new JSONArray();

        sensors.put(id);
        data.put("sensors", sensors);

        // perform actual request
        Map<String, String> response = SenseApi.request(context, url, data,
                cookie);

        String responseCode = response.get(RESPONSE_CODE);
        boolean result = false;
        if ("201".equalsIgnoreCase(responseCode)) {
            Log.i(TAG, "Sensor shared successfully");

            result = true;
        } else {
            Log.w(TAG, "Failed to share sensor! Response code: " + response.get(RESPONSE_CONTENT));
            result = false;
        }

        return result;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    private void wifiRunning() {
        mHandler.post(new ToastRunnable("Wifi connected"));
    }

    private void wifiNotRunning() {
        mHandler.post(new ToastRunnable("Wifi not connected"));
    }

    private void serviceRunning() {
        mHandler.post(new ToastRunnable("Starting Data Sending Service"));
    }

    private void dataUpload() {
        mHandler.post(new ToastRunnable("Finishing Data Sending Service"));
    }

    private void dataNotUpload() {
        mHandler.post(new ToastRunnable("Data Upload Unsuccessful"));
    }

    @Override
    public void onDestroy() {
        running.set(false);
        this.mHandler.removeCallbacks(thread);
        mHandler.removeCallbacksAndMessages(null);
    }

    public class SendThread implements Runnable {

        int sleep;

        public SendThread(int sleep) {
            this.sleep = sleep;

        }

        public void setSleep(int sleep) {
            this.sleep = sleep;
        }

        public void terminate() {

            running.set(false);
        }

        public void setRuning() {

            running.set(true);
        }

        @Override
        public void run() {

            while (running.get()) {
                if (firstTime.get()) {
                    serviceRunning();
                }
                firstTime.set(false);
                if (senseService.checkIfWifiWorking()) {
                    wifiRunning();
                    String result = "";

                    result = sendUnuploadedData();
                    if (result != null) {
                        dataUpload();
                    } else {
                        dataNotUpload();
                    }
                    // Toast.makeText(context,
                    // "Data Uploaded,result="+result,Toast.LENGTH_SHORT).show();

                    Log.i(TAG, "Data Uploaded");
                } else {
                    wifiNotRunning();
                }
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    Log.e(TAG, e.toString());
                    Log.e(TAG, "Exception while sending data", e);
                }

            }
        }
    }

    private class ToastRunnable implements Runnable {
        String mText;

        public ToastRunnable(String text) {
            mText = text;
        }

        @Override
        public void run() {
            Toast.makeText(getApplicationContext(), mText, Toast.LENGTH_SHORT)
                    .show();
        }
    }

} // end CallAPI

