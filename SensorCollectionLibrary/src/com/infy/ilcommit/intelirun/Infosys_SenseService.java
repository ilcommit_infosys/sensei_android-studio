/**Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this 
 * Infosys proprietary software program ("Program"), this Program is protected 
 * by copyright laws, international treaties and other pending or existing 
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation 
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and 
 * will be prosecuted to the maximum extent possible under the law
 **/

package com.infy.ilcommit.intelirun;

import java.io.IOException;
import java.util.Map;

import nl.sense_os.service.ISenseServiceCallback;
import nl.sense_os.service.commonsense.SenseApi;
import nl.sense_os.service.constants.SensePrefs;
import nl.sense_os.service.constants.SensePrefs.Main.Advanced;
import nl.sense_os.service.constants.SenseUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.RemoteException;
import android.util.Log;

/**
 * @author Shruti_Bansal01@Infosys.com
 * @description Service class to register/login users. send datapoints for
 *              location, accelerometer, distance, step frequency and run
 *              summary sensors share sensors with sensei_group join group
 */

public class Infosys_SenseService {
	private static final String TAG = "SenseService";
	Context context;
	public static final String RESPONSE_CODE = "http response code";
	public static final String RESPONSE_CONTENT = "content";
	private static SharedPreferences sAuthPrefs;
	private static SharedPreferences sMainPrefs;

	public Infosys_SenseService(Context context) {

		this.context = context;
		// plat = mApplication.getSensePlatform();
		// startSense();

	}

	/*
	 * private void startSense() { try { senseService =
	 * mApplication.getSenseService(); } catch (Exception e) { Log.e(TAG,
	 * "Sense service not up!"); } if (senseService != null) {
	 * 
	 * SetPreferences(); }
	 * 
	 * //return senseService;
	 * 
	 * }
	 * 
	 * private void SetPreferences() { Log.v(TAG, "Setting preferences");
	 * 
	 * // enable main state senseService.toggleMain(false);
	 * senseService.setPrefBool(Advanced.ENCRYPT_CREDENTIAL, true);
	 * senseService.setPrefString(
	 * SensePrefs.Main.Advanced.ENCRYPT_CREDENTIAL_SALT,
	 * "some salt !@#$%XCBCV");
	 * senseService.setPrefBool(SensePrefs.Main.Advanced.ENCRYPT_DATABASE,
	 * true); senseService.setPrefString(
	 * SensePrefs.Main.Advanced.ENCRYPT_DATABASE_SALT, "some salt !@#$%XCBCV");
	 * 
	 * // turn off some specific sensors
	 * senseService.setPrefBool(Ambience.LIGHT, true);
	 * senseService.setPrefBool(Ambience.CAMERA_LIGHT, false);
	 * senseService.setPrefBool(Ambience.PRESSURE, false);
	 * 
	 * // turn on specific sensors senseService.setPrefBool(Ambience.MIC, true);
	 * // NOTE: spectrum might be too heavy for the phone or consume too much //
	 * energy senseService.setPrefBool(Ambience.AUDIO_SPECTRUM, true);
	 * 
	 * // use the location sensor with the Google Play Service //
	 * FusedLocationProvider senseService.setPrefBool(Location.FUSED_PROVIDER,
	 * true); senseService.setPrefString(Location.FUSED_PROVIDER_PRIORITY,
	 * Location.FusedProviderPriority.BALANCED);
	 * 
	 * // set how often to sample // 1 := rarely (~every 15 min) // 0 := normal
	 * (~every 1 min) // -1 := often (~every 10 sec) // -2 := real time (~every
	 * sec, this setting affects power consumption // considerably!)
	 * senseService.setPrefString(SensePrefs.Main.SAMPLE_RATE,
	 * SensePrefs.Main.SampleRate.REAL_TIME);
	 * 
	 * // set how often to upload // 1 := eco mode (buffer data for 30 minutes
	 * before bulk uploading) // 0 := normal (buffer 5 min) // -1 := often
	 * (buffer 1 min) // -2 := real time (every new data point is uploaded
	 * immediately) senseService.setPrefString(SensePrefs.Main.SYNC_RATE,
	 * SensePrefs.Main.SyncRate.REAL_TIME); // // enable some specific sensor
	 * modules senseService.togglePhoneState(false);
	 * senseService.toggleAmbience(false); senseService.toggleMotion(false);
	 * senseService.toggleLocation(false);
	 * 
	 * // show message Log.v(TAG, "Preferences set");
	 * 
	 * }
	 */

	public int attemptRegistrationAtCloud(final String username,
			final String password, final String email, String address,
			String zipCode, String country, final String firstName,
			final String surname, final String mobileNumber,
			final ISenseServiceCallback callback) throws IllegalStateException,
			RemoteException {
		/*
		 * // startSense(); int result = -1; Log.v(TAG,
		 * "Attempting registration"); try { result =
		 * SenseApi.registerUser(context, username,
		 * SenseApi.hashPassword(password), firstName, surname, email,
		 * mobileNumber); } catch (JSONException e) { Log.e(TAG, e.toString());
		 * } catch (IOException e) { Log.e(TAG, e.toString()); } return result;
		 */
		new Thread() {

			@Override
			public void run() {
				int result = -1;
				try {
					result = SenseApi.registerUser(context, username,
							SenseApi.hashPassword(password), firstName,
							surname, email, mobileNumber);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				try {
					callback.onRegisterResult(result);
				} catch (RemoteException e) {
					Log.e(TAG,
							"Failed to call back to bound activity after registration: "
									+ e);
				}
			}
		}.start();
		return -1;
	}

	/**
	 * Joins a group
	 * 
	 * @param context
	 *            Context for getting preferences
	 * @param groupId
	 *            Id of the group to join
	 * @return true if joined successfully, false otherwise
	 * @throws JSONException
	 *             In case of unparseable response from CommonSense
	 * @throws IOException
	 *             In case of communication failure to CommonSense
	 */
	private static boolean joinGroup(Context context, String groupId)
			throws JSONException, IOException {
		if (null == sAuthPrefs) {
			sAuthPrefs = context.getSharedPreferences(SensePrefs.AUTH_PREFS,
					Context.MODE_PRIVATE);
		}
		if (null == sMainPrefs) {
			sMainPrefs = context.getSharedPreferences(SensePrefs.MAIN_PREFS,
					Context.MODE_PRIVATE);
		}

		String cookie;
		try {
			cookie = SenseApi.getCookie(context);
		} catch (IllegalAccessException e) {
			cookie = null;
		}
		boolean devMode = sMainPrefs.getBoolean(Advanced.DEV_MODE, false);

		// get userId
		String userId = SenseApi.getUser(context).getString("id");

		String url = devMode ? SenseUrls.GROUP_USERS_DEV
				: SenseUrls.GROUP_USERS;
		url = url.replaceFirst("%1", groupId);

		// create JSON object to POST
		final JSONObject data = new JSONObject();
		final JSONArray users = new JSONArray();
		final JSONObject item = new JSONObject();
		final JSONObject user = new JSONObject();
		user.put("id", userId);
		// user.put("access_password", "96162119D7509CA4B8D9165F0483BFF3");

		item.put("user", user);
		String pwd = SenseApi.hashPassword(Constants.grpAccessPwd);
		// item.put("access_password", pwd.toLowerCase());
		item.put("access_password", pwd.toLowerCase());
		users.put(item);
		data.put("users", users);
		// perform actual request
		Map<String, String> response = SenseApi.request(context, url, data,
				cookie);

		String responseCode = response.get(RESPONSE_CODE);
		boolean result = false;
		if ("201".equalsIgnoreCase(responseCode)) {
			result = true;
			Log.w(TAG, "Joined group: " + responseCode + "Response: "
					+ response);
		} else {
			Log.w(TAG, "Failed to join group! Response code: " + responseCode
					+ "Response: " + response);
			result = false;
		}

		return result;
	}

	public void afterLoginSuccess() throws JSONException, IOException {
		// startSense();
		// join group sensei_group

		boolean flag = joinGroup(context, Constants.sensor_groupId);

		if (flag)
			Log.i(TAG, "Joined Sensei_group");
		else
			Log.i(TAG, "Failed to join Sensei_group");

	}

	int result = -1;

	public void attemptLogin(final String user, final String password,
			final ISenseServiceCallback callback) throws IllegalStateException,
			RemoteException {
		// startSense();

		Log.v(TAG, "Attempting login");
		new Thread() {

			@Override
			public void run() {
				try {
					result = SenseApi.login(context, user,
							SenseApi.hashPassword(password));
				} catch (JSONException e) {
					Log.e(TAG, e.toString());
				} catch (IOException e) {
					Log.e(TAG, e.toString());
				}
				try {
					Log.d(TAG, "trying onChangeLoginResult");
					callback.onChangeLoginResult(result);

				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					Log.d(TAG, "exception in onChangeLoginResult");
					e.printStackTrace();
				}

			}
		}.start();
	}

	public boolean isUserLoggedIn() {
		boolean flag = false;
		try {
			JSONObject json = SenseApi.getUser(context);
			if (json != null) {
				flag = true;
			}
		} catch (IOException e) {
			Log.e(TAG, "IOException while getUser");
		} catch (JSONException e) {
			Log.e(TAG, "JSONException while getUser");
		}

		return flag;
	}

	public boolean checkIfWifiWorking() {
		ConnectivityManager connManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if (mWifi.isConnected()) {
			Log.i(TAG, "wi-fi service connected");
			return true;
		}
		Log.w(TAG, "wi-fi service not connected");
		return false;

	}

}
