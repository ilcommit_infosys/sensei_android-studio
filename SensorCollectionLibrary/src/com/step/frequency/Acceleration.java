package com.step.frequency;

/**
 * @author Cristian-Stefan Neacsu <neacsu.cristianstefan@gmail.com>
 * @description Class that stores the acceleration on each three axis, as well as the time stamp of the current acceleration 
 */
 
public class Acceleration{
	public double acc_x;
	public double acc_y;
	public double acc_z;
	public double timestamp;
	
	public Acceleration(double x, double y, double z, double t){
		acc_x = x;
		acc_y = y;
		acc_z = z;
		timestamp = t;
	}
}