package com.step.frequency;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.ArrayList;

/**
 * @author Cristian-Stefan Neacsu <neacsu.cristianstefan@gmail.com>
 * @description Class that implements common functions required in the step frequency detection process
 */
 
public class BaseFunctions{
	static BufferedReader reader;

	/**
	* @description 	Find the next power of 2, closed to input n
	* @param 		Integer n
	* @return  		Next power of 2, closest to n
	*/
	public static int nextPow2(int n) {
		int count = 0;

		/* First n in the below condition is for the case where n is 0*/
		if (n > 0 && ((n&(n-1)) == 0))
			return n;

		while( n != 0)
		{
			n  >>= 1;
			count += 1;
		}

		return 1<<count;
	}

	/**
	* @description 	Read acceleration used in debugging version
	* @param 		Integer no_samples_per_acc - number of samples to combine into an Acceleration object
	* @return  		Acceleration resulted from no_samples_per_acc samples
	*/
	public static Acceleration readAcc(int no_samples_per_acc){
		double acc_x, acc_y, acc_z, timestamp;
		acc_x = 0;
		acc_y = 0;
		acc_z = 0;
		timestamp = 0;

		//read the sum of accelerations
		for(int i=0; i<no_samples_per_acc; i++){
			String line="-1,-1,-1,-1,-1,-1";
			try {
				line = reader.readLine();
			} catch (IOException e) {
				System.out.println("Error reader:" + e);
			}
			String[] accs = line.split(",");
			acc_x += Double.parseDouble(accs[0]);
			acc_y += Double.parseDouble(accs[1]);
			acc_z += Double.parseDouble(accs[2]);
			timestamp += Double.parseDouble(accs[3]);
		}

		// compute the average
		acc_x /= no_samples_per_acc;
		acc_y /= no_samples_per_acc;
		acc_z /= no_samples_per_acc;
		timestamp /= no_samples_per_acc;

		Acceleration acc = new Acceleration(acc_x, acc_y, acc_z, timestamp);
		return acc;
	}

	/**
	* @description 	Interpolates inside the period p, starting from previous_values point
	* @param 		Period p , represent input period
	* @param 		Double frequency, represent the frequency of reading accelerations
	* @param  		int previous_values, threshold when the interpolation will be done
	* @return       the new interpolated period
	*/	
	public static Period interpolate(Period p, double frequency, int previous_values){

		Period result = new Period();
		for(int i=0; i<previous_values; i++){
			Acceleration a = p.getAtX(i);
			result.add(new Acceleration(
					a.acc_x,
					a.acc_y,
					a.acc_z,
					a.timestamp));
		}
		double ax, ay, az;
		double time_step = 1000/frequency; //ms
		double deltaT;
		Acceleration a1;
		Acceleration a2;
		double curr_time;


		if(previous_values != 0 ){
			a1 = p.getAtX(previous_values - 1);
			curr_time = a1.timestamp + time_step;
		}
		else{
			a1 = p.getAtX(0);
			previous_values = 1;
			result.add(new Acceleration(a1.acc_x,
					a1.acc_y,
					a1.acc_z,
					a1.timestamp));
			curr_time = a1.timestamp - (a1.timestamp%time_step) + time_step;
		}

		for(int i=previous_values; i<p.accelerations.size(); i++){
			a2 = p.getAtX(i);

			if(curr_time == a2.timestamp){
				result.add(new Acceleration(a2.acc_x,
						a2.acc_y,
						a2.acc_z,
						a2.timestamp));
				a1 = a2;
				curr_time += time_step;
				continue;
			}

			while(curr_time < a2.timestamp){
				deltaT = a2.timestamp - a1.timestamp;
				ax = (a2.acc_x - a1.acc_x)/deltaT *(curr_time - a1.timestamp) + a1.acc_x;
				ay = (a2.acc_y - a1.acc_y)/deltaT *(curr_time - a1.timestamp) + a1.acc_y;
				az = (a2.acc_z - a1.acc_z)/deltaT *(curr_time - a1.timestamp) + a1.acc_z;
				result.add(new Acceleration(ax, ay, az, curr_time));
				curr_time += time_step;
			}

			a1 = a2;
		}

		return result;
	}

	//linear interpolation2 function
	//I am supposing that timestamp(x) will be always positive
	/**
	* @description 	Interpolates two acceleration
	* @param 		Acceleration a1, first acceleration parameter
	* @param 		Acceleration a2, second acceleration parameter
	* @param  		double time, time stamp of the result
	* @return       the new interpolated acceleration
	*/	
	public static Acceleration interpolate(Acceleration a1, Acceleration a2, double time){

		double deltaT = a2.timestamp - a1.timestamp;
		double ax = (a2.acc_x - a1.acc_x)/deltaT *(time - a1.timestamp) + a1.acc_x;
		double ay = (a2.acc_y - a1.acc_y)/deltaT *(time - a1.timestamp) + a1.acc_y;
		double az = (a2.acc_z - a1.acc_z)/deltaT *(time - a1.timestamp) + a1.acc_z;
		Acceleration result = new Acceleration(ax, ay, az, time);
		return result;
	}

	/**
	* @description 	Returns the maximum size equal with value that represent the next power of two, closest to the double of largest period size
	* @param 		Period x
	* @param 		Period y
	* @return       maximum size required
	*/	
	public static int get_max_size(Period x, Period y){
		if(x.getSize() > y.getSize()){
			return BaseFunctions.nextPow2(2*x.getSize()-1);
		}
		else{
			return BaseFunctions.nextPow2(2*y.getSize()-1);
		}
	}

	/**
	* @description 	Returns the minimum size equal with value that represent the next power of two, closest to the double of smallest period size
	* @param 		Period x
	* @param 		Period y
	* @return       minimum size required
	*/	
	public static int get_min_size(Period x, Period y){
		if(x.getSize() < y.getSize()){
			return BaseFunctions.nextPow2(2*x.getSize()-1);
		}
		else{
			return BaseFunctions.nextPow2(2*y.getSize()-1);
		}
	}

	/**
	* @description 	Calculate median between Accelerations
	* @param 		ArrayList<Acceleration> al, array of accelerations
	* @param 		int time_ind, time stamp of the result
	* @return       Acceleration that represent median of the input array
	*/	
	public static Acceleration median(ArrayList<Acceleration> al, int time_ind){

		ArrayList<Double> axs = new ArrayList<Double>();
		ArrayList<Double> ays = new ArrayList<Double>();
		ArrayList<Double> azs = new ArrayList<Double>();
		for(int i=0; i<al.size(); i++){
			Acceleration acc = al.get(i);
			axs.add(acc.acc_x);
			ays.add(acc.acc_y);
			azs.add(acc.acc_z);
		}
		double time = al.get(time_ind).timestamp;
		Collections.sort(axs);
		Collections.sort(ays);
		Collections.sort(azs);

		Acceleration result;
		if(axs.size()%2 == 1){
			int ind = axs.size()/2;
			result = new Acceleration(axs.get(ind), ays.get(ind), azs.get(ind), time);
		}
		else{
			int ind = axs.size()/2;
			result = new Acceleration( (axs.get(ind) + axs.get(ind-1))/2, 
					(ays.get(ind) + ays.get(ind-1))/2, 
					(azs.get(ind) + azs.get(ind-1))/2, time);
		}

		return result;
	}

	/**
	* @description 	Calculate median of the Integer array
	* @param 		ArrayList<Integer> x, array of integers
	* @return       Integer value that represent median of the input array
	*/	
	public static double median(ArrayList<Integer> x){

		ArrayList<Integer> x_nan = new ArrayList<Integer>();
		for(int i=0; i<x.size(); i++)
			if(x.get(i) != -100)
				x_nan.add(x.get(i));

		Collections.sort(x_nan);

		if(x_nan.size() == 0)
			return 0;

		if(x_nan.size()%2 == 1){
			int ind = x_nan.size()/2;
			return x_nan.get(ind);
		}
		else{
			int ind = x_nan.size()/2;
			return (x_nan.get(ind) + x_nan.get(ind-1))/2;
		}
	}

	/**
	* @description 	Calculate percentile of the Double array
	* @param 		ArrayList<Double> x, array of doubles
	* @return       Double value that represent percentile of the input array
	*/	
	public static double percentile(ArrayList<Double> x){
		ArrayList<Double> x_sort = new ArrayList<Double>();
		for(int i=0;i<x.size();i++){
			int j=0;
			double x_i = x.get(i);
			while(j<x_sort.size() && x_sort.get(j)<x_i)
				j++;
			x_sort.add(j, x_i);
		}
		double i = x_sort.size()*0.2;
		double r = 0.2;
		return (0.5 - r) * x_sort.get((int) Math.floor(i-1)) + 
				(0.5 + r) * x_sort.get((int) Math.ceil(i-1));
	}

	/**
	* @description 	Calculate mean of the Integer array inside x_range
	* @param 		ArrayList<Integer> x, array of integers
	* @param		int x_range, the range needed
	* @return       Integer value that represent mean of the input array
	*/	
	public static double mean(ArrayList<Integer> x, int x_range){

		ArrayList<Integer> x_nan = new ArrayList<Integer>();
		for(int i=x.size()-x_range; i<x.size(); i++)
			if(x.get(i) != -100)
				x_nan.add(x.get(i));

		if(x_nan.size() == 0)
			return -1;

		int sum = 0;
		for(int i=0; i<x_nan.size(); i++){
			sum += x_nan.get(i);
		}
		return sum*1.0/x_nan.size();

	}

	/**
	* @description 	Calculate mean of the Acceleration array
	* @param 		ArrayList<Acceleration> al, array of accelerations
	* @return       Acceleration value that represent mean of the input array
	*/
	public static Acceleration mean(ArrayList<Acceleration> al){
		double sum_x=0, sum_y=0, sum_z=0, sum_t=0;
		for(int i=al.size(); i<al.size(); i++){
			Acceleration acc = al.get(i);
			sum_x += acc.acc_x;
			sum_y += acc.acc_y;
			sum_z += acc.acc_z;
			sum_t += acc.timestamp;
		}

		sum_x /= al.size();
		sum_y /= al.size();
		sum_z /= al.size();
		sum_t /= al.size();

		Acceleration result = new Acceleration(sum_x, sum_y, sum_z, sum_t);
		return result;
	}

	/**
	* @description 	Calculate minima in the Double array
	* @param 		ArrayList<Double> signal, values inside signal
	* @return       Integer value that represent the minima inside the signal
	*/
	public static int minimaCalc(ArrayList<Double> signal){
		ArrayList<Integer> signal_rank = new ArrayList<Integer>();
		ArrayList<Integer> signal_rank_sort = new ArrayList<Integer>();
		ArrayList<Double> signal_sort = new ArrayList<Double>();
		for(int i=0; i<signal.size(); i++){
			signal_rank.add(i);
			signal_sort.add(signal.get(i));
		}

		boolean swapped = true;
		int j = 0;
		while (swapped) {
			swapped = false;
			j++;
			int tmp;
			double tmp_sort;
			for (int i = 0; i < signal_sort.size() - j; i++) {                                       
				if (signal_sort.get(i) > signal_sort.get(i+1)) {                          
					tmp_sort = signal_sort.get(i);                   
					tmp = signal_rank.get(i);
					signal_sort.set(i, signal_sort.get(i+1));
					signal_rank.set(i, signal_rank.get(i+1));
					signal_sort.set(i+1, tmp_sort);
					signal_rank.set(i+1, tmp);
					swapped = true;
				}
			}      
		}
		
		int p = (int) Math.round(signal.size()/3.0);
		for(int i=0; i<p; i++)
			signal_rank_sort.add(signal_rank.get(i));
		Collections.sort(signal_rank_sort);
		int rank;
		//do the median
		if(p%2 == 1){
			int ind = p/2;
			rank = signal_rank_sort.get(ind);
		}
		else{
			int ind = p/2;
			rank = (int) Math.round((signal_rank_sort.get(ind) + signal_rank_sort.get(ind-1))/2.0);
		}
		
		int min_rank_index = Integer.MAX_VALUE;
		int min_sum = Integer.MAX_VALUE;
		for(int i=0; i<p; i++){
			if(Math.abs(signal_rank.get(i) - rank) < min_sum){
				min_rank_index = i;
				min_sum = Math.abs(signal_rank.get(i) - rank);
			}
		}

		return signal_rank.get(min_rank_index);
	}
}