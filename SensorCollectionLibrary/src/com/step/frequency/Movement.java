package com.step.frequency;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.MathIllegalStateException;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.TransformType;
import org.apache.commons.math3.util.FastMath;

import java.util.ArrayList;

/**
 * @author Cristian-Stefan Neacsu <neacsu.cristianstefan@gmail.com>
 * @description Class in charge with all the main functions needed by step detection algorithm
 */
public class Movement{
	private final int periods_no = 5;
	public ArrayList<Integer> prevDurationPeriods;
	public Period incommingPeriod, interpolatedPeriod;
	public double periodExtremeMin, periodExtremeMax; //in seconds
	public int incommingPeriodDuration; //in milliseconds
	public int newsampfreq = 50;

	public Movement(){
		//initialize the previous periods vector
		prevDurationPeriods = new ArrayList<Integer>();
		for(int i=0; i<periods_no; i++)
			prevDurationPeriods.add(-100);

		//initialize the current period
		incommingPeriod = new Period();
		interpolatedPeriod = new Period();

		//initialize the period extremes
		periodExtremeMin = 0.4;
		periodExtremeMax = 2;
		incommingPeriodDuration = 100;
	}


	//============================APACHE==============================================//
	/**
	 * {@code W_SUB_N_R[i]} is the real part of
	 * {@code exp(- 2 * i * pi / n)}:
	 * {@code W_SUB_N_R[i] = cos(2 * pi/ n)}, where {@code n = 2^i}.
	 */
	private static final double[] W_SUB_N_R =
		{  0x1.0p0, -0x1.0p0, 0x1.1a62633145c07p-54, 0x1.6a09e667f3bcdp-1
				, 0x1.d906bcf328d46p-1, 0x1.f6297cff75cbp-1, 0x1.fd88da3d12526p-1, 0x1.ff621e3796d7ep-1
				, 0x1.ffd886084cd0dp-1, 0x1.fff62169b92dbp-1, 0x1.fffd8858e8a92p-1, 0x1.ffff621621d02p-1
				, 0x1.ffffd88586ee6p-1, 0x1.fffff62161a34p-1, 0x1.fffffd8858675p-1, 0x1.ffffff621619cp-1
				, 0x1.ffffffd885867p-1, 0x1.fffffff62161ap-1, 0x1.fffffffd88586p-1, 0x1.ffffffff62162p-1
				, 0x1.ffffffffd8858p-1, 0x1.fffffffff6216p-1, 0x1.fffffffffd886p-1, 0x1.ffffffffff621p-1
				, 0x1.ffffffffffd88p-1, 0x1.fffffffffff62p-1, 0x1.fffffffffffd9p-1, 0x1.ffffffffffff6p-1
				, 0x1.ffffffffffffep-1, 0x1.fffffffffffffp-1, 0x1.0p0, 0x1.0p0
				, 0x1.0p0, 0x1.0p0, 0x1.0p0, 0x1.0p0
				, 0x1.0p0, 0x1.0p0, 0x1.0p0, 0x1.0p0
				, 0x1.0p0, 0x1.0p0, 0x1.0p0, 0x1.0p0
				, 0x1.0p0, 0x1.0p0, 0x1.0p0, 0x1.0p0
				, 0x1.0p0, 0x1.0p0, 0x1.0p0, 0x1.0p0
				, 0x1.0p0, 0x1.0p0, 0x1.0p0, 0x1.0p0
				, 0x1.0p0, 0x1.0p0, 0x1.0p0, 0x1.0p0
				, 0x1.0p0, 0x1.0p0, 0x1.0p0 };

	/**
	 * {@code W_SUB_N_I[i]} is the imaginary part of
	 * {@code exp(- 2 * i * pi / n)}:
	 * {@code W_SUB_N_I[i] = -sin(2 * pi/ n)}, where {@code n = 2^i}.
	 */
	private static final double[] W_SUB_N_I =
		{  0x1.1a62633145c07p-52, -0x1.1a62633145c07p-53, -0x1.0p0, -0x1.6a09e667f3bccp-1
				, -0x1.87de2a6aea963p-2, -0x1.8f8b83c69a60ap-3, -0x1.917a6bc29b42cp-4, -0x1.91f65f10dd814p-5
				, -0x1.92155f7a3667ep-6, -0x1.921d1fcdec784p-7, -0x1.921f0fe670071p-8, -0x1.921f8becca4bap-9
				, -0x1.921faaee6472dp-10, -0x1.921fb2aecb36p-11, -0x1.921fb49ee4ea6p-12, -0x1.921fb51aeb57bp-13
				, -0x1.921fb539ecf31p-14, -0x1.921fb541ad59ep-15, -0x1.921fb5439d73ap-16, -0x1.921fb544197ap-17
				, -0x1.921fb544387bap-18, -0x1.921fb544403c1p-19, -0x1.921fb544422c2p-20, -0x1.921fb54442a83p-21
				, -0x1.921fb54442c73p-22, -0x1.921fb54442cefp-23, -0x1.921fb54442d0ep-24, -0x1.921fb54442d15p-25
				, -0x1.921fb54442d17p-26, -0x1.921fb54442d18p-27, -0x1.921fb54442d18p-28, -0x1.921fb54442d18p-29
				, -0x1.921fb54442d18p-30, -0x1.921fb54442d18p-31, -0x1.921fb54442d18p-32, -0x1.921fb54442d18p-33
				, -0x1.921fb54442d18p-34, -0x1.921fb54442d18p-35, -0x1.921fb54442d18p-36, -0x1.921fb54442d18p-37
				, -0x1.921fb54442d18p-38, -0x1.921fb54442d18p-39, -0x1.921fb54442d18p-40, -0x1.921fb54442d18p-41
				, -0x1.921fb54442d18p-42, -0x1.921fb54442d18p-43, -0x1.921fb54442d18p-44, -0x1.921fb54442d18p-45
				, -0x1.921fb54442d18p-46, -0x1.921fb54442d18p-47, -0x1.921fb54442d18p-48, -0x1.921fb54442d18p-49
				, -0x1.921fb54442d18p-50, -0x1.921fb54442d18p-51, -0x1.921fb54442d18p-52, -0x1.921fb54442d18p-53
				, -0x1.921fb54442d18p-54, -0x1.921fb54442d18p-55, -0x1.921fb54442d18p-56, -0x1.921fb54442d18p-57
				, -0x1.921fb54442d18p-58, -0x1.921fb54442d18p-59, -0x1.921fb54442d18p-60 };

	/** The type of DFT to be performed. */
	private DftNormalization normalization = null;

	/**
	 * Creates a new instance of this class, with various normalization
	 * conventions.
	 *
	 * @param normalization the type of normalization to be applied to the
	 * transformed data
	 */

	/**
	 * Performs identical index bit reversal shuffles on two arrays of identical
	 * size. Each element in the array is swapped with another element based on
	 * the bit-reversal of the index. For example, in an array with length 16,
	 * item at binary index 0011 (decimal 3) would be swapped with the item at
	 * binary index 1100 (decimal 12).
	 *
	 * @param a the first array to be shuffled
	 * @param b the second array to be shuffled
	 */
	private static void bitReversalShuffle2(double[] a, double[] b) {
		final int n = a.length;
		final int halfOfN = n >> 1;

		int j = 0;
		for (int i = 0; i < n; i++) {
			if (i < j) {
				// swap indices i & j
				double temp = a[i];
				a[i] = a[j];
				a[j] = temp;

				temp = b[i];
				b[i] = b[j];
				b[j] = temp;
			}

			int k = halfOfN;
			while (k <= j && k > 0) {
				j -= k;
				k >>= 1;
			}
			j += k;
		}
	}

	/**
	 * Applies the proper normalization to the specified transformed data.
	 *
	 * @param dataRI the unscaled transformed data
	 * @param normalization the normalization to be applied
	 * @param type the type of transform (forward, inverse) which resulted in the specified data
	 */
	private static void normalizeTransformedData(final double[][] dataRI,
			final DftNormalization normalization, final TransformType type) {

		final double[] dataR = dataRI[0];
		final double[] dataI = dataRI[1];
		final int n = dataR.length;

		switch (normalization) {
		case STANDARD:
			if (type == TransformType.INVERSE) {
				final double scaleFactor = 1.0 / ((double) n);
				for (int i = 0; i < n; i++) {
					dataR[i] *= scaleFactor;
					dataI[i] *= scaleFactor;
				}
			}
			break;
		case UNITARY:
			final double scaleFactor = 1.0 / FastMath.sqrt(n);
			for (int i = 0; i < n; i++) {
				dataR[i] *= scaleFactor;
				dataI[i] *= scaleFactor;
			}
			break;
		default:
			/*
			 * This should never occur in normal conditions. However this
			 * clause has been added as a safeguard if other types of
			 * normalizations are ever implemented, and the corresponding
			 * test is forgotten in the present switch.
			 */
			throw new MathIllegalStateException();
		}
	}

	/**
	 * Computes the standard transform of the specified complex data. The
	 * computation is done in place. The input data is laid out as follows
	 * <ul>
	 *   <li>{@code dataRI[0][i]} is the real part of the {@code i}-th data point,</li>
	 *   <li>{@code dataRI[1][i]} is the imaginary part of the {@code i}-th data point.</li>
	 * </ul>
	 *
	 * @param dataRI the two dimensional array of real and imaginary parts of the data
	 * @param normalization the normalization to be applied to the transformed data
	 * @param type the type of transform (forward, inverse) to be performed
	 * @throws DimensionMismatchException if the number of rows of the specified
	 *   array is not two, or the array is not rectangular
	 * @throws MathIllegalArgumentException if the number of data points is not
	 *   a power of two
	 */
	public static void transformInPlace(final double[][][] dataRI, int dataRIlen,
			final DftNormalization normalization, final TransformType type) {

		//        if (dataRI.length != 2) {
		//            throw new DimensionMismatchException(dataRI.length, 2);
		//        }
		for(int e=0; e<dataRIlen; e++)
		{
			final double[] dataR = dataRI[e][0];
			final double[] dataI = dataRI[e][1];
			//        if (dataR.length != dataI.length) {
			//            throw new DimensionMismatchException(dataI.length, dataR.length);
			//        }

			final int n = dataR.length;
			//        if (!ArithmeticUtils.isPowerOfTwo(n)) {
			//            throw new MathIllegalArgumentException(
			//                LocalizedFormats.NOT_POWER_OF_TWO_CONSIDER_PADDING,
			//                Integer.valueOf(n));
			//        }

			if (n == 1) {
				return;
			} else if (n == 2) {
				final double srcR0 = dataR[0];
				final double srcI0 = dataI[0];
				final double srcR1 = dataR[1];
				final double srcI1 = dataI[1];

				// X_0 = x_0 + x_1
				dataR[0] = srcR0 + srcR1;
				dataI[0] = srcI0 + srcI1;
				// X_1 = x_0 - x_1
				dataR[1] = srcR0 - srcR1;
				dataI[1] = srcI0 - srcI1;

				normalizeTransformedData(dataRI[e], normalization, type);
				return;
			}

			bitReversalShuffle2(dataR, dataI);

			// Do 4-term DFT.
			if (type == TransformType.INVERSE) {
				for (int i0 = 0; i0 < n; i0 += 4) {
					final int i1 = i0 + 1;
					final int i2 = i0 + 2;
					final int i3 = i0 + 3;

					final double srcR0 = dataR[i0];
					final double srcI0 = dataI[i0];
					final double srcR1 = dataR[i2];
					final double srcI1 = dataI[i2];
					final double srcR2 = dataR[i1];
					final double srcI2 = dataI[i1];
					final double srcR3 = dataR[i3];
					final double srcI3 = dataI[i3];

					// 4-term DFT
					// X_0 = x_0 + x_1 + x_2 + x_3
					dataR[i0] = srcR0 + srcR1 + srcR2 + srcR3;
					dataI[i0] = srcI0 + srcI1 + srcI2 + srcI3;
					// X_1 = x_0 - x_2 + j * (x_3 - x_1)
					dataR[i1] = srcR0 - srcR2 + (srcI3 - srcI1);
					dataI[i1] = srcI0 - srcI2 + (srcR1 - srcR3);
					// X_2 = x_0 - x_1 + x_2 - x_3
					dataR[i2] = srcR0 - srcR1 + srcR2 - srcR3;
					dataI[i2] = srcI0 - srcI1 + srcI2 - srcI3;
					// X_3 = x_0 - x_2 + j * (x_1 - x_3)
					dataR[i3] = srcR0 - srcR2 + (srcI1 - srcI3);
					dataI[i3] = srcI0 - srcI2 + (srcR3 - srcR1);
				}
			} else {
				for (int i0 = 0; i0 < n; i0 += 4) {
					final int i1 = i0 + 1;
					final int i2 = i0 + 2;
					final int i3 = i0 + 3;

					final double srcR0 = dataR[i0];
					final double srcI0 = dataI[i0];
					final double srcR1 = dataR[i2];
					final double srcI1 = dataI[i2];
					final double srcR2 = dataR[i1];
					final double srcI2 = dataI[i1];
					final double srcR3 = dataR[i3];
					final double srcI3 = dataI[i3];

					// 4-term DFT
					// X_0 = x_0 + x_1 + x_2 + x_3
					dataR[i0] = srcR0 + srcR1 + srcR2 + srcR3;
					dataI[i0] = srcI0 + srcI1 + srcI2 + srcI3;
					// X_1 = x_0 - x_2 + j * (x_3 - x_1)
					dataR[i1] = srcR0 - srcR2 + (srcI1 - srcI3);
					dataI[i1] = srcI0 - srcI2 + (srcR3 - srcR1);
					// X_2 = x_0 - x_1 + x_2 - x_3
					dataR[i2] = srcR0 - srcR1 + srcR2 - srcR3;
					dataI[i2] = srcI0 - srcI1 + srcI2 - srcI3;
					// X_3 = x_0 - x_2 + j * (x_1 - x_3)
					dataR[i3] = srcR0 - srcR2 + (srcI3 - srcI1);
					dataI[i3] = srcI0 - srcI2 + (srcR1 - srcR3);
				}
			}

			int lastN0 = 4;
			int lastLogN0 = 2;
			while (lastN0 < n) {
				int n0 = lastN0 << 1;
				int logN0 = lastLogN0 + 1;
				double wSubN0R = W_SUB_N_R[logN0];
				double wSubN0I = W_SUB_N_I[logN0];
				if (type == TransformType.INVERSE) {
					wSubN0I = -wSubN0I;
				}

				// Combine even/odd transforms of size lastN0 into a transform of size N0 (lastN0 * 2).
				for (int destEvenStartIndex = 0; destEvenStartIndex < n; destEvenStartIndex += n0) {
					int destOddStartIndex = destEvenStartIndex + lastN0;

					double wSubN0ToRR = 1;
					double wSubN0ToRI = 0;

					for (int r = 0; r < lastN0; r++) {
						double grR = dataR[destEvenStartIndex + r];
						double grI = dataI[destEvenStartIndex + r];
						double hrR = dataR[destOddStartIndex + r];
						double hrI = dataI[destOddStartIndex + r];

						// dest[destEvenStartIndex + r] = Gr + WsubN0ToR * Hr
						dataR[destEvenStartIndex + r] = grR + wSubN0ToRR * hrR - wSubN0ToRI * hrI;
						dataI[destEvenStartIndex + r] = grI + wSubN0ToRR * hrI + wSubN0ToRI * hrR;
						// dest[destOddStartIndex + r] = Gr - WsubN0ToR * Hr
						dataR[destOddStartIndex + r] = grR - (wSubN0ToRR * hrR - wSubN0ToRI * hrI);
						dataI[destOddStartIndex + r] = grI - (wSubN0ToRR * hrI + wSubN0ToRI * hrR);

						// WsubN0ToR *= WsubN0R
						double nextWsubN0ToRR = wSubN0ToRR * wSubN0R - wSubN0ToRI * wSubN0I;
						double nextWsubN0ToRI = wSubN0ToRR * wSubN0I + wSubN0ToRI * wSubN0R;
						wSubN0ToRR = nextWsubN0ToRR;
						wSubN0ToRI = nextWsubN0ToRI;
					}
				}

				lastN0 = n0;
				lastLogN0 = logN0;
			}

			normalizeTransformedData(dataRI[e], normalization, type);
		}
	}

	//=================================================================================//

	//function that will read the accelerations in a period
	//estimatedTime in milliseconds
	public void readPeriod(Period p, double estimatedTime){
		//here should be change with time reading, for testing purposes,
		// I will read it manually 100 readings
		// (2sec, 1read=10sec, 1sample=2reads)

		for(int i=0; i<100; i++) {
			Acceleration acc = BaseFunctions.readAcc(2);
			p.add(acc);
		}
	}

	/**
	* @description 	Check if the current period might contain potential step data (Standard deviation > 0.5)
	* @param 		Period p, the input period
	* @param		Double threshold, the threshold value
	* @return       Boolean that indicates if might contain step data
	*/
	public boolean checkThreshold(Period p, double threshold){
		Acceleration acc;

		double[] std = new double[3];
		double mean_x = p.mean_x / p.getSize();
		double mean_y = p.mean_y / p.getSize();
		double mean_z = p.mean_z / p.getSize();
		for(int i=0; i<p.getSize(); i++){
			acc = p.getAtX(i);
			std[0] += Math.pow((acc.acc_x - mean_x), 2.0);
			std[1] += Math.pow((acc.acc_y - mean_y), 2.0);
			std[2] += Math.pow((acc.acc_z - mean_z), 2.0);
		}
		std[0] /= p.getSize()-1;
		std[1] /= p.getSize()-1;
		std[2] /= p.getSize()-1;
		std[0] = Math.sqrt(std[0]);
		std[1] = Math.sqrt(std[1]);
		std[2] = Math.sqrt(std[2]);

		double result = Math.sqrt(Math.pow(std[0], 2.0) + Math.pow(std[1], 2.0) + Math.pow(std[2], 2.0));		
		return result > 0.5;
	}

	public void xcov(Period x, Period y,
			double[][] X, ArrayList<Integer> Lags_peak, int size,
			double perMin, double perMax, int freq){
		int[] Lags = new int[size];
		int min_size = Math.min(x.getSize(), y.getSize());

		double[] x_accx_vec = new double[min_size];
		double[] x_accy_vec = new double[min_size];
		double[] x_accz_vec = new double[min_size];
		double[] y_accx_vec = new double[min_size];
		double[] y_accy_vec = new double[min_size];
		double[] y_accz_vec = new double[min_size];
		Acceleration acc;

		int offset = x.getSize()-min_size;

		double mx_x = x.mean_x;
		double mx_y = x.mean_y;
		double mx_z = x.mean_z;
		double my_x = y.mean_x;
		double my_y = y.mean_y;
		double my_z = y.mean_z;
		for(int i=0; i<x.getSize()-min_size; i++){
			acc = x.getAtX(i);
			mx_x -= acc.acc_x;
			mx_y -= acc.acc_y;
			mx_z -= acc.acc_z;
		}
		for(int i=min_size; i<y.getSize()-min_size; i++){
			acc = y.getAtX(i);
			my_x -= acc.acc_x;
			my_y -= acc.acc_y;
			my_z -= acc.acc_z;
		}

		double mean_x = (mx_x + my_x) / (2*min_size);
		double mean_y = (mx_y + my_y) / (2*min_size);
		double mean_z = (mx_z + my_z) / (2*min_size);
		//		Log.e("Area52", "here mean: " + mean_x + " " + mean_y + " " + mean_z);

		for(int i=offset; i<x.getSize(); i++){
			acc = x.getAtX(i);
			x_accx_vec[i - offset] = acc.acc_x - mean_x;
			x_accy_vec[i - offset] = acc.acc_y - mean_y;
			x_accz_vec[i - offset] = acc.acc_z - mean_z;
//						Log.e("Area52", "here: " + x_accx_vec[i - offset] + " " + 
//								x_accy_vec[i - offset] + " " + x_accz_vec[i - offset]);
			Lags[i - offset] = i - offset + 1;
		}
		
//		Log.e("Area52", "XXXX<<<<<<<<<<<<<<<<<>>>>>>>>>>>>><<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>XXXXXXXXXXXXX");

		for(int i=0; i<min_size; i++){
			acc = y.getAtX(i);
			y_accx_vec[i] = acc.acc_x - mean_x;
			y_accy_vec[i] = acc.acc_y - mean_y;
			y_accz_vec[i] = acc.acc_z - mean_z;
//						Log.e("Area52", "here: " + y_accx_vec[i] + " " + 
//								y_accy_vec[i] + " " + y_accz_vec[i]);
		}

		this.normalization = DftNormalization.STANDARD;

		double[][][] dataRI = new double[6][2][size];
		for(int i=0; i<min_size; i++){
			dataRI[0][0][i] = x_accx_vec[i];
			dataRI[1][0][i] = x_accy_vec[i];
			dataRI[2][0][i] = x_accz_vec[i];
			dataRI[3][0][i] = y_accx_vec[i];
			dataRI[4][0][i] = y_accy_vec[i];
			dataRI[5][0][i] = y_accz_vec[i];
		}

		transformInPlace(dataRI, 6, normalization, TransformType.FORWARD);

		Complex[] X_x = new Complex[size];
		Complex[] X_y = new Complex[size];
		Complex[] X_z = new Complex[size];
		Complex[] Y_x = new Complex[size];
		Complex[] Y_y = new Complex[size];
		Complex[] Y_z = new Complex[size];

		for(int j=0; j<size; j++){
			X_x[j] = new Complex(dataRI[0][0][j], dataRI[0][1][j]);
			X_y[j] = new Complex(dataRI[1][0][j], dataRI[1][1][j]);
			X_z[j] = new Complex(dataRI[2][0][j], dataRI[2][1][j]);
			Y_x[j] = new Complex(dataRI[3][0][j], dataRI[3][1][j]);
			Y_y[j] = new Complex(dataRI[4][0][j], dataRI[4][1][j]);
			Y_z[j] = new Complex(dataRI[5][0][j], dataRI[5][1][j]);
		}

		Complex[][] C = new Complex[9][size];
		for(int i=0;i<9;i++){
			C[i] = new Complex[size];
			X[i] = new double[size];
		}

		double[][][] dataRI2 = new double[9][2][size];
		for(int i=0;i<size;i++){
			C[0][i] = Y_x[i].multiply(X_x[i].conjugate());
			C[1][i] = Y_x[i].multiply(X_y[i].conjugate());
			C[2][i] = Y_x[i].multiply(X_z[i].conjugate());
			C[3][i] = Y_y[i].multiply(X_x[i].conjugate());
			C[4][i] = Y_y[i].multiply(X_y[i].conjugate());
			C[5][i] = Y_y[i].multiply(X_z[i].conjugate());
			C[6][i] = Y_z[i].multiply(X_x[i].conjugate());
			C[7][i] = Y_z[i].multiply(X_y[i].conjugate());
			C[8][i] = Y_z[i].multiply(X_z[i].conjugate());

			dataRI2[0][0][i] = C[0][i].getReal();
			dataRI2[0][1][i] = C[0][i].getImaginary();

			dataRI2[1][0][i] = C[1][i].getReal();
			dataRI2[1][1][i] = C[1][i].getImaginary();

			dataRI2[2][0][i] = C[2][i].getReal();
			dataRI2[2][1][i] = C[2][i].getImaginary();

			dataRI2[3][0][i] = C[3][i].getReal();
			dataRI2[3][1][i] = C[3][i].getImaginary();

			dataRI2[4][0][i] = C[4][i].getReal();
			dataRI2[4][1][i] = C[4][i].getImaginary();

			dataRI2[5][0][i] = C[5][i].getReal();
			dataRI2[5][1][i] = C[5][i].getImaginary();

			dataRI2[6][0][i] = C[6][i].getReal();
			dataRI2[6][1][i] = C[6][i].getImaginary();

			dataRI2[7][0][i] = C[7][i].getReal();
			dataRI2[7][1][i] = C[7][i].getImaginary();

			dataRI2[8][0][i] = C[8][i].getReal();
			dataRI2[8][1][i] = C[8][i].getImaginary();
		}

		int M = min_size - 1;
		double[] SumXcorr = new double[M];
		transformInPlace(dataRI2, 9, normalization, TransformType.INVERSE);

		//		for(int i=0;i<size;i++){
		//			Log.e("Area52", dataRI2[0][0][i] + " ");
		//		}

		int maxlag = min_size - 1;
		int of = size-maxlag;
		for(int j=of; j<size; j++){
			X[0][j-of] = dataRI2[0][0][j] / Lags[j-of];
			SumXcorr[j-of] += X[0][j-of];
			X[1][j-of] = dataRI2[1][0][j] / Lags[j-of];
			//			SumXcorr[j-of] += X[1][j-of];
			X[2][j-of] = dataRI2[2][0][j] / Lags[j-of];
			//			SumXcorr[j-of] += X[2][j-of];
			X[3][j-of] = dataRI2[3][0][j] / Lags[j-of];
			//			SumXcorr[j-of] += X[3][j-of];
			X[4][j-of] = dataRI2[4][0][j] / Lags[j-of];
			SumXcorr[j-of] += X[4][j-of];
			X[5][j-of] = dataRI2[5][0][j] / Lags[j-of];
			//			SumXcorr[j-of] += X[5][j-of];
			X[6][j-of] = dataRI2[6][0][j] / Lags[j-of];
			//			SumXcorr[j-of] += X[6][j-of];
			X[7][j-of] = dataRI2[7][0][j] / Lags[j-of];
			//			SumXcorr[j-of] += X[7][j-of];
			X[8][j-of] = dataRI2[8][0][j] / Lags[j-of];
			SumXcorr[j-of] += X[8][j-of];
		}

		//		ind_Prng = ind_Prng&Lags<length(IncomingCycle);

//		        for(int i=0;i<M;i++){
//		            Log.i("Area52", "sumx: " + SumXcorr[i]);
//		        }
		for(int i=0;i<M;i++){

			//			ind_Prng = (Lags>=PeriodRng(1) & Lags<=PeriodRng(2)); %within minimal and maximal period

			if(Lags[i] < perMin * freq ||
					Lags[i] > perMax * freq||
					Lags[i] >= y.getSize())
				continue;

			//			ind_pos = SumXcorr>0;
			if(SumXcorr[i] < 0)
				continue;

			//			ind_peak = [upordown(1)<0; diff(upordown)<0; upordown(end)<0];
			if(i==0){
				if((int) Math.signum(SumXcorr[1] - SumXcorr[0]) >= 0)
					continue;
			}
			else if(i>0 && i<M-1){
				if((int) Math.signum(SumXcorr[i+1] - SumXcorr[i])-(int) Math.signum(SumXcorr[i] - SumXcorr[i-1]) >= 0)
					continue;
			}
			else{
				if((int) Math.signum(SumXcorr[i] - SumXcorr[i-1]) >= 0)
					continue;
			}

			//			Log.i("Area52", "Lags: " + Lags[i]);
			Lags_peak.add(Lags[i]);
		}
	}

	public boolean SylvesterCriterion(double[] X, int Lag, double[] results){
		//add x_transp to x
		double a,b,c,d,e,f,g,h,q;
			
		a = X[0];
		b = X[3];
		c = X[6];
		d = X[1];
		e = X[4];
		f = X[7];
		g = X[2];
		h = X[5];
		q = X[8];
		X[0] = X[0]+a;
		X[1] = X[1]+b;
		X[2] = X[2]+c;
		X[3] = X[3]+d;
		X[4] = X[4]+e;
		X[5] = X[5]+f;
		X[6] = X[6]+g;
		X[7] = X[7]+h;
		X[8] = X[8]+q;

		results[0] = X[0] + X[4] + X[8];
			
		//det 1x1
		if(X[0] <= 0)
			return false;

		//det 2x2
		if(X[0]*X[4] - X[1]*X[3] <= 0)
			return false;
		//System.out.println("prod:"+ X[0][i] + " " + X[4][i] + " " + X[1][i] + " " + X[3][i]);

		//det 3x3
		if(X[0]*X[4]*X[8] +
				X[1]*X[5]*X[6] +
				X[3]*X[7]*X[2] -
				X[6]*X[4]*X[2] -
				X[7]*X[5]*X[0] -
				X[3]*X[1]*X[8] <= 0)
			return false;
		results[1] = Lag;
		return true;
	}

	/**
	* @description 	Function that determine the end point of the stride.
	* @param 		Period b1, that contains first part of the signal
	* @param		Period b2, that contains second part of the signal
	* @param		int period_x, point of the interest that represent the point of stride dection
	* @return       Return the value of the end point of the stride
	*/
	public int Snap2Point(Period b1, Period b2, int period_x){
		int target = b1.accelerations.size();
		int rng0 = (int) Math.round(target + period_x * -0.5);
		int rng1 = (int) Math.round(target + period_x * 0.5);
		
		double mean_x = 0;
		double mean_y = 0;
		double mean_z = 0;
		
		for(int i=rng0-1; i<rng1;i++){
			Acceleration acc;
			if(i < b1.accelerations.size())
				acc = b1.accelerations.get(i);
			else
				acc = b2.accelerations.get(i - b1.accelerations.size());
			mean_x += acc.acc_x;
			mean_y += acc.acc_y;
			mean_z += acc.acc_z;
		}
		mean_x /= rng1-rng0+1;
		mean_y /= rng1-rng0+1;
		mean_z /= rng1-rng0+1;
		

		int rng0_2 = (int) Math.round(period_x + period_x * -0.5 - periodExtremeMin*newsampfreq) * -1;
		int rng1_2 = (int) Math.round(period_x + period_x * 0.5 - periodExtremeMax*newsampfreq);
		
		if(rng0_2<0)
			rng0_2=0;
		if(rng1_2<0)
			rng1_2=0;

		//Log.e("Area52", mean_x+" "+mean_y+" "+mean_z + " " + rng0_2 + " " + rng1_2);

		Integer prev_value = null;
		ArrayList<Integer> down = new ArrayList<Integer>();
		for(int i=rng0-1+rng0_2; i<rng1-rng1_2; i++){
			Acceleration acc;
			if(i < b1.accelerations.size())
				acc = b1.accelerations.get(i);
			else
				acc = b2.accelerations.get(i - b1.accelerations.size());
			//			Log.e("Area52", "acc: " + acc.acc_x + " " + acc.acc_y + " " + acc.acc_z);

			double value = (acc.acc_x-mean_x) + 
						   (acc.acc_y-mean_y) + 
						   (acc.acc_z-mean_z);
			//Log.e("Area52", ">> " + value);
			
			int sign;
			if(value>0)
				sign = 1;
			else
				sign = 0;

			
			if(prev_value != null && sign-prev_value == -1)
				down.add(i-rng0+1-rng0_2);
			
			prev_value = sign;
		}
		
		if(down.isEmpty())
			return b1.getSize()-1;
		
		double t = b1.getSize() - rng0 - rng0_2;
		int min = Integer.MAX_VALUE;
		int min_ind=0;
		for(int i=0;i<down.size(); i++)
			if(Math.abs(down.get(i)-t) < min){
				min = (int) Math.abs(down.get(i)-t);
				min_ind = i;
		}
		return down.get(min_ind)-1+rng0+rng0_2 - 1;

	}

	/**
	* @description 	Main function that determine if stride data exists
	* @param 		Period PrevCycle, the buffered data of the signal
	* @param		Period IncomingCycle, new values of the signal
	* @return       Return the step duration or a value < 0 if the step was not detected
	*/
	public int PeriodXCorr(Period PrevCycle, Period IncomingCycle){

		double periodMin = this.periodExtremeMin;
		double periodMax = this.periodExtremeMax;
		int sampfreq = this.newsampfreq;
		int size = BaseFunctions.get_min_size(PrevCycle, IncomingCycle);

		double X[][] = new double[9][size];
//		int max_tmp = Math.max(PrevCycle.getSize(), IncomingCycle.getSize());

		ArrayList<Integer> Lags_peak = new ArrayList<Integer>();
		xcov(PrevCycle, IncomingCycle, X, Lags_peak, size, periodMin,
				periodMax, sampfreq);
		
		Double r_backup = null;
		double[] results = new double[2];
		for(int i=0; i<Lags_peak.size(); i++){
			int real_index = Lags_peak.get(i) - 1;
			
			//create order [0, 1, -1 , 2 , -2
			int lags_order[] = new int[5];
			lags_order[0] = real_index;
			lags_order[1] = real_index+1;
			lags_order[2] = real_index-1;
			lags_order[3] = real_index+2;
			lags_order[4] = real_index-2;

			for(int j=0; j<5; j++){
				int my_lag = lags_order[j];
				if(my_lag<1)
					continue;
				if(my_lag>=size-1)
					continue;
				double[] X_peakaverage = new double[9];
				int rli2 = my_lag - 1;
				int rri2 = my_lag + 1;
				for(int k=rli2; k<=rri2; k++){
					for(int l=0; l<9; l++)
						X_peakaverage[l] += X[l][k];
				}
				for(int l=0; l<9; l++)
					X_peakaverage[l] /= (rri2-rli2 + 1);
				
				boolean res = SylvesterCriterion(X_peakaverage, lags_order[j]+1, results);
				if(res==true)
					return (int)results[1];
				if(i==0 && j==0)
					r_backup = results[0];
			}
		}
		if(r_backup == null)
			return -2;
		else
			return -1;

	}

}