package com.step.frequency;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import org.hva.createit.scl.data.AccelerometerData;
import org.hva.createit.scl.data.StepfrequencyData;
import org.hva.createit.scl.data.StridefrequencyData;

import java.io.BufferedReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * @author Cristian-Stefan Neacsu <neacsu.cristianstefan@gmail.com>
 * @description Class in charge with the implementation of the service
 */
public class SFService extends Service {

	public InputStream f;
	public BufferedReader fin;
	public long startProgramTime, readTime, currReadTime;
	int iteration, next_sample, LastSample;
	long start_time;
	double crt_time;
	public ArrayList<Acceleration> AccelerationsBuffer;
	public List<Acceleration> SyncAccBuff;
	private Thread main_thread;
	private Brain main_thread_runnable;
	ArrayList<Acceleration> median_buff = new ArrayList<Acceleration>();
	Acceleration prev_0_acc = null;

	public static String TAG = "StepFrequencyService";

	/**
	* @description 	Reading accelerations corresponding to a period time stamp, and encapsulate into a Period object
	* @param 		Period per, the period object where accelerations will be encapsulated
	* @param		int freq, frequency of the reading data
	* @return       double next_time, the time stamp value until data should be read
	*/
	public void initializePeriod(Period per, int freq, double next_time){
		int time_step = 1000 / freq;
		Acceleration acc = null;

		crt_time = start_time;
		
		if(crt_time >= next_time)
			return;

		do{
			Acceleration prev = acc;
			while (SyncAccBuff.size() == 0){}
			synchronized (SyncAccBuff) {
				if(SyncAccBuff.size() > 0) {
					acc = SyncAccBuff.get(0);
				}
			}
			if(acc.timestamp < crt_time + time_step){
				synchronized (SyncAccBuff) {
					SyncAccBuff.remove(0);
				}
			}
			else{
				if(prev == null){
					prev = prev_0_acc;
				}
				if(prev != null)
				{
					prev_0_acc = prev;
					do{
						
						// case 5270 next_time and values of prev= 5265 and next 5275 => cannot calculate 5280 => drop
						if(acc.timestamp<crt_time + time_step)
							return;
						
						Acceleration final_acc = null;
						crt_time += time_step;
						Acceleration a = BaseFunctions.interpolate(prev, acc, crt_time);
						start_time = (long) a.timestamp;
						
						median_buff.add(a);
						if(median_buff.size() >= 4){
							final_acc = BaseFunctions.median(median_buff, 1);		

							per.accelerations.add(final_acc);
							per.mean_x += final_acc.acc_x;
							per.mean_y += final_acc.acc_y;
							per.mean_z += final_acc.acc_z;
							median_buff.remove(0);
						}
					}while(crt_time + time_step < acc.timestamp);
				}
			}
		}while(acc.timestamp < next_time);
	}
	
	/**
	* @description Class that start the algorithm steps
	*/
	class Brain implements Runnable{
		Period b1;
		Period b2;
		private boolean stop;
		
		public Brain(){
			stop = false;
		}
		
		public void stop(){
			stop = true;
		}
		
		@Override
		public void run(){
			Movement CycleDecision = new Movement();

			readTime = 0;
			iteration = 0;
			next_sample = 100;
			LastSample = 0;
			double sf = -2;

			double next_length_b2 = 0;
			int max_length = (int) (CycleDecision.periodExtremeMax*CycleDecision.newsampfreq);
			
			b1 = new Period();
			b2 = new Period();
			
			//read and resample at 50 hz, 100 samples at b1 and b2
			initializePeriod(b1, CycleDecision.newsampfreq, next_sample * 20);
			//start_time should be moved 2 seconds in front
			//start_time += next_sample * 20;
			initializePeriod(b2, CycleDecision.newsampfreq, next_sample * 20 * 2 + 20);
			//start_time should be moved 2 seconds in front
			//start_time += next_sample * 20 + 20;
			while(b1.accelerations.size() < 100){
				Acceleration a_tmp = b2.accelerations.get(0);
				b1.accelerations.add(a_tmp);
				b1.mean_x += a_tmp.acc_x;
				b1.mean_y += a_tmp.acc_y;
				b1.mean_z += a_tmp.acc_z;
				b2.mean_x -= a_tmp.acc_x;
				b2.mean_y -= a_tmp.acc_y;
				b2.mean_z -= a_tmp.acc_z;
				b2.accelerations.remove(0);
			}
			
			while(!stop){
	
				boolean pass = CycleDecision.checkThreshold(b2, 0.5);

				int snap_back = -1;
				int period_x = -1;
				
				if(pass == true){
					period_x = CycleDecision.PeriodXCorr(b1, b2);
					if(period_x == -2){//no movement or no covariance peak (r)
						pass = false;
						CycleDecision.prevDurationPeriods.add(0);
					} else if(period_x == -1){
						pass = false;
						CycleDecision.prevDurationPeriods.add(-100);
					}
					else{
						snap_back = CycleDecision.Snap2Point(b1, b2, period_x);
						CycleDecision.prevDurationPeriods.add(period_x);
					}
				}
				else
					CycleDecision.prevDurationPeriods.add(0);
				
				//remove the left value from previous periods
				if(CycleDecision.prevDurationPeriods.size() > 0)
					CycleDecision.prevDurationPeriods.remove(0);

				double nanmean = 0;
				double nan_elems = 0;
				for(int i=0; i<CycleDecision.prevDurationPeriods.size(); i++){
					int elem = CycleDecision.prevDurationPeriods.get(i);
					if(elem > 0){
						nanmean += elem;
						nan_elems++;
					}
				} 
				if(nanmean>0)
					nanmean /= nan_elems;
				
				if(sf != nanmean){
					Log.d("Area52", "nanmean: " + nanmean);	
					sf=nanmean;
					int ts;
					if(period_x == -1 || snap_back == -1)
						ts = (int)b1.accelerations.get(b1.accelerations.size()-1).timestamp;
					else
						ts = (int)b1.accelerations.get(Math.min(snap_back, b1.accelerations.size()-1)).timestamp;
					
					StridefrequencyData stepd = new StridefrequencyData(0, (int)nanmean, ts);
					StepfrequencyData sd = new StepfrequencyData(0, 2*(int)nanmean, ts);
					EventBus.getDefault().post(sd);
					EventBus.getDefault().post(stepd);
				}

				//round nanmean
				nanmean = Math.round(nanmean);
				//if a point was find, transfer from b2->b1 median(prevDurations) -  and expect
				//a b2 of nanmean*1.5
				if(snap_back != -1)
					next_length_b2 = nanmean*1.5;
				//else transfer from b2->b1 mean(prevDurations) and expect a new n2 of a size
				//old_b2 * 1.2
				else
					next_length_b2 = b2.accelerations.size()*1.2;
				
				//put everything in b1 from b2 and return
				if(nanmean == 0){
					Period b1_new = new Period();
					Period b2_new = new Period();
					nanmean = max_length;
					for(int i=0; i<max_length-b2.getSize()	; i++){
						Acceleration acc = b1.accelerations.get(i);
						b1_new.add(acc);
						b1_new.mean_x += acc.acc_x;
						b1_new.mean_y += acc.acc_y;
						b1_new.mean_z += acc.acc_z;
					}
					int shft = max_length-b2.getSize();
					for(int i=shft; i<max_length; i++){
						Acceleration acc = b2.accelerations.get(i-shft);
						b1_new.add(acc);
						b1_new.mean_x += acc.acc_x;
						b1_new.mean_y += acc.acc_y;
						b1_new.mean_z += acc.acc_z;
					}
					
					double shift = start_time + max_length * 20;
					initializePeriod(b2_new, CycleDecision.newsampfreq, shift);
				
					b1 = b1_new;
					b2 = b2_new;
					continue;
				}

				if(next_length_b2 > max_length)
					next_length_b2 = max_length;

				//copy b2 into b1
				Period b1_new = new Period();
				int keep_from_b1 = 0;
				int p = (int) Math.round(nanmean);
				double start_time_n;
				if(snap_back == -1){
					start_time_n = b1.getAtX(max_length-1).timestamp;
				}
				else if(snap_back < b1.accelerations.size())
				{
					keep_from_b1 = b1.accelerations.size() - 1 - snap_back;
					start_time_n = b1.getAtX(snap_back).timestamp;
					p -= 	keep_from_b1;
				}else{
					keep_from_b1 = snap_back - (b1.accelerations.size()-1);
					start_time_n = b2.getAtX(snap_back-b1.accelerations.size()).timestamp;
					p += keep_from_b1;
				}
						
				int prev_values_length = b2.accelerations.size();
				
				for(int i=p; i<max_length; i++){
					Acceleration acc = b1.accelerations.get(i);
					b1_new.add(acc);
					b1_new.mean_x += acc.acc_x;
					b1_new.mean_y += acc.acc_y;
					b1_new.mean_z += acc.acc_z;
				}
				
				if(b2.accelerations.size() < p){
					initializePeriod(b2, CycleDecision.newsampfreq, 
							start_time + (p-prev_values_length) * 20);
					//start_time should be moved in front
					start_time += (p-prev_values_length) * 20;
				}
				for(int i=0; i<p; i++){
					Acceleration acc = b2.accelerations.get(i);
					b1_new.add(acc);
					b1_new.mean_x += acc.acc_x;
					b1_new.mean_y += acc.acc_y;
					b1_new.mean_z += acc.acc_z;
				}
				
				b1 = b1_new;
				if(b2.accelerations.size() < p + next_length_b2 - 1){
					double shift = start_time_n + (next_length_b2 + nanmean + 1) * 20;
					initializePeriod(b2, CycleDecision.newsampfreq, shift);
				}

				Period b2_new = new Period();
				for(int i=p; i< b2.accelerations.size(); i++){
					Acceleration acc = b2.accelerations.get(i);
					b2_new.add(acc);
					b2_new.mean_x += acc.acc_x;
					b2_new.mean_y += acc.acc_y;
					b2_new.mean_z += acc.acc_z;
				}
				b2 = b2_new;
			}
		}
	}
	
	long startTimeStamp = -1;
	public void onEvent(AccelerometerData event) {
		long timeStamp = event.getTimestamp()/1000000;
		
		//store the first time stamp if was not store before
		if(startTimeStamp == -1)
			startTimeStamp = timeStamp;
		
		//shift the time stamp to count the stamp from the starting of the service
		timeStamp -= startTimeStamp;
		Acceleration acc = new Acceleration(event.getX(), event.getY(), event.getZ(), timeStamp);
		synchronized (SyncAccBuff) {
			SyncAccBuff.add(acc);
		}

	}


	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		start_time = 0;
		startProgramTime = System.currentTimeMillis();

		AccelerationsBuffer = new ArrayList<Acceleration>();
		SyncAccBuff = Collections.synchronizedList(AccelerationsBuffer);
	
        main_thread_runnable = new Brain();
		main_thread = new Thread(main_thread_runnable);
		main_thread.start();
		
		if(!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this);

			Log.d(TAG, "service START");
		}else {
			Log.d(TAG, "service allready started");
		}

		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		main_thread_runnable.stop();
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}

}
