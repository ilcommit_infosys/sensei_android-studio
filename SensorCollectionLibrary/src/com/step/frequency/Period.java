package com.step.frequency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Cristian-Stefan Neacsu <neacsu.cristianstefan@gmail.com>
 * @description Class that store that encapsulate the Acceleration objects, that are in the same time period
 */
public class Period{
	public List<Acceleration> SyncAccBuff;
	public ArrayList<Acceleration> accelerations;
	public double duration;
	public double sum_acc_x;
	public double sum_acc_y;
	public double sum_acc_z;
	public double mean_x;
	public double mean_y;
	public double mean_z;
	
	public Period(){
		accelerations = new ArrayList<Acceleration>();
		SyncAccBuff = Collections.synchronizedList(accelerations);
		duration = 6;
		sum_acc_x = 0;
		sum_acc_y = 0;
		sum_acc_z = 0;
		mean_x = 0;
		mean_y = 0;
		mean_z = 0;
	}
	
	public void setDuration(double val){
		duration = val;
	}
	
	public void clean(){
		synchronized (SyncAccBuff) {
			accelerations.clear();
		}
		duration = 2;
		sum_acc_x = 0;
		sum_acc_y = 0;
		sum_acc_z = 0;
		mean_x = 0;
		mean_y = 0;
		mean_z = 0;
	}
	
	public double getDuration(){
		return duration;
	}
	
	public void add(Acceleration acc){
		synchronized (SyncAccBuff) {
			accelerations.add(acc);

			//shortcut to calculate the mean for threshold
			sum_acc_x += acc.acc_x;
			sum_acc_y += acc.acc_y;
			sum_acc_z += acc.acc_z;
		}
	}
	
	public Acceleration getAtX(int i){
		synchronized (SyncAccBuff) {
			return accelerations.get(i);
		}
	}
	
	public int getSize(){
		synchronized (SyncAccBuff) {
			return accelerations.size();
		}
	}
}