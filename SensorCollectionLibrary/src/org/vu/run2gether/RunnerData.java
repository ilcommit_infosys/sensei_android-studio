package org.vu.run2gether;

/**
 * Created by biejh on 13-06-16.
 */

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vladimir on 6/1/16.
 */
public class RunnerData {

    private String runnerId;
    private double averageDistance;
    private double averageSpeed;
    private double averageDuration;
    private int points;
    private Date lastSeen;
    private boolean uploaded;

    public RunnerData(String runnerData) {
        Map<String, String> dataMap = getRunnerDataMap(runnerData);

        setRunnerId(dataMap.get("username"));
        setAverageDistance(Double.parseDouble(dataMap.get("averageDistance")));
        setAverageDuration(Double.parseDouble(dataMap.get("averageDuration")));
        setAverageSpeed(Double.parseDouble(dataMap.get("averageSpeed")));
        setPoints(Integer.parseInt(dataMap.get("points")));
        setLastSeen(new Date());
    }

    public RunnerData(String runnerId, double averageDistance, double averageSpeed, double averageDuration, int points, Date lastSeen, boolean uploaded){
        this.setRunnerId(runnerId);
        this.setAverageDistance(averageDistance);
        this.setAverageDuration(averageDuration);
        this.setAverageSpeed(averageSpeed);
        this.setPoints(points);
        this.setLastSeen(lastSeen);
        this.setUploaded(uploaded);
    }

    private Map<String, String> getRunnerDataMap(String data) {
        Map<String, String> dataMap = new HashMap<String, String>();
        String[] dataParts = data.split("&");

        for (String dataItem : dataParts) {
            String[] dataPair = dataItem.split("=", -1);
            dataMap.put(dataPair[0], dataPair[1]);
        }

        return dataMap;
    }

    public String getRunnerId() {
        return runnerId;
    }

    public void setRunnerId(String runnerId) {
        this.runnerId = runnerId;
    }

    public double getAverageDistance() {
        return averageDistance;
    }

    public void setAverageDistance(double averageDistance) {
        this.averageDistance = averageDistance;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public double getAverageDuration() {
        return averageDuration;
    }

    public void setAverageDuration(double averageDuration) {
        this.averageDuration = averageDuration;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RunnerData that = (RunnerData) o;

        return runnerId.equals(that.runnerId);

    }

    @Override
    public int hashCode() {
        return runnerId.hashCode();
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }
}

