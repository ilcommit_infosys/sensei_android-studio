/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl;

import java.util.concurrent.atomic.AtomicInteger;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ThreadSafeDatabaseManager extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "SensorCollectionLibrary.db";
	private static boolean isInitialized = false;

	public ThreadSafeDatabaseManager(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	private AtomicInteger mOpenCounter = new AtomicInteger();

	private static ThreadSafeDatabaseManager instance;
	private SQLiteDatabase mDatabase;

	public static synchronized void initializeInstance(Context c) {
		if (instance == null) {
			instance = new ThreadSafeDatabaseManager(c);
			isInitialized = true;
		}
	}

	public static synchronized ThreadSafeDatabaseManager getInstance() {
		if (instance == null) {
			throw new IllegalStateException(
					ThreadSafeDatabaseManager.class.getSimpleName()
							+ " is not initialized, call initializeInstance(..) method first.");
		}

		return instance;
	}

	public static boolean isInitialized() {
		return isInitialized;
	}

	public synchronized SQLiteDatabase openDatabase() {
		if (mOpenCounter.incrementAndGet() == 1) {
			// Opening new database
			mDatabase = getWritableDatabase();
		}
		return mDatabase;
	}

	public synchronized void closeDatabase() {
		if (mOpenCounter.decrementAndGet() == 0) {
			// Closing database
			mDatabase.close();
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
}
