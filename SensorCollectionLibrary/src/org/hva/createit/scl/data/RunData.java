/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.data;

public class RunData extends MeasurementData {
	public static final String name = "rundata";
	public static final int RUNDATA_TYPE_RUNSTART = 1;
	public static final int RUNDATA_TYPE_RUNEND = 2;

	public RunData(MeasurementData measurement) {
		super(measurement.getDeviceId(), name, measurement.getValueAsInt(),
				measurement.getTimestamp());
	}

	public RunData(int deviceId, int value, long timestamp) {
		super(deviceId, name, value, timestamp);
	}

	public int getRunDataType() {
		return this.getValueAsInt();
	}

	public void setRunDataType(int value) {
		this.setValue(value);
	}
}
