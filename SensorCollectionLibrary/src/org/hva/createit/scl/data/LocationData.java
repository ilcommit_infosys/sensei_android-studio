/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.data;

import android.location.Location;

public class LocationData implements Comparable<LocationData> {
	private String sensorName;
	private String sensorType = "location";
	private String manufacturer;
	private int deviceId;
	private long timestamp;
	private MeasurementData latitude;
	private MeasurementData longitude;
	private MeasurementData altitude;
	private MeasurementData bearing;
	private MeasurementData accuracy;
	private MeasurementData speed;

	public LocationData(String sensorName, String manufacturer, int deviceId,
			long timestamp, double latitude, double longitude, double altitude,
			float bearing, float accuracy, float speed) {
		super();
		this.sensorName = sensorName;
		this.manufacturer = manufacturer;
		this.deviceId = deviceId;
		this.timestamp = timestamp;
		this.latitude = new MeasurementData(deviceId, "latitude", latitude,
				timestamp);
		this.longitude = new MeasurementData(deviceId, "longitude", longitude,
				timestamp);
		this.altitude = new MeasurementData(deviceId, "altitude", altitude,
				timestamp);
		this.bearing = new MeasurementData(deviceId, "bearing", bearing,
				timestamp);
		this.accuracy = new MeasurementData(deviceId, "accuracy", accuracy,
				timestamp);
		this.speed = new MeasurementData(deviceId, "speed", speed, timestamp);
	}

	public LocationData(int deviceId, long timestamp) {
		super();
		this.deviceId = deviceId;
		this.timestamp = timestamp;
	}

	public LocationData(Location location) {
		super();
		this.sensorName = location.getProvider();
		this.manufacturer = android.os.Build.MANUFACTURER;
		this.timestamp = location.getTime();
		this.deviceId = 0;
		this.latitude = new MeasurementData(deviceId, "latitude",
				location.getLatitude(), timestamp);
		this.longitude = new MeasurementData(deviceId, "longitude",
				location.getLongitude(), timestamp);
		this.altitude = new MeasurementData(deviceId, "altitude",
				location.getAltitude(), timestamp);
		this.bearing = new MeasurementData(deviceId, "bearing",
				location.getBearing(), timestamp);
		this.accuracy = new MeasurementData(deviceId, "accuracy",
				location.getAccuracy(), timestamp);
		this.speed = new MeasurementData(deviceId, "speed",
				location.getSpeed(), timestamp);
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public MeasurementData getLatitude() {
		return latitude;
	}

	public void setLatitude(MeasurementData latitude) {
		this.latitude = latitude;
	}

	public MeasurementData getLongitude() {
		return longitude;
	}

	public void setLongitude(MeasurementData longitude) {
		this.longitude = longitude;
	}

	public MeasurementData getAltitude() {
		return altitude;
	}

	public void setAltitude(MeasurementData altitude) {
		this.altitude = altitude;
	}

	public MeasurementData getBearing() {
		return bearing;
	}

	public void setBearing(MeasurementData bearing) {
		this.bearing = bearing;
	}

	public MeasurementData getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(MeasurementData accuracy) {
		this.accuracy = accuracy;
	}

	public MeasurementData getSpeed() {
		return speed;
	}

	public void setSpeed(MeasurementData speed) {
		this.speed = speed;
	}

	@Override
	public int compareTo(LocationData another) {
		if (another.getTimestamp() > this.getTimestamp()) {
			return 1;
		} else if (another.getTimestamp() == this.getTimestamp()) {
			return 0;
		}
		return -1;
	}
}
