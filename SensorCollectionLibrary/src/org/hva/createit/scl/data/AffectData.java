/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.data;

public class AffectData {
	private String sensorName;
	public static String sensorType = "accelerometer";
	private String manufacturer;
	private int deviceId;
	private long timestamp;
	private MeasurementData pleasure;
	private MeasurementData dominance;
	private MeasurementData arousal;

	public AffectData(int deviceId, long timestamp, double pleasure,
			double dominance, double arousal) {
		this.deviceId = deviceId;
		this.timestamp = timestamp;

		this.setMeasurementPleasure(new MeasurementData(deviceId, "pleasure",
				pleasure, this.timestamp));
		this.setMeasurementDominance(new MeasurementData(deviceId, "dominance",
				dominance, this.timestamp));
		this.setMeasurementArousal(new MeasurementData(deviceId, "arousal",
				arousal, this.timestamp));
	}

	public AffectData() {
	}

	public AffectData(int deviceId, long timestamp) {
		this.deviceId = deviceId;
		this.timestamp = timestamp;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public MeasurementData getMeasurementPleasure() {
		return pleasure;
	}

	public MeasurementData getMeasurementDominance() {
		return dominance;
	}

	public MeasurementData getMeasurementArousal() {
		return arousal;
	}

	public double getPleasure() {
		return pleasure.getValueAsDouble();
	}

	public double getDominance() {
		return dominance.getValueAsDouble();
	}

	public double getArousal() {
		return arousal.getValueAsDouble();
	}

	public String getSensortype() {
		return sensorType;
	}

	public void setSensortype(String sensorType) {
		this.sensorType = sensorType;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
		MeasurementData arousal = getMeasurementArousal();
		arousal.setTimestamp(timestamp);
		setMeasurementArousal(arousal);
		MeasurementData dominance = getMeasurementDominance();
		dominance.setTimestamp(timestamp);
		setMeasurementDominance(dominance);
		MeasurementData pleasure = getMeasurementPleasure();
		pleasure.setTimestamp(timestamp);
		setMeasurementPleasure(pleasure);
	}

	@Override
	public String toString() {
		return manufacturer + ", " + this.sensorName + ", " + sensorType + ", "
				+ timestamp + ", " + getPleasure() + ", " + getDominance()
				+ ", " + getArousal() + ";";
	}

	public void setMeasurementPleasure(MeasurementData pleasure) {
		this.pleasure = pleasure;
	}

	public void setMeasurementDominance(MeasurementData dominance) {
		this.dominance = dominance;
	}

	public void setMeasurementArousal(MeasurementData arousal) {
		this.arousal = arousal;
	}
}
