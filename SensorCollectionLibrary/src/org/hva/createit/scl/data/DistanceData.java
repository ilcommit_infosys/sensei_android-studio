/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.data;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DistanceData extends MeasurementData {
	public static final String name = "distance";

	public DistanceData(int deviceId, double value, long timestamp) {
		super(deviceId, name, value, timestamp);
	}

	public DistanceData(MeasurementData measurement) {
		super(measurement.getId(), measurement.getDeviceId(), measurement
				.getName(), measurement.getValue(), measurement.getTimestamp(),
				measurement.isUploaded());
	}

	public double getDistance() {
		return round(getValueAsDouble(), 2);
	}

	public void setDistance(double value) {
		this.setValue(value);
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd;
		try {
			bd = new BigDecimal(value);
		} catch (NumberFormatException e) {
			bd = new BigDecimal(0);
		}
		bd = bd.setScale(places, RoundingMode.HALF_UP);

		return bd.doubleValue();
	}

	/*
	 * Return distance in meters
	 */
	public static double getDistance(LocationData location1,
			LocationData location2) {
		return haversine(location1.getLatitude().getValueAsDouble(), location1
				.getLongitude().getValueAsDouble(), location2.getLatitude()
				.getValueAsDouble(), location2.getLongitude()
				.getValueAsDouble());
	}

	// en.wikipedia.org/wiki/Haversine_formula
	private static double getDistance(double lat1, double lon1, double lat2,
			double lon2) {
		double latA = Math.toRadians(lat1);
		double lonA = Math.toRadians(lon1);
		double latB = Math.toRadians(lat2);
		double lonB = Math.toRadians(lon2);
		double cosAng = (Math.cos(latA) * Math.cos(latB) * Math
				.cos(lonB - lonA)) + (Math.sin(latA) * Math.sin(latB));
		double ang = Math.acos(cosAng);
		double dist = ang * 6371.8;
		return dist;
	}

	// http://rosettacode.org/wiki/Haversine_formula#Java
	// public static final double R = 6372.8; // In kilometers
	public static final double R = 6372800; // In meters

	private static double haversine(double lat1, double lon1, double lat2,
			double lon2) {
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);

		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2)
				* Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.asin(Math.sqrt(a));
		return round(R * c, 0);
	}
}
