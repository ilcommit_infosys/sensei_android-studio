/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.data;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RunOverviewData {
	private String sensorName = "runoverview";
	private long start_timestamp;
	private long stop_timestamp;
	private int id;
	private boolean uploaded;
	private MeasurementData distance;
	private MeasurementData averageSpeed;
	private MeasurementData averageStepFrequency;
	private MeasurementData averageHeartRate;
	private MeasurementData duration;
	private MeasurementData averageStrideFrequency;

	private List<AffectData> affect;

	public RunOverviewData(long start_timestamp) {
		this.start_timestamp = start_timestamp;
		this.stop_timestamp = 0l;
		this.uploaded = false;
	}

	public RunOverviewData(long start_timestamp, long stop_timestamp) {
		this.start_timestamp = start_timestamp;
		this.stop_timestamp = stop_timestamp;
		this.uploaded = false;
	}

	public RunOverviewData(int id, long start_timestamp, long stop_timestamp) {
		this.id = id;
		this.start_timestamp = start_timestamp;
		this.stop_timestamp = stop_timestamp;
		this.uploaded = false;
	}

	public RunOverviewData(int id, long start_timestamp, long stop_timestamp,
			boolean uploaded) {
		this.id = id;
		this.start_timestamp = start_timestamp;
		this.stop_timestamp = stop_timestamp;
		this.uploaded = uploaded;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getStart_timestamp() {
		return start_timestamp;
	}

	public void setStart_timestamp(long start_timestamp) {
		this.start_timestamp = start_timestamp;
	}

	public long getStop_timestamp() {
		return stop_timestamp;
	}

	public void setStop_timestamp(long stop_timestamp) {
		this.stop_timestamp = stop_timestamp;
	}

	public boolean isUploaded() {
		return uploaded;
	}

	public void setUploaded(boolean uploaded) {
		this.uploaded = uploaded;
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public MeasurementData getDistanceMeasurementData() {
		return distance;
	}

	/*
	 * Returns distance in meters
	 */
	public double getDistance() {
		return distance.getValueAsDouble();
	}

	public void setDistance(MeasurementData distance) {
		this.distance = distance;
	}

	public MeasurementData getAverageSpeedMeasurementData() {
		return averageSpeed;
	}

	/*
	 * Returns speed in m/s
	 */
	public float getAverageSpeed() {
		return averageSpeed.getValueAsFloat();
	}

	public void setAverageSpeed(MeasurementData averageSpeed) {
		this.averageSpeed = averageSpeed;
	}

	public MeasurementData getAverageStepFrequencyMeasurementData() {
		return averageStepFrequency;
	}

	public void setAverageStepFrequency(MeasurementData averageStepFrequency) {
		this.averageStepFrequency = averageStepFrequency;
	}

	public MeasurementData getAverageStrideFrequencyMeasurementData() {
		return averageStrideFrequency;
	}

	public void setAverageStrideFrequency(MeasurementData averageStrideFrequency) {
		this.averageStrideFrequency = averageStrideFrequency;
	}

	public float getAverageStepFrequency() {
		return averageStepFrequency.getValueAsFloat();
	}

	public MeasurementData getAverageHeartRateMeasurementData() {
		return averageHeartRate;
	}

	public void setAverageHeartRate(MeasurementData averageHeartRate) {
		this.averageHeartRate = averageHeartRate;
	}

	public int getAverageHeartRate() {

		DecimalFormat df = new DecimalFormat("##0");
		try {
			return df.parse("" + averageHeartRate.getValueAsFloat()).intValue();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public MeasurementData getDurationMeasurementData() {
		return duration;
	}

	public long getDuration() {
		return duration.getValueAsLong();
	}

	public void setDuration(MeasurementData duration) {
		this.duration = duration;
	}

	public List<AffectData> getAffect() {
		return affect;
	}

	public void setAffect(List<AffectData> affect) {
		this.affect = affect;
	}

	public String getStartDateAsString() {
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT,
				DateFormat.SHORT, Locale.GERMAN);
		return df.format(new Date(this.start_timestamp));
	}

	public MeasurementData getAverageStrideFrequency() {
		return averageStrideFrequency;
	}

}
