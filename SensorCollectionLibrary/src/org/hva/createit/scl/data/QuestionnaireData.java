/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.data;

public class QuestionnaireData {
	private int _id;
	private String questionnaire_type;
	private String result;
	private long timestamp;
	private boolean uploaded;
	
	
	
	public QuestionnaireData(String questionnaire_type, String result,
			long timestamp) {
		super();
		this.questionnaire_type = questionnaire_type;
		this.result = result;
		this.timestamp = timestamp;
	}

	public QuestionnaireData(int _id, String questionnaire_type, String result,
							 long timestamp, boolean uploaded) {
		super();
		this.set_id(_id);
		this.questionnaire_type = questionnaire_type;
		this.result = result;
		this.timestamp = timestamp;
		this.uploaded = uploaded;
	}

	public String getQuestionnaire_type() {
		return questionnaire_type;
	}
	public void setQuestionnaire_type(String questionnaire_type) {
		this.questionnaire_type = questionnaire_type;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public boolean isUploaded() {
		return uploaded;
	}
	public void setUploaded(boolean uploaded) {
		this.uploaded = uploaded;
	}


	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}
}