/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.data;

import java.nio.ByteBuffer;

public class MeasurementData implements Comparable<MeasurementData> {
	private int id;
	private int deviceId;
	private String name;
	private byte[] value;
	private long timestamp;
	private boolean uploaded;

	public void setValue(byte[] value) {
		this.value = value;
	}

	public MeasurementData(int id, int deviceId, String name, float value,
			long timestamp, boolean uploaded) {
		super();
		this.id = id;
		this.deviceId = deviceId;
		this.name = name;
		this.value = floatToByteArray(value);
		this.timestamp = timestamp;
		this.uploaded = uploaded;
	}

	public MeasurementData(int id, int deviceId, String name, float value,
			long timestamp) {
		super();
		this.id = id;
		this.deviceId = deviceId;
		this.name = name;
		this.value = floatToByteArray(value);
		this.timestamp = timestamp;
		this.uploaded = false;
	}

	public MeasurementData(int deviceId, String name, float value,
			long timestamp, boolean uploaded) {
		super();
		this.deviceId = deviceId;
		this.name = name;
		this.value = floatToByteArray(value);
		this.timestamp = timestamp;
		this.uploaded = uploaded;
	}

	public MeasurementData(int deviceId, String name, float value,
			long timestamp) {
		super();
		this.deviceId = deviceId;
		this.name = name;
		this.value = floatToByteArray(value);
		this.timestamp = timestamp;
		this.uploaded = false;
	}

	public MeasurementData(int deviceId, String name, byte[] value,
			long timestamp) {
		super();
		this.deviceId = deviceId;
		this.name = name;
		this.value = value;
		this.timestamp = timestamp;
		this.uploaded = false;
	}

	public MeasurementData(int id, int deviceId, String name, byte[] value,
			long timestamp, boolean uploaded) {
		super();
		this.id = id;
		this.deviceId = deviceId;
		this.name = name;
		this.value = value;
		this.timestamp = timestamp;
		this.uploaded = uploaded;
	}

	public MeasurementData(int deviceId, String name, double value,
			long timestamp) {
		super();
		this.deviceId = deviceId;
		this.name = name;
		this.value = doubleToByteArray(value);
		this.timestamp = timestamp;
		this.uploaded = false;
	}

	public MeasurementData(int deviceId, String name, int value, long timestamp) {
		super();
		this.deviceId = deviceId;
		this.name = name;
		this.value = intToByteArray(value);
		this.timestamp = timestamp;
		this.uploaded = false;
	}

	public MeasurementData(int deviceId, String name, long value, long timestamp) {
		super();
		this.deviceId = deviceId;
		this.name = name;
		this.value = longToByteArray(value);
		this.timestamp = timestamp;
		this.uploaded = false;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = floatToByteArray(value);
	}

	public void setValue(int value) {
		this.value = intToByteArray(value);
	}

	public void setValue(double value) {
		this.value = doubleToByteArray(value);
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isUploaded() {
		return uploaded;
	}

	public void setUploaded(boolean uploaded) {
		this.uploaded = uploaded;
	}

	public static byte[] longToByteArray(long value) {
		return ByteBuffer.allocate(8).putLong(value).array();
	}

	public static byte[] floatToByteArray(float value) {
		return ByteBuffer.allocate(4).putFloat(value).array();
	}

	public static byte[] doubleToByteArray(double value) {
		return ByteBuffer.allocate(8).putDouble(value).array();
	}

	public static byte[] intToByteArray(int value) {
		return ByteBuffer.allocate(4).putInt(value).array();
	}

	public static long ByteArrayToLong(byte[] value) {
		return ByteBuffer.wrap(value).asLongBuffer().get();
	}

	public static float ByteArrayToFloat(byte[] value) {
		return ByteBuffer.wrap(value).asFloatBuffer().get();
	}

	public static double ByteArrayToDouble(byte[] value) {
		return ByteBuffer.wrap(value).asDoubleBuffer().get();
	}

	public static int ByteArrayToInt(byte[] value) {
		return ByteBuffer.wrap(value).asIntBuffer().get();
	}

	public double getValueAsDouble() {
		if(value != null){
			return ByteBuffer.wrap(value).asDoubleBuffer().get();
		}else{
			return 0.0d;
		}
	}

	public float getValueAsFloat() {
		if(value != null){
			return ByteBuffer.wrap(value).asFloatBuffer().get();
		}else{
			return 0.0f;
		}
	}

	public int getValueAsInt() {
		if(value != null){
			return ByteBuffer.wrap(value).asIntBuffer().get();
		}else{
			return 0;
		}
	}

	public long getValueAsLong() {
		if(value != null){
			return ByteBuffer.wrap(value).asLongBuffer().get();
		}else{
			return 0l;
		}
	}

	@Override
	public int compareTo(MeasurementData another) {
		if (another.getTimestamp() > this.getTimestamp()) {
			return 1;
		} else if (another.getTimestamp() == this.getTimestamp()) {
			return 0;
		}
		return -1;
	}

}
