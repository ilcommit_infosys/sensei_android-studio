/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.data;

import android.os.Environment;

public class ExertionLevelData extends MeasurementData {

	public static final String name = "exertionlevel";


	public ExertionLevelData(int deviceId, int value, long timestamp) {
		super(deviceId, name, value, timestamp);
	}

	public ExertionLevelData(MeasurementData measurement) {
		super(measurement.getId(), measurement.getDeviceId(), measurement
				.getName(), measurement.getValue(), measurement.getTimestamp(),
				measurement.isUploaded());
	}

	public int getExertionLevel() throws NullPointerException{
		return this.getValueAsInt();
	}

	public void setExertionLevel(int value) {
		this.setValue(value);
	}

	public String getAudioLocation() throws NullPointerException{
		return Environment.getExternalStorageDirectory().toString() + "/intelirun/Voice/"+this.getTimestamp()+".pcm";
	}
}
