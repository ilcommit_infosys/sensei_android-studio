package org.hva.createit.scl.data;

import java.nio.charset.Charset;
/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
/**
 * Created by biejh on 29-06-16.
 */
public class ExertionData {
    ExertionLevelData exertionLevel;
    MeasurementData exertionFilePath;

    public ExertionData(int exertionLevel, String exertionFilePath, long timestamp) {
        this.exertionLevel = new ExertionLevelData(0, exertionLevel, timestamp);
        this.exertionFilePath = new MeasurementData(0,"exertionfilelocation",exertionFilePath.getBytes(Charset.forName("utf-8")), timestamp);
    }

    public ExertionData(ExertionLevelData exertionLevel, MeasurementData exertionFilePath) {
        this.exertionLevel = exertionLevel;
        this.exertionFilePath = exertionFilePath;
    }

    public int getExertionLevel(){
        return exertionLevel.getExertionLevel();
    }

    public String getExertionFilePath(){
        return new String(exertionFilePath.getValue(), Charset.forName("utf-8"));
    }

    public ExertionLevelData getExertionLevelData(){
        return exertionLevel;
    }

    public MeasurementData getExertionFilePathData(){
        return exertionFilePath;
    }

    public void setUploaded(boolean uploaded){
        exertionLevel.setUploaded(uploaded);
        exertionFilePath.setUploaded(uploaded);
    }

    public void setExertionLevel(ExertionLevelData exertionLevel) {
        this.exertionLevel = exertionLevel;
    }

    public void setExertionFilePath(MeasurementData exertionFilePath) {
        this.exertionFilePath = exertionFilePath;
    }
}
