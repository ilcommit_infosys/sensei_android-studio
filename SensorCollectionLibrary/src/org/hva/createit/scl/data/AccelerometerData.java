/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.data;

public class AccelerometerData {
	private String sensorName;
	public static String sensorType = "accelerometer";
	private String manufacturer;
	private int deviceId;
	private long timestamp;
	private MeasurementData x;
	private MeasurementData y;
	private MeasurementData z;
	private Float forceVector = null;

	public AccelerometerData(int deviceId, long timestamp, float x, float y,
			float z) {
		this.deviceId = deviceId;
		this.timestamp = timestamp;
		this.setX(x);
		this.setY(y);
		this.setZ(z);
	}

	public AccelerometerData() {
	}

	public AccelerometerData(int deviceId, long timestamp) {
		this.deviceId = deviceId;
		this.timestamp = timestamp;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public long getTimeStamp() {
		return timestamp;
	}

	public MeasurementData getMeasurementX() {
		return x;
	}

	public MeasurementData getMeasurementY() {
		return y;
	}

	public MeasurementData getMeasurementZ() {
		return z;
	}

	public float getX() {
		return x.getValueAsFloat();
	}

	public float getY() {
		return y.getValueAsFloat();
	}

	public float getZ() {
		return z.getValueAsFloat();
	}

	public float getForceVector() {
		if (forceVector == null) {
			forceVector = (float) Math.sqrt(getX() * getX() + getY() * getY()
					+ getZ() * getZ());
		}
		return forceVector;
	}

	public String getSensortype() {
		return sensorType;
	}

	public void setSensortype(String sensorType) {
		this.sensorType = sensorType;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public void setX(Float x) {
		this.x = new MeasurementData(this.deviceId, "x", x, this.timestamp);
	}

	public void setY(Float y) {
		this.y = new MeasurementData(this.deviceId, "y", y, this.timestamp);
	}

	public void setZ(Float z) {
		this.z = new MeasurementData(this.deviceId, "z", z, this.timestamp);
	}

	@Override
	public String toString() {
		return manufacturer + ", " + this.sensorName + ", " + sensorType + ", "
				+ timestamp + ", " + getX() + ", " + getY() + ", " + getZ()
				+ ";";
	}

	public void setMeasurementX(MeasurementData x) {
		this.x = x;
	}

	public void setMeasurementY(MeasurementData y) {
		this.y = y;
	}

	public void setMeasurementZ(MeasurementData z) {
		this.z = z;
	}
}
