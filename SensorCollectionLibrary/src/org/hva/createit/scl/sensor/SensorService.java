/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.sensor;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.step.frequency.SFService;

import org.hva.createit.scl.NotificationHolder;
import org.hva.createit.scl.R;
import org.hva.createit.scl.data.AccelerometerData;
import org.hva.createit.scl.data.DistanceData;
import org.hva.createit.scl.data.HeartRateData;
import org.hva.createit.scl.data.LocationData;
import org.hva.createit.scl.data.MeasurementData;
import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.scl.data.StepfrequencyData;
import org.hva.createit.scl.data.StridefrequencyData;
import org.hva.createit.scl.dataaccess.AccelerometerDAO;
import org.hva.createit.scl.dataaccess.DistanceDAO;
import org.hva.createit.scl.dataaccess.HeartRateDAO;
import org.hva.createit.scl.dataaccess.LocationDAO;
import org.hva.createit.scl.dataaccess.RunDataDAO;
import org.hva.createit.scl.dataaccess.RunStatisticsDAO;
import org.hva.createit.scl.dataaccess.StepfrequencyDAO;
import org.hva.createit.scl.dataaccess.StridefrequencyDAO;

import de.greenrobot.event.EventBus;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Subscription;
import rx.functions.Action1;

/*
 * TO-DO:
 * - implement messenger and bind functionality as described here: http://developer.android.com/reference/android/app/Service.html
 * - extend messenger to turn on/off specific sensors
 * - implement location listener in this service using the Google Play Services locationlistener as used in https://github.com/joeyvanderbie/SenseiLogger/blob/location/src/org/hva/sensei/logger/MainMovementActivity.java
 * - implement link to VU stepcounter, when delivered.
 * - implement temporary stepcounter (Hanning-window + FFT + take highest peak between 0-220hz) as described by Bart Moens http://www.ipem.ugent.be/bartmoens/uploads/STSM.BartMoens.pdf
 */
public class SensorService extends Service implements SensorEventListener {
	private final String TAG = "SensorService";
	public final static String NEW_MEASUREMENT = "org.hva.createit.scl.SensorService.NEW_MEASUREMENT";
	public final static String LOG_TAG = "SensorService";
	public final static String WAKELOCK_TAG = "SensorService";
	final int MEASUREMENT_TIMEOUT = 5000;
	final static int NOTIFICATION_ID = 101010;

	private AccelerometerDAO accelerometerDataAccessObject;
	private StepfrequencyDAO stepFrequencyDataAccessObject;
	private StridefrequencyDAO strideFrequencyDataAccessObject;
	private LocationDAO locationDataAccessObject;
	private DistanceDAO distanceDataAccessObject;
	private RunStatisticsDAO runStatisticsDataAccessObject;
	private RunDataDAO runDataAccessObject;
	private HeartRateDAO heartRateDataAccessObject;
	private SensorManager manager;

	private RunOverviewData runOverview;

	SharedPreferences settings;

	int bufferSelection = 0;
	PendingIntent pendingIntent;
	WakeLock wakeLock;

	Subscription subscription;

	private long sensorTimeReference = 0l;
	private long myTimeReference = 0l;
	private long publishRunDataPreference = 30000;//in milis

	@Override
	public void onCreate() {
		Log.d(LOG_TAG, "onStartCommand");
		manager = (SensorManager) getSystemService(SENSOR_SERVICE);

		settings = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

		accelerometerDataAccessObject = new AccelerometerDAO(this);
		stepFrequencyDataAccessObject = new StepfrequencyDAO(this);
		strideFrequencyDataAccessObject = new StridefrequencyDAO(this);
		locationDataAccessObject = new LocationDAO(this);
		distanceDataAccessObject = new DistanceDAO(this);
		runStatisticsDataAccessObject = new RunStatisticsDAO(this);
		runDataAccessObject = new RunDataDAO(this);
		heartRateDataAccessObject = new HeartRateDAO(this);

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK
				| PowerManager.ACQUIRE_CAUSES_WAKEUP, WAKELOCK_TAG);

		// this.startForeground(NOTIFICATION_ID, NotificationHolder
		// .showPageNotifications(this, "Sensei",
		// "running with intelligence"));

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "Starting SensorService");
		if (!EventBus.getDefault().isRegistered(this)) {
			Log.d(TAG, "Registering EventBus");
			EventBus.getDefault().register(this);
		} else {
			Log.d(TAG, "EventBus service allready started");
		}

		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		stopLocks();

		stopLocationUpdates();
		EventBus.getDefault().unregister(this);

		stopService(new Intent(getBaseContext(), SFService.class));
	}

	/** Command to the service to display a message */
	public static final int MSG_SAY_HELLO = 1;
	public static final int MSG_START_SENSORS = 11;
	public static final int MSG_STOP_SENSORS = 10;

	public static final int MSG_START_SENSOR_LOCATION = 21;
	public static final int MSG_STOP_SENSOR_LOCATION = 20;

	public static final int MSG_START_SENSOR_ACCELEROMETER = 31;
	public static final int MSG_STOP_SENSOR_ACCELEROMETER = 30;

	public static final int MSG_START_SENSOR_STEPFREQUENCY = 33;
	public static final int MSG_STOP_SENSOR_STEPFREQUENCY = 32;

	/**
	 * Handler of incoming messages from clients.
	 */
	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_START_SENSORS:
				startRun();

				Toast.makeText(getApplicationContext(), "Starting sensors",
						Toast.LENGTH_SHORT).show();
				SensorService.this.startForeground(NOTIFICATION_ID,
						NotificationHolder.showPageNotifications(
								SensorService.this, getString(R.string.app_name), getString(R.string.slogan)));

				if (!wakeLock.isHeld()) {
					wakeLock.acquire();
				}

				startLocationUpdates();
				startDistanceUpdates();
				startStepFrequencySensor();

				for (Sensor sensor : manager
						.getSensorList(Sensor.TYPE_ACCELEROMETER)) {
					manager.registerListener(SensorService.this, sensor,
							1000000 / 20);// SensorManager.SENSOR_DELAY_FASTEST);

					// Log.d(LOG_TAG,
					// "Max events: " + sensor.getFifoMaxEventCount());
					// Log.d(LOG_TAG,
					// "Reserved: " + sensor.getFifoReservedEventCount());
				}

				break;
			case MSG_STOP_SENSORS:
				stopRun();

				stopLocks();
				Toast.makeText(getApplicationContext(), "Stopping senors",
						Toast.LENGTH_SHORT).show();
				SensorService.this.startForeground(NOTIFICATION_ID,
						NotificationHolder.showPageNotifications(
								SensorService.this, getString(R.string.app_name),
								getString(R.string.slogan)));
				for (Sensor sensor : manager
						.getSensorList(Sensor.TYPE_ACCELEROMETER)) {
					manager.unregisterListener(SensorService.this, sensor);

					// Log.d(LOG_TAG,
					// "Max events: " + sensor.getFifoMaxEventCount());
					// Log.d(LOG_TAG,
					// "Reserved: " + sensor.getFifoReservedEventCount());
				}

				stopLocationUpdates();
				stopDistanceUpdates();
				stopStepFrequencySensor();

				break;
			case MSG_START_SENSOR_LOCATION:
				startLocationUpdates();
				break;
			case MSG_STOP_SENSOR_LOCATION:
				stopLocationUpdates();
				break;
			case MSG_START_SENSOR_STEPFREQUENCY:
				startStepFrequencySensor();
				break;
			case MSG_STOP_SENSOR_STEPFREQUENCY:
				stopStepFrequencySensor();
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	MeasurementData averageStepfrequency = new MeasurementData(0,
			"averageStepFrequency", 0, 0);
	MeasurementData averageStridefrequency = new MeasurementData(0,
			"averageStepFrequency", 0, 0);
	MeasurementData averageHeartRate = new MeasurementData(0,
			"averageHeartRate", 0, 0);
	MeasurementData averageSpeed = new MeasurementData(0, "averageSpeed", 0, 0);

	long stepCount = 0;
	long strideCount = 0;
	long heartRateCount = 0;
	long speedCount = 0;

	private void startRun() {
		averageStepfrequency = new MeasurementData(0, "averageStepFrequency",
				0, 0);
		averageStridefrequency = new MeasurementData(0,
				"averageStrideFrequency", 0, 0);
		averageHeartRate = new MeasurementData(0, "averageHeartRate", 0, 0);
		averageSpeed = new MeasurementData(0, "averageSpeed", 0, 0);

		stepCount = 0;
		strideCount = 0;
		heartRateCount = 0;
		speedCount = 0;

		runOverview = new RunOverviewData(System.currentTimeMillis());
		runOverview.setAverageHeartRate(averageHeartRate);
		runOverview.setAverageSpeed(averageSpeed);
		runOverview.setAverageStepFrequency(averageStepfrequency);
		runOverview.setAverageStrideFrequency(averageStridefrequency);
		runOverview.setDistance(totalDistance);
		runOverview.setDuration(new MeasurementData(0, "duration", 0,
				runOverview.getStart_timestamp()));
		// new SaveRunOverviewData().execute(runOverview);
	}

	private void stopRun() {
		// runOverview.setStart_timestamp(runDataAccessObject.getLastRunData().getTimestamp());

		runOverview.setStop_timestamp(System.currentTimeMillis());
		totalDistance.setTimestamp(runOverview.getStop_timestamp());
		runOverview.setDistance(totalDistance);
		runOverview.setDuration(new MeasurementData(0, "duration", runOverview
				.getStop_timestamp() - runOverview.getStart_timestamp(),
				runOverview.getStop_timestamp()));
		averageHeartRate.setTimestamp(runOverview.getStop_timestamp());
		runOverview.setAverageHeartRate(averageHeartRate);
		averageSpeed.setTimestamp(runOverview.getStop_timestamp());
		runOverview.setAverageSpeed(averageSpeed);
		averageStepfrequency.setTimestamp(runOverview.getStop_timestamp());
		runOverview.setAverageStepFrequency(averageStepfrequency);
		runOverview.setAverageStrideFrequency(averageStridefrequency);

		new SaveRunOverviewData().execute(runOverview);
		EventBus.getDefault().post(runOverview);
	}

	private void updateRunOverview() {
		// This update should be happening every time a new locationevent is
		// received
		runOverview.setStop_timestamp(System.currentTimeMillis());
		totalDistance.setTimestamp(runOverview.getStop_timestamp());
		runOverview.setDistance(totalDistance);
		runOverview.setDuration(new MeasurementData(0, "duration", runOverview
				.getStop_timestamp() - runOverview.getStart_timestamp(),
				runOverview.getStop_timestamp()));
		averageHeartRate.setTimestamp(runOverview.getStop_timestamp());
		runOverview.setAverageHeartRate(averageHeartRate);
		averageSpeed.setTimestamp(runOverview.getStop_timestamp());
		runOverview.setAverageSpeed(averageSpeed);
		averageStepfrequency.setTimestamp(runOverview.getStop_timestamp());
		runOverview.setAverageStepFrequency(averageStepfrequency);
		runOverview.setAverageStrideFrequency(averageStridefrequency);

//		new UpdateRunOverviewData().execute(runOverview);
		EventBus.getDefault().post(runOverview);
	}

	private void stopLocks() {
		if (wakeLock.isHeld()) {
			wakeLock.release();
		}
		stopForeground(true);
	}

	/**
	 * Target we publish for clients to send messages to IncomingHandler.
	 */
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	/**
	 * When binding to the service, we return an interface to our messenger for
	 * sending messages to the service.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		Toast.makeText(getApplicationContext(), "binding", Toast.LENGTH_SHORT)
				.show();
		return mMessenger.getBinder();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	long mcount = 0;
	Thread t;

	@Override
	public void onSensorChanged(SensorEvent event) {
		// Check if timestamp in event matches with clock time
		// if not, correct to clocktimestamp in nanosecond
		if (System.nanoTime() - event.timestamp < 1000) {
			if (sensorTimeReference == 0l && myTimeReference == 0l) {
				sensorTimeReference = event.timestamp;
				myTimeReference = System.currentTimeMillis() * 1000000;
			}
			// set event timestamp to current time in nanoseconds
			event.timestamp = myTimeReference + event.timestamp
					- sensorTimeReference;
		}

		switch (event.sensor.getType()) {

		case (Sensor.TYPE_ACCELEROMETER):
			AccelerometerData ad = new AccelerometerData(0, event.timestamp,
					event.values[0], event.values[1], event.values[2]);
			// new SaveAccelerometerData().execute(ad);
			EventBus.getDefault().post(ad);

			break;

		// case (Sensor.TYPE_GYROSCOPE):
		// break;
		//
		// case (Sensor.TYPE_STEP_DETECTOR):
		// break;

		}

	}

	private void startLocationUpdates() {
		ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(
				this.getApplicationContext());
		locationProvider.getLastKnownLocation().subscribe(
				new Action1<Location>() {
					@Override
					public void call(Location location) {
						// Log.d(TAG,
						// location.getLatitude()+" "+location.getLongitude()) ;
					}
				});

		LocationRequest request = LocationRequest.create()
				// standard GMS LocationRequest
				.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
				.setInterval(1000);
		subscription = locationProvider.getUpdatedLocation(request).subscribe(
				new Action1<Location>() {
					@Override
					public void call(Location location) {
						updateLocation(location);
					}
				});
	}

	private void stopLocationUpdates() {
		if (subscription != null) {
			subscription.unsubscribe();
		}
	}

	private boolean distanceUpdates = false;

	private void startDistanceUpdates() {
		distanceUpdates = true;
		previousLocation = null;
		totalDistance = new DistanceData(0, 0, System.currentTimeMillis());
	}

	private void stopDistanceUpdates() {
		distanceUpdates = false;
	}

	private void updateLocation(Location location) {
		Log.d(TAG, location.getLatitude() + " " + location.getLongitude());
		LocationData ld = new LocationData(location);
		new SaveLocationData().execute(ld);
		EventBus.getDefault().post(ld);
	}

	// distance calculator not so nice integrated
	// this should have its own service
	private LocationData previousLocation;
	private DistanceData totalDistance = new DistanceData(0, 0,
			System.currentTimeMillis());

	public void onEvent(LocationData event) {
		if (distanceUpdates) {
			updateDistance(event);
		}
		averageSpeed
				.setValue(((averageSpeed.getValueAsFloat() * speedCount) + event
						.getSpeed().getValueAsFloat()) / (speedCount + 1));
		averageSpeed.setTimestamp(event.getTimestamp());
		speedCount++;

//		 update every x seconds;
		 if (System.currentTimeMillis() - runOverview.getStop_timestamp() > publishRunDataPreference) {
		 updateRunOverview();
		 }

	}

	private void updateDistance(LocationData location) {
		if (previousLocation == null) {
			previousLocation = location;
		}
		DistanceData dd;
		if (location.getSpeed().getValueAsFloat() > 0) {
			dd = new DistanceData(0, DistanceData.getDistance(previousLocation,
					location), location.getTimestamp());
			new SaveDistancedata().execute(dd);

			totalDistance.setDistance(totalDistance.getDistance()
					+ dd.getDistance());
			totalDistance.setTimestamp(dd.getTimestamp());

		} else {
			dd = new DistanceData(0, 0, location.getTimestamp());
		}
		previousLocation = location;

		EventBus.getDefault().post(totalDistance);

	}

	// end of distance calculator

	// This method will be called when a StepFrequencyData event is posted
	public void onEvent(StepfrequencyData event) {
		new SaveStepfrequencyData().execute(event);
		averageStepfrequency
				.setValue(((averageStepfrequency.getValueAsFloat() * stepCount) + event
						.getValueAsInt()) / (stepCount + 1));
		averageStepfrequency.setTimestamp(event.getTimestamp());
		stepCount++;
	}

	// This method will be called when a StepFrequencyData event is posted
	public void onEvent(StridefrequencyData event) {
		new SaveStridefrequencyData().execute(event);
		averageStridefrequency.setValue(((averageStridefrequency
				.getValueAsFloat() * strideCount) + event.getValueAsInt())
				/ (strideCount + 1));
		averageStridefrequency.setTimestamp(event.getTimestamp());
		strideCount++;
	}

	// This method will be called when a StepFrequencyData event is posted
	public void onEvent(HeartRateData event) {
		new SaveHeartRateData().execute(event);
		averageHeartRate
				.setValue(((averageHeartRate.getValueAsFloat() * heartRateCount) + event
						.getHeartRate()) / (heartRateCount + 1));
		averageHeartRate.setTimestamp(event.getTimestamp());
		heartRateCount++;
	}

	private void startStepFrequencySensor() {
		startService(new Intent(getBaseContext(), SFService.class));
	}

	private void stopStepFrequencySensor() {
		stopService(new Intent(getBaseContext(), SFService.class));
	}

	private class SaveAccelerometerData extends
			AsyncTask<AccelerometerData, Void, String> {

		@Override
		protected void onPreExecute() {
		};

		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 * 
		 * @return
		 */
		@Override
		protected String doInBackground(AccelerometerData... acm) {
			accelerometerDataAccessObject.store(acm[0]);
			return "";
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		@Override
		protected void onPostExecute(String result) {
		}
	}

	private class SaveLocationData extends
			AsyncTask<LocationData, Void, String> {

		@Override
		protected void onPreExecute() {
		};

		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 * 
		 * @return
		 */
		@Override
		protected String doInBackground(LocationData... acm) {
			locationDataAccessObject.store(acm[0]);
			return "";
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		@Override
		protected void onPostExecute(String result) {
		}
	}

	private class SaveHeartRateData extends
			AsyncTask<HeartRateData, Void, String> {

		@Override
		protected void onPreExecute() {
		};

		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 * 
		 * @return
		 */
		@Override
		protected String doInBackground(HeartRateData... acm) {
			heartRateDataAccessObject.store(acm[0]);
			return "";
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		@Override
		protected void onPostExecute(String result) {
		}
	}

	private class SaveStridefrequencyData extends
			AsyncTask<StridefrequencyData, Void, String> {

		@Override
		protected void onPreExecute() {
		};

		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 * 
		 * @return
		 */
		@Override
		protected String doInBackground(StridefrequencyData... acm) {
			strideFrequencyDataAccessObject.store(acm[0]);
			return "";
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		@Override
		protected void onPostExecute(String result) {
		}
	}

	private class SaveStepfrequencyData extends
			AsyncTask<StepfrequencyData, Void, String> {

		@Override
		protected void onPreExecute() {
		};

		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 * 
		 * @return
		 */
		@Override
		protected String doInBackground(StepfrequencyData... acm) {
			stepFrequencyDataAccessObject.store(acm[0]);
			return "";
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		@Override
		protected void onPostExecute(String result) {
		}
	}

	private class SaveDistancedata extends
			AsyncTask<DistanceData, Void, String> {

		@Override
		protected void onPreExecute() {
		};

		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 * 
		 * @return
		 */
		@Override
		protected String doInBackground(DistanceData... acm) {
			distanceDataAccessObject.store(acm[0]);
			return "";
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		@Override
		protected void onPostExecute(String result) {
		}
	}

	private class SaveRunOverviewData extends
			AsyncTask<RunOverviewData, Void, String> {

		@Override
		protected void onPreExecute() {
		};

		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 * 
		 * @return
		 */
		@Override
		protected String doInBackground(RunOverviewData... acm) {
			runStatisticsDataAccessObject.store(acm[0]);
			return "";
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		@Override
		protected void onPostExecute(String result) {
		}
	}

	private class UpdateRunOverviewData extends
			AsyncTask<RunOverviewData, Void, String> {

		@Override
		protected void onPreExecute() {
		};

		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 * 
		 * @return
		 */
		@Override
		protected String doInBackground(RunOverviewData... acm) {
			runStatisticsDataAccessObject.update(acm[0]);
			return "";
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		@Override
		protected void onPostExecute(String result) {
		}
	}
}