/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl;

import org.hva.createit.scl.ble.HeartrateActivity;
import org.hva.createit.scl.sensor.SensorService;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import de.greenrobot.event.EventBus;

//test
public class SCLControllerActivity extends HeartrateActivity {
	public static final String TAG = "SensorCollectionLibraryController";

	SensorServiceReceiver sensorReceiver = null;
	Intent sensorIntent;
	Handler startupHandler = new Handler();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");

		sensorIntent = new Intent(this, SensorService.class);
		startService(sensorIntent);
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "onResume/registering receiver");

		sensorReceiver = new SensorServiceReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(SensorService.NEW_MEASUREMENT);
		registerReceiver(sensorReceiver, intentFilter);

		EventBus.getDefault().register(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d(TAG, "onPause/unregistering receiver");
		if (sensorReceiver != null) {
			unregisterReceiver(sensorReceiver);
		}
		sensorReceiver = null;

		EventBus.getDefault().unregister(this);

	}

	@Override
	protected void onStart() {
		super.onStart();
		// Bind to the service
		bindService(new Intent(this, SensorService.class), mConnection,
				Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop");

		if (sensorReceiver != null) {
			unregisterReceiver(sensorReceiver);
		}
		// Unbind from the service
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
		stopService(sensorIntent);

	}

	private class SensorServiceReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context arg0, Intent arg1) {

		}
	}

	/** Messenger for communicating with the service. */
	Messenger mService = null;

	/** Flag indicating whether we have called bind on the service. */
	boolean mBound;

	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the object we can use to
			// interact with the service. We are communicating with the
			// service using a Messenger, so here we get a client-side
			// representation of that from the raw IBinder object.
			mService = new Messenger(service);
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			mService = null;
			mBound = false;
		}
	};

	public void startSensors() {
		startService(sensorIntent);

		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null, SensorService.MSG_START_SENSORS, 0,
				0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}

	}

	public void stopSensors() {
		if (!mBound) {
			bindService(new Intent(this, SensorService.class), mConnection,
					Context.BIND_AUTO_CREATE);
		}
		// if (!mBound) {
		// return;
		// }
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message
				.obtain(null, SensorService.MSG_STOP_SENSORS, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		stopService(sensorIntent);
	}

	public void startLocationSensor() {
		startService(sensorIntent);

		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null,
				SensorService.MSG_START_SENSOR_LOCATION, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	public void stopLocationSensor() {
		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null,
				SensorService.MSG_STOP_SENSOR_LOCATION, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		stopService(sensorIntent);
	}

	public void startStepFrequencySensor() {
		startService(sensorIntent);

		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null,
				SensorService.MSG_START_SENSOR_STEPFREQUENCY, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	public void stopStepFrequencySensor() {
		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null,
				SensorService.MSG_STOP_SENSOR_STEPFREQUENCY, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		// stopService(sensorIntent);
	}

}
