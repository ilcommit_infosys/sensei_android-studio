/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.hva.createit.scl.data.LocationData;
import org.hva.createit.scl.data.MeasurementData;

import android.content.Context;
import android.database.sqlite.SQLiteStatement;

public class LocationDAO extends AbstractDataAccessObject<LocationData> {

	private final static String sensortype = "location";
	private SQLiteStatement insertStatement;
	private String myTable = Database.Measurement.TABLE_MEASUREMENT;
	private String TAG = "LocationDAO";

	public LocationDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();
		this.prepareDefaultInsertStatement();
		this.prepareDefaultUpdateStatement();

	}

	@Override
	public void store(LocationData ld) {
		store(ld.getLatitude());
		store(ld.getLongitude());
		store(ld.getAccuracy());
		store(ld.getBearing());
		store(ld.getAltitude());
		store(ld.getSpeed());
	}

	@Override
	public List<LocationData> getItems(long startDateTime, long endDateTime) {
		HashMap<Long, LocationData> hashMapLocationData = new HashMap<Long, LocationData>();

		// rewrite this to getunuploaded items for name "" and use timestamps to
		// fill all the locationdataobjects

		List<MeasurementData> latitude = getMeasurements("latitude",
				startDateTime, endDateTime);
		List<MeasurementData> longitude = getMeasurements("longitude",
				startDateTime, endDateTime);
		List<MeasurementData> accuracy = getMeasurements("accuracy",
				startDateTime, endDateTime);
		List<MeasurementData> speed = getMeasurements("speed", startDateTime,
				endDateTime);
		List<MeasurementData> altitude = getMeasurements("altitude",
				startDateTime, endDateTime);
		List<MeasurementData> bearing = getMeasurements("bearing",
				startDateTime, endDateTime);

		for (MeasurementData measurement : latitude) {
			LocationData ad = new LocationData(measurement.getDeviceId(),
					measurement.getTimestamp());
			ad.setLatitude(measurement);
			hashMapLocationData.put(measurement.getTimestamp(), ad);
		}
		for (MeasurementData measurement : longitude) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad != null) {
				ad.setLongitude(measurement);
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}
		for (MeasurementData measurement : accuracy) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad != null) {
				ad.setAccuracy(measurement);
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}
		for (MeasurementData measurement : speed) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad != null) {
				ad.setSpeed(measurement);
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}
		for (MeasurementData measurement : altitude) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad != null) {
				ad.setAltitude(measurement);
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}
		for (MeasurementData measurement : bearing) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad != null) {
				ad.setBearing(measurement);
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}

		List<LocationData> loc = new ArrayList<LocationData>(
				hashMapLocationData.values());
		Collections.sort(loc);
		return loc;
	}

	@Override
	public void update(LocationData acm) {
		update(acm.getAltitude());
		update(acm.getBearing());
		update(acm.getLatitude());
		update(acm.getLongitude());
		update(acm.getSpeed());
		update(acm.getAccuracy());
	}

	@Override
	public long getUnuploadedItemCount() {
		long itemCount = 0;
		itemCount += getUnuploadedItemCount("accuracy");
		itemCount += getUnuploadedItemCount("altitude");
		itemCount += getUnuploadedItemCount("bearing");
		itemCount += getUnuploadedItemCount("latitude");
		itemCount += getUnuploadedItemCount("longitude");
		itemCount += getUnuploadedItemCount("speed");

		return itemCount;
	}

	@Override
	public List<LocationData> getUnuploadedItems() {
		// select x,y,z values from db with join on deviceid, timestamp
		// run loop to create objects from results
		// return results
		HashMap<Long, LocationData> hashMapLocationData = new HashMap<Long, LocationData>();

		// rewrite this to getunuploaded items for name "" and use timestamps to
		// fill all the locationdataobjects

		List<MeasurementData> latitude = getMeasurements("latitude", false);
		for (MeasurementData measurement : latitude) {
			LocationData ad = new LocationData(measurement.getDeviceId(),
					measurement.getTimestamp());
			ad.setLatitude(measurement);
			ad.setLongitude(getMeasurement("longitude",
					measurement.getTimestamp()));
			ad.setAccuracy(getMeasurement("accuracy",
					measurement.getTimestamp()));
			ad.setSpeed(getMeasurement("speed", measurement.getTimestamp()));
			ad.setAltitude(getMeasurement("altitude",
					measurement.getTimestamp()));
			ad.setBearing(getMeasurement("bearing", measurement.getTimestamp()));
			hashMapLocationData.put(measurement.getTimestamp(), ad);
		}

		List<MeasurementData> longitude = getMeasurements("longitude", false);
		for (MeasurementData measurement : longitude) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new LocationData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setLatitude(getMeasurement("latitude",
						measurement.getTimestamp()));
				ad.setLongitude(measurement);
				ad.setAccuracy(getMeasurement("accuracy",
						measurement.getTimestamp()));
				ad.setSpeed(getMeasurement("speed", measurement.getTimestamp()));
				ad.setAltitude(getMeasurement("altitude",
						measurement.getTimestamp()));
				ad.setBearing(getMeasurement("bearing",
						measurement.getTimestamp()));
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}

		List<MeasurementData> speeds = getMeasurements("speed", false);
		for (MeasurementData measurement : speeds) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new LocationData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setLatitude(getMeasurement("latitude",
						measurement.getTimestamp()));
				ad.setLongitude(getMeasurement("longitude",
						measurement.getTimestamp()));
				ad.setAccuracy(getMeasurement("accuracy",
						measurement.getTimestamp()));
				ad.setSpeed(measurement);
				ad.setAltitude(getMeasurement("altitude",
						measurement.getTimestamp()));
				ad.setBearing(getMeasurement("bearing",
						measurement.getTimestamp()));
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}

		List<MeasurementData> altitude = getMeasurements("altitude", false);
		for (MeasurementData measurement : altitude) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new LocationData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setLatitude(getMeasurement("latitude",
						measurement.getTimestamp()));
				ad.setLongitude(getMeasurement("longitude",
						measurement.getTimestamp()));
				ad.setAccuracy(getMeasurement("accuracy",
						measurement.getTimestamp()));
				ad.setSpeed(getMeasurement("speed", measurement.getTimestamp()));
				ad.setAltitude(measurement);
				ad.setBearing(getMeasurement("bearing",
						measurement.getTimestamp()));
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}

		List<MeasurementData> bearing = getMeasurements("bearing", false);
		for (MeasurementData measurement : bearing) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new LocationData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setLatitude(getMeasurement("latitude",
						measurement.getTimestamp()));
				ad.setLongitude(getMeasurement("longitude",
						measurement.getTimestamp()));
				ad.setAccuracy(getMeasurement("accuracy",
						measurement.getTimestamp()));
				ad.setSpeed(getMeasurement("speed", measurement.getTimestamp()));
				ad.setAltitude(getMeasurement("altitude",
						measurement.getTimestamp()));
				ad.setBearing(measurement);
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}

		List<MeasurementData> accuracy = getMeasurements("accuracy", false);
		for (MeasurementData measurement : accuracy) {
			LocationData ad = hashMapLocationData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new LocationData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setLongitude(getMeasurement("longitude",
						measurement.getTimestamp()));
				ad.setLatitude(getMeasurement("latitude",
						measurement.getTimestamp()));
				ad.setSpeed(getMeasurement("speed", measurement.getTimestamp()));
				ad.setAltitude(getMeasurement("altitude",
						measurement.getTimestamp()));
				ad.setBearing(getMeasurement("bearing",
						measurement.getTimestamp()));
				ad.setAccuracy(measurement);
				hashMapLocationData.put(measurement.getTimestamp(), ad);
			}
		}

		List<LocationData> accels = new ArrayList<LocationData>(
				hashMapLocationData.values());

		return accels;
	}

}
