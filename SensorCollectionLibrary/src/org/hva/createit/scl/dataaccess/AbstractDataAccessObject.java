/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import org.hva.createit.scl.data.MeasurementData;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDataAccessObject<T> {
	public SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	private String tableName = Database.Measurement.TABLE_MEASUREMENT;
	private SQLiteStatement DEFAULT_INSERT_STATEMENT;
	private SQLiteStatement DEFAULT_UPDATE_STATEMENT;
	private String TAG = "AbstractDataAccessObject";

	public AbstractDataAccessObject(DatabaseHelper dbHelper) {
		this.dbHelper = dbHelper;
		this.tableName = Database.Measurement.TABLE_MEASUREMENT;
	}

	public AbstractDataAccessObject(DatabaseHelper dbHelper, String tableName) {
		this.dbHelper = dbHelper;
		this.tableName = tableName;
	}

	// public AbstractDataAccessObject(SQLiteDatabase database, String
	// tableName) {
	// this.database = database;
	// this.tableName = tableName;
	// }

	public void open() throws SQLException {
		this.database = dbHelper.getWritableDatabase();
	}

	public SQLiteStatement prepareDefaultInsertStatement() {
		this.DEFAULT_INSERT_STATEMENT = this.database
				.compileStatement("INSERT INTO "
						+ Database.Measurement.TABLE_MEASUREMENT + " ("
						+ Database.Measurement.COLUMN_MEASUREMENT_DEVICEID
						+ Database.COMMA_SEP
						+ Database.Measurement.COLUMN_MEASUREMENT_NAME
						+ Database.COMMA_SEP
						+ Database.Measurement.COLUMN_MEASUREMENT_VALUE
						+ Database.COMMA_SEP
						+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP
						+ Database.COMMA_SEP
						+ Database.Measurement.COLUMN_MEASUREMENT_UPLOADED
						+ " )" + " VALUES ( ?, ?, ?, ?,?) ;");
		return this.DEFAULT_INSERT_STATEMENT;
	}

	public SQLiteStatement prepareDefaultUpdateStatement() {
		this.DEFAULT_UPDATE_STATEMENT = this.database
				.compileStatement("UPDATE "
						+ Database.Measurement.TABLE_MEASUREMENT + " SET "
						+ Database.Measurement.COLUMN_MEASUREMENT_DEVICEID
						+ " = ? " + Database.COMMA_SEP
						+ Database.Measurement.COLUMN_MEASUREMENT_NAME
						+ " = ? " + Database.COMMA_SEP
						+ Database.Measurement.COLUMN_MEASUREMENT_VALUE
						+ " = ? " + Database.COMMA_SEP
						+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP
						+ " = ? " + Database.COMMA_SEP
						+ Database.Measurement.COLUMN_MEASUREMENT_UPLOADED
						+ " = ? " + "WHERE " + Database.Measurement._ID
						+ " = ? ;");
		return this.DEFAULT_UPDATE_STATEMENT;
	}

	public void close() {
		this.dbHelper.close();
	}

	public void store(MeasurementData data) {
		DEFAULT_INSERT_STATEMENT.bindLong(1, data.getDeviceId());
		DEFAULT_INSERT_STATEMENT.bindString(2, data.getName());
		DEFAULT_INSERT_STATEMENT.bindBlob(3, data.getValue());
		DEFAULT_INSERT_STATEMENT.bindLong(4, data.getTimestamp());
		DEFAULT_INSERT_STATEMENT.bindLong(5, (data.isUploaded() ? 1 : 0));
		DEFAULT_INSERT_STATEMENT.execute();
	}

	public void update(MeasurementData data) {
		DEFAULT_UPDATE_STATEMENT.bindLong(1, data.getDeviceId());
		DEFAULT_UPDATE_STATEMENT.bindString(2, data.getName());
		DEFAULT_UPDATE_STATEMENT.bindBlob(3, data.getValue());
		DEFAULT_UPDATE_STATEMENT.bindLong(4, data.getTimestamp());
		DEFAULT_UPDATE_STATEMENT.bindLong(5, (data.isUploaded() ? 1 : 0));
		DEFAULT_UPDATE_STATEMENT.bindLong(6, data.getId());
		DEFAULT_UPDATE_STATEMENT.execute();
	}

	public abstract void store(T acm);

	public abstract void update(T acm);

	public List<MeasurementData> getUploaded() {
		List<MeasurementData> md = new ArrayList<MeasurementData>();
		Cursor cursor = database.rawQuery("SELECT * FROM " + tableName
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_UPLOADED
				+ " = 1" + " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			MeasurementData measurement = cursorToMMeasurementData(cursor);
			md.add(measurement);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();

		return md;
	}

	public MeasurementData getMeasurement(String name, long timestamp) {
		MeasurementData measurement = null;
		String[] values = { ("" + timestamp), name, "1" };

		Cursor cursor = database.rawQuery("SELECT * FROM " + tableName
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP
				+ " = ? AND " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = ? " + " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME + " LIMIT ?",
				values);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement = cursorToMMeasurementData(cursor);
			cursor.moveToNext();
		}
		cursor.close();

		return measurement;
	}

	public List<MeasurementData> getMeasurements(String name, boolean uploaded) {
		// MeasurementData measurement = null;
		String[] values = { name , (uploaded ? "1" : "0") };
		Cursor cursor = database.rawQuery("SELECT * FROM "
				+ tableName
				+ " WHERE "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = ? "
				 + " AND " + Database.Measurement.COLUMN_MEASUREMENT_UPLOADED
				 + " = ? "
				+ " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME + ";", values);
		List<MeasurementData> measurement = new ArrayList<MeasurementData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToMMeasurementData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return measurement;
	}

	public List<MeasurementData> getMeasurements(String name, int limit) {
		// MeasurementData measurement = null;
		String[] values = { name, ("" + limit) };
		Cursor cursor = database.rawQuery("SELECT * FROM " + tableName
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = ? " + " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " DESC "
				+ "LIMIT ?", values);
		List<MeasurementData> measurement = new ArrayList<MeasurementData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToMMeasurementData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return measurement;
	}

	public List<MeasurementData> getMeasurements(String name,
			long startDateTime, long endDateTime) {
		// MeasurementData measurement = null;
		String[] values = { name, "" + startDateTime, "" + endDateTime };
		Cursor cursor = database.rawQuery("SELECT * FROM " + tableName
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = ? " + " AND "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " >= ? "
				+ " AND " + Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP
				+ " <= ? " + " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " DESC ",
				values);
		List<MeasurementData> measurement = new ArrayList<MeasurementData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToMMeasurementData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return measurement;
	}

	public List<MeasurementData> getMeasurements(String name,
			long startDateTime, long endDateTime, boolean orderAsc) {
		// MeasurementData measurement = null;

		List<MeasurementData> measurementData = getMeasurements(name,
				startDateTime, endDateTime);

		if (orderAsc) {
			List<MeasurementData> tmpMeasurementData = new ArrayList<MeasurementData>();
			for (int i = measurementData.size() - 1; i >= 0; i--) {
				tmpMeasurementData.add(measurementData.get(i));
			}
			measurementData = tmpMeasurementData;
		}

		return measurementData;
	}

	public long getItemCount() {
		long itemCount = 0;
		itemCount = 0;
		Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM "
				+ tableName, null);
		if (resultSet != null && resultSet.getCount() > 0) {
			resultSet.moveToFirst();
			String str = resultSet.getString(0);
			itemCount = Long.parseLong(str);
		}
		return itemCount;
	}

	public long getUnuploadedItemCount() {
		long itemCount = 0;
		itemCount = 0;
		Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM "
				+ tableName + " WHERE "
				+ Database.Measurement.COLUMN_MEASUREMENT_UPLOADED
				+ " = 0 LIMIT 1;", null);
		if (resultSet != null && resultSet.getCount() > 0) {
			resultSet.moveToFirst();
			String str = resultSet.getString(0);
			itemCount = Long.parseLong(str);
		}
		return itemCount;
	}

	public long getUnuploadedItemCount(String name) {
		long itemCount = 0;
		itemCount = 0;
		String[] values = { name };
		Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM "
				+ tableName + " WHERE "
				+ Database.Measurement.COLUMN_MEASUREMENT_UPLOADED + " = 0 "
				+ " AND " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = ?" + " LIMIT 1;", values);
		if (resultSet != null && resultSet.getCount() > 0) {
			resultSet.moveToFirst();
			String str = resultSet.getString(0);
			itemCount = Long.parseLong(str);
		}
		return itemCount;
	}

	public long getItemCount(MeasurementData data) {
		long itemCount = 0;
		itemCount = 0;
		Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM "
				+ tableName + " WHERE "
				+ Database.Measurement.COLUMN_MEASUREMENT_DEVICEID + " = "
				+ data.getDeviceId(), null);
		if (resultSet != null && resultSet.getCount() > 0) {
			resultSet.moveToFirst();
			String str = resultSet.getString(0);
			itemCount = Long.parseLong(str);
		}
		return itemCount;
	}

	public List<T> getItems() {
		return getItems(0, getItemCount());
	}

	public List<T> getUnuploadedItems() {
		return getItems(0, getItemCount());
	}

	public abstract List<T> getItems(long fromIndex, long toIndex);

	public void clearItems() {
		database.delete(tableName, null, null);
	}

	public MeasurementData cursorToMMeasurementData(Cursor cursor) {
		MeasurementData measurement = new MeasurementData(cursor.getInt(0),
				cursor.getInt(1), cursor.getString(2), cursor.getBlob(3),
				cursor.getLong(4), Boolean.parseBoolean(cursor.getString(5)));
		return measurement;
	}
}
