/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hva.createit.scl.data.AccelerometerData;
import org.hva.createit.scl.data.MeasurementData;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

public class AccelerometerDAO extends
		AbstractDataAccessObject<AccelerometerData> {
	private final static String sensortype = AccelerometerData.sensorType;
	private SQLiteStatement insertStatement;
	private String myTable = Database.Measurement.TABLE_MEASUREMENT;
	private String TAG = "AccelerometerDAO";

	public AccelerometerDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();
		this.prepareDefaultInsertStatement();
		this.prepareDefaultUpdateStatement();
	}

	// @Override
	// public void store(AccelerometerData acm) {
	// insertStatement.bindString(1, acm.getManufacturer());
	// insertStatement.bindString(2, acm.getSensorName());
	// insertStatement.bindString(3, acm.getSensortype());
	// insertStatement.bindString(4, "x");
	// insertStatement.bindDouble(5, acm.getX());
	// insertStatement.bindLong(6, acm.getTimeStamp());
	// insertStatement.bindLong(7, 0);
	// insertStatement.execute();
	//
	// insertStatement.bindString(1, acm.getManufacturer());
	// insertStatement.bindString(2, acm.getSensorName());
	// insertStatement.bindString(3, acm.getSensortype());
	// insertStatement.bindString(4, "y");
	// insertStatement.bindDouble(5, acm.getY());
	// insertStatement.bindLong(6, acm.getTimeStamp());
	// insertStatement.bindLong(7, 0);
	// insertStatement.execute();
	//
	// insertStatement.bindString(1, acm.getManufacturer());
	// insertStatement.bindString(2, acm.getSensorName());
	// insertStatement.bindString(3, acm.getSensortype());
	// insertStatement.bindString(4, "z");
	// insertStatement.bindDouble(5, acm.getZ());
	// insertStatement.bindLong(6, acm.getTimeStamp());
	// insertStatement.bindLong(7, 0);
	// insertStatement.execute();
	// }

	@Override
	public void store(AccelerometerData acm) {
		// store manufacturer if not exist
		store(acm.getMeasurementX());
		store(acm.getMeasurementY());
		store(acm.getMeasurementZ());
	}

	// @Override
	// public List<AccelerometerData> getItems(long fromIndex, long toIndex) {
	// // TODO Auto-generated method stub
	//
	// //select x,y,z values from db with join on deviceid, timestamp
	// //run loop to create objects from results
	// //return results
	//
	// return null;
	// }

	@Override
	public void update(AccelerometerData acm) {
		update(acm.getMeasurementX());
		update(acm.getMeasurementY());
		update(acm.getMeasurementZ());
	}

	@Override
	public long getUnuploadedItemCount() {
		long itemCount = 0;
		itemCount += getUnuploadedItemCount("x");
		itemCount += getUnuploadedItemCount("y");
		itemCount += getUnuploadedItemCount("z");

		return itemCount;
	}

	@Override
	public List<AccelerometerData> getUnuploadedItems() {
		// select x,y,z values from db with join on deviceid, timestamp
		// run loop to create objects from results
		// return results
		HashMap<Long, AccelerometerData> hashMapAccelerometerData = new HashMap<Long, AccelerometerData>();

		List<MeasurementData> measurements = getMeasurements("x", false);
		for (MeasurementData measurement : measurements) {
			AccelerometerData ad = new AccelerometerData(
					measurement.getDeviceId(), measurement.getTimestamp());
			ad.setMeasurementX(measurement);

			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
		}

		measurements = getMeasurements("y", false);
		for (MeasurementData measurement : measurements) {
			AccelerometerData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new AccelerometerData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setMeasurementX(getMeasurement("x",
						measurement.getTimestamp()));
			}
			ad.setMeasurementY(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
		}

		measurements = getMeasurements("z", false);
		for (MeasurementData measurement : measurements) {
			AccelerometerData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new AccelerometerData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setMeasurementX(getMeasurement("x",
						measurement.getTimestamp()));
				ad.setMeasurementY(getMeasurement("y",
						measurement.getTimestamp()));
			}
			ad.setMeasurementZ(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
		}

		List<AccelerometerData> accels = new ArrayList<AccelerometerData>(
				hashMapAccelerometerData.values());

		return accels;
	}

	public List<AccelerometerData> getUploadedItems() {
		// select x,y,z values from db with join on deviceid, timestamp
		// run loop to create objects from results
		// return results
		HashMap<Long, AccelerometerData> hashMapAccelerometerData = new HashMap<Long, AccelerometerData>();

		Cursor cursor = database.rawQuery("SELECT * FROM " + myTable
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = 'x'" + " AND "
				+ Database.Measurement.COLUMN_MEASUREMENT_UPLOADED + " = 1"
				+ " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			MeasurementData measurement = cursorToMMeasurementData(cursor);
			AccelerometerData ad = new AccelerometerData(
					measurement.getDeviceId(), measurement.getTimestamp());
			ad.setMeasurementX(measurement);

			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();

		cursor = database.rawQuery("SELECT * FROM " + myTable + " WHERE "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME + " = 'y'"
				+ " AND " + Database.Measurement.COLUMN_MEASUREMENT_UPLOADED
				+ " = 1" + " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			MeasurementData measurement = cursorToMMeasurementData(cursor);

			AccelerometerData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			ad.setMeasurementY(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();

		cursor = database.rawQuery("SELECT * FROM " + myTable + " WHERE "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME + " = 'z'"
				+ " AND " + Database.Measurement.COLUMN_MEASUREMENT_UPLOADED
				+ " = 1" + " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			MeasurementData measurement = cursorToMMeasurementData(cursor);

			AccelerometerData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			ad.setMeasurementZ(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();

		List<AccelerometerData> accels = new ArrayList<AccelerometerData>(
				hashMapAccelerometerData.values());

		return accels;
	}

	@Override
	public long getItemCount() {
		long itemCount = 0;

		Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM " + myTable
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = 'x' LIMIT 1 ", null);
		if (resultSet != null && resultSet.getCount() > 0) {
			resultSet.moveToFirst();
			String str = resultSet.getString(0);
			itemCount = Long.parseLong(str);
		}
		return itemCount;
	}

	@Override
	public List<AccelerometerData> getItems(long fromIndex, long toIndex) {
		return null;
	}

	// @Override
	// public List<AccelerometerData> getItems(long fromIndex, long toIndex) {
	// if (fromIndex >= toIndex)
	// return null;
	// ArrayList<AccelerometerData> acmList = new
	// ArrayList<AccelerometerData>();
	// Cursor resultSet = database.rawQuery("SELECT * FROM " + myTable +
	// " LIMIT " + fromIndex + "," + (toIndex - fromIndex) + " WHERE " +
	// Database.Measurement.COLUMN_MEASUREMENT_SENSORTYPE + " = " + sensortype ,
	// null);
	// if (resultSet != null && resultSet.getCount() > 0) {
	// resultSet.moveToFirst();
	// do {
	// acmList.add(new AccelerometerData(resultSet.getString(0),
	// resultSet.getString(1), .getLong(0), resultSet.getDouble(1),
	// resultSet.getDouble(2), resultSet.getDouble(3)));
	// } while (resultSet.moveToNext());
	// }
	// return acmList;
	// }
	//
	// public List<AccelerometerData> getItemsFromTime(long fromTime, long
	// fromIndex, long toIndex) {
	// if (fromIndex >= toIndex)
	// return null;
	// ArrayList<AccelerometerData> acmList = new
	// ArrayList<AccelerometerData>();
	// Cursor resultSet = database.rawQuery("SELECT * FROM " + myTable +
	// " LIMIT " + fromIndex + "," + (toIndex - fromIndex) +
	// " WHERE TimeStamp >= "+fromTime + " AND " +
	// Database.Measurement.COLUMN_MEASUREMENT_SENSORTYPE + " = " + sensortype +
	// "ORDER BY "+ Database.Measurement._ID, null);
	// if (resultSet != null && resultSet.getCount() > 0) {
	// resultSet.moveToFirst();
	// do {
	// acmList.add(new AccelerometerData(resultSet.getLong(1),
	// resultSet.getDouble(2), resultSet.getDouble(2), resultSet.getDouble(3)));
	// } while (resultSet.moveToNext());
	// }
	// return acmList;
	// }

}
