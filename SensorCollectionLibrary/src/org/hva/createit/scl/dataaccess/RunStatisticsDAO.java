/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import org.hva.createit.scl.data.AffectData;
import org.hva.createit.scl.data.DistanceData;
import org.hva.createit.scl.data.MeasurementData;
import org.hva.createit.scl.data.RunOverviewData;

import java.util.ArrayList;
import java.util.List;

public class RunStatisticsDAO extends AbstractDataAccessObject<RunOverviewData> {
	private final static String sensortype = "runstatistics";
	private String myTable = Database.MeasurementOverview.TABLE_MEASUREMENTOVERVIEW;
	private String TAG = "RunStatisticsDAO";
	private SQLiteStatement RUNSTATISTICS_INSERT_STATEMENT;
	private SQLiteStatement RUNSTATISTICS_UPDATE_STATEMENT;

	private RunDataDAO rdd;
	private AffectDAO afd;

	public RunStatisticsDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();
		this.prepareRunstatisticsInsertStatement();
		this.prepareRunstatisticsUpdateStatement();
		this.prepareDefaultInsertStatement();
		this.prepareDefaultUpdateStatement();
		rdd = new RunDataDAO(context);
		afd = new AffectDAO(context);
	}

	@Override
	public void store(RunOverviewData acm) {
		store(acm.getSensorName(), acm.getStart_timestamp(),
				acm.getStop_timestamp(), acm.isUploaded());
		super.store(acm.getAverageSpeedMeasurementData());
		super.store(acm.getDurationMeasurementData());
		super.store(acm.getAverageHeartRateMeasurementData());
		super.store(acm.getAverageStepFrequencyMeasurementData());
		super.store(acm.getAverageStrideFrequencyMeasurementData());
		super.store(acm.getDistanceMeasurementData());
	}

	@Override
	public void update(RunOverviewData acm) {
		update(acm.getId(), acm.getSensorName(), acm.getStart_timestamp(),
				acm.getStop_timestamp(), acm.isUploaded());
		super.update(acm.getAverageSpeedMeasurementData());
		super.update(acm.getDurationMeasurementData());
		super.update(acm.getAverageHeartRateMeasurementData());
		super.update(acm.getAverageStepFrequencyMeasurementData());
		super.update(acm.getAverageStrideFrequencyMeasurementData());
		super.update(acm.getDistanceMeasurementData());
	}

	public RunOverviewData getLastRunOverview() {

		database.beginTransaction();
		RunOverviewData runOverviewData = null;
		try {
			runOverviewData = getLastRunOverviewData();
			runOverviewData.setDuration(super.getMeasurement("duration",
					runOverviewData.getStop_timestamp()));
			runOverviewData.setDistance(super.getMeasurement("distance",
					runOverviewData.getStop_timestamp()));
			runOverviewData.setAverageHeartRate(super.getMeasurement(
					"averageHeartRate", runOverviewData.getStop_timestamp()));
			runOverviewData
					.setAverageStepFrequency(super.getMeasurement(
							"averageStepFrequency",
							runOverviewData.getStop_timestamp()));
			runOverviewData.setAverageStrideFrequency(super.getMeasurement(
					"averageStrideFrequency",
					runOverviewData.getStop_timestamp()));
			runOverviewData.setAverageSpeed(super.getMeasurement(
					"averageSpeed", runOverviewData.getStop_timestamp()));
//			runOverviewData
//					.setAffect(afd.getLastAffectData());
			runOverviewData.setAffect(getAffectDataForRunOverview(runOverviewData));

			runOverviewData = fillIfEmpty(runOverviewData);

			database.setTransactionSuccessful();
		} catch (NullPointerException e) {
			Log.e(TAG, e.toString());
		} finally {
			database.endTransaction();
		}
		return runOverviewData;
	}

	@Override
	public long getUnuploadedItemCount() {
		long itemCount = 0;
		Cursor resultSet = database.rawQuery("SELECT COUNT("+Database.MeasurementOverview._ID+") FROM "
				+ Database.MeasurementOverview.TABLE_MEASUREMENTOVERVIEW + " WHERE "
				+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_UPLOADED
				+ " = 0 LIMIT 1;", null);
		if (resultSet != null && resultSet.getCount() > 0) {
			resultSet.moveToFirst();
			String str = resultSet.getString(0);
			itemCount = Long.parseLong(str);
		}
		return itemCount;
	}

	@Override
	public List<RunOverviewData> getUnuploadedItems() {
		List<RunOverviewData> runOverviewItems = new ArrayList<RunOverviewData>();
		String[] values = { "0" };
		Cursor cursor = database
				.rawQuery(
						"SELECT "
								+ Database.MeasurementOverview._ID
								+ " FROM "
								+ myTable
								+ " WHERE "
								+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_UPLOADED
								+ " = ? " + ";", values);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			runOverviewItems.add(getRunOverview(cursor.getInt(0)));
			cursor.moveToNext();
		}
		cursor.close();

		return runOverviewItems;
	}

	public RunOverviewData getRunOverview(int runId) {
		database.beginTransaction();
		RunOverviewData runOverviewData;
		try {
			runOverviewData = getRunOverviewData(runId);

			runOverviewData.setDuration(super.getMeasurement("duration",
					runOverviewData.getStop_timestamp()));
			runOverviewData.setDistance(super.getMeasurement("distance",
					runOverviewData.getStop_timestamp()));
			runOverviewData.setAverageHeartRate(super.getMeasurement(
					"averageHeartRate", runOverviewData.getStop_timestamp()));
			runOverviewData
					.setAverageStepFrequency(super.getMeasurement(
							"averageStepFrequency",
							runOverviewData.getStop_timestamp()));
			runOverviewData.setAverageStrideFrequency(super.getMeasurement(
					"averageStrideFrequency",
					runOverviewData.getStop_timestamp()));
			runOverviewData.setAverageSpeed(super.getMeasurement(
					"averageSpeed", runOverviewData.getStop_timestamp()));
			runOverviewData
					.setAffect(getAffectDataForRunOverview(runOverviewData));

			runOverviewData = fillIfEmpty(runOverviewData);

			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
		return runOverviewData;
	}

	public List<RunOverviewData> getLastRunOverviews(int limit) {
		List<RunOverviewData> runOverviews = getLastRunOverviewData(limit);
		for (RunOverviewData runOverviewData : runOverviews) {
			runOverviewData.setDuration(super.getMeasurement("duration",
					runOverviewData.getStop_timestamp()));
			runOverviewData.setDistance(super.getMeasurement("distance",
					runOverviewData.getStop_timestamp()));
			runOverviewData.setAverageHeartRate(super.getMeasurement(
					"averageHeartRate", runOverviewData.getStop_timestamp()));
			runOverviewData
					.setAverageStepFrequency(super.getMeasurement(
							"averageStepFrequency",
							runOverviewData.getStop_timestamp()));
			runOverviewData.setAverageStrideFrequency(super.getMeasurement(
					"averageStrideFrequency",
					runOverviewData.getStop_timestamp()));
			runOverviewData.setAverageSpeed(super.getMeasurement(
					"averageSpeed", runOverviewData.getStop_timestamp()));

			runOverviewData
					.setAffect(getAffectDataForRunOverview(runOverviewData));

			runOverviewData = fillIfEmpty(runOverviewData);

		}
		return runOverviews;
	}

	public RunOverviewData getAverageRunOverView(){
		List<RunOverviewData> runs = getLastRunOverviews(50);
		DistanceData averageDistance = new DistanceData(0, 0, 0);
		MeasurementData averageDuration = new MeasurementData(0,"", 0.0,0);
		MeasurementData averageSpeed = new MeasurementData(0, "", 0.0, 0);

		int i = 1;
		for(RunOverviewData run: runs){
			averageDistance.setDistance((averageDistance.getDistance() * i + run.getDistance())/(i+1));
			averageDuration.setValue((averageDuration.getValueAsDouble() * i + run.getDuration()) / (i + 1));
			averageSpeed.setValue((averageSpeed.getValueAsDouble() * i + ((double)run.getAverageSpeed())) / (i + 1));
			i++;
		}
		RunOverviewData thisRun = new RunOverviewData(0,0,System.currentTimeMillis());
		thisRun.setAverageSpeed(averageSpeed);
		thisRun.setDistance(averageDistance);

		thisRun.setDuration(averageDuration);

		return thisRun;

	}




	public List<AffectData> getAffectDataForRunOverview(RunOverviewData run) {
		List<AffectData> afList;
		try {
			if(rdd.getRunDataEndAfter(run.getStop_timestamp()) == null){
				afList = afd.getAffectData(
						rdd.getRunDataStartBefore(run.getStart_timestamp())
								.getTimestamp(),
						rdd.getRunDataEndBefore(run.getStop_timestamp()).getTimestamp());
			}else {
				afList = afd.getAffectData(
						rdd.getRunDataStartBefore(run.getStart_timestamp())
								.getTimestamp(),
						rdd.getRunDataEndAfter(run.getStop_timestamp()).getTimestamp());
			}


		}catch(NullPointerException ex){
			afList = new ArrayList<AffectData>();
			afList.add(new AffectData(0, run.getStart_timestamp(), 0, 0, 0));
			afList.add(new AffectData(0, run.getStop_timestamp(), 0, 0, 0));
		}
		return afList;
	}

	private RunOverviewData fillIfEmpty(RunOverviewData run) {
		if (run.getAffect() == null) {
			List<AffectData> af = new ArrayList<AffectData>();

			try {
				af.add(afd.getAffect(rdd.getRunDataStartBefore(
						run.getStart_timestamp()).getTimestamp()));
			} catch (NullPointerException e) {
				Log.e(TAG, e.toString());
				af.add(new AffectData(0, run.getStart_timestamp(), 0, 0, 0));
			}
			try {
				af.add(afd.getAffect(rdd.getRunDataEndAfter(
						run.getStop_timestamp()).getTimestamp()));
			} catch (NullPointerException e) {
				Log.e(TAG, e.toString());
				af.add(new AffectData(0, run.getStop_timestamp(), 0, 0, 0));
			}
			run.setAffect(af);
		}
		if (run.getAverageHeartRateMeasurementData() == null) {
			run.setAverageHeartRate(new MeasurementData(0, "averageHeartRate",
					0, run.getStop_timestamp()));
		}
		if (run.getAverageStepFrequencyMeasurementData() == null) {
			run.setAverageStepFrequency(new MeasurementData(0,
					"averageStepFrequency", 0, run.getStop_timestamp()));
		}
		if (run.getAverageStrideFrequencyMeasurementData() == null) {
			run.setAverageStrideFrequency(new MeasurementData(0,
					"averageStrideFrequency", 0, run.getStop_timestamp()));
		}
		if (run.getAverageSpeedMeasurementData() == null) {
			run.setAverageSpeed(new MeasurementData(0, "averageSpeed", 0, run
					.getStop_timestamp()));
		}
		if (run.getDistanceMeasurementData() == null) {
			run.setDistance(new MeasurementData(0, "distance", 0, run
					.getStop_timestamp()));
		}
		if (run.getDurationMeasurementData() == null) {
			run.setDuration(new MeasurementData(0, "duration", run
					.getStop_timestamp() - run.getStart_timestamp(), run
					.getStop_timestamp()));
		}

		return run;
	}

	@Override
	public List<RunOverviewData> getItems(long fromIndex, long toIndex) {
		return null;
	}

	private SQLiteStatement prepareRunstatisticsInsertStatement() {

		this.RUNSTATISTICS_INSERT_STATEMENT = this.database
				.compileStatement("INSERT INTO "
						+ Database.MeasurementOverview.TABLE_MEASUREMENTOVERVIEW
						+ " ("
						+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_NAME
						+ Database.COMMA_SEP
						+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP
						+ Database.COMMA_SEP
						+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_STOP_TIMESTAMP
						+ Database.COMMA_SEP
						+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_UPLOADED
						+ " )" + " VALUES ( ?, ?, ?, ?) ;");
		return this.RUNSTATISTICS_INSERT_STATEMENT;
	}

	public SQLiteStatement prepareRunstatisticsUpdateStatement() {
		this.RUNSTATISTICS_UPDATE_STATEMENT = this.database
				.compileStatement("UPDATE "
						+ Database.MeasurementOverview.TABLE_MEASUREMENTOVERVIEW
						+ " SET "
						+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_NAME
						+ " = ? "
						+ Database.COMMA_SEP
						+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP
						+ " = ? "
						+ Database.COMMA_SEP
						+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_STOP_TIMESTAMP
						+ " = ? "
						+ Database.COMMA_SEP
						+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_UPLOADED
						+ " = ? " + "WHERE " + Database.MeasurementOverview._ID
						+ " = ? ;");
		return this.RUNSTATISTICS_UPDATE_STATEMENT;
	}

	public void store(String name, long start_timestamp, long stop_timestamp,
			boolean isUploaded) {
		RUNSTATISTICS_INSERT_STATEMENT.bindString(1, name);
		RUNSTATISTICS_INSERT_STATEMENT.bindLong(2, start_timestamp);
		RUNSTATISTICS_INSERT_STATEMENT.bindLong(3, stop_timestamp);
		RUNSTATISTICS_INSERT_STATEMENT.bindLong(4, (isUploaded ? 1 : 0));
		RUNSTATISTICS_INSERT_STATEMENT.execute();
	}

	public void update(int id, String name, long start_timestamp,
			long stop_timestamp, boolean isUploaded) {
		RUNSTATISTICS_UPDATE_STATEMENT.bindString(1, name);
		RUNSTATISTICS_UPDATE_STATEMENT.bindLong(2, start_timestamp);
		RUNSTATISTICS_UPDATE_STATEMENT.bindLong(3, stop_timestamp);
		RUNSTATISTICS_UPDATE_STATEMENT.bindLong(4, (isUploaded ? 1 : 0));
		RUNSTATISTICS_UPDATE_STATEMENT.bindLong(5, id);
		RUNSTATISTICS_UPDATE_STATEMENT.execute();
	}

	private RunOverviewData getLastRunOverviewData() {
		String[] values = { "1" };
		Cursor cursor = database
				.rawQuery(
						"SELECT * FROM "
								+ myTable
								+ " ORDER BY "
								+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP
								+ " DESC " + "LIMIT ? " + ";", values);
		RunOverviewData runOverview = null;
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			runOverview = cursorToRunOverviewData(cursor);
			cursor.moveToNext();
		}
		cursor.close();

		return runOverview;
	}

	private RunOverviewData getRunOverviewData(int runId) {
		String[] values = { "" + runId };
		Cursor cursor = database
				.rawQuery(
						"SELECT * FROM "
								+ myTable
								+ " WHERE "
								+ Database.MeasurementOverview._ID
								+ " = ? "
								+ " ORDER BY "
								+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP
								+ " DESC " + "LIMIT 1 " + ";", values);
		RunOverviewData runOverview = null;
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			runOverview = cursorToRunOverviewData(cursor);
			cursor.moveToNext();
		}
		cursor.close();

		return runOverview;
	}

	private List<RunOverviewData> getLastRunOverviewData(int limit) {
		String[] values = { "" + limit };
		Cursor cursor = database
				.rawQuery(
						"SELECT * FROM "
								+ myTable
								+ " ORDER BY "
								+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP
								+ " DESC " + "LIMIT ? " + ";", values);
		List<RunOverviewData> runOverview = new ArrayList<RunOverviewData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			runOverview.add(cursorToRunOverviewData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return runOverview;
	}

	private RunOverviewData cursorToRunOverviewData(Cursor cursor) {
		RunOverviewData runOverviewData = new RunOverviewData(cursor.getInt(0),
		// cursor.getString(1),
				cursor.getLong(2), cursor.getLong(3),
				Boolean.parseBoolean(cursor.getString(4)));
		return runOverviewData;
	}

}
