/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import android.provider.BaseColumns;

public final class Database {
	// To prevent someone from accidentally instantiating the contract class,
	// give it an empty constructor.
	public Database() {
	}

	/* Inner class that defines the table contents */
	public static abstract class Measurement implements BaseColumns {
		public static final String TABLE_MEASUREMENT = "measurement";
		public static final String COLUMN_MEASUREMENT_DEVICEID = "deviceid";
		public static final String COLUMN_MEASUREMENT_NAME = "name";
		public static final String COLUMN_MEASUREMENT_VALUE = "value";
		public static final String COLUMN_MEASUREMENT_TIMESTAMP = "timestamp";
		public static final String COLUMN_MEASUREMENT_UPLOADED = "uploaded";
	}

	public static abstract class Device implements BaseColumns {
		public static final String TABLE_DEVICE = "device";
		public static final String COLUMN_DEVICE_MANUFACTURER = "manufacturer";
		public static final String COLUMN_DEVICE_SENSORNAME = "sensorname";
		public static final String COLUMN_DEVICE_SENSORTYPE = "sensortype";
		public static final String COLUMN_DEVICE_ID = "deviceid";
		public static final String COLUMN_DEVICE_UPLOADED = "uploaded";
	}

	public static abstract class MeasurementOverview implements BaseColumns {
		public static final String TABLE_MEASUREMENTOVERVIEW = "measurementoverview";
		public static final String COLUMN_MEASUREMENTOVERVIEW_NAME = "name";
		public static final String COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP = "starttimestamp";
		public static final String COLUMN_MEASUREMENTOVERVIEW_STOP_TIMESTAMP = "stoptimestamp";
		public static final String COLUMN_MEASUREMENTOVERVIEW_UPLOADED = "uploaded";
	}

	public static abstract class Questionnaire implements BaseColumns {
		public static final String TABEL_QUESTIONNAIRE = "question";
		public static final String COLUMN_QUESTIONNAIRE_TYPE = "questionnaire_type";
		public static final String COLUMN_QUESTIONNAIRE_RESULT = "questionniare_result";
		public static final String COLUMN_QUESTIONNAIRE_TIMESTAMP = "timestamp";
		public static final String COLUMN_QUESTIONNAIRE_UPLOADED = "uploaded";
	}

	public static abstract class Challenges implements BaseColumns {
		public static final String TABLE_SENTCHALLENGES = "sentChallenges";
		public static final String COLUMN_TITLE = "title";
		public static final String COLUMN_CHALLENGER = "challenger";
		public static final String COLUMN_CHALLENGEE = "challengee";
		public static final String COLUMN_CHALLENGER_NAME = "challengerName";
		public static final String COLUMN_RATED_MESSAGE = "ratedMessage";
		public static final String COLUMN_CHALLENGEE_NAME = "challengeeName";
		public static final String COLUMN_RATED = "rated";
		public static final String COLUMN_XP_REWARD = "xpReward";
		public static final String COLUMN_AMOUNT_HOURS = "amountHours";
		public static final String COLUMN_GPS = "gps";
		public static final String COLUMN_STATUS = "status";
		public static final String COLUMN_REWARD = "reward";
		public static final String COLUMN_EVIDENCE_TYPE = "evidenceType";
		public static final String COLUMN_EVIDENCE_AMOUNT = "evidenceAmount";
		public static final String COLUMN_CONTENT = "content";
		public static final String COLUMN_LOGIN_DATE = "loginDate";
		public static final String COLUMN_START_DATE = "startDate";
		public static final String COLUMN_END_DATE = "endDate";
		public static final String COLUMN_CHALLENGE_ID = "challengeId";
		public static final String COLUMN_SENSE_ID = "senseId";
		public static final String COLUMN_EVIDENCE = "evidence";
		public static final String COLUMN_COMMENTS = "comments";
		public static final String COLUMN_UPDATE_PENDING = "updatePending";
		public static final String COLUMN_UPLOADED = "uploaded";
	}

	public static abstract class ReceivedChallenges implements BaseColumns {
		public static final String TABLE_RECEIVEDCHALLENGES = "receivedChallenges";
	}

	public static abstract class Conversations implements BaseColumns {
		public static final String TABLE_CONVERSATIONS = "conversations";
		public static final String COLUMN_PARTNER_ID = "partnerId";
		public static final String COLUMN_PARTNER_NAME = "partnerName";
		public static final String COLUMN_CATEGORY = "category";
		public static final String COLUMN_RECEIVER = "receiver";
		public static final String COLUMN_AUTHOR = "author";
		public static final String COLUMN_CONTENT = "content";
		public static final String COLUMN_TITLE = "title";
		public static final String COLUMN_DATE = "date";
		public static final String COLUMN_LIKED = "liked";
		public static final String UPLOADED = "uploaded";
	}

	public static abstract class FollowedFriends implements BaseColumns {
		public static final String TABLE_FOLLOWED_FRIENDS = "followedFriends";
		public static final String COLUMN_LIST = "list";
		public static final String COLUMN_UPLOADED = "uploaded";
	}

	public static abstract class Notifications implements BaseColumns {
		public static final String TABLE_NOTIFICATIONS = "notifications";
		public static final String COLUMN_NOTIFIER_ID = "notifierId";
		public static final String COLUMN_NOTIFIER_NAME = "notifierName";
		public static final String COLUMN_MESSAGE = "message";
		public static final String COLUMN_NEW = "new";
		public static final String COLUMN_UPLOADED = "uploaded";
	}

	public static abstract class Run2Gether implements BaseColumns {
		public static final String TABLE_NAME = "run2gether";
		public static final String COLUMN_RUNNERID= "runner_id";
		public static final String COLUMN_AVERAGE_DISTANCE= "average_distance";
		public static final String COLUMN_AVERAGE_SPEED = "average_speed";
		public static final String COLUMN_AVERAGE_DURATION = "average_duration";
		public static final String COLUMN_POINTS = "points";
		public static final String COLUMN_LAST_SEEN = "date_last_seen";
		public static final String COLUMN_UPLOADED = "uploaded";
	}

	private static final String TEXT_TYPE = " TEXT";
	private static final String INTEGER_TYPE = " INTEGER";
	private static final String REAL_TYPE = " REAL";
	private static final String BLOB_TYPE = " BLOB";

	public static final String COMMA_SEP = ",";

	public static final String SQL_CREATE_MEASUREMENT = "CREATE TABLE IF NOT EXISTS "
			+ Measurement.TABLE_MEASUREMENT
			+ " ("
			+ Measurement._ID
			+ " INTEGER PRIMARY KEY, "
			+ Measurement.COLUMN_MEASUREMENT_DEVICEID
			+ INTEGER_TYPE
			+ COMMA_SEP
			+ Measurement.COLUMN_MEASUREMENT_NAME
			+ TEXT_TYPE
			+ COMMA_SEP
			+ Measurement.COLUMN_MEASUREMENT_VALUE
			+ BLOB_TYPE
			+ COMMA_SEP
			+ Measurement.COLUMN_MEASUREMENT_TIMESTAMP
			+ INTEGER_TYPE
			+ COMMA_SEP
			+ Measurement.COLUMN_MEASUREMENT_UPLOADED + INTEGER_TYPE + " )";

	public static final String SQL_DELETE_MEASUREMENT = "DROP TABLE IF EXISTS "
			+ Measurement.TABLE_MEASUREMENT;

	public static final String SQL_CREATE_DEVICE = "CREATE TABLE IF NOT EXISTS "
			+ Device.TABLE_DEVICE
			+ " ("
			+ Device._ID
			+ " INTEGER PRIMARY KEY, "
			+ Device.COLUMN_DEVICE_MANUFACTURER
			+ TEXT_TYPE
			+ COMMA_SEP
			+ Device.COLUMN_DEVICE_SENSORNAME
			+ TEXT_TYPE
			+ COMMA_SEP
			+ Device.COLUMN_DEVICE_SENSORTYPE
			+ TEXT_TYPE
			+ COMMA_SEP
			+ Device.COLUMN_DEVICE_ID
			+ INTEGER_TYPE
			+ COMMA_SEP + Device.COLUMN_DEVICE_UPLOADED + INTEGER_TYPE + " )";

	public static final String SQL_DELETE_DEVICE = "DROP TABLE IF EXISTS "
			+ Device.TABLE_DEVICE;

	public static final String SQL_CREATE_MEASUREMENTOVERVIEW = "CREATE TABLE IF NOT EXISTS "
			+ MeasurementOverview.TABLE_MEASUREMENTOVERVIEW
			+ " ("
			+ MeasurementOverview._ID
			+ " INTEGER PRIMARY KEY, "
			+ MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_NAME
			+ TEXT_TYPE
			+ COMMA_SEP
			+ MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP
			+ INTEGER_TYPE
			+ COMMA_SEP
			+ MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_STOP_TIMESTAMP
			+ INTEGER_TYPE
			+ COMMA_SEP
			+ MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_UPLOADED
			+ INTEGER_TYPE + " )";

	public static final String SQL_DELETE_MEASUREMENTOVERVIEW = "DROP TABLE IF EXISTS "
			+ MeasurementOverview.TABLE_MEASUREMENTOVERVIEW;

	public static final String SQL_CREATE_QUESTIONNAIRE = "CREATE TABLE IF NOT EXISTS "
			+ Questionnaire.TABEL_QUESTIONNAIRE
			+ " ("
			+ Questionnaire._ID
			+ " INTEGER PRIMARY KEY, "
			+ Questionnaire.COLUMN_QUESTIONNAIRE_TYPE
			+ TEXT_TYPE
			+ COMMA_SEP
			+ Questionnaire.COLUMN_QUESTIONNAIRE_RESULT
			+ TEXT_TYPE
			+ COMMA_SEP
			+ Questionnaire.COLUMN_QUESTIONNAIRE_TIMESTAMP
			+ INTEGER_TYPE
			+ COMMA_SEP
			+ Questionnaire.COLUMN_QUESTIONNAIRE_UPLOADED
			+ INTEGER_TYPE + " )";

	public static final String SQL_DELETE_QUESTIONNAIRE = "DROP TABLE IF EXISTS "
			+ Questionnaire.TABEL_QUESTIONNAIRE;

	public static final String SQL_CREATE_SENT_CHALLENGES = "CREATE TABLE IF NOT EXISTS "
			+ Challenges.TABLE_SENTCHALLENGES + " ("
			+ Challenges._ID + " INTEGER PRIMARY KEY, "
			+ Challenges.COLUMN_TITLE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGER + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGEE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGER_NAME + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGEE_NAME + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_RATED_MESSAGE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_RATED + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_XP_REWARD + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_AMOUNT_HOURS + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_GPS + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_STATUS + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_REWARD + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_EVIDENCE_TYPE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_EVIDENCE_AMOUNT + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CONTENT + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_LOGIN_DATE + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_START_DATE + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_END_DATE + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGE_ID + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_SENSE_ID + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_EVIDENCE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_COMMENTS + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_UPDATE_PENDING + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_UPLOADED + INTEGER_TYPE + " )";

	public static final String SQL_DELETE_SENT_CHALLENGES = "DROP TABLE IF EXISTS "
			+ Challenges.TABLE_SENTCHALLENGES;

	public static final String SQL_CREATE_RECEIVED_CHALLENGES = "CREATE TABLE IF NOT EXISTS "
			+ ReceivedChallenges.TABLE_RECEIVEDCHALLENGES + " ("
			+ Challenges._ID + " INTEGER PRIMARY KEY, "
			+ Challenges.COLUMN_TITLE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGER + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGEE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGER_NAME + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGEE_NAME + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_RATED_MESSAGE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_RATED + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_XP_REWARD + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_AMOUNT_HOURS + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_GPS + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_STATUS + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_REWARD + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_EVIDENCE_TYPE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_EVIDENCE_AMOUNT + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CONTENT + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_LOGIN_DATE + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_START_DATE + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_END_DATE + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_CHALLENGE_ID + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_SENSE_ID + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_EVIDENCE + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_COMMENTS + TEXT_TYPE + COMMA_SEP
			+ Challenges.COLUMN_UPDATE_PENDING + INTEGER_TYPE + COMMA_SEP
			+ Challenges.COLUMN_UPLOADED + INTEGER_TYPE + " )";

	public static final String SQL_DELETE_RECEIVED_CHALLENGES = "DROP TABLE IF EXISTS "
			+ ReceivedChallenges.TABLE_RECEIVEDCHALLENGES;

	public static final String SQL_CREATE_CONVERSATIONS = "CREATE TABLE IF NOT EXISTS "
			+ Conversations.TABLE_CONVERSATIONS + " ("
			+ Conversations._ID + " INTEGER PRIMARY KEY, "
			+ Conversations.COLUMN_PARTNER_ID + TEXT_TYPE + COMMA_SEP
			+ Conversations.COLUMN_PARTNER_NAME + TEXT_TYPE + COMMA_SEP
			+ Conversations.COLUMN_CATEGORY + TEXT_TYPE + COMMA_SEP
			+ Conversations.COLUMN_RECEIVER + TEXT_TYPE + COMMA_SEP
			+ Conversations.COLUMN_AUTHOR + TEXT_TYPE + COMMA_SEP
			+ Conversations.COLUMN_CONTENT + TEXT_TYPE + COMMA_SEP
			+ Conversations.COLUMN_TITLE + TEXT_TYPE + COMMA_SEP
			+ Conversations.COLUMN_DATE + INTEGER_TYPE + COMMA_SEP
			+ Conversations.COLUMN_LIKED + TEXT_TYPE + COMMA_SEP
			+ Conversations.UPLOADED + INTEGER_TYPE + " )";

	public static final String SQL_DELETE_CONVERSATIONS = "DROP TABLE IF EXISTS "
			+ Conversations.TABLE_CONVERSATIONS;

	public static final String SQL_CREATE_FOLLOWED_FRIENDS = "CREATE TABLE IF NOT EXISTS "
			+ FollowedFriends.TABLE_FOLLOWED_FRIENDS + " ("
			+ FollowedFriends._ID + " INTEGER PRIMARY KEY, "
			+ FollowedFriends.COLUMN_LIST + TEXT_TYPE + COMMA_SEP
			+ FollowedFriends.COLUMN_UPLOADED + INTEGER_TYPE + " )";

	public static final String SQL_DELETE_FOLLOWED_FRIENDS = "DROP TABLE IF EXISTS "
			+ FollowedFriends.TABLE_FOLLOWED_FRIENDS;

	public static final String SQL_CREATE_NOTIFICATIONS = "CREATE TABLE IF NOT EXISTS "
			+ Notifications.TABLE_NOTIFICATIONS + " ("
			+ Notifications._ID + " INTEGER PRIMARY KEY, "
			+ Notifications.COLUMN_NOTIFIER_ID + TEXT_TYPE + COMMA_SEP
			+ Notifications.COLUMN_NOTIFIER_NAME + TEXT_TYPE + COMMA_SEP
			+ Notifications.COLUMN_MESSAGE + TEXT_TYPE + COMMA_SEP
			+ Notifications.COLUMN_NEW + INTEGER_TYPE + COMMA_SEP
			+ Notifications.COLUMN_UPLOADED + INTEGER_TYPE + " )";

	public static final String SQL_DELETE_NOTIFICATIONS = "DROP TABLE IF EXISTS "
			+ Notifications.TABLE_NOTIFICATIONS;

	public static final String SQL_CREATE_RUN2GETHER = "CREATE TABLE IF NOT EXISTS "
			+ Run2Gether.TABLE_NAME
			+ " ("
			+ Run2Gether.COLUMN_RUNNERID
			+ TEXT_TYPE
			+ COMMA_SEP
			+ Run2Gether.COLUMN_AVERAGE_DISTANCE
			+ REAL_TYPE
			+ COMMA_SEP
			+ Run2Gether.COLUMN_AVERAGE_DURATION
			+ REAL_TYPE
			+ COMMA_SEP
			+ Run2Gether.COLUMN_AVERAGE_SPEED
			+ REAL_TYPE
			+ COMMA_SEP
			+ Run2Gether.COLUMN_POINTS
			+ INTEGER_TYPE
			+ COMMA_SEP
			+ Run2Gether.COLUMN_LAST_SEEN
			+ INTEGER_TYPE
			+ COMMA_SEP
			+ Run2Gether.COLUMN_UPLOADED
			+ INTEGER_TYPE + " )";

	public static final String SQL_DELETE_RUN2GETHER = "DROP TABLE IF EXISTS "
			+ Run2Gether.TABLE_NAME;

}
