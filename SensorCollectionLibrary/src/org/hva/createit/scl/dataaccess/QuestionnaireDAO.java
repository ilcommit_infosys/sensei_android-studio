/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.hva.createit.scl.data.QuestionnaireData;

import java.util.ArrayList;
import java.util.List;

public class QuestionnaireDAO extends AbstractDataAccessObject<QuestionnaireData> {
	private final static String sensortype = "questionniare";
	private String myTable = Database.Questionnaire.TABEL_QUESTIONNAIRE;
	private String TAG = "QuestionnaireDAO";
	private SQLiteStatement QUESTIONNAIRE_INSERT_STATEMENT;
	private SQLiteStatement QUESTIONNAIRE_UPDATE_STATEMENT;

	public QuestionnaireDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();
		this.prepareQuestionnaireInsertStatement();
		this.prepareDefaultUpdateStatement();
	}

	private SQLiteStatement prepareQuestionnaireInsertStatement() {
		this.QUESTIONNAIRE_INSERT_STATEMENT = this.database
				.compileStatement("INSERT INTO "
						+ Database.Questionnaire.TABEL_QUESTIONNAIRE
						+ " ("
						+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_TYPE
						+ Database.COMMA_SEP
						+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_RESULT
						+ Database.COMMA_SEP
						+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_TIMESTAMP
						+ Database.COMMA_SEP
						+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_UPLOADED
						+ " )" + " VALUES ( ?, ?, ?, ?) ;");
		return this.QUESTIONNAIRE_INSERT_STATEMENT;
	}

	@Override
	public SQLiteStatement prepareDefaultUpdateStatement() {
		this.QUESTIONNAIRE_UPDATE_STATEMENT = this.database
				.compileStatement("UPDATE "
						+ Database.Questionnaire.TABEL_QUESTIONNAIRE + " SET "
						+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_TYPE
						+ " = ? " + Database.COMMA_SEP
						+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_RESULT
						+ " = ? " + Database.COMMA_SEP
						+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_TIMESTAMP
						+ " = ? " + Database.COMMA_SEP
						+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_UPLOADED
						+ " = ? " + "WHERE " + Database.Questionnaire._ID
						+ " = ? ;");
		return this.QUESTIONNAIRE_UPDATE_STATEMENT;
	}

	@Override
	public void store(QuestionnaireData acm) {
		QUESTIONNAIRE_INSERT_STATEMENT.bindString(1, acm.getQuestionnaire_type());
		QUESTIONNAIRE_INSERT_STATEMENT.bindString(2, acm.getResult());
		QUESTIONNAIRE_INSERT_STATEMENT.bindLong(3,  acm.getTimestamp());
		QUESTIONNAIRE_INSERT_STATEMENT.bindLong(4,  acm.isUploaded()? 1:0);
		QUESTIONNAIRE_INSERT_STATEMENT.executeInsert();
	}

	@Override
	public void update(QuestionnaireData acm) {
		QUESTIONNAIRE_UPDATE_STATEMENT.bindString(1, acm.getQuestionnaire_type());
		QUESTIONNAIRE_UPDATE_STATEMENT.bindString(2, acm.getResult());
		QUESTIONNAIRE_UPDATE_STATEMENT.bindLong(3,  acm.getTimestamp());
		QUESTIONNAIRE_UPDATE_STATEMENT.bindLong(4, acm.isUploaded() ? 1 : 0);
		QUESTIONNAIRE_UPDATE_STATEMENT.bindLong(5, acm.get_id());
		QUESTIONNAIRE_UPDATE_STATEMENT.execute();
    }

	@Override
	public List<QuestionnaireData> getItems(long fromIndex, long toIndex) {
		return null;
	}

	@Override
	public List<QuestionnaireData> getUnuploadedItems() {
		// MeasurementData measurement = null;
		boolean uploaded = false;
		String[] values = { (uploaded ? "1" : "0") };
		Cursor cursor = database.rawQuery("SELECT * FROM "
				+ myTable
				+ " WHERE "
				+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_UPLOADED
				+ " = ? "
				// + " AND " + Database.Measurement.COLUMN_MEASUREMENT_UPLOADED
				// + " = ? "
				+ " ORDER BY "
				+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_TIMESTAMP + ", "
				+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_TYPE + ";", values);
		List<QuestionnaireData> measurement = new ArrayList<QuestionnaireData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToQuestionnaireData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return measurement;
	}

	private QuestionnaireData cursorToQuestionnaireData(Cursor cursor){
		QuestionnaireData measurement = new QuestionnaireData(cursor.getInt(0),
				cursor.getString(1),cursor.getString(2), cursor.getLong(3), Boolean.parseBoolean(cursor.getString(4)));
		return measurement;
	}

	@Override
	public long getUnuploadedItemCount() {
		long itemCount = 0;
		itemCount = 0;
		Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM "
				+ Database.Questionnaire.TABEL_QUESTIONNAIRE + " WHERE "
				+ Database.Questionnaire.COLUMN_QUESTIONNAIRE_UPLOADED
				+ " = 0 LIMIT 1;", null);
		if (resultSet != null && resultSet.getCount() > 0) {
			resultSet.moveToFirst();
			String str = resultSet.getString(0);
			itemCount = Long.parseLong(str);
		}
		return itemCount;
	}

}
