/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.hva.createit.scl.data.RunOverviewData;
import org.vu.run2gether.RunnerData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Run2GetherDAO extends AbstractDataAccessObject<RunnerData> {
	private final static String sensortype = "run2gether";
	private String myTable = Database.Run2Gether.TABLE_NAME;
	private String TAG = "Run2GetherDAO";
	private SQLiteStatement INSERT_STATEMENT;
	private SQLiteStatement UPDATE_STATEMENT;
	private Context context;

	public Run2GetherDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		this.context = context;
		open();
		this.prepareInsertStatement();
		this.prepareDefaultUpdateStatement();
	}

	private SQLiteStatement prepareInsertStatement() {
		this.INSERT_STATEMENT = this.database
				.compileStatement("INSERT INTO "
						+ Database.Run2Gether.TABLE_NAME
						+ " ("
						+ Database.Run2Gether.COLUMN_RUNNERID
						+ Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_AVERAGE_DISTANCE
						+ Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_AVERAGE_SPEED
						+ Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_AVERAGE_DURATION
						+ Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_POINTS
						+ Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_LAST_SEEN
						+ Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_UPLOADED
						+ " )" + " VALUES ( ?, ?, ?, ?, ?, ?, ?) ;");
		return this.INSERT_STATEMENT;
	}

	@Override
	public SQLiteStatement prepareDefaultUpdateStatement() {
		this.UPDATE_STATEMENT = this.database
				.compileStatement("UPDATE "
						+ Database.Run2Gether.TABLE_NAME + " SET "
						+ Database.Run2Gether.COLUMN_AVERAGE_DISTANCE
						+ " = ? " + Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_AVERAGE_SPEED
						+ " = ? " + Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_AVERAGE_DURATION
						+ " = ? " + Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_POINTS
						+ " = ? " + Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_LAST_SEEN
						+ " = ? " + Database.COMMA_SEP
						+ Database.Run2Gether.COLUMN_UPLOADED
						+ " = ? " + "WHERE " + Database.Run2Gether.COLUMN_RUNNERID
						+ " = ? ;");
		return this.UPDATE_STATEMENT;
	}

	@Override
	public void store(RunnerData runnerData) {
		INSERT_STATEMENT.bindString(1, runnerData.getRunnerId());
		INSERT_STATEMENT.bindDouble(2, runnerData.getAverageDistance());
		INSERT_STATEMENT.bindDouble(3, runnerData.getAverageSpeed());
		INSERT_STATEMENT.bindDouble(4, runnerData.getAverageDuration());
		INSERT_STATEMENT.bindLong(5, runnerData.getPoints());
		INSERT_STATEMENT.bindLong(6, runnerData.getLastSeen().getTime());
		INSERT_STATEMENT.bindLong(7, runnerData.isUploaded() ? 1 : 0);
		INSERT_STATEMENT.executeInsert();
	}

	@Override
	public void update(RunnerData runnerData) {
		UPDATE_STATEMENT.bindDouble(1, runnerData.getAverageDistance());
		UPDATE_STATEMENT.bindDouble(2, runnerData.getAverageSpeed());
		UPDATE_STATEMENT.bindDouble(3, runnerData.getAverageDuration());
		UPDATE_STATEMENT.bindLong(4, runnerData.getPoints());
		UPDATE_STATEMENT.bindLong(5, runnerData.getLastSeen().getTime());
		UPDATE_STATEMENT.bindLong(6, runnerData.isUploaded() ? 1 : 0);
		UPDATE_STATEMENT.bindString(7, runnerData.getRunnerId());
		UPDATE_STATEMENT.execute();	}


	@Override
	public List<RunnerData> getUnuploadedItems() {
		// MeasurementData measurement = null;
		boolean uploaded = false;
		String[] values = { (uploaded ? "1" : "0") };
		Cursor cursor = database.rawQuery("SELECT * FROM "
				+ myTable
				+ " WHERE "
				+ Database.Run2Gether.COLUMN_UPLOADED
				+ " = ? "
				+ " ORDER BY "
				+ Database.Run2Gether.COLUMN_RUNNERID + ";", values);
		List<RunnerData> measurement = new ArrayList<RunnerData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToRunnerData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return measurement;
	}

	@Override
	public List<RunnerData> getItems(long fromIndex, long toIndex) {
		return null;
	}

	public List<RunnerData> getAllRunnerData(){
		String[] values = { };
		Cursor cursor = database.rawQuery("SELECT * FROM "
				+ myTable
				+ " ORDER BY "
				+ Database.Run2Gether.COLUMN_RUNNERID + ";", values);
		List<RunnerData> measurement = new ArrayList<RunnerData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToRunnerData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return measurement;
	}

	public RunnerData getRunnerData(String runnerId){
		String[] values = { runnerId, "1" };
		Cursor cursor = database.rawQuery("SELECT * FROM "
				+ myTable
				+ " WHERE "
				+ Database.Run2Gether.COLUMN_RUNNERID
				+ " = ? "
				+ " LIMIT ? ;", values);
		RunnerData measurement = null;
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement = cursorToRunnerData(cursor);
			cursor.moveToNext();
		}
		cursor.close();

		return measurement;
	}

	private RunnerData cursorToRunnerData(Cursor cursor){
		RunnerData measurement = new RunnerData(cursor.getString(cursor.getColumnIndex(Database.Run2Gether.COLUMN_RUNNERID)),
				cursor.getDouble(cursor.getColumnIndex(Database.Run2Gether.COLUMN_AVERAGE_DISTANCE)),
				cursor.getDouble(cursor.getColumnIndex(Database.Run2Gether.COLUMN_AVERAGE_SPEED)),
				cursor.getDouble(cursor.getColumnIndex(Database.Run2Gether.COLUMN_AVERAGE_DURATION)),
				(int) cursor.getLong(cursor.getColumnIndex(Database.Run2Gether.COLUMN_POINTS)),
				new Date(cursor.getLong(cursor.getColumnIndex(Database.Run2Gether.COLUMN_LAST_SEEN))),
				cursor.getLong(cursor.getColumnIndex(Database.Run2Gether.COLUMN_UPLOADED))==1
				);
		return measurement;
	}

	public RunnerData getCurrentRunnerDataStatistics(){
		RunStatisticsDAO runStatisticsDAO = new RunStatisticsDAO(context);
		RunOverviewData runOverviewData = runStatisticsDAO.getAverageRunOverView();
		return new RunnerData("0", runOverviewData.getDistance(), runOverviewData.getAverageSpeed(), runOverviewData.getDuration(), 0, new Date(), false);
	}

	@Override
	public long getUnuploadedItemCount() {
		long itemCount = 0;
		itemCount = 0;
		Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM "
				+ Database.Run2Gether.TABLE_NAME + " WHERE "
				+ Database.Run2Gether.COLUMN_UPLOADED
				+ " = 0 LIMIT 1;", null);
		if (resultSet != null && resultSet.getCount() > 0) {
			resultSet.moveToFirst();
			String str = resultSet.getString(0);
			itemCount = Long.parseLong(str);
		}
		return itemCount;
	}

}
