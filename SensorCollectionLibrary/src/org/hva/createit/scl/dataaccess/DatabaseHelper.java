/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	// If you change the database schema, you must increment the database
	// version.
	public static final int DATABASE_VERSION = 6;
	public static final String DATABASE_NAME = "SensorCollectionLibrary.db";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// create simple table
		db.execSQL(Database.SQL_CREATE_DEVICE);
		db.execSQL(Database.SQL_CREATE_MEASUREMENT);
		db.execSQL(Database.SQL_CREATE_MEASUREMENTOVERVIEW);
		db.execSQL("CREATE INDEX measurementtimestamp ON "
				+ Database.Measurement.TABLE_MEASUREMENT + "("
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ");");
		db.execSQL("CREATE INDEX overviewId ON "
				+ Database.MeasurementOverview.TABLE_MEASUREMENTOVERVIEW + "("
				+ Database.MeasurementOverview._ID + ");");
		db.execSQL(Database.SQL_CREATE_QUESTIONNAIRE);
		db.execSQL(Database.SQL_CREATE_CONVERSATIONS);
		db.execSQL(Database.SQL_CREATE_FOLLOWED_FRIENDS);
		db.execSQL(Database.SQL_CREATE_RECEIVED_CHALLENGES);
		db.execSQL(Database.SQL_CREATE_SENT_CHALLENGES);
		db.execSQL(Database.SQL_CREATE_NOTIFICATIONS);
		db.execSQL(Database.SQL_CREATE_RUN2GETHER);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion == 3) {
			//added since 4
			db.execSQL(Database.SQL_CREATE_QUESTIONNAIRE);
		} if (oldVersion == 4) {
			//added since 5
			db.execSQL(Database.SQL_CREATE_CONVERSATIONS);
			db.execSQL(Database.SQL_CREATE_FOLLOWED_FRIENDS);
			db.execSQL(Database.SQL_CREATE_RECEIVED_CHALLENGES);
			db.execSQL(Database.SQL_CREATE_SENT_CHALLENGES);
			db.execSQL(Database.SQL_CREATE_NOTIFICATIONS);
		} if(oldVersion == 5) {
			//added since 6
			db.execSQL(Database.SQL_CREATE_RUN2GETHER);
		}else{
			doSaveDelete(db);
			onCreate(db);
		}
	}

	public void doSaveDelete(SQLiteDatabase db) {
		// This database is only a cache for online data, so its upgrade policy
		// is
		// to simply to check if data is uploaded, if so discard the data and
		// start over

		// check if all data is uploaded
		// todo

		// backup data and upload

		// delete all data
		db.execSQL(Database.SQL_DELETE_DEVICE);
		db.execSQL(Database.SQL_DELETE_MEASUREMENT);
		db.execSQL(Database.SQL_DELETE_MEASUREMENTOVERVIEW);
		db.execSQL(Database.SQL_DELETE_QUESTIONNAIRE);
		db.execSQL(Database.SQL_DELETE_CONVERSATIONS);
		db.execSQL(Database.SQL_DELETE_FOLLOWED_FRIENDS);
		db.execSQL(Database.SQL_DELETE_RECEIVED_CHALLENGES);
		db.execSQL(Database.SQL_DELETE_SENT_CHALLENGES);
		db.execSQL(Database.SQL_DELETE_NOTIFICATIONS);
		db.execSQL(Database.SQL_DELETE_RUN2GETHER);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		onUpgrade(db, oldVersion, newVersion);
	}

	private static DatabaseHelper instance;

	public static synchronized DatabaseHelper getHelper(Context context) {
		if (instance == null)
			instance = new DatabaseHelper(context);

		return instance;
	}

}
