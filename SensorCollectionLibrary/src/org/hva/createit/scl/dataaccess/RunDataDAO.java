/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import org.hva.createit.scl.data.MeasurementData;
import org.hva.createit.scl.data.RunData;

import java.util.ArrayList;
import java.util.List;

public class RunDataDAO extends AbstractDataAccessObject<RunData> {
	private final static String sensortype = RunData.name;
	private SQLiteStatement insertStatement;
	private String myTable = Database.Measurement.TABLE_MEASUREMENT;
	private String TAG = "RunDataDAO";

	public RunDataDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();
		this.prepareDefaultInsertStatement();
		this.prepareDefaultUpdateStatement();
	}

	@Override
	public long getItemCount() {
		long itemCount = 0;
		// itemCount = 0;
		//
		// Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM "
		// + myTable + " WHERE "+ Database.Measurement.COLUMN_MEASUREMENT_DEV +
		// " = '" + sensortype + "' LIMIT 1 ", null);
		// if (resultSet != null && resultSet.getCount() > 0) {
		// resultSet.moveToFirst();
		// String str = resultSet.getString(0);
		// itemCount = Long.parseLong(str);
		// }
		return itemCount;
	}

	@Override
	public void store(RunData acm) {
		super.store((MeasurementData) acm);
	}

	@Override
	public void update(RunData acm) {
		super.update((MeasurementData) acm);
	}

	public List<RunData> getRunData(int count) {
		List<MeasurementData> measurements = getMeasurements("rundata", count);
		List<RunData> runDataSet = new ArrayList<RunData>();
		for (MeasurementData measurement : measurements) {
			runDataSet.add(new RunData(measurement));
		}
		return runDataSet;
	}

	@Override
	public List<RunData> getItems(long fromIndex, long toIndex) {
		return null;
	}

	public RunData getRunDataAfter(long timestamp) {
		// MeasurementData measurement = null;
		String[] values = { sensortype, "" + timestamp, "1" };
		Cursor cursor = database.rawQuery("SELECT * FROM " + myTable
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = ? " + " AND "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " > ? "
				+ " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " DESC "
				+ "LIMIT ?", values);
		List<MeasurementData> measurement = new ArrayList<MeasurementData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToMMeasurementData(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return new RunData(measurement.get(0));
	}

	public RunData getRunDataEndAfter(long timestamp) {
		// MeasurementData measurement = null;
		String[] values = { sensortype, "" + timestamp, "1" };
		Cursor cursor = database.rawQuery("SELECT * FROM " + myTable
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = ? " + " AND "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " >= ? "
				+ " AND " +  Database.Measurement.COLUMN_MEASUREMENT_VALUE + " = x'00000002' "
				+ " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " ASC "
				+ "LIMIT ?", values);
		List<MeasurementData> measurement = new ArrayList<MeasurementData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToMMeasurementData(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		if (measurement.size() > 0) {
			return new RunData(measurement.get(0));
		} else {
			return null;
		}
	}

	public RunData getRunDataEndBefore(long timestamp) {
		// MeasurementData measurement = null;
		String[] values = { sensortype, "" + timestamp, "1" };
		Cursor cursor = database.rawQuery("SELECT * FROM " + myTable
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = ? " + " AND "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " <= ? "
				+ " AND " +  Database.Measurement.COLUMN_MEASUREMENT_VALUE + " = x'00000002' "
				+ " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " DESC "
				+ "LIMIT ?", values);
		Log.d(TAG, "getRunDataEndBefore query: "+ cursor.toString());

				List < MeasurementData > measurement = new ArrayList<MeasurementData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToMMeasurementData(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		if (measurement.size() > 0) {
			return new RunData(measurement.get(0));
		} else {
			return null;
		}
	}

	public RunData getRunDataStartBefore(long timestamp) {
		String[] values = { sensortype, "" + timestamp, "1" };
		Cursor cursor = database.rawQuery("SELECT * FROM " + myTable
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = ? " + " AND "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " < ? "
				+ " AND " +  Database.Measurement.COLUMN_MEASUREMENT_VALUE + " = x'00000001' "
				+ " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + " DESC "
				+ "LIMIT ?", values);
		List<MeasurementData> measurement = new ArrayList<MeasurementData>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			measurement.add(cursorToMMeasurementData(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		if (measurement.size() > 0) {
			return new RunData(measurement.get(0));
		} else {
			return null;
		}
	}

	public RunData getLastRunData() {
		return getRunData(1).get(0);
	}
}
