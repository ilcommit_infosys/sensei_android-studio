/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import android.content.Context;
import android.database.sqlite.SQLiteStatement;

import org.hva.createit.scl.data.ExertionData;
import org.hva.createit.scl.data.ExertionLevelData;
import org.hva.createit.scl.data.MeasurementData;

import java.util.ArrayList;
import java.util.List;

public class ExertionDAO extends AbstractDataAccessObject<ExertionData> {
	private final static String sensortype = ExertionLevelData.name;
	private SQLiteStatement insertStatement;
	private String myTable = Database.Measurement.TABLE_MEASUREMENT;
	private String TAG = ExertionLevelData.class.getSimpleName();

	public ExertionDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();
		this.prepareDefaultInsertStatement();
		this.prepareDefaultUpdateStatement();
	}

	@Override
	public void store(ExertionData dist) {
		// store manufacturer if not exist
		super.store((MeasurementData) dist.getExertionLevelData());
		super.store((MeasurementData) dist.getExertionFilePathData());
//		Log.d(TAG, "storing exertion " + dist.getExertionLevel());
	}

	@Override
	public List<ExertionData> getItems(long fromIndex, long toIndex) {
		// TODO Auto-generated method stub

		// select x,y,z values from db with join on deviceid, timestamp
		// run loop to create objects from results
		// return results

		return null;
	}

	@Override
	public long getUnuploadedItemCount() {
		long itemCount = 0;
		itemCount += getUnuploadedItemCount(ExertionLevelData.name);

		return itemCount;
	}

	@Override
	public List<ExertionData> getUnuploadedItems() {

		List<MeasurementData> measurements = getMeasurements(
				ExertionLevelData.name, false);
		List<ExertionData> stepfrequencies = new ArrayList<ExertionData>();
		for (MeasurementData measurement : measurements) {
			stepfrequencies.add(new ExertionData(new ExertionLevelData(measurement), getMeasurement("exertionfilelocation", measurement.getTimestamp())));
		}

		return stepfrequencies;
	}

	@Override
	public long getItemCount() {
		long itemCount = 0;
		// itemCount = 0;
		//
		// Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM "
		// + myTable + " WHERE "+ Database.Measurement.COLUMN_MEASUREMENT_DEV +
		// " = '" + sensortype + "' LIMIT 1 ", null);
		// if (resultSet != null && resultSet.getCount() > 0) {
		// resultSet.moveToFirst();
		// String str = resultSet.getString(0);
		// itemCount = Long.parseLong(str);
		// }
		return itemCount;
	}

	@Override
	public void update(ExertionData acm) {
		super.update((MeasurementData) acm.getExertionLevelData());
		super.update((MeasurementData) acm.getExertionFilePathData());
	}
}
