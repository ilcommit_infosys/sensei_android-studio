/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl.dataaccess;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.hva.createit.scl.data.AffectData;
import org.hva.createit.scl.data.MeasurementData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AffectDAO extends AbstractDataAccessObject<AffectData> {
	private final static String sensortype = AffectData.sensorType;
	private SQLiteStatement insertStatement;
	private String myTable = Database.Measurement.TABLE_MEASUREMENT;
	private String TAG = "AffectDAO";

	public AffectDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();
		this.prepareDefaultInsertStatement();
		this.prepareDefaultUpdateStatement();
	}

	@Override
	public void store(AffectData acm) {
		// store manufacturer if not exist
		store(acm.getMeasurementPleasure());
		store(acm.getMeasurementDominance());
		store(acm.getMeasurementArousal());
	}

	@Override
	public void update(AffectData acm) {
		update(acm.getMeasurementPleasure());
		update(acm.getMeasurementDominance());
		update(acm.getMeasurementArousal());
	}

	public AffectData getAffect(long timestamp) {
		AffectData af = new AffectData();
		af.setMeasurementPleasure(super.getMeasurement("pleasure", timestamp));
		af.setMeasurementDominance(super.getMeasurement("dominance", timestamp));
		af.setMeasurementArousal(super.getMeasurement("arousal", timestamp));
		return af;
	}

	@Override
	public long getUnuploadedItemCount() {
		long itemCount = 0;
		itemCount += getUnuploadedItemCount("pleasure");
		itemCount += getUnuploadedItemCount("dominance");
		itemCount += getUnuploadedItemCount("arousal");

		return itemCount;
	}

	public List<AffectData> getAffectData(long startDateTime, long endDateTime) {
		List<MeasurementData> pleasure = super.getMeasurements("pleasure",
				startDateTime, endDateTime, true);
		List<MeasurementData> dominance = super.getMeasurements("dominance",
				startDateTime, endDateTime, true);
		List<MeasurementData> arousal = super.getMeasurements("arousal",
				startDateTime, endDateTime, true);

		HashMap<Long, AffectData> hashMapAccelerometerData = new HashMap<Long, AffectData>();
		for (MeasurementData measurement : pleasure) {
			AffectData ad = new AffectData(measurement.getDeviceId(),
					measurement.getTimestamp());
			ad.setMeasurementPleasure(measurement);

			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
		}

		for (MeasurementData measurement : arousal) {
			AffectData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new AffectData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setMeasurementPleasure(getMeasurement("pleasure",
						measurement.getTimestamp()));
			}
			ad.setMeasurementArousal(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
		}

		for (MeasurementData measurement : dominance) {
			AffectData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new AffectData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setMeasurementPleasure(getMeasurement("pleasure",
						measurement.getTimestamp()));
				ad.setMeasurementDominance(getMeasurement("arousal",
						measurement.getTimestamp()));
			}
			ad.setMeasurementDominance(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
		}

		List<AffectData> affect = new ArrayList<AffectData>(
				hashMapAccelerometerData.values());

		return affect;
	}

	@Override
	public List<AffectData> getUnuploadedItems() {
		HashMap<Long, AffectData> hashMapAccelerometerData = new HashMap<Long, AffectData>();

		List<MeasurementData> measurements = getMeasurements("pleasure", false);
		for (MeasurementData measurement : measurements) {
			AffectData ad = new AffectData(measurement.getDeviceId(),
					measurement.getTimestamp());
			ad.setMeasurementPleasure(measurement);

			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
		}

		measurements = getMeasurements("arousal", false);
		for (MeasurementData measurement : measurements) {
			AffectData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new AffectData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setMeasurementPleasure(getMeasurement("pleasure",
						measurement.getTimestamp()));
			}
			ad.setMeasurementDominance(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
		}

		measurements = getMeasurements("dominance", false);
		for (MeasurementData measurement : measurements) {
			AffectData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			if (ad == null) {
				ad = new AffectData(measurement.getDeviceId(),
						measurement.getTimestamp());
				ad.setMeasurementPleasure(getMeasurement("pleasure",
						measurement.getTimestamp()));
				ad.setMeasurementDominance(getMeasurement("arousal",
						measurement.getTimestamp()));
			}
			ad.setMeasurementArousal(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
		}

		List<AffectData> accels = new ArrayList<AffectData>(
				hashMapAccelerometerData.values());

		return accels;
	}

	public List<AffectData> getUploadedItems() {
		// select x,y,z values from db with join on deviceid, timestamp
		// run loop to create objects from results
		// return results
		HashMap<Long, AffectData> hashMapAccelerometerData = new HashMap<Long, AffectData>();

		Cursor cursor = database.rawQuery("SELECT * FROM " + myTable
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = 'pleasure'" + " AND "
				+ Database.Measurement.COLUMN_MEASUREMENT_UPLOADED + " = 1"
				+ " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			MeasurementData measurement = cursorToMMeasurementData(cursor);
			AffectData ad = new AffectData(measurement.getDeviceId(),
					measurement.getTimestamp());
			ad.setMeasurementPleasure(measurement);

			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();

		cursor = database.rawQuery("SELECT * FROM " + myTable + " WHERE "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = 'dominance'" + " AND "
				+ Database.Measurement.COLUMN_MEASUREMENT_UPLOADED + " = 1"
				+ " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			MeasurementData measurement = cursorToMMeasurementData(cursor);

			AffectData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			ad.setMeasurementDominance(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();

		cursor = database.rawQuery("SELECT * FROM " + myTable + " WHERE "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME + " = 'arousal'"
				+ " AND " + Database.Measurement.COLUMN_MEASUREMENT_UPLOADED
				+ " = 1" + " ORDER BY "
				+ Database.Measurement.COLUMN_MEASUREMENT_TIMESTAMP + ", "
				+ Database.Measurement.COLUMN_MEASUREMENT_NAME, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			MeasurementData measurement = cursorToMMeasurementData(cursor);

			AffectData ad = hashMapAccelerometerData.get(measurement
					.getTimestamp());
			ad.setMeasurementArousal(measurement);
			hashMapAccelerometerData.put(measurement.getTimestamp(), ad);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();

		List<AffectData> accels = new ArrayList<AffectData>(
				hashMapAccelerometerData.values());

		return accels;
	}

	@Override
	public long getItemCount() {
		long itemCount = 0;

		Cursor resultSet = database.rawQuery("SELECT COUNT(*) FROM " + myTable
				+ " WHERE " + Database.Measurement.COLUMN_MEASUREMENT_NAME
				+ " = 'pleasure' LIMIT 1 ", null);
		if (resultSet != null && resultSet.getCount() > 0) {
			resultSet.moveToFirst();
			String str = resultSet.getString(0);
			itemCount = Long.parseLong(str);
		}
		return itemCount;
	}

	@Override
	public List<AffectData> getItems(long fromIndex, long toIndex) {
		return null;
	}

	/*
	 * Returns the last two AffectData objects
	 */
	public List<AffectData> getLastAffectData() {
		List<AffectData> affectDataList = new ArrayList<AffectData>();

		List<MeasurementData> measurements = getMeasurements("pleasure", 2);
		for (MeasurementData measurement : measurements) {
			AffectData ad = new AffectData(measurement.getDeviceId(),
					measurement.getTimestamp());
			ad.setMeasurementPleasure(measurement);
			ad.setMeasurementDominance(getMeasurement("dominance",
					measurement.getTimestamp()));
			ad.setMeasurementArousal(getMeasurement("arousal",
					measurement.getTimestamp()));
			affectDataList.add(ad);
		}

		return affectDataList;
	}

}
