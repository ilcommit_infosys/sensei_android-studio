/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.scl;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class NotificationHolder {

	private static int notificationIdOne = 101010;

	public static Notification showPageNotifications(Context c, String title,
			String content) {
		// Invoking the default notification service

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(c);
		mBuilder.setContentTitle(title);
		mBuilder.setContentText(content);
		mBuilder.setTicker("Starting sensors");
		mBuilder.setSmallIcon(R.drawable.logo_vertical_white);
		mBuilder.setNumber(0);
		mBuilder.setOngoing(true);
		Intent resultIntent = new Intent(c, MainActivity.class);
		resultIntent.putExtra("notificationId", notificationIdOne);
		PendingIntent pendingIntent = PendingIntent.getActivity(c, 0,
				resultIntent, 0);

		mBuilder.setContentIntent(pendingIntent);
		//

		NotificationManager myNotificationManager = (NotificationManager) c
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification noti = mBuilder.build();
		myNotificationManager.notify(notificationIdOne, noti);
		return noti;

	}
}
