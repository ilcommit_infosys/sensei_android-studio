package com.infy.weather;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

/**
 * Created by Sowmea on 2/13/2016.
 */
public class Location  {
    Geocoder geocoder;
    List<Address> addresses;
    Context mContext;
    String city, country, state, loc;

    public Location(Context c) {
        this.mContext = c;
    }

    public String getContextValues() {
        GPS gps = new GPS(mContext);
        String loc = gps.getContextValues();
        JSONObject jObj = null;
        try {
            jObj = new JSONObject(loc);
            loc = getLocation(jObj.getDouble("lat"), jObj.getDouble("long"));
        } catch (JSONException e) {
            Log.e("Location",e.getMessage());
        }
        return loc;
    }

    public String getLocation(double lat, double lng) {
        try {
            geocoder = new Geocoder(mContext, Locale.getDefault());
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            loc = city + "," + country;
        } catch (Exception e) {
            Log.e("Location",e.getMessage());
        }
        return loc;
    }


}
