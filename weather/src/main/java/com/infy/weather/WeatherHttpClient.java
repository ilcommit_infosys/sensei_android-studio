package com.infy.weather;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherHttpClient {

    private static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static String IMG_URL = "http://openweathermap.org/img/w/";

    private static String BASE_FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?mode=json&q=";

    public String getWeatherData(String location, String lang) {
        HttpURLConnection con = null;
        InputStream is = null;

        try {
            String url = BASE_URL + location;
            if (lang != null)
                url = url + "&lang=" + lang;
            url = url + "&appid=fc719d61100defbc6693990c67c66571";
            con = (HttpURLConnection) (new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ((line = br.readLine()) != null)
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();

            return buffer.toString();
        } catch (Throwable t) {
            Log.e("WeatherHTTPClient",t.getMessage());
        } finally {
            try {
                is.close();
            } catch (Throwable t) {
                Log.e("WeatherHTTPClient",t.getMessage());
            }
            try {
                con.disconnect();
            } catch (Throwable t) {
                Log.e("WeatherHTTPClient",t.getMessage());
            }
        }

        return null;

    }

    public Bitmap getImage(String code) {
        HttpURLConnection con = null;
        InputStream is = null;
        try {
            con = (HttpURLConnection) (new URL(IMG_URL + code)).openConnection();
            con.setRequestMethod("GET");
            //con.setDoInput(true);
            //con.setDoOutput(true);
            con.connect();

            /*// Let's read the response
            int status = con.getResponseCode();

            if (status >= 400) {
                is = con.getErrorStream();
                Log.i("con", "error");
            } else {
                is = con.getInputStream();
                Log.i("con", "connected");
            }*/
            is = con.getInputStream();
            /*byte[] buffer = new byte[2048];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            while (is.read(buffer) != -1)
                baos.write(buffer);
           Bitmap bm = BitmapFactory.decodeStream(is);*/

            Bitmap b = BitmapFactory.decodeStream(new FlushedInputStream(is));
            return b;

            //return baos.toByteArray();
        } catch (Throwable t) {Log.e("WeatherHTTPClient",t.getMessage());
        } finally {
            try {
                is.close();
            } catch (Throwable t) {
                Log.e("WeatherHTTPClient",t.getMessage());}
            try {
                con.disconnect();
            } catch (Throwable t) {Log.e("WeatherHTTPClient",t.getMessage());
            }
        }

        return null;

    }



    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }
}
