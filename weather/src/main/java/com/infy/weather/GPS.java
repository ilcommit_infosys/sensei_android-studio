package com.infy.weather;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

/**
 * Created by msowmea.TRN on 2/9/2016.
 */
public class GPS {
    Context mContext;
    String address, city, state, country, postalCode;
    JSONObject jObj;
    public GPS(Context c) {
        this.mContext = c;
    }

    public String getContextValues() {
        GPSService gps = new GPSService(mContext);
        if(gps.canGetLocation()){
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            latlngToAddress(latitude, longitude);
            Log.i("latitude ", latitude + "," + longitude + "");
            String location = address+";"+city+","+state+","+country+","+postalCode;

            try {
                jObj = new JSONObject();
                jObj.put("id","gps");
                jObj.put("lat", latitude);
                jObj.put("long",longitude);
                jObj.put("location",location);
            }catch(Exception e) {
                Log.e("GPS",e.getMessage());
            }
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
        return jObj.toString();
    }
    public void latlngToAddress(double lat, double lng) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(mContext, Locale.getDefault());

            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            Log.i("address ",address+";"+city+","+state+","+country+","+postalCode);

        } catch (Exception ex) {
            Log.e("GPS",ex.getMessage());
        }
    }
}
