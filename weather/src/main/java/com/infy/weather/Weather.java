package com.infy.weather;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Weather {
    private long temp, rain_percentage,min_temp,max_temp;
    private String sunny;
    private double wind_speed;
    private Bitmap image;
    private Bitmap bMap;

    public long getTemp() {
        return temp;
    }

    public long getRain_percentage() {
        return rain_percentage;
    }

    public long getMin_temp() {
        return min_temp;
    }

    public long getMax_temp() {
        return max_temp;
    }

    public String getSunny() {
        return sunny;
    }

    public double getWind_speed() {
        return wind_speed;
    }

    public Bitmap getImage() {
        return image;
    }

    public Bitmap getbMap() {
        return bMap;
    }





Activity activity;
   public Weather(Activity activity){
this.activity=activity;
      //  getCurrentWeather();
//



        // Inflate the layout for this fragment

    }








}
