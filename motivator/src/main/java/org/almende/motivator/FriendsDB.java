package org.almende.motivator;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;

import org.almende.motivator.dataaccess.FollowedFriendsDAO;
import org.almende.motivator.dataaccess.FollowedFriendsData;
import org.almende.motivator.models.User;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 4-11-15
 *
 * @author Dominik Egger
 */
public class FriendsDB {

	private static FriendsDB instance;

	private final Context context;

	private final FollowedFriendsDAO _followedFriendsDAO;

	private FollowedFriendsData _followedFriendsData;

	private FriendsDB(Context context) {
		this.context = context;

		_followedFriendsDAO = new FollowedFriendsDAO(context);
		_followedFriendsData = _followedFriendsDAO.getFollowedFriends();
	}

	public static FriendsDB getInstance(Context context) {
		if (instance == null) {
			instance = new FriendsDB(context);
		}
		return instance;
	}

	public boolean isFriendFollowed(String friendId) {
		return _followedFriendsData.isFriendFollowed(friendId);
	}

	public boolean isFriendFollowed(User friend) {
		return isFriendFollowed(friend.getFacebookId());
	}

	public void addFriend(User friend) {
		if (!isFriendFollowed(friend)) {
			_followedFriendsData.addFriend(friend);
			_followedFriendsDAO.store(_followedFriendsData);
		}
	}

	public void removeFriend(User friend) {
		if (isFriendFollowed(friend)) {
			_followedFriendsData.removeFriend(friend);
			_followedFriendsDAO.store(_followedFriendsData);
		}
	}

//	public ArrayList<User> getFollowedFriends() {
//		return _followedFriendsData.getFollowedFriends();
//	}

	public ArrayList<User> getRecentChats() {
		return _followedFriendsData.getFollowedFriends();
	}
}
