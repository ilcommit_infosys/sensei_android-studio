package org.almende.motivator;

import android.content.Context;

import org.almende.motivator.dataaccess.NotificationsListDAO;
import org.almende.motivator.dataaccess.NotificationsListData;
import org.almende.motivator.models.Notification;

import java.util.ArrayList;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 4-11-15
 *
 * @author Dominik Egger
 */
public class NotificationDB {

	private static NotificationDB instance;

	private final Context context;

	private NotificationsListDAO _notificationsListDAO;
	private NotificationsListData _notificationsListData;

	private NotificationDB(Context context) {
		this.context = context;

		_notificationsListDAO = new NotificationsListDAO(context);
		_notificationsListData = _notificationsListDAO.getNotifications();
	}

	public static NotificationDB getInstance(Context context) {
		if (instance == null) {
			instance = new NotificationDB(context);
		}
		return instance;
	}

	public void addNotification(Notification notification) {
		_notificationsListData.addNotification(notification);
		_notificationsListDAO.store(_notificationsListData);
	}

	public ArrayList<Notification> getNotifications() {
		return _notificationsListData.getNotifications();
	}

	public void updateDB() {
		_notificationsListDAO.update(_notificationsListData);
	}

	public int getNumUnreadNotifications() {
		int num = 0;
		for (Notification notification : getNotifications()) {
			if (notification.isNew()) {
				num++;
			}
		}
		return num;
	}
}
