package org.almende.motivator.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.almende.motivator.R;
import org.almende.motivator.models.Evidence;
import org.almende.motivator.models.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class RequiredEvidenceGridAdapter extends ArrayAdapter<Uri> {

	private Activity activity;
//	private ArrayList<Uri> items;
	private LayoutInflater vi;
	private HashMap<Uri, Bitmap> bitmapCache;

	public RequiredEvidenceGridAdapter(Activity activity, ArrayList<Uri> items) {
		super(activity, 0, items);
		this.activity = activity;
//		this.items = items;
		this.bitmapCache = new HashMap<>();
		vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	private static class ViewHolder {
		RelativeLayout evidenceLoad;
		ImageView evidenceImage;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Uri uri = getItem(position);

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = vi.inflate(R.layout.complete_challenge_required_evidence_item, null);

			viewHolder = new ViewHolder();
			viewHolder.evidenceLoad = (RelativeLayout) convertView.findViewById(R.id.evidenceLoad);
			viewHolder.evidenceImage = (ImageView) convertView.findViewById(R.id.evidenceImage);

			convertView.setTag(viewHolder);
		}

		viewHolder = (ViewHolder) convertView.getTag();

		if (uri != null) {
			try {
				Bitmap bmp;
				if ((bmp = bitmapCache.get(uri)) == null) {
					bmp = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
					bitmapCache.put(uri, bmp);
				}
				viewHolder.evidenceImage.setImageBitmap(bmp);
				viewHolder.evidenceLoad.setVisibility(View.INVISIBLE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return convertView;
	}

}



