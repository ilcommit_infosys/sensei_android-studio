package org.almende.motivator.adapters;

/**
 * Created by AsterLaptop on 4/13/14.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.almende.motivator.Profile;
import org.almende.motivator.R;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.models.ChallengeHeader;
import org.almende.motivator.models.Notification;
import org.almende.motivator.utils.RoundedImageView;

import java.util.ArrayList;

public class NotificationsAdapter extends ArrayAdapter<Notification> {

	private Context context;
	private ArrayList<Notification> items;
	private LayoutInflater vi;

	public NotificationsAdapter(Context context, ArrayList<Notification> items) {
		super(context, 0, items);
		this.context = context;
		this.items = items;
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	private static class ViewHolder {
		TextView notification;
		Target imageTarget;
		RoundedImageView image;
	}

	@Override
	public Notification getItem(int position) {
		return items.get(items.size() - position - 1);
	}

	@Override
	public int getPosition(Notification item) {
		int position = items.indexOf(item);
		return items.size() - position - 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Notification notification = getItem(position);

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = vi.inflate(R.layout.list_notifications_entry, null);

			viewHolder = new ViewHolder();
			viewHolder.notification = (TextView) convertView.findViewById(R.id.entryNotification);
			viewHolder.image = (RoundedImageView) convertView.findViewById(R.id.entryImage);

			convertView.setTag(viewHolder);
		}

		viewHolder = (ViewHolder) convertView.getTag();

		viewHolder.notification.setText(Html.fromHtml(String.format("<b>%s</b> %s", notification.getNotifierName(), notification.getMessage())));
		if (notification.isNew()) {
//					viewHolder.alert.setVisibility(View.VISIBLE);
			convertView.setBackgroundColor(context.getResources().getColor(R.color.unreadMessage));
//					convertView.setBackgroundColor(0x660000FF);
		} else {
//					viewHolder.alert.setVisibility(View.INVISIBLE);
			convertView.setBackgroundColor(context.getResources().getColor(R.color.readMessage));
//					convertView.setBackgroundColor(Color.TRANSPARENT);
		}


		String imgId = "https://graph.facebook.com/" + notification.getNotifierId() + "/picture";

		// imgId = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash1/t1.0-1/c0.33.200.200/p200x200/248489_10150308474960968_2461155_n.jpg";

		//Log.d("facebook", "url = " + imgId);
		final ViewHolder finalViewHolder = viewHolder;
		viewHolder.imageTarget = new Target() {
			@Override
			public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
				finalViewHolder.image.setImageBitmap(bitmap);
			}

			@Override
			public void onBitmapFailed(Drawable drawable) {

			}

			@Override
			public void onPrepareLoad(Drawable drawable) {

			}
		};

		Picasso.with(context).load(imgId).into(viewHolder.imageTarget);
		return convertView;
	}
}



