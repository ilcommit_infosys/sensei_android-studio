package org.almende.motivator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.almende.motivator.Cookie;
import org.almende.motivator.R;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.models.ChallengeHeader;
import org.almende.motivator.models.Conversation;
import org.almende.motivator.MyLocationListener;

import java.util.ArrayList;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 4-11-15
 *
 * @author Dominik Egger
 */
public class ConversationAdapter extends ArrayAdapter<Conversation> {

	private Context context;
	private ArrayList<Conversation> items;
	private LayoutInflater vi;

	public ConversationAdapter(Context context, ArrayList<Conversation> items) {
		super(context, 0, items);
		this.context = context;
		this.items = items;
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		boolean sentChallenge = false;
		View v = convertView;
		Conversation conversation = items.get(position);
		if (conversation != null) {
			v = vi.inflate(R.layout.list_item_detail_message, null);

			TextView textView = (TextView) v.findViewById(R.id.text1);
			textView.setText(conversation.getPartnerName());
		}
		return v;
	}

	public void setItems(ArrayList<Conversation> conversations) {
		items = conversations;
		notifyDataSetChanged();
	}
}
