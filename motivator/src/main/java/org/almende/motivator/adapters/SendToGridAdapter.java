package org.almende.motivator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.almende.motivator.R;
import org.almende.motivator.models.User;

import java.util.ArrayList;

public class SendToGridAdapter extends ArrayAdapter<User> {

	private Context context;
	private ArrayList<User> items;
	private LayoutInflater vi;

	public SendToGridAdapter(Context context, ArrayList<User> items) {
		super(context, 0, items);
		this.context = context;
		this.items = items;
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	private static class ViewHolder {
		TextView name;
		ImageView remove;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final User user = getItem(position);

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = vi.inflate(R.layout.create_challenge_sendto_item, null);

			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) convertView.findViewById(R.id.txtSendToName);
			viewHolder.remove = (ImageView) convertView.findViewById(R.id.sendToItemRemove);

			convertView.setTag(viewHolder);
		}

		viewHolder = (ViewHolder) convertView.getTag();

		viewHolder.name.setText(user.getName());
		viewHolder.remove.setTag(position);
		viewHolder.remove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				items.remove((int) v.getTag());
				notifyDataSetChanged();
			}
		});

		return convertView;
	}
}



