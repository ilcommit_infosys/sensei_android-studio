package org.almende.motivator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.almende.motivator.R;
import org.almende.motivator.models.Message;

import java.util.ArrayList;

/**
 * Copyright (c) 2016 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 30-3-16
 *
 * @author Dominik Egger
 */
public class ChatAdapter extends ArrayAdapter<Message> {

	private Context context;
	private ArrayList<Message> messages;
	private LayoutInflater inflater;

	private String ownName;

	private static class ViewHolder {
		TextView message;
	}

	public ChatAdapter(Context context, ArrayList<Message> messages) {
		super(context, 0, messages);
		this.context = context;
		this.messages = messages;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setOwnName(String name) {
		this.ownName = name;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Message message = getItem(position);

		ViewHolder viewHolder;
		if (message.getAuthor().equals(ownName)) {
//		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_item_message_sent, null);
		} else {
			convertView = inflater.inflate(R.layout.list_item_message_received, null);
		}

		viewHolder = new ViewHolder();
		viewHolder.message = (TextView) convertView.findViewById(R.id.txtChatMessage);

		viewHolder.message.setText(message.getContent());

		return convertView;


//			convertView.setTag(viewHolder);
//		}

//		viewHolder = (ViewHolder) convertView.getTag();

//		View v = convertView;

//		final Message message = messages.get(position);
//		if (message != null) {
//
//
//
//			v = inflater.inflate(R.layout.list_item_challengemessage, null);
//			final TextView author = (TextView) v.findViewById(R.id.messegeAuthor);
//			final TextView date = (TextView) v.findViewById(R.id.messageDate);
//			final TextView content = (TextView) v.findViewById(R.id.messageContent);
//
//			if (author != null)
//				author.setText(message.getAuthor());
//			if (date != null) {
//				Date theDate = message.getDate();
//				if (theDate != null) {
//					String dateString = new SimpleDateFormat("MMM d yyyy").format(theDate);
//					date.setText("on " + dateString);
//				} else {
//					date.setText("");
//				}
//			}
//
//			if (content != null)
//				content.setText(message.getContent());
//		}
//		return v;
	}


}