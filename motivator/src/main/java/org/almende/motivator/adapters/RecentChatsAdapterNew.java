package org.almende.motivator.adapters;

//import alm.motiv.AlmendeMotivator.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.almende.motivator.R;
import org.almende.motivator.models.Conversation;
import org.almende.motivator.utils.RoundedImageView;

import java.util.ArrayList;

//import com.facebook.model.GraphUser;

/**
 * Created by thijs on 11/4/13.
 */
public class RecentChatsAdapterNew extends BaseAdapter {

	private ArrayList<Conversation> items;
	private LayoutInflater inflater;
	private Context context;

	public RecentChatsAdapterNew(Activity context, ArrayList<Conversation> items) {
		this.context = context;
		this.items = items;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	private static class ViewHolder {
		Target target;
		RoundedImageView image;
		TextView name;
	}

//	public void removeModel(int position){
//		this.items.remove(position);
//		notifyDataSetChanged();
//	}

	public void setItems(ArrayList<Conversation> items) {
		this.items = items;
		notifyDataSetChanged();
	}

	public ArrayList<Conversation> getItems() {
		return this.items;
	}

	@Override
	public int getCount() {
		return items.size();
	}


	@Override
	public Conversation getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Conversation conversation = getItem(position);

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_recent_chats_entry, null);

			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) convertView.findViewById(R.id.entryName);
			viewHolder.image = (RoundedImageView) convertView.findViewById(R.id.entryImage);

			convertView.setTag(viewHolder);
		}

		viewHolder = (ViewHolder) convertView.getTag();

		viewHolder.name.setText(conversation.getPartnerName());

		String imgId = "https://graph.facebook.com/" + conversation.getPartnerId() + "/picture";

		// imgId = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash1/t1.0-1/c0.33.200.200/p200x200/248489_10150308474960968_2461155_n.jpg";

		//Log.d("facebook", "url = " + imgId);
		final ViewHolder finalViewHolder = viewHolder;
		viewHolder.target = new Target() {
			@Override
			public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
				finalViewHolder.image.setImageBitmap(bitmap);
			}

			@Override
			public void onBitmapFailed(Drawable drawable) {
				finalViewHolder.image.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.profilepic));
			}

			@Override
			public void onPrepareLoad(Drawable drawable) {

			}
		};

		Picasso.with(context).load(imgId).into(viewHolder.target);

		return convertView;
	}
}