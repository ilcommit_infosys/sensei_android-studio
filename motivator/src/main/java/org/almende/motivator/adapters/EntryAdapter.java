package org.almende.motivator.adapters;

/**
 * Created by AsterLaptop on 4/13/14.
 */

import java.util.ArrayList;

//import alm.motiv.AlmendeMotivator.Cookie;
//import alm.motiv.AlmendeMotivator.Database;
//import alm.motiv.AlmendeMotivator.R;
//import alm.motiv.AlmendeMotivator.models.ChallengeHeader;
//import alm.motiv.AlmendeMotivator.models.Challenge;
//import alm.motiv.AlmendeMotivator.models.User;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.almende.motivator.Profile;
import org.almende.motivator.R;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.models.ChallengeHeader;
import org.almende.motivator.utils.RoundedImageView;
//import com.mongodb.DB;
//import com.mongodb.DBCollection;
//import com.mongodb.MongoClient;

public class EntryAdapter extends ArrayAdapter<Entry> {

	private Context context;
	private ArrayList<Entry> items;
	private LayoutInflater vi;

	public EntryAdapter(Context context, ArrayList<Entry> items) {
		super(context, 0, items);
		this.context = context;
		this.items = items;
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	private static class ViewHolder {
		TextView title;
		TextView challengee;
		TextView status;
//		View alert;
		Target imageTarget;
		RoundedImageView image;
	}

	@Override
	public Entry getItem(int position) {
		return items.get(items.size() - position - 1);
	}

	@Override
	public int getPosition(Entry item) {
		int position = items.indexOf(item);
		return items.size() - position - 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		boolean sentChallenge = false;
		final Entry i = getItem(position);

		if (i != null) {
			if (i.isSection()) {
				ViewHolder viewHolder;
				if (convertView == null) {
					convertView = vi.inflate(R.layout.list_challenge_entry, null);

					convertView.setOnClickListener(null);
					convertView.setOnLongClickListener(null);
					convertView.setLongClickable(false);

					viewHolder = new ViewHolder();
					viewHolder.title = (TextView) convertView.findViewById(R.id.list_item_section_text);

					convertView.setTag(viewHolder);
				}

				viewHolder = (ViewHolder) convertView.getTag();

				ChallengeHeader si = (ChallengeHeader) i;
				viewHolder.title.setText(si.getTitle());

			} else {

				ViewHolder viewHolder;
				if (convertView == null) {
					convertView = vi.inflate(R.layout.list_challenge_entry, null);

					viewHolder = new ViewHolder();
					viewHolder.title = (TextView) convertView.findViewById(R.id.entryTitle);
					viewHolder.challengee = (TextView) convertView.findViewById(R.id.entryName);
					viewHolder.status = (TextView) convertView.findViewById(R.id.entryStatus);
//					viewHolder.alert = convertView.findViewById(R.id.entryUpdate);
					viewHolder.image = (RoundedImageView) convertView.findViewById(R.id.entryImage);

					convertView.setTag(viewHolder);
				}

				viewHolder = (ViewHolder) convertView.getTag();

				Challenge ei = (Challenge) i;
				if (Profile.getInstance(context).getFacebookId().equals(ei.getChallenger())) {
					sentChallenge = true;
				}
				if (viewHolder.title != null)
					viewHolder.title.setText(ei.getTitle());

				String fbId = "";
				if (viewHolder.challengee != null) {
					if (sentChallenge) {
						viewHolder.challengee.setText(ei.getChallengeeName());
						fbId = ei.getChallengee();
					} else {
						viewHolder.challengee.setText(ei.getChallengerName());
						fbId = ei.getChallenger();
					}
				}
				if (viewHolder.status != null) {
//					viewHolder.status.setText("Status: " + ei.getStatus());
					viewHolder.status.setText(ei.getStatus());
				}
				if (ei.hasUpdate()) {
//					viewHolder.alert.setVisibility(View.VISIBLE);
					convertView.setBackgroundColor(context.getResources().getColor(R.color.unreadMessage));
//					convertView.setBackgroundColor(0x660000FF);
				} else {
//					viewHolder.alert.setVisibility(View.INVISIBLE);
					convertView.setBackgroundColor(context.getResources().getColor(R.color.readMessage));
//					convertView.setBackgroundColor(Color.TRANSPARENT);
				}


				String imgId = "https://graph.facebook.com/" + fbId + "/picture";

				// imgId = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash1/t1.0-1/c0.33.200.200/p200x200/248489_10150308474960968_2461155_n.jpg";

				//Log.d("facebook", "url = " + imgId);
				final ViewHolder finalViewHolder = viewHolder;
				viewHolder.imageTarget = new Target() {
					@Override
					public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
						finalViewHolder.image.setImageBitmap(bitmap);
					}

					@Override
					public void onBitmapFailed(Drawable drawable) {

					}

					@Override
					public void onPrepareLoad(Drawable drawable) {

					}
				};

				Picasso.with(context).load(imgId).into(viewHolder.imageTarget);

			}
		}
		return convertView;
	}
}



