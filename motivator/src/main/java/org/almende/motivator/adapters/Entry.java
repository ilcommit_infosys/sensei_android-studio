package org.almende.motivator.adapters;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by AsterLaptop on 4/13/14.
 */
public interface Entry {

    boolean isSection();

}