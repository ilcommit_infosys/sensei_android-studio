package org.almende.motivator.adapters;

//import alm.motiv.AlmendeMotivator.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.almende.motivator.R;
import org.almende.motivator.models.User;
import org.almende.motivator.utils.RoundedImageView;

import java.util.ArrayList;

//import com.facebook.model.GraphUser;

/**
 * Created by thijs on 11/4/13.
 */
public class FriendsAdapterNew extends ArrayAdapter<User> {

	private ArrayList<User> items;
	private LayoutInflater inflater;
	private Context context;

	private ArrayList<User> suggestions;

	private boolean ignoreFirst = false;

	public FriendsAdapterNew(Activity context, ArrayList<User> items) {
		super(context, 0, items);
		this.context = context;
		this.items = items;
		this.suggestions = new ArrayList<>();
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setIgnoreFirst(boolean ignore) {
		this.ignoreFirst = ignore;
	}

	private static class ViewHolder {
		Target target;
		RoundedImageView image;
		TextView name;
	}

//	public void removeModel(int position){
//		this.items.remove(position);
////		notifyDataSetChanged();
//	}

	public void setItems(ArrayList<User> items) {
		this.items = items;
//		notifyDataSetChanged();
	}

	public ArrayList<User> getItems() {
		return this.items;
	}

	@Override
	public int getCount() {
		return items.size();
	}


	@Override
	public User getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final User item = getItem(position);

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_recent_chats_entry, null);

			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) convertView.findViewById(R.id.entryName);
			viewHolder.image = (RoundedImageView) convertView.findViewById(R.id.entryImage);

			convertView.setTag(viewHolder);
		}

		viewHolder = (ViewHolder) convertView.getTag();

		if (ignoreFirst && position == 0) {
			convertView.setVisibility(View.GONE);
			return convertView;
		}

		viewHolder.name.setText(item.getName());

		String imgId = "https://graph.facebook.com/" + item.getFacebookId() + "/picture";

		// imgId = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash1/t1.0-1/c0.33.200.200/p200x200/248489_10150308474960968_2461155_n.jpg";

		//Log.d("facebook", "url = " + imgId);
		final ViewHolder finalViewHolder = viewHolder;
		viewHolder.target = new Target() {
			@Override
			public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
				finalViewHolder.image.setImageBitmap(bitmap);
			}

			@Override
			public void onBitmapFailed(Drawable drawable) {
				finalViewHolder.image.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.profilepic));
			}

			@Override
			public void onPrepareLoad(Drawable drawable) {

			}
		};

		Picasso.with(context).load(imgId).into(viewHolder.target);

		return convertView;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getView(position, convertView, parent);
	}

	@Override
	public Filter getFilter() {
		return nameFilter;
	}

	Filter nameFilter = new Filter() {
		@Override
		public String convertResultToString(Object resultValue) {
			String str = ((User) (resultValue)).getName();
			return str;
		}

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			if (constraint != null) {
				suggestions.clear();
				for (User user : items) {
					if (user.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
						suggestions.add(user);
					}
				}
				FilterResults filterResults = new FilterResults();
				filterResults.values = suggestions;
				filterResults.count = suggestions.size();
				return filterResults;
			} else {
				return new FilterResults();
			}
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			ArrayList<User> filteredList = (ArrayList<User>) results.values;
			if (results != null && results.count > 0) {
				clear();
				for (User c : filteredList) {
					add(c);
				}
				notifyDataSetChanged();
			}
		}
	};
}