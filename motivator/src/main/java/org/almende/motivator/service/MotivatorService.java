package org.almende.motivator.service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestBatch;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.ConversationDB;
import org.almende.motivator.NotificationDB;
import org.almende.motivator.Profile;
import org.almende.motivator.SenseWrapper;
import org.almende.motivator.dataaccess.ChallengesListDAO;
import org.almende.motivator.dataaccess.ChallengesListData;
import org.almende.motivator.dataaccess.ConversationListDAO;
import org.almende.motivator.dataaccess.ConversationsListData;
import org.almende.motivator.dataaccess.FollowedFriendsDAO;
import org.almende.motivator.dataaccess.FollowedFriendsData;
import org.almende.motivator.dataaccess.NotificationsListDAO;
import org.almende.motivator.facebook.FacebookManager;
import org.almende.motivator.facebook.models.ChallengeRequestAnswer;
import org.almende.motivator.facebook.models.ChallengeRequestComplete;
import org.almende.motivator.facebook.models.ChallengeRequestData;
import org.almende.motivator.facebook.models.ChallengeRequestMessage;
import org.almende.motivator.facebook.models.ChallengeRequestResult;
import org.almende.motivator.facebook.models.FacebookRequest;
import org.almende.motivator.facebook.models.FacebookRequestFactory;
import org.almende.motivator.facebook.models.FacebookRequestHeader;
import org.almende.motivator.facebook.models.PersonalMessage;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.models.Evidence;
import org.almende.motivator.models.Message;
import org.almende.motivator.models.Notification;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

// todo: load profile from SENSE and handle changing user

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 11-11-15
 *
 * @author Dominik Egger
 */
public class MotivatorService extends Service {

	private static final String TAG = MotivatorService.class.getCanonicalName();

	public static final int NEW_CHALLENGE_NOTIFICATION_ID = 11100;
	public static final int UPDATE_CHALLENGE_NOTIFICATION_ID = 11101;

	private static final long SENSE_SYNC_DELAY = 5000;
	public static final int FACEBOOK_SYNC_DELAY = 10000;

	private final boolean DELETE = true;

	private Handler _handler;

	private ChallengeDB _challengeDB;
	private ConversationDB _conversationDB;
	private NotificationDB _notificationDB;

	private static MotivatorService instance;

	private ChallengesListDAO _challengesListDAO;
//	private ReceivedChallengesDAO _receivedChallengeDAO;
//	private SentChallengesDAO _sentChallengesDAO;
	private ConversationListDAO _conversationListDAO;
	private FollowedFriendsDAO _followedFriendsDAO;
	private NotificationsListDAO _notificationsListDAO;

	private Profile _profile;

	private ArrayList<EventListener> _eventListeners = new ArrayList<>();

	public class MotivatorBinder extends Binder {
		public MotivatorService getService() { return instance; }
	}

	private final IBinder _binder = new MotivatorBinder();

	@Override
	public IBinder onBind(Intent intent) {
		return _binder;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		instance = this;

		HandlerThread ht = new HandlerThread("motivatorService");
		ht.start();
		_handler = new Handler(ht.getLooper());

		_challengeDB = ChallengeDB.getInstance(this);
		_conversationDB = ConversationDB.getInstance(this);
		_notificationDB = NotificationDB.getInstance(this);

		_challengesListDAO = new ChallengesListDAO(this);
//		_receivedChallengeDAO = new ReceivedChallengesDAO(this);
//		_sentChallengesDAO = new SentChallengesDAO(this);
		_conversationListDAO = new ConversationListDAO(this);
		_followedFriendsDAO = new FollowedFriendsDAO(this);
		_notificationsListDAO = new NotificationsListDAO(this);

		_profile = Profile.getInstance(this);

		_handler.post(_fbCheckRunnable);
		_handler.post(_senseDbSyncRunnable);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		_handler.removeCallbacksAndMessages(null);
	}

	public void registerEventListener(EventListener listener) {
		if (!_eventListeners.contains(listener)) {
			_eventListeners.add(listener);
		}
	}

	public void unregisterEventListener(EventListener listener) {
		if (_eventListeners.contains(listener)) {
			_eventListeners.remove(listener);
		}
	}

	private void onEvent(EventListener.Event event) {
		for (EventListener listener : _eventListeners) {
			listener.onEvent(event);
		}
	}

	private Runnable _fbCheckRunnable = new Runnable() {
		@Override
		public void run() {
			if (FacebookManager.isLoggedIn()) {
				new GraphRequest(
						AccessToken.getCurrentAccessToken(),
						"me/apprequests",
						null,
						HttpMethod.GET,
						new GraphRequest.Callback() {
							@Override
							public void onCompleted(GraphResponse response) {
								JSONObject graphObject = response.getJSONObject();
								if (graphObject != null) {
									Log.d(TAG, graphObject.toString());
									try {
										JSONArray data = response.getJSONObject().getJSONArray("data");
										Log.i(TAG, String.format("%s new requests", data.length()));

										Collection<GraphRequest> deleteRequests = new ArrayList<>();

										// iterate backwards, since oldest request is at the end, so to keep
										// requests consistent to time, get oldest request first, otherwise
										// in the worst case we could receive updates to a challenge before
										// the challenge was created
										for (int i = data.length() - 1; i >= 0; --i) {
											JSONObject request = data.getJSONObject(i);
											try {
												if (request.has("data")) {

													FacebookRequestHeader header = new FacebookRequestHeader(request.toString());
													FacebookRequest challengeData = FacebookRequestFactory.fromString(request.getString("data"));

													if (challengeData != null) {
														switch (challengeData.getType()) {
															case FacebookRequest.NEW_CHALLENGE: {
																Log.i(TAG, "new challenge received");
																onChallengeReceived(header, (ChallengeRequestData) challengeData);
																break;
															}
															case FacebookRequest.CHALLENGE_ACCEPT: {
																Log.i(TAG, "challenge accept received");
																onChallengeAccepted(header, (ChallengeRequestAnswer) challengeData);
																break;
															}
															case FacebookRequest.CHALLENGE_REJECT: {
																Log.i(TAG, "challenge reject received");
																onChallengeRejected(header, (ChallengeRequestAnswer) challengeData);
																break;
															}
															case FacebookRequest.CHALLENGE_COMPLETE: {
																Log.i(TAG, "challenge completed received");
																onChallengeCompleted(header, (ChallengeRequestComplete) challengeData);
																break;
															}
															case FacebookRequest.CHALLENGE_APPROVE: {
																Log.i(TAG, "challenge approve received");
																onChallengeApproved(header, (ChallengeRequestResult) challengeData);
																break;
															}
															case FacebookRequest.CHALLENGE_DISAPPROVE: {
																Log.i(TAG, "challenge disapprove received");
																onChallengeDisapproved(header, (ChallengeRequestResult) challengeData);
																break;
															}
															case FacebookRequest.CHALLENGE_MESSAGE: {
																Log.i(TAG, "challenge message received");
																onChallengeMessage(header, (ChallengeRequestMessage) challengeData);
																break;
															}
															case FacebookRequest.PERSONAL_MESSAGE: {
																Log.i(TAG, "general message received");
																onPersonalMessage(header, (PersonalMessage) challengeData);
																break;
															}
															default:
																continue;
														}
													} else {
														Log.e(TAG, "failed to parse request: " + request.toString());
													}
												}
											} catch (Exception e) {
												e.printStackTrace();
											}

											if (DELETE) {
												FacebookManager.addDeleteRequest(deleteRequests, request);
											}
										}

										if (!deleteRequests.isEmpty()) {
											GraphRequestBatch batch = new GraphRequestBatch(deleteRequests);
											batch.executeAsync();
										}
									} catch (JSONException e) {
										e.printStackTrace();
									}
								}
							}
						}
				).executeAsync();
			}

			_handler.postDelayed(_fbCheckRunnable, FACEBOOK_SYNC_DELAY);
		}
	};

	private void onChallengeReceived(FacebookRequestHeader header, ChallengeRequestData data) {

		try {
			String challengeId = header.getRequestId();

			if (_challengeDB.getChallenge(challengeId) == null) {
				Challenge challenge = data.getChallenge();

//				JSONObject challenger = request.getJSONObject("from");
//				challenge.setChallenger(challenger.getString("id"));
//				challenge.setChallengerName(challenger.getString("name"));
//
//				JSONObject challengee = request.getJSONObject("to");
//				challenge.setChallengee(challengee.getString("id"));
//				challenge.setChallengeeName(challengee.getString("name"));
//
//				challenge.setChallengeId(request.getString("id").split("_")[0]);

				challenge.setChallenger(header.getFromId());
				challenge.setChallengerName(header.getFromName());

				challenge.setChallengee(header.getToId());
				challenge.setChallengeeName(header.getToName());

				challenge.setChallengeId(challengeId);

				challenge.setUpdate(true);

				_challengeDB.addNewReceived(challenge);

				showNewChallengeNotification(header);
				onEvent(EventListener.Event.CHALLENGE_UPDATE);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void onChallengeMessage(FacebookRequestHeader header, ChallengeRequestMessage challengeData) throws JSONException {
		String challengeId = challengeData.getChallengeId();

		Challenge challenge = _challengeDB.getChallenge(challengeId);
		if (challenge != null) {
			challenge.addComment(challengeData.getMessage());
			challenge.setUpdate(true);
			_challengeDB.updateChallenge(challenge);

			showUpdateChallengeNotification(header);
			onEvent(EventListener.Event.CHALLENGE_UPDATE);
		}
	}

	private void onChallengeDisapproved(FacebookRequestHeader header, ChallengeRequestResult data) throws JSONException {
		String challengeId = data.getChallengeId();

		Challenge challenge = _challengeDB.getChallenge(challengeId);
		if (challenge != null) {
			challenge.setStatus(Challenge.CLOSED);
			challenge.setRated(Challenge.DISAPPROVED);
			challenge.setRatedMessage(data.getMessage());
			challenge.setUpdate(true);
			_challengeDB.updateChallenge(challenge);

			showUpdateChallengeNotification(header);
			onEvent(EventListener.Event.CHALLENGE_UPDATE);

			// todo: delete evidence?
//			for (Uri uri : challenge.getEvidence()) {
//				new File(uri.getPath()).delete();
//			}
//			challenge.setEvidence(null);
		}
	}

	private void onChallengeApproved(FacebookRequestHeader header, ChallengeRequestResult data) throws JSONException {
		String challengeId = data.getChallengeId();

		Challenge challenge = _challengeDB.getChallenge(challengeId);
		if (challenge != null) {
			challenge.setStatus(Challenge.CLOSED);
			challenge.setRated(Challenge.APPROVED);
			challenge.setRatedMessage(data.getMessage());

			int reward = challenge.getEvidenceAmount() * 300;
			Profile.getInstance(this).incXP(reward);
			challenge.setUpdate(true);
			_challengeDB.updateChallenge(challenge);

			showUpdateChallengeNotification(header);
			onEvent(EventListener.Event.CHALLENGE_UPDATE);

			// todo: delete evidence?
//			for (Uri uri : challenge.getEvidence()) {
//				new File(uri.getPath()).delete();
//			}
//			challenge.setEvidence(null);
		}
	}

	private void onChallengeCompleted(FacebookRequestHeader header, ChallengeRequestComplete data) throws JSONException {
		Evidence evidence = data.getEvidence();

		Challenge challenge = _challengeDB.getChallenge(evidence.getChallengeId());
		if (challenge != null) {
			challenge.setStatus(Challenge.COMPLETED);
			challenge.setAmountHours(evidence.getAmountHours());
			challenge.setGps(evidence.getGps());
			challenge.setEndDate(evidence.getEndDate());
			challenge.setUpdate(true);
			_challengeDB.updateChallenge(challenge);

			showUpdateChallengeNotification(header);
			onEvent(EventListener.Event.CHALLENGE_UPDATE);
		}
	}

	private void onChallengeAccepted(FacebookRequestHeader header, ChallengeRequestAnswer data) throws JSONException {
		String challengeId = data.getChallengeId();

		Challenge challenge = _challengeDB.getChallenge(challengeId);
		if (challenge != null) {
			challenge.setStatus(Challenge.ACCEPTED);
			challenge.setUpdate(true);
			_challengeDB.updateChallenge(challenge);

			showUpdateChallengeNotification(header);
			onEvent(EventListener.Event.CHALLENGE_UPDATE);
		}
	}

	private void onChallengeRejected(FacebookRequestHeader header, ChallengeRequestAnswer data) throws JSONException {
		String challengeId = data.getChallengeId();

		Challenge challenge = _challengeDB.getChallenge(challengeId);
		if (challenge != null) {
			challenge.setStatus(Challenge.REJECTED);
			challenge.setUpdate(true);
			_challengeDB.updateChallenge(challenge);

			showUpdateChallengeNotification(header);
			onEvent(EventListener.Event.CHALLENGE_UPDATE);
		}
	}

	public void showUpdateChallengeNotification(FacebookRequestHeader header){
		try{
			Notification notification = new Notification();
			notification.setNotifierName(header.getFromName());
			notification.setNotifierId(header.getFromId());
			notification.setMessage("has updated a challenge");
			_notificationDB.addNotification(notification);
			onEvent(EventListener.Event.NOTIFICATION_UPDATE);

//			Intent myIntent = new Intent(getActivity(), MainActivity.class);
//			PendingIntent myPendingIntent = PendingIntent.getActivity(getActivity(), 0, myIntent, 0);
			//TODO: if statement updateList vullen
			//NOTIFICATION
//			for(int i = 0 ; i < updateList.size() ; i++){
//				if(lastLogin < Long.parseLong(updateList.get(i))){
//					updateNotification = true;
//				}
//			}
//			if(updateNotification == true){
			String message = "One of your Challenges has been updated";

			// todo: DE need an icon, otherwise the notifications are ignored
			NotificationCompat.Builder mBuilder =
					new NotificationCompat.Builder(this)
//							.setSmallIcon(R.drawable.ic_launcher)
							.setContentTitle("Challenges")
//							.setContentIntent(myPendingIntent)
							.setAutoCancel(true)
							.setContentText(message);
			NotificationManager mNotifyMgr =
					(NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
			mNotifyMgr.notify(UPDATE_CHALLENGE_NOTIFICATION_ID, mBuilder.build());
//			}
		}catch (Exception e){
			System.out.println(e);
		}
	}

	public void showNewChallengeNotification(FacebookRequestHeader header) {
		try{
			Notification notification = new Notification();
			notification.setNotifierName(header.getFromName());
			notification.setNotifierId(header.getFromId());
			notification.setMessage("has sent you a new challenge");
			_notificationDB.addNotification(notification);
			onEvent(EventListener.Event.NOTIFICATION_UPDATE);

//			Intent myIntent = new Intent(getActivity(), MainActivity.class);
//			PendingIntent myPendingIntent = PendingIntent.getActivity(getActivity(), 0, myIntent, 0);
			//TODO: if statement updateList vullen
			//NOTIFICATION
//			for(int i = 0 ; i < updateList.size() ; i++){
//				if(lastLogin < Long.parseLong(updateList.get(i))){
//					updateNotification = true;
//				}
//			}
//			if(updateNotification == true){
			String message = "You have received a new challenge";

			// todo: DE need an icon, otherwise the notifications are ignored
			NotificationCompat.Builder mBuilder =
					new NotificationCompat.Builder(this)
//							.setSmallIcon(R.drawable.ic_launcher)
							.setContentTitle("Challenges")
//							.setContentIntent(myPendingIntent)
							.setAutoCancel(true)
							.setContentText(message);
			NotificationManager mNotifyMgr =
					(NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
			mNotifyMgr.notify(NEW_CHALLENGE_NOTIFICATION_ID, mBuilder.build());
//			}
		}catch (Exception e){
			System.out.println(e);
		}
	}

	private void showNewMessageNotification(FacebookRequestHeader header) {
		try {
			Notification notification = new Notification();
			notification.setNotifierName(header.getFromName());
			notification.setNotifierId(header.getFromId());
			notification.setMessage("has sent you a new message");
			_notificationDB.addNotification(notification);
			onEvent(EventListener.Event.NOTIFICATION_UPDATE);
		} catch (JSONException e) {
			System.out.println(e);
		}
	}

	private void onPersonalMessage(FacebookRequestHeader header, PersonalMessage data) throws JSONException {

//		JSONObject partner = request.getJSONObject("from");
//		String partnerId = partner.getString("id");
//		String partnerName = partner.getString("name");
		String partnerId = header.getFromId();
		String partnerName = header.getFromName();

		Message message = data.getMessage();
		_conversationDB.addMessage(partnerId, partnerName, message);

		showNewMessageNotification(header);
		onEvent(EventListener.Event.CONVERSATION_UPDATE);
	}

	private Runnable _senseDbSyncRunnable = new Runnable() {

		@Override
		public void run() {
			try {

				if (_conversationListDAO.hasUnuploadedItems()) {
					List<ConversationsListData> unuploadedItems = _conversationListDAO.getUnuploadedItems();
					if (unuploadedItems.size() != 1) {
						Log.w(TAG, String.format("size != 1!! size: %d", unuploadedItems.size()));
					}
					ConversationsListData item = unuploadedItems.get(0);
					SenseWrapper.setConversations(instance, item.getConversations());
					item.setUploaded(true);
					_conversationListDAO.update(item);
				}

				if (_challengesListDAO.hasUnuploadedItems()) {
					List<ChallengesListData> unuploadedItems = _challengesListDAO.getUnuploadedItems();
					if (unuploadedItems.size() != 1) {
						Log.w(TAG, String.format("size != 1!! size: %d", unuploadedItems.size()));
					}
					ChallengesListData item = unuploadedItems.get(0);
					if (!item.isReceivedChallengesUploaded()) {
						SenseWrapper.setReceivedChallenges(instance, item.getReceivedChallenges());
					}
					if (!item.isSentChallengesUploaded()) {
						SenseWrapper.setSentChallenges(instance, item.getSentChallenges());
					}
					item.setUploaded(true);
					_challengesListDAO.update(item);
				}

//				if (_receivedChallengeDAO.hasUnuploadedItems()) {
//					List<ReceivedChallengesData> unuploadedItems = _receivedChallengeDAO.getUnuploadedItems();
//					if (unuploadedItems.size() != 1) {
//						Log.w(TAG, String.format("size != 1!! size: %d", unuploadedItems.size()));
//					}
//					ReceivedChallengesData item = unuploadedItems.get(0);
//					SenseWrapper.setReceivedChallenges(instance, item.getChallenges());
//					item.setUploaded(true);
//					_receivedChallengeDAO.update(item);
//				}
//
//				if (_sentChallengesDAO.hasUnuploadedItems()) {
//					List<SentChallengesData> unuploadedItems = _sentChallengesDAO.getUnuploadedItems();
//					if (unuploadedItems.size() != 1) {
//						Log.w(TAG, String.format("size != 1!! size: %d", unuploadedItems.size()));
//					}
//					SentChallengesData item = unuploadedItems.get(0);
//					SenseWrapper.setSentChallenges(instance, item.getChallenges());
//					item.setUploaded(true);
//					_sentChallengesDAO.update(item);
//				}

				if (_followedFriendsDAO.hasUnuploadedItems()) {
					List<FollowedFriendsData> unuploadedItems = _followedFriendsDAO.getUnuploadedItems();
					if (unuploadedItems.size() != 1) {
						Log.w(TAG, String.format("size != 1!! size: %d", unuploadedItems.size()));
					}
					FollowedFriendsData item = unuploadedItems.get(0);
					SenseWrapper.setFollowedFriends(instance, item.getFollowedFriends());
					item.setUploaded(true);
					_followedFriendsDAO.update(item);
				}

				if (!_profile.isUploaded()) {
					SenseWrapper.storeProfile(instance, _profile.getUser());
					_profile.setUploaded(true);
				}

			} catch (IOException | JSONException | IllegalAccessException e) {
				e.printStackTrace();
			}

			_handler.postDelayed(this, SENSE_SYNC_DELAY);
		}
	};

}
