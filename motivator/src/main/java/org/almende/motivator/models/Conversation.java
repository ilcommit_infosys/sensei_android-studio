package org.almende.motivator.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 4-11-15
 *
 * @author Dominik Egger
 */
public class Conversation extends JSONObject {

	public static final String FIELD_PARTNER_ID = "partnerId";
	public static final String FIELD_PARTNER_NAME = "partnerName";
	public static final String FIELD_MESSAGES = "messages";

//	private String partnerId;
//	private String partnerName;
	private ArrayList<Message> messages = null;

	private boolean modified;
	private long _id = -1;

	public Conversation(String partnerId, String partnerName) {
		setPartnerId(partnerId);
		setPartnerName(partnerName);
		setModified(true);
	}

	public Conversation(String json) throws JSONException {
		super(json);
		setModified(false);
	}

	public boolean isModified() {
		return modified;
	}

	public void setModified(boolean modified) {
		this.modified = modified;
	}

	public String getPartnerId() {
		try {
			return getString(FIELD_PARTNER_ID);
		} catch (JSONException e) {
			if (has(FIELD_PARTNER_ID)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setPartnerId(String partnerId) {
		try {
			if (getPartnerId() != partnerId) {
				put(FIELD_PARTNER_ID, partnerId);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getPartnerName() {
		try {
			return getString(FIELD_PARTNER_NAME);
		} catch (JSONException e) {
			if (has(FIELD_PARTNER_NAME)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setPartnerName(String partnerName) {
		try {
			if (getPartnerName() != partnerName) {
				put(FIELD_PARTNER_NAME, partnerName);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Message> getMessages() {
		if (messages == null) {
			messages = new ArrayList<>();
			try {
				JSONArray jsonArray = getJSONArray(FIELD_MESSAGES);
				for (int i = 0; i < jsonArray.length(); ++i) {
					Message message = new Message(jsonArray.getString(i));
					messages.add(message);
				}
			} catch (JSONException e) {
				if (has(FIELD_MESSAGES)) {
					e.printStackTrace();
				}
			}
		}
		return messages;
	}

//	public void setMessages(ArrayList<Message> messages) {
//		this.messages = messages;
//	}

	public void addMessage(Message message) {
		try {
			if (!has(FIELD_MESSAGES)) {
				put(FIELD_MESSAGES, new JSONArray());
			}
			if (messages == null) {
				getMessages();
			}
			getJSONArray(FIELD_MESSAGES).put(message.toString());
			messages.add(message);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public boolean isUploaded() {
		for (Message message : messages) {
			if (!message.isUploaded()) {
				return false;
			}
		}
		return true;
	}

	public void setUploaded(boolean uploaded) {
		for (Message message : messages) {
			message.setUploaded(uploaded);
		}
	}
}
