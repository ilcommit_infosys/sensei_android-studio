package org.almende.motivator.models;

import org.json.JSONException;

/**
 * Copyright (c) 2016 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 24-3-16
 *
 * @author Dominik Egger
 */
public class Notification extends DBObject {

	public static final String FIELD_NOTIFIER_ID = "notifierId";
	public static final String FIELD_NOTIFIER_NAME = "notifierName";
	public static final String FIELD_MESSAGE = "message";
	public static final String FIELD_NEW = "new";

	public Notification() {
		super();
		setNew(true);
	}

	public Notification(String json) throws JSONException {
		super(json);
	}

	public void setNotifierId(String id) {
		try {
			if (getNotifierId() != id) {
				put(FIELD_NOTIFIER_ID, id);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getNotifierId() {
		try {
			return getString(FIELD_NOTIFIER_ID);
		} catch (JSONException e) {
			if (has(FIELD_NOTIFIER_ID)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setNotifierName(String name) {
		try {
			if (getNotifierName() != name) {
				put(FIELD_NOTIFIER_NAME, name);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getNotifierName() {
		try {
			return getString(FIELD_NOTIFIER_NAME);
		} catch (JSONException e) {
			if (has(FIELD_NOTIFIER_NAME)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setMessage(String message) {
		try {
			if (getMessage() != message) {
				put(FIELD_MESSAGE, message);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getMessage() {
		try {
			return getString(FIELD_MESSAGE);
		} catch (JSONException e) {
			if (has(FIELD_MESSAGE)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public boolean isNew() {
		try {
			return getBoolean(FIELD_NEW);
		} catch (JSONException e) {
			if (has(FIELD_NEW)) {
				e.printStackTrace();
			}
			return false;
		}
	}

	public void setNew(boolean isNew) {
		try {
			if (isNew() != isNew) {
				put(FIELD_NEW, isNew);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


}
