package org.almende.motivator.models;

//import alm.motiv.AlmendeMotivator.adapters.Entry;
//import com.mongodb.BasicDBObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by AsterLaptop on 3/31/14.
 */
public class Evidence extends JSONObject{

	public static final String FIELD_AMOUNT_HOURS = "amountHours";
	public static final String FIELD_GPS = "gps";
	public static final String LOGIN_DATE = "loginDate";
	public static final String END_DATE = "endDate";
	public static final String FIELD_CHALLENGE_ID = "fbId";

	public Evidence() {}

	public Evidence(String json) throws JSONException {
		super(json);
		updateLoginDate();
	}

//	public Evidence(String senseId, String json) throws JSONException {
//		this(json);
//		setSenseId(senseId);
//	}

	public void updateLoginDate()  {
		try {
			put(LOGIN_DATE, new Date().getTime());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void setEndDate(Date endDate) {
		try {
			if (getEndDate() != endDate) {
				put(END_DATE, endDate.getTime());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public Date getEndDate() {
		try {
			return new Date(getLong(END_DATE));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getChallengeId() {
		try {
			return getString(FIELD_CHALLENGE_ID);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setChallengeId(String id) {
		try {
			if (getChallengeId() != id) {
				put(FIELD_CHALLENGE_ID, id);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getGps() {
		try {
			return getString(FIELD_GPS);
		} catch (JSONException e) {
			e.printStackTrace();
			return "";
		}
	}

	public void setGps(String gps) {
		try {
			if (getGps() != gps) {
				put(FIELD_GPS, gps);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public int getAmountHours() {
		try {
			return getInt(FIELD_AMOUNT_HOURS);
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public void setAmountHours(Integer amountHours) {
		try {
			if (getAmountHours() != amountHours) {
				put(FIELD_AMOUNT_HOURS, amountHours);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
