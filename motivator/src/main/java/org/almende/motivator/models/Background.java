package org.almende.motivator.models;

import org.json.JSONArray;
import org.json.JSONException;

public class Background extends DBObject {

    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_HOURS = "hours";
    public static final String FIELD_MOTIVATION = "motivation";
    public static final String FIELD_PERIOD = "period";
    public static final String FIELD_PART_OF_DAY = "partOfDay";
    public static final String FIELD_GENDER = "gender";
    public static final String FIELD_LIVING = "living";
    public static final String FIELD_COMPANY = "company";
    public static final String FIELD_REASONS = "reasons";


    public Background() {}

    public void setEmail(String email){
        try {
            put(FIELD_EMAIL, email);
            setModified(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setHours(int hours){
        try {
            put(FIELD_HOURS, hours);
            setModified(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setMotivations(String ... motivations){
        try {
            JSONArray jsonArray = new JSONArray();
            for (String motivation : motivations) {
                jsonArray.put(motivation);
            }
            put(FIELD_MOTIVATION, jsonArray.toString());
            setModified(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setPeriod(String period){
        try {
            put(FIELD_PERIOD, period);
            setModified(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setPartOfTheDay(String partOfTheDay){
        try {
            put(FIELD_PART_OF_DAY, partOfTheDay);
            setModified(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setGender(String gender){
        try {
            put(FIELD_GENDER, gender);
            setModified(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setLiving(String living){
        try {
            put(FIELD_LIVING, living);
            setModified(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setCompany(String company){
        try {
            put(FIELD_COMPANY, company);
            setModified(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setReasonsNotToSport(String reasonsNotToSport){
        try {
            put(FIELD_REASONS, reasonsNotToSport);
            setModified(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
