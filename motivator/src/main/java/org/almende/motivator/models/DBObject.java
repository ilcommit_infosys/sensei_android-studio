package org.almende.motivator.models;

import android.support.annotation.Nullable;

import org.almende.motivator.utils.JSONExcludeObject;
import org.json.JSONException;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 12-11-15
 *
 * @author Dominik Egger
 */
public class DBObject extends JSONExcludeObject {

	public static final String FIELD_ID = "_id";
	public static final String FIELD_UPLOADED = "uploaded";

	protected boolean modified;
	protected long _id = -1;

	@Nullable
	@Override
	protected String[] defineExcludeFields() {
		return new String[] { FIELD_UPLOADED, FIELD_ID };
	}

	DBObject() {
		super();
	}

	DBObject(String json) throws JSONException {
		super(json);
	}

	public boolean isUploaded() {
		try {
			return getBoolean(FIELD_UPLOADED);
		} catch (JSONException e) {
			if (has(FIELD_UPLOADED)) {
				e.printStackTrace();
			}
			return false;
		}
	}

	public void setUploaded(boolean uploaded) {
		try {
			if (isUploaded() != uploaded) {
				put(FIELD_UPLOADED, uploaded);
				this.modified = true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public boolean isModified() {
		return modified;
	}

	public void setModified(boolean modified) {
		if (modified) {
			setUploaded(false);
		}
		this.modified = modified;
	}

	public long getDbId() {
		return _id;
	}

	public void setDbId(long id) {
		this._id = id;
	}

}
