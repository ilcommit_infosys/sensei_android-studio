package org.almende.motivator.models;

//import alm.motiv.AlmendeMotivator.adapters.Entry;
//import com.mongodb.BasicDBObject;

import android.net.Uri;

import org.almende.motivator.adapters.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class Challenge extends DBObject implements Entry {

	public static final String NEW = "new";
	public static final String ACCEPTED = "accepted";
	public static final String REJECTED = "rejected";
	public static final String COMPLETED = "completed";
	public static final String APPROVED = "approved";
	public static final String DISAPPROVED = "disapproved";

	public static final String FIELD_TITLE = "title";
	public static final String FIELD_CHALLENGER = "challenger";
	public static final String FIELD_CHALLENGEE = "challengee";
	public static final String FIELD_CHALLENGER_NAME = "challengerName";
	public static final String FIELD_CHALLENGEE_NAME = "challengeeName";
	public static final String FIELD_RATED_MESSAGE = "ratedMessage";
	public static final String FIELD_RATED = "rated";
	public static final String FIELD_XP_REWARD = "xpReward";
	public static final String FIELD_AMOUNT_HOURS = "amountHours";
	public static final String FIELD_GPS = "gps";
	public static final String FIELD_STATUS = "status";
	public static final String FIELD_REWARD = "reward";
	public static final String FIELD_EVIDENCE_TYPE = "evidenceType";
	public static final String FIELD_EVIDENCE_AMOUNT = "evidenceAmount";
	public static final String FIELD_CONTENT = "content";
	public static final String FIELD_LOGIN_DATE = "loginDate";
	public static final String FIELD_START_DATE = "startDate";
	public static final String FIELD_END_DATE = "endDate";
	public static final String FIELD_CHALLENGE_ID = "challengeId";
//	public static final String FIELD_SENSE_ID = "senseId";
	public static final String FIELD_EVIDENCE = "evidence";
	public static final String FIELD_COMMENTS = "comments";
	public static final String FIELD_UPDATE = "updatePending";
	public static final String CLOSED = "closed";

	//	private ArrayList<Message> _comments;

	public Challenge() {
//		setModified(true);
	}

	public Challenge(String json) throws JSONException {
		super(json);
//		updateLoginDate();
	}

//	public Challenge(String senseId, String json) throws JSONException {
//		this(json);
//		setSenseId(senseId);
//	}

	public void setLoginDate(Date loginDate) {
		try {
			if (getLoginDate() != loginDate) {
				put(FIELD_LOGIN_DATE, loginDate.getTime());
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public Date getLoginDate() {
		try {
			return new Date(getLong(FIELD_LOGIN_DATE));
		} catch (JSONException e) {
			if (has(FIELD_LOGIN_DATE)) {
				e.printStackTrace();
			}
			return null;
		}
	}

	//    public Challenge(String title, String challenger, String challengee, String content, int evidence_amount, String evidence_type, String reward, String status, String gps, String amountHours, int XPreward) {
//        this.title = title;
//        this.challenger = challenger;
//        this.challengee = challengee;
//        this.content = content;
//        this.evidence_amount = evidence_amount;
//        this.evidence_type = evidence_type;
//        this.reward = reward;
//        this.status = status;
//        this.gps = gps;
//        this.amountHours = amountHours;
//        this.xpRreward = XPreward;
//		this.date = new Date();
//    }

	public void updateLoginDate() {
		setLoginDate(new Date());
	}

	public void setStartDate(Date startDate) {
		try {
			if (getStartDate() != startDate) {
				put(FIELD_START_DATE, startDate.getTime());
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public Date getStartDate() {
		try {
			return new Date(getLong(FIELD_START_DATE));
		} catch (JSONException e) {
			if (has(FIELD_START_DATE)) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public void setEndDate(Date endDate) {
		try {
			if (getEndDate() != endDate) {
				put(FIELD_END_DATE, endDate.getTime());
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public Date getEndDate() {
		try {
			return new Date(getLong(FIELD_END_DATE));
		} catch (JSONException e) {
			if (has(FIELD_END_DATE)) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public String getChallengeId() {
		try {
			return getString(FIELD_CHALLENGE_ID);
		} catch (JSONException e) {
			if (has(FIELD_CHALLENGE_ID)) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public void setChallengeId(String id) {
		try {
			if (getChallengeId() != id) {
				put(FIELD_CHALLENGE_ID, id);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

//	public String getSenseId() {
//		try {
//			return getString(FIELD_SENSE_ID);
//		} catch (JSONException e) {
//			if (has(FIELD_SENSE_ID)) {
//				e.printStackTrace();
//			}
//			return null;
//		}
//	}

//	public void setSenseId(String id) {
//		try {
//			put(FIELD_SENSE_ID, id);
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//	}

	public String getTitle() {
		try {
			return getString(FIELD_TITLE);
		} catch (JSONException e) {
			if (has(FIELD_TITLE)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setTitle(String title) {
		try {
			if (getTitle() != title) {
				put(FIELD_TITLE, title);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getChallenger() {
		try {
			return getString(FIELD_CHALLENGER);
		} catch (JSONException e) {
			if (has(FIELD_CHALLENGER)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setChallenger(String challenger) {
		try {
			if (getChallenger() != challenger) {
				put(FIELD_CHALLENGER, challenger);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getChallengee() {
		try {
			return getString(FIELD_CHALLENGEE);
		} catch (JSONException e) {
			if (has(FIELD_CHALLENGEE)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setChallengee(String challengee) {
		try {
			if (getChallengee() != challengee) {
				put(FIELD_CHALLENGEE, challengee);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getContent() {
		try {
			return getString(FIELD_CONTENT);
		} catch (JSONException e) {
			if (has(FIELD_CONTENT)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setContent(String content) {
		try {
			if (getContent() != content) {
				put(FIELD_CONTENT, content);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public int getEvidenceAmount() {
		try {
			return getInt(FIELD_EVIDENCE_AMOUNT);
		} catch (JSONException e) {
			if (has(FIELD_EVIDENCE_AMOUNT)) {
				e.printStackTrace();
			}
			return 0;
		}
	}

	public void setEvidenceAmount(int evidenceAmount) {
		try {
			if (getEvidenceAmount() != evidenceAmount) {
				put(FIELD_EVIDENCE_AMOUNT, evidenceAmount);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getEvidenceType() {
		try {
			return getString(FIELD_EVIDENCE_TYPE);
		} catch (JSONException e) {
			if (has(FIELD_EVIDENCE_TYPE)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setEvidenceType(String evidenceType) {
		try {
			if (getEvidenceType() != evidenceType) {
				put(FIELD_EVIDENCE_TYPE, evidenceType);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getReward() {
		try {
			return getString(FIELD_REWARD);
		} catch (JSONException e) {
			if (has(FIELD_REWARD)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setReward(String reward) {
		try {
			if (getReward() != reward) {
				put(FIELD_REWARD, reward);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getStatus() {
		try {
			return getString(FIELD_STATUS);
		} catch (JSONException e) {
			if (has(FIELD_STATUS)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setStatus(String status) {
		try {
			if (getStatus() != status) {
				put(FIELD_STATUS, status);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getGps() {
		try {
			return getString(FIELD_GPS);
		} catch (JSONException e) {
			if (has(FIELD_GPS)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setGps(String gps) {
		try {
			if (getGps() != gps) {
				put(FIELD_GPS, gps);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public int getAmountHours() {
		try {
			return getInt(FIELD_AMOUNT_HOURS);
		} catch (JSONException e) {
			if (has(FIELD_AMOUNT_HOURS)) {
				e.printStackTrace();
			}
			return 0;
		}
	}

	public void setAmountHours(int amountHours) {
		try {
			if (getAmountHours() != amountHours) {
				put(FIELD_AMOUNT_HOURS, amountHours);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public int getXpReward() {
		try {
			return getInt(FIELD_XP_REWARD);
		} catch (JSONException e) {
			if (has(FIELD_XP_REWARD)) {
				e.printStackTrace();
			}
			return 0;
		}
	}

	public void setXpReward(int xpRreward) {
		try {
			if (getXpReward() != xpRreward) {
				put(FIELD_XP_REWARD, xpRreward);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isSection() {
		return false;
	}

	public boolean hasUpdate() {
		try {
			return getBoolean(FIELD_UPDATE);
		} catch (JSONException e) {
			if (has(FIELD_UPDATE)) {
				e.printStackTrace();
			}
			return false;
		}
	}

	public void setUpdate(boolean update) {
		try {
			if (hasUpdate() != update) {
				put(FIELD_UPDATE, update);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void setRated(String rated) {
		try {
			if (getRated() != rated) {
				put(FIELD_RATED, rated);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getRated() {
		try {
			return getString(FIELD_RATED);
		} catch (JSONException e) {
			if (has(FIELD_RATED)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setRatedMessage(String ratedMessage) {
		try {
			if (getRatedMessage() != ratedMessage) {
				put(FIELD_RATED_MESSAGE, ratedMessage);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getRatedMessage() {
		try {
			return getString(FIELD_RATED_MESSAGE);
		} catch (JSONException e) {
			if (has(FIELD_RATED_MESSAGE)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setChallengeeName(String challengeeName) {
		try {
			if (getChallengeeName() != challengeeName) {
				put(FIELD_CHALLENGEE_NAME, challengeeName);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getChallengeeName() {
		try {
			return getString(FIELD_CHALLENGEE_NAME);
		} catch (JSONException e) {
			if (has(FIELD_CHALLENGEE_NAME)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setChallengerName(String challengerName) {
		try {
			if (getChallengerName() != challengerName) {
				put(FIELD_CHALLENGER_NAME, challengerName);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getChallengerName() {
		try {
			return getString(FIELD_CHALLENGER_NAME);
		} catch (JSONException e) {
			if (has(FIELD_CHALLENGER_NAME)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setCommentsJson(String json) {
		try {
			JSONArray jsonArray = new JSONArray(json);
			put(FIELD_COMMENTS, jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getCommentsJson() {
		try {
			return getJSONArray(FIELD_COMMENTS).toString();
		} catch (JSONException e) {
			if (has(FIELD_COMMENTS)) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public ArrayList<Message> getComments() {
		ArrayList<Message> comments = new ArrayList<>();
		try {
			JSONArray jsonArray = getJSONArray(FIELD_COMMENTS);
			for (int i = 0; i < jsonArray.length(); ++i) {
				Message message = new Message(jsonArray.getString(i));
				comments.add(message);
			}
		} catch (JSONException e) {
			if (has(FIELD_COMMENTS)) {
				e.printStackTrace();
			}
		}
		return comments;
	}

	public void addComment(Message message) {
		try {
			if (!has(FIELD_COMMENTS)) {
				put(FIELD_COMMENTS, new JSONArray());
			}
			getJSONArray(FIELD_COMMENTS).put(message.toString());
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void setEvidenceJson(String json) {
		try {
			JSONArray evidence = new JSONArray(json);
			put(FIELD_EVIDENCE, evidence);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getEvidenceJson() {
		try {
			return getJSONArray(FIELD_EVIDENCE).toString();
		} catch (JSONException e) {
			if (has(FIELD_EVIDENCE)) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public void setEvidence(ArrayList<Uri> list) {
		JSONArray evidence = new JSONArray();
		// if list is null, just add an empty json array
		if (list != null) {
			for (Uri uri : list) {
				evidence.put(uri.toString());
			}
		}
		try {
			put(FIELD_EVIDENCE, evidence);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Uri> getEvidence() {
		ArrayList<Uri> result = new ArrayList<>();

		JSONArray evidence;
		try {
			evidence = getJSONArray(FIELD_EVIDENCE);

			for (int i = 0; i < evidence.length(); ++i) {
				result.add(Uri.parse(evidence.getString(i)));
			}

		} catch (JSONException e) {
			if (has(FIELD_EVIDENCE)) {
				e.printStackTrace();
			}
			return null;
		}

		return result;
	}

	public Challenge copy() throws JSONException {
		return new Challenge(this.toString());
	}

	public boolean isNewChallenge() {
		return getStatus().equals(Challenge.NEW);
	}
}

//public class Challenge extends BasicDBObject implements Entry {
//    private static final long serialVersionUID = 1L;
//
//    public Challenge() {}
//
//    //TODO add likeAmount to challenge. Different way then it is now. Solution below causes DBThreadExceptions
//    public Challenge(String title, String challenger, String challengee, String content, int evidence_amount, String evidence_type, String reward, String status, String gps, String amountHours, int XPreward) {
//        put("title", title);
//        put("challenger", challenger);
//        put("challengee", challengee);
//        put("content", content);
//        put("evidence_amount", evidence_amount);
//        put("evidence_type", evidence_type);
//        put("reward", reward);
//        put("status", status);
//        put("gps", gps);
//        put("amountHours", amountHours);
//        put("XPreward", XPreward);
//        long time= System.currentTimeMillis();
//        this.put("Date", time);
//    }
//
//    public void updateLoginDate(){
//        long time= System.currentTimeMillis();
//        this.put("Date", time);
//    }
//
//    public void setStatus(String status) {
//        put("status", status);
//    }
//
//    public Object getID() {
//        return this.get("_id");
//    }
//
//    public String getTitle() {
//        return this.getString("title");
//    }
//
//    public String getChallenger() {
//        return this.getString("challenger");
//    }
//
//    public String getChallengee() {
//        return this.getString("challengee");
//    }
//
//    public String getContent() {
//        return this.getString("content");
//    }
//
//    public int getEvidenceAmount() {
//        return this.getInt("evidence_amount");
//    }
//
//    public String getEvidenceType() {
//        return this.getString("evidence_type");
//    }
//
//    public String getReward() {
//        return this.getString("reward");
//    }
//
//    public String getStatus() {
//        return this.getString("status");
//    }
//
//    public ArrayList<BasicDBObject> getEvidence() {
//        return (ArrayList<BasicDBObject>) this.get("evidence");
//    }
//
//    public void setEvidence(String evidence){
//        this.put("evidence", evidence);
//    }
//
//    public void setComments(Message message){
//        this.put("$push", new BasicDBObject("comments", message));
//    }
//
//    public ArrayList<BasicDBObject> getComments(){
//        return (ArrayList<BasicDBObject>) this.get("comments");
//    }
//
//    public void setRated(String rated){
//        this.put("rated",rated);
//    }
//
//    public String getRated(){
//        return this.get("rated").toString();
//    }
//
//    public void setRatedMessage(String ratedMessage){
//        this.put("ratedMessage", ratedMessage);
//    }
//
//    public String getRatedMessage(){
//        return this.get("ratedMessage").toString();
//    }
//
//    public void setStartDate(Date date){
//        this.put("startDate", date);
//    }
//
//    public int getXpReward(){
//        return this.getInt("XPreward");
//    }
//
//    @Override
//    public boolean isSection() {
//        return false;
//    }
//
//    public String getChallengeeName() {
//        return this.getString("challengeeName");
//    }
//
//    public void setChallengeeName(String challengeeName) {
//        this.put("challengeeName",challengeeName);
//    }
//
//    public String getChallengerName() {
//        return this.getString("challengerName");
//    }
//
//    public void setChallengerName(String challengerName) {
//        this.put("challengerName",challengerName);
//    }
//}
