package org.almende.motivator.models;

//import alm.motiv.AlmendeMotivator.adapters.Entry;

import org.almende.motivator.adapters.Entry;

/**
 * Created by AsterLaptop on 4/13/14.
 */

public class ChallengeHeader implements Entry {

    private final String title;

    public ChallengeHeader(String title) {
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    @Override
    public boolean isSection() {
        return true;
    }

}
