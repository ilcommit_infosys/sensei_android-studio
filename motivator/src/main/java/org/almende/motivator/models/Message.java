package org.almende.motivator.models;

//import com.mongodb.BasicDBObject;
//import com.mongodb.DBObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

// todo: handle messages longer than 255 bytes (total json size, not content)

/**
 * Created by AsterLaptop on 4/1/14.
 */
public class Message extends DBObject {

	public static final String FIELD_CATEGORY = "category";
	public static final String FIELD_RECEIVER = "receiver";
	public static final String FIELD_AUTHOR = "author";
	public static final String FIELD_CONTENT = "content";
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_DATE = "date";
	public static final String FIELD_LIKED = "liked";

//	public static final String FIELD_CATEGORY = "cat";
//	public static final String FIELD_RECEIVER = "rec";
//	public static final String FIELD_AUTHOR = "aut";
//	public static final String FIELD_CONTENT = "cont";
//	public static final String FIELD_TITLE = "tit";
//	public static final String FIELD_DATE = "dat";
//	public static final String FIELD_LIKED = "lik";

	//private ArrayList<String> receivedMessages = new ArrayList<String>();

	public Message() {
		super();
	}

	public Message(String json) throws JSONException {
		super(json);
	}

	public void setLiked(String liked) {
		try {
			if (getLiked() != liked) {
				put(FIELD_LIKED, liked);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getLiked() {
		try {
			return getString(FIELD_LIKED);
		} catch (JSONException e) {
			if (has(FIELD_LIKED)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setDate(Date date) {
		try {
			if (getDate() != date) {
				put(FIELD_DATE, date.getTime());
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public Date getDate() {
		try {
			return new Date(getLong(FIELD_DATE));
		} catch (JSONException e) {
			if (has(FIELD_LIKED)) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public void setTitle(String title) {
		try {
			if (getTitle() != title) {
				put(FIELD_TITLE, title);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getTitle() {
		try {
			return getString(FIELD_TITLE);
		} catch (JSONException e) {
			if (has(FIELD_LIKED)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setContent(String content) {
		try {
			if (getContent() != content) {
				this.put(FIELD_CONTENT, content);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getContent() {
		try {
			return getString(FIELD_CONTENT);
		} catch (JSONException e) {
			if (has(FIELD_LIKED)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setAuthor(String author) {
		try {
			if (getAuthor() != author) {
				put(FIELD_AUTHOR, author);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getAuthor() {
		try {
			return getString(FIELD_AUTHOR);
		} catch (JSONException e) {
			if (has(FIELD_LIKED)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setReceiver(String receiver) {
		try {
			if (getReceiver() != receiver) {
				put(FIELD_RECEIVER, receiver);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getReceiver() {
		try {
			return getString(FIELD_RECEIVER);
		} catch (JSONException e) {
			if (has(FIELD_LIKED)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setCategory(String category) {
		try {
			if (getCatgeory() != category) {
				put(FIELD_CATEGORY, category);
				setModified(true);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getCatgeory() {
		try {
			return getString(FIELD_CATEGORY);
		} catch (JSONException e) {
			if (has(FIELD_LIKED)) {
				e.printStackTrace();
			}
			return "";
		}
	}

}

