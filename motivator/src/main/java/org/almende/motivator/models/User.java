package org.almende.motivator.models;

import org.json.JSONException;

/**
 * Created by AsterLaptop on 4/1/14.
 */
public class User extends DBObject {

	public static final String FIELD_FACEBOOK_ID = "facebookId";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_AGE = "age";
	public static final String FIELD_GOAL = "goal";
	public static final String FIELD_CITY = "city";
	public static final String FIELD_SPORTS = "sports";
	public static final String FIELD_ABOUT = "about";
	public static final String FIELD_XP = "xp";

	public User() {
		setFacebookId("");
		setName("");
	}

	public User(String facebookID, String name) {
		setFacebookId(facebookID);
		setName(name);
	}

	public User(String json) throws JSONException {
		super(json);
	}

	public String getFacebookId() {
		try {
			return getString(FIELD_FACEBOOK_ID);
		} catch (JSONException e) {
			if (has(FIELD_FACEBOOK_ID)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setFacebookId(String facebookId) {
		try {
			put(FIELD_FACEBOOK_ID, facebookId);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getName() {

		try {
			return getString(FIELD_NAME);
		} catch (JSONException e) {
			if (has(FIELD_NAME)) {
				e.printStackTrace();
			}
			return "";
		}

	}

	public void setName(String name) {

		try {
			put(FIELD_NAME, name);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public int getAge() {

		try {
			return getInt(FIELD_AGE);
		} catch (JSONException e) {
			if (has(FIELD_AGE)) {
				e.printStackTrace();
			}
			return 0;
		}
	}

	public void setAge(int age) {

		try {
			put(FIELD_AGE, age);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getGoal() {

		try {
			return getString(FIELD_GOAL);
		} catch (JSONException e) {
			if (has(FIELD_GOAL)) {
				e.printStackTrace();
			}
			return "";
		}

	}

	public void setGoal(String goal) {

		try {
			put(FIELD_GOAL, goal);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getCity() {

		try {
			return getString(FIELD_CITY);
		} catch (JSONException e) {
			if (has(FIELD_CITY)) {
				e.printStackTrace();
			}
			return "";
		}

	}

	public void setCity(String city) {

		try {
			put(FIELD_CITY, city);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	//TODO make arraylist, subarray inside user
	public String getSports() {

		try {
			return getString(FIELD_SPORTS);
		} catch (JSONException e) {
			if (has(FIELD_SPORTS)) {
				e.printStackTrace();
			}
			return "";
		}

	}

	public void setSports(String sports) {

		try {
			put(FIELD_SPORTS, sports);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getAbout() {

		try {
			return getString(FIELD_ABOUT);
		} catch (JSONException e) {
			if (has(FIELD_ABOUT)) {
				e.printStackTrace();
			}
			return "";
		}
	}

	public void setAbout(String about) {

		try {
			put(FIELD_ABOUT, about);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public int getXP() {

		try {
			return getInt(FIELD_XP);
		} catch (JSONException e) {
			if (has(FIELD_XP)) {
				e.printStackTrace();
			}
			return 0;
		}

	}

	public void setXP(int xp) {
		try {
			put(FIELD_XP, xp);
			setModified(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
