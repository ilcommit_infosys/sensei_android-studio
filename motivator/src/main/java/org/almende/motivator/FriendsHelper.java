package org.almende.motivator;

import android.content.Context;

import org.almende.motivator.dataaccess.FollowedFriendsDAO;
import org.almende.motivator.facebook.FacebookManager;
import org.almende.motivator.misc.CustomCallback;
import org.almende.motivator.models.User;

import java.util.ArrayList;

/**
 * Copyright (c) 2016 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 30-3-16
 *
 * @author Dominik Egger
 */
public class FriendsHelper {

	private static FriendsHelper instance;

	private final Context context;

	private FriendsHelper(Context context) {
		this.context = context;
	}

	public static FriendsHelper getInstance(Context context) {
		if (instance == null) {
			instance = new FriendsHelper(context.getApplicationContext());
		}
		return instance;
	}

	public void retreiveFacebookFriends(final CustomCallback callback) {

		if (Cookie.getInstance().facebookFriends != null) {
			callback.callback(Cookie.getInstance().facebookFriends);
		} else {
			FacebookManager.getFriendsOfUser(new CustomCallback() {
				@Override
				public Object callback(Object object) {
					if (object != null) {
						callback.callback(Cookie.getInstance().facebookFriends);
					} else {
						callback.callback(null);
					}
					return null;
				}
			});
		}
	}
}
