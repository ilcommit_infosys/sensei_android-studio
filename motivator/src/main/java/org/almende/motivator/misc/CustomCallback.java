package org.almende.motivator.misc;
/**
 * Created by Thijs on 19-3-14.
 */
import java.io.Serializable;

public interface CustomCallback extends Serializable {
    Object callback(Object object);
}