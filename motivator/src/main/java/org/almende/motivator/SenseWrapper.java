package org.almende.motivator;

import android.content.Context;
import android.util.Log;

import org.almende.motivator.models.Background;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.models.Conversation;
import org.almende.motivator.models.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import nl.sense_os.service.commonsense.SenseApi;
import nl.sense_os.service.constants.SenseDataTypes;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 23-10-15
 *
 * @author Dominik Egger
 */
public class SenseWrapper {

	private static final String TAG = SenseWrapper.class.getCanonicalName();

	private static final String MOTIVATION_BACKGROUND = "motivationBackground";
	private static final String MOTIVATION_PROFILE = "motivationProfile";
	private static final String MOTIVATION_SENT_CHALLENGES = "motivationSentChallenges";
	private static final String MOTIVATION_RECEIVED_CHALLENGES = "motivationReceivedChallenges";
	private static final String MOTIVATION_CONVERSATIONS = "motivationConversations";
	private static final String MOTIVATION_FOLLOWED_FRIENDS = "motivationFollowedFriends";

	private static String getSensor(Context context, String sensor) throws IOException, JSONException {
//		SenseApi.clearCache(context);

//		String deviceUuid = SenseApi.getDefaultDeviceUuid(context);

		String registered = SenseApi.getSensorId(context, sensor, sensor,
				SenseDataTypes.JSON, SenseApi.NO_DEVICE_UUID);

		if (registered == null) {
			SenseApi.clearCache(context);
			SenseApi.registerSensor(context, sensor, sensor, sensor,
					SenseDataTypes.JSON, "", null, SenseApi.NO_DEVICE_UUID);
			Log.d(TAG, "registered sensor " + sensor);
		}

		String urlString = SenseApi.getSensorUrl(context,
				sensor, sensor, SenseDataTypes.JSON, SenseApi.NO_DEVICE_UUID);

		return urlString;
	}

	private static User getProfile(Context context) {

		try {
			String cookie = SenseApi.getCookie(context);
			String urlString = getSensor(context, MOTIVATION_PROFILE);

			Map<String, String> response = SenseApi.request(context, urlString,
					null, cookie);

			String code = response.get(SenseApi.RESPONSE_CODE);
			Log.d(TAG, "Response code" + code);
			if (!code.equals("200")) {
				Log.w(TAG,
						"Failed to get sensor data from CommonSense! Response code: "
								+ code);
				throw new IOException("Incorrect response from CommonSense: "
						+ code);
			} else {
				JSONObject content = new JSONObject(response.get(SenseApi.RESPONSE_CONTENT));
				JSONArray data = content.getJSONArray("data");
				String userContent = data.getJSONObject(data.length() - 1).getString("value");
				return new User(userContent);
			}

		} catch (IOException | JSONException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}

	}

	public static boolean storeBackground(Context context, Background background) throws JSONException, IllegalAccessException, IOException {

		JSONObject sensorData = new JSONObject();
		JSONObject value = new JSONObject();
		value.put("value", background);
		sensorData.put("data", value);

		String cookie = SenseApi.getCookie(context);
		String urlString = getSensor(context, MOTIVATION_BACKGROUND);

		Map<String, String> response = SenseApi.request(context, urlString,
				sensorData, cookie);

		String code = response.get(SenseApi.RESPONSE_CODE);
		Log.d(TAG, "Response code" + code);
		if (!"201".equals(code)) {
			Log.w(TAG,
					"Failed to Send sensor data to CommonSense! Response code: "
							+ code);
			throw new IOException("Incorrect response from CommonSense: "
					+ code);
		} else {
			Log.d(TAG, "success");
		}

		return true;
	}

	public static boolean storeProfile(Context context, User user) throws JSONException, IllegalAccessException, IOException {

		JSONObject sensorData = new JSONObject();
		JSONObject value = new JSONObject();
		value.put("value", user);
		sensorData.put("data", value);

		String cookie = SenseApi.getCookie(context);
		String urlString = getSensor(context, MOTIVATION_PROFILE);

		Map<String, String> response = SenseApi.request(context, urlString,
				sensorData, cookie);

		String code = response.get(SenseApi.RESPONSE_CODE);
		Log.d(TAG, "Response code" + code);
		if (!"201".equals(code)) {
			Log.w(TAG,
					"Failed to Send sensor data to CommonSense! Response code: "
							+ code);
			throw new IOException("Incorrect response from CommonSense: "
					+ code);
		} else {
			Log.d(TAG, "success");
		}

		return true;
	}

	public static ArrayList<Challenge> getSentChallenges(Context context) {
		ArrayList<Challenge> list = new ArrayList<>();

		try {
			String cookie = SenseApi.getCookie(context);
			String urlString = getSensor(context, MOTIVATION_SENT_CHALLENGES);

			Map<String, String> response = SenseApi.request(context, urlString,
					null, cookie);

			String code = response.get(SenseApi.RESPONSE_CODE);
			Log.d(TAG, "Response code" + code);
			if (!code.equals("200")) {
				Log.w(TAG,
						"Failed to get sensor data from CommonSense! Response code: "
								+ code);
				throw new IOException("Incorrect response from CommonSense: "
						+ code);
			} else {
				JSONObject content = new JSONObject(response.get(SenseApi.RESPONSE_CONTENT));
				JSONArray data = content.getJSONArray("data");
				if (data.length() > 0) {

					// todo: DE this will probably fail once number of entries are too large for one
					// page request. better would be to let sense return results newest first,
					// but not possible in sdk??
					String challengesContent = data.getJSONObject(data.length() - 1).getString("value");
					JSONArray challenges = new JSONArray(challengesContent);

					for (int i = 0; i < challenges.length(); ++i) {
						Challenge challenge = new Challenge(challenges.getString(i));
						challenge.setModified(false);
						list.add(challenge);
					}
				}
			}

		} catch (IOException | JSONException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return list;
	}

	public static void setSentChallenges(Context context, ArrayList<Challenge> sentChallenges) throws IOException, JSONException, IllegalAccessException {

		JSONArray challenges = new JSONArray();

		for (Challenge challenge : sentChallenges) {
			challenges.put(challenge);
		}

		JSONObject sensorData = new JSONObject();
		JSONObject value = new JSONObject();
		value.put("value", challenges);
		sensorData.put("data", value);
		String cookie = SenseApi.getCookie(context);

		String urlString = getSensor(context, MOTIVATION_SENT_CHALLENGES);

		Map<String, String> response = SenseApi.request(context, urlString,
				sensorData, cookie);

		String code = response.get(SenseApi.RESPONSE_CODE);
		Log.d(TAG, "Response code" + code);
		if (!"201".equals(code)) {
			Log.w(TAG,
					"Failed to Send sensor data to CommonSense! Response code: "
							+ code);
			throw new IOException("Incorrect response from CommonSense: "
					+ code);
		} else {
			Log.d(TAG, "success");
		}
	}


	public static ArrayList<Challenge> getReceivedChallenges(Context context) {
		ArrayList<Challenge> list = new ArrayList<>();

//		String deviceUuid = SenseApi.getDefaultDeviceUuid(context);

		try {
			String cookie = SenseApi.getCookie(context);

			String urlString = getSensor(context, MOTIVATION_RECEIVED_CHALLENGES);

			Map<String, String> response = SenseApi.request(context, urlString,
					null, cookie);

			String code = response.get(SenseApi.RESPONSE_CODE);
			Log.d(TAG, "Response code" + code);
			if (!code.equals("200")) {
				Log.w(TAG,
						"Failed to get sensor data from CommonSense! Response code: "
								+ code);
				throw new IOException("Incorrect response from CommonSense: "
						+ code);
			} else {
				JSONObject content = new JSONObject(response.get(SenseApi.RESPONSE_CONTENT));
				JSONArray data = content.getJSONArray("data");

				if (data.length() > 0) {

					// todo: DE this will probably fail once number of entries are too large for one
					// page request. better would be to let sense return results newest first,
					// but not possible in sdk??
					String challengesContent = data.getJSONObject(data.length() - 1).getString("value");
					JSONArray challenges = new JSONArray(challengesContent);

					for (int i = 0; i < challenges.length(); ++i) {
						Challenge challenge = new Challenge(challenges.getString(i));
						challenge.setModified(false);
						list.add(challenge);
					}
				}
			}
//			}

		} catch (IOException | JSONException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return list;
	}

	public static void setReceivedChallenges(Context context, ArrayList<Challenge> receivedChallenges) throws IOException, JSONException, IllegalAccessException {

		JSONArray challenges = new JSONArray();

		for (Challenge challenge : receivedChallenges) {
			challenges.put(challenge);
		}

		JSONObject sensorData = new JSONObject();
		JSONObject value = new JSONObject();
		value.put("value", challenges);
		sensorData.put("data", value);
		String cookie = SenseApi.getCookie(context);

		String urlString = getSensor(context, MOTIVATION_RECEIVED_CHALLENGES);

		Map<String, String> response = SenseApi.request(context, urlString,
				sensorData, cookie);

		String code = response.get(SenseApi.RESPONSE_CODE);
		Log.d(TAG, "Response code" + code);
		if (!"201".equals(code)) {
			Log.w(TAG,
					"Failed to Send sensor data to CommonSense! Response code: "
							+ code);
			throw new IOException("Incorrect response from CommonSense: "
					+ code);
		} else {
			Log.d(TAG, "success");
		}
	}

	public static ArrayList<Conversation> getConversations(Context context) {

		ArrayList<Conversation> list = new ArrayList<>();

//		String deviceUuid = SenseApi.getDefaultDeviceUuid(context);


		try {
			String cookie = SenseApi.getCookie(context);

			String urlString = getSensor(context, MOTIVATION_CONVERSATIONS);

			Map<String, String> response = SenseApi.request(context, urlString,
					null, cookie);

			String code = response.get(SenseApi.RESPONSE_CODE);
			Log.d(TAG, "Response code" + code);
			if (!code.equals("200")) {
				Log.w(TAG,
						"Failed to get sensor data from CommonSense! Response code: "
								+ code);
				throw new IOException("Incorrect response from CommonSense: "
						+ code);
			} else {
				JSONObject content = new JSONObject(response.get(SenseApi.RESPONSE_CONTENT));
				JSONArray data = content.getJSONArray("data");

				if (data.length() > 0) {

					// todo: DE this will probably fail once number of entries are too large for one
					// page request. better would be to let sense return results newest first,
					// but not possible in sdk??
					String conversationsContent = data.getJSONObject(data.length() - 1).getString("value");
					JSONArray conversations = new JSONArray(conversationsContent);

					for (int i = 0; i < conversations.length(); ++i) {
						Conversation conversation = new Conversation(conversations.getString(i));
						list.add(conversation);
					}
				}
			}

		} catch (IOException | JSONException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return list;
	}

	public static void setConversations(Context context, ArrayList<Conversation> conversations) throws IOException, JSONException, IllegalAccessException {

		JSONArray conversationsJson = new JSONArray();

		for (Conversation conversation : conversations) {
			conversationsJson.put(conversation);
		}

		JSONObject sensorData = new JSONObject();
		JSONObject value = new JSONObject();
		value.put("value", conversationsJson);
		sensorData.put("data", value);
		String cookie = SenseApi.getCookie(context);

		String urlString = getSensor(context, MOTIVATION_CONVERSATIONS);

		Map<String, String> response = SenseApi.request(context, urlString,
				sensorData, cookie);

		String code = response.get(SenseApi.RESPONSE_CODE);
		Log.d(TAG, "Response code" + code);
		if (!"201".equals(code)) {
			Log.w(TAG,
					"Failed to Send sensor data to CommonSense! Response code: "
							+ code);
			throw new IOException("Incorrect response from CommonSense: "
					+ code);
		} else {
			Log.d(TAG, "success");
		}
	}

	public static ArrayList<User> getFollowedFriends(Context context) {
		ArrayList<User> list = new ArrayList<>();

//		String deviceUuid = SenseApi.getDefaultDeviceUuid(context);

		try {
			String cookie = SenseApi.getCookie(context);

			String urlString = getSensor(context, MOTIVATION_FOLLOWED_FRIENDS);

			Map<String, String> response = SenseApi.request(context, urlString,
					null, cookie);

			String code = response.get(SenseApi.RESPONSE_CODE);
			Log.d(TAG, "Response code" + code);
			if (!code.equals("200")) {
				Log.w(TAG,
						"Failed to get sensor data from CommonSense! Response code: "
								+ code);
				throw new IOException("Incorrect response from CommonSense: "
						+ code);
			} else {
				JSONObject content = new JSONObject(response.get(SenseApi.RESPONSE_CONTENT));
				JSONArray data = content.getJSONArray("data");

				if (data.length() > 0) {
					// todo: DE this will probably fail once number of entries are too large for one
					// page request. better would be to let sense return results newest first,
					// but not possible in sdk??
					String followedFriendsContent = data.getJSONObject(data.length() - 1).getString("value");
					JSONArray followedFriends = new JSONArray(followedFriendsContent);

					for (int i = 0; i < followedFriends.length(); ++i) {
						User user = new User(followedFriends.getString(i));
						list.add(user);
					}
				}
			}

		} catch (IOException | JSONException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return list;
	}

	public static void setFollowedFriends(Context context, ArrayList<User> followedFriends) throws JSONException, IllegalAccessException, IOException {

		JSONArray conversationsJson = new JSONArray();

		for (User friend : followedFriends) {
			conversationsJson.put(friend);
		}

		JSONObject sensorData = new JSONObject();
		JSONObject value = new JSONObject();
		value.put("value", conversationsJson);
		sensorData.put("data", value);
		String cookie = SenseApi.getCookie(context);

		String urlString = getSensor(context, MOTIVATION_FOLLOWED_FRIENDS);

		Map<String, String> response = SenseApi.request(context, urlString,
				sensorData, cookie);

		String code = response.get(SenseApi.RESPONSE_CODE);
		Log.d(TAG, "Response code" + code);
		if (!"201".equals(code)) {
			Log.w(TAG,
					"Failed to Send sensor data to CommonSense! Response code: "
							+ code);
			throw new IOException("Incorrect response from CommonSense: "
					+ code);
		} else {
			Log.d(TAG, "success");
		}
	}

}
