package org.almende.motivator.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import org.almende.motivator.models.User;
import org.hva.createit.scl.dataaccess.AbstractDataAccessObject;
import org.hva.createit.scl.dataaccess.Database;
import org.hva.createit.scl.dataaccess.DatabaseHelper;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class FollowedFriendsDAO extends AbstractDataAccessObject<FollowedFriendsData> {

	private String TAG = FollowedFriendsDAO.class.getCanonicalName();

	private String myTable = Database.FollowedFriends.TABLE_FOLLOWED_FRIENDS;

	public FollowedFriendsDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();
	}

	@Override
	public void store(FollowedFriendsData data) {
		ContentValues values = followedFriendsToContentValues(data);
		long id = database.insert(myTable, null, values);
		data.setId(id);
//		data.setModified(false);
	}

	@Override
	public void update(FollowedFriendsData data) {
		ContentValues values = followedFriendsToContentValues(data);
		database.update(myTable, values, "_id" + "=?",
				new String[]{String.valueOf(data.getId())});
//		user.setModified(false);
	}

	public FollowedFriendsData getFollowedFriends() {

		database.beginTransaction();
		FollowedFriendsData followedFriendsData = null;
		try {
			followedFriendsData = getLastFollowedFriendsData(false);
//			followedFriendsData = fillIfEmpty(followedFriendsData);
			database.setTransactionSuccessful();
		} catch (NullPointerException e) {
			Log.e(TAG, e.toString());
		} finally {
			database.endTransaction();
		}
		return followedFriendsData;
	}

	public boolean hasUnuploadedItems() {
		return DatabaseUtils.queryNumEntries(database, myTable, "uploaded" + "=?", new String[] { "0" }) > 0;
//		return DatabaseUtils.queryNumEntries(database, myTable, "uploaded" + "=0", null) > 0;
	}
	
	@Override
	public List<FollowedFriendsData> getUnuploadedItems() {
		ArrayList<FollowedFriendsData> result = new ArrayList<>();
		if (hasUnuploadedItems()) {
			result.add(getLastFollowedFriendsData(true));
		}
		return result;
	}

	private FollowedFriendsData getLastFollowedFriendsData(boolean unuploaded) {
		FollowedFriendsData data = new FollowedFriendsData();

		Cursor cursor;
		if (unuploaded) {
			cursor = database.query(myTable, null, "uploaded" + "=?", new String[]{"0"}, null, null, null);
		} else {
			cursor = database.query(myTable, null, null, null, null, null, null);
		}
		if (cursor != null) {
			cursor.moveToLast();
			while (!cursor.isAfterLast()) {
				cursorToFollowedFriends(data, cursor);
				cursor.moveToNext();
			}
		}

		return data;
	}

	@Override
	public List<FollowedFriendsData> getItems(long fromIndex, long toIndex) {
		return null;
	}

	private ContentValues followedFriendsToContentValues(FollowedFriendsData data) {
		ArrayList<User> list = data.getFollowedFriends();

		JSONArray array = new JSONArray();
		for (User user : list) {
			array.put(user.toString());
		}

		ContentValues values = new ContentValues();

		// these fields are mandatory for any user
		values.put("list", array.toString());
		values.put("uploaded", data.isUploaded());

		return values;
	}

	private void cursorToFollowedFriends(FollowedFriendsData data, Cursor cursor) {
		if (cursor != null) {
			try {
				String list = cursor.getString(cursor.getColumnIndexOrThrow("list"));
				JSONArray array = new JSONArray(list);
				for (int i = 0; i < array.length(); ++i) {
					User user = new User(array.getString(i));
					data.addFriend(user);
				}

				data.setId(cursor.getInt(cursor.getColumnIndexOrThrow("_id")));
				data.setUploaded(cursor.getInt(cursor.getColumnIndexOrThrow("uploaded")) == 1);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

}
