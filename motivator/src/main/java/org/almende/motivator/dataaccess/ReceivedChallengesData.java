package org.almende.motivator.dataaccess;

import org.almende.motivator.models.Challenge;

import java.util.ArrayList;

public class ReceivedChallengesData {
	private String sensorName = "motivationReceivedChallenges";

	private ArrayList<Challenge> list;

	public ReceivedChallengesData() {
		list = new ArrayList<>();
	}

	public void addChallenge(Challenge challenge) {
		list.add(challenge);
	}

	public void updateChallenge(Challenge challenge) {

		for (int i = 0; i < list.size(); ++i) {
			Challenge item = list.get(i);
			if (item.getChallengeId().equals(challenge.getChallengeId())) {
				list.set(i, challenge);
			}
		}
	}

	public ArrayList<Challenge> getChallenges() {
		return list;
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public boolean isUploaded() {
		for (Challenge challenge : list) {
			if (!challenge.isUploaded()) {
				return false;
			}
		}
		return true;
	}

	public void setUploaded(boolean uploaded) {
		for (Challenge challenge : list) {
			challenge.setUploaded(uploaded);
		}
	}

}
