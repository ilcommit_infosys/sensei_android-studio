package org.almende.motivator.dataaccess;

import org.almende.motivator.models.User;

import java.util.ArrayList;
import java.util.HashMap;

public class FollowedFriendsData {
	private String sensorName = "motivationFollowedFriends";

	private HashMap<String, User> map = null;

	private boolean _uploaded = true;
	private long _id;

	public FollowedFriendsData() {
		map = new HashMap<>();
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public boolean isUploaded() {
		return _uploaded;
	}

	public void setUploaded(boolean uploaded) {
		_uploaded = uploaded;
	}

	public void addFriend(User fiend) {
		map.put(fiend.getFacebookId(), fiend);
		setUploaded(false);
	}

	public ArrayList<User> getFollowedFriends() {
		return new ArrayList<>(map.values());
	}

	public boolean isFriendFollowed(String friendId) {
		return map.containsKey(friendId);
	}

	public void removeFriend(User friend) {
		map.remove(friend.getFacebookId());
		setUploaded(false);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		this._id = id;
	}
}
