package org.almende.motivator.dataaccess;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import org.almende.motivator.models.Challenge;
import org.hva.createit.scl.dataaccess.AbstractDataAccessObject;
import org.hva.createit.scl.dataaccess.Database;
import org.hva.createit.scl.dataaccess.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class ReceivedChallengesDAO extends AbstractDataAccessObject<ReceivedChallengesData> {

	private String TAG = ReceivedChallengesDAO.class.getCanonicalName();

	private String myTable = Database.ReceivedChallenges.TABLE_RECEIVEDCHALLENGES;

	private ChallengeDAO cd;

	public ReceivedChallengesDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();

		cd = new ChallengeDAO(context, myTable);
	}

	@Override
	public void store(ReceivedChallengesData receivedChallenges) {
//		for (Challenge challenge : receivedChallenges.getChallenges()) {
//			cd.store(challenge);
//		}
		update(receivedChallenges);
	}

	@Override
	public void update(ReceivedChallengesData receivedChallenges) {
		for (Challenge challenge : receivedChallenges.getChallenges()) {
			if (challenge.getDbId() == -1) {
				cd.store(challenge);
			} else if (challenge.isModified()) {
				cd.update(challenge);
			}
		}
	}

//	public void update(Challenge challenge) {
//
//	}

	public ReceivedChallengesData getReceivedChallenges() {

		database.beginTransaction();
		ReceivedChallengesData receivedChallengesData = null;
		try {
			receivedChallengesData = getReceivedChallengesData().get(0);
//			receivedChallengesData = fillIfEmpty(receivedChallengesData);
			database.setTransactionSuccessful();
		} catch (NullPointerException e) {
			Log.e(TAG, e.toString());
		} finally {
			database.endTransaction();
		}
		return receivedChallengesData;
	}

	public boolean hasUnuploadedItems() {
		return DatabaseUtils.queryNumEntries(database, myTable, Challenge.FIELD_UPLOADED + "=?", new String[] { "0" }) > 0;
	}
	
	@Override
	public List<ReceivedChallengesData> getUnuploadedItems() {
		if (hasUnuploadedItems()) {
			return getReceivedChallengesData();
		} else {
			return new ArrayList<>();
		}
	}

	public List<ReceivedChallengesData> getReceivedChallengesData() {
		List<ReceivedChallengesData> receivedChallengesData = new ArrayList<>();
		ReceivedChallengesData data = new ReceivedChallengesData();

		Cursor cursor = database.query(myTable, new String[]{ Challenge.FIELD_ID }, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				data.addChallenge(cd.getChallengeData(cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_ID))));
				cursor.moveToNext();
			}
		}

		receivedChallengesData.add(data);
		return receivedChallengesData;
	}

	@Override
	public List<ReceivedChallengesData> getItems(long fromIndex, long toIndex) {
		return null;
	}

}
