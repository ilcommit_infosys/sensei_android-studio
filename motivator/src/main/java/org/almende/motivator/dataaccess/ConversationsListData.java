package org.almende.motivator.dataaccess;

import org.almende.motivator.models.Conversation;
import org.almende.motivator.models.Message;

import java.util.ArrayList;
import java.util.HashMap;

public class ConversationsListData {
	private String sensorName = "motivationConversations";

	private HashMap<String, Conversation> map;

	public ConversationsListData() {
		map = new HashMap<>();
	}

	public ArrayList<Conversation> getConversations() {
		return new ArrayList<>(map.values());
	}


	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public boolean isUploaded() {
		for (Conversation conversation : map.values()) {
			if (!conversation.isUploaded()) {
				return false;
			}
		}
		return true;
	}

	public void setUploaded(boolean uploaded) {
		for (Conversation conversation : map.values()) {
			conversation.setUploaded(uploaded);
		}
	}

	public boolean hasConversation(String partnerId) {
		return map.containsKey(partnerId);
	}

	public void addConversation(Conversation conversation) {
		map.put(conversation.getPartnerId(), conversation);
	}

	public Conversation getConversation(String partnerId) {
		return map.get(partnerId);
	}

	public void addMessage(String partnerId, Message message) {
		Conversation conversation = getConversation(partnerId);
		conversation.addMessage(message);
	}
}
