package org.almende.motivator.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import org.almende.motivator.models.Notification;
import org.almende.motivator.models.Notification;
import org.hva.createit.scl.dataaccess.AbstractDataAccessObject;
import org.hva.createit.scl.dataaccess.DatabaseHelper;

import java.util.Date;
import java.util.List;

public class NotificationDAO extends AbstractDataAccessObject<Notification> {

	private static final String TAG = NotificationDAO.class.getCanonicalName();

	private String tableName;

	public NotificationDAO(Context context, String tableName) {
		super(DatabaseHelper.getHelper(context));

		this.tableName = tableName;

		open();
	}

	@Override
	public long getItemCount() {
		return 0;
	}

	@Override
	public void store(Notification acm) {
		ContentValues values = notificationToContentValues(acm);
		long id = database.insert(tableName, null, values);
		acm.setDbId(id);
		acm.setModified(false);
	}

	@Override
	public void update(Notification acm) {
		ContentValues values = notificationToContentValues(acm);
		database.update(tableName, values, Notification.FIELD_ID + "=?",
				new String[]{String.valueOf(acm.getDbId())});
		acm.setModified(false);
	}

	public Notification getNotificationData(int id) {
		Notification data = null;

		Cursor cursor = database.query(tableName, null,
				Notification.FIELD_ID + "=" + id,
				null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			if (!cursor.isAfterLast()) {
				data = cursorToNotificationData(cursor);
			}
			cursor.close();
		}

		return data;
	}

//	private ContentValues notificationToContentValues(JSONObject json) {
//		ContentValues values = new ContentValues();
//		Iterator<?> keys = json.keys();
//		while (keys.hasNext()) {
//			String key = (String) keys.next();
//			try {
//				Object value = json.get(key);
//				if (value instanceof String) {
//					values.put(key, (String) value);
//				} else if (value instanceof Long) {
//					values.put(key, (Long) value);
//				} else if (value instanceof Integer) {
//					values.put(key, (Integer) value);
//				} else if (value instanceof Boolean) {
//					values.put(key, (Boolean) value);
//				} else {
//					Log.e(TAG, "unsupported value type!!!");
//				}
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//		}
//		return values;
//	}

	private ContentValues notificationToContentValues(Notification notification) {
		ContentValues values = new ContentValues();

		// these fields are mandatory and are filled at Notification creation
		values.put(Notification.FIELD_NOTIFIER_ID, notification.getNotifierId());
		values.put(Notification.FIELD_NOTIFIER_NAME, notification.getNotifierName());
		values.put(Notification.FIELD_MESSAGE, notification.getMessage());
		values.put(Notification.FIELD_NEW, notification.isNew());

		values.put(Notification.FIELD_UPLOADED, notification.isUploaded());

		return values;
	}

	public Notification cursorToNotificationData(Cursor cursor) {

		Notification data = new Notification();
		if (cursor != null) {
			// these fields are mandatory and are filled at Notification creation
			data.setDbId(cursor.getInt(cursor.getColumnIndexOrThrow(Notification.FIELD_ID)));
			data.setNotifierId(cursor.getString(cursor.getColumnIndexOrThrow(Notification.FIELD_NOTIFIER_ID)));
			data.setNotifierName(cursor.getString(cursor.getColumnIndexOrThrow(Notification.FIELD_NOTIFIER_NAME)));
			data.setMessage(cursor.getString(cursor.getColumnIndexOrThrow(Notification.FIELD_MESSAGE)));
			data.setNew(cursor.getInt(cursor.getColumnIndexOrThrow(Notification.FIELD_NEW)) == 1);

			// needs to be at the end
			data.setUploaded(cursor.getInt(cursor.getColumnIndexOrThrow(Notification.FIELD_UPLOADED)) == 1);
			data.setModified(false);
		}
		return data;
	}

	@Override
	public List<Notification> getItems(long fromIndex, long toIndex) {
		return null;
	}

}

