package org.almende.motivator.dataaccess;

import org.almende.motivator.models.Challenge;

import java.util.ArrayList;

public class ChallengesListData {

	private static final String SENT_CHALLENGES_SENSOR_NAME = "motivationSentChallenges";
	private static final String RECEIVED_CHALLENGES_SENSOR_NAME = "motivationReceivedChallenges";

	private ArrayList<Challenge> _sentChallenges;
	private ArrayList<Challenge> _receivedChallenges;

	public ChallengesListData() {
		_sentChallenges = new ArrayList<>();
		_receivedChallenges = new ArrayList<>();
	}

	public void addSentChallenge(Challenge challenge) {
		_sentChallenges.add(challenge);
	}

	public void addReceivedChallenge(Challenge challenge) {
		_receivedChallenges.add(challenge);
	}

	public void updateChallenge(Challenge challenge) {

		for (int i = 0; i < _receivedChallenges.size(); ++i) {
			Challenge item = _receivedChallenges.get(i);
			if (item.getChallengeId().equals(challenge.getChallengeId())) {
				_receivedChallenges.set(i, challenge);
				return;
			}
		}

		for (int i = 0; i < _sentChallenges.size(); ++i) {
			Challenge item = _sentChallenges.get(i);
			if (item.getChallengeId().equals(challenge.getChallengeId())) {
				_sentChallenges.set(i, challenge);
				return;
			}
		}
	}

	public Challenge getChallenge(String challengeId) {

		for (Challenge item : _receivedChallenges) {
			if (item.getChallengeId().equals(challengeId)) {
				return item;
			}
		}

		for (Challenge item : _sentChallenges) {
			if (item.getChallengeId().equals(challengeId)) {
				return item;
			}
		}

		return null;
	}

	public ArrayList<Challenge> getSentChallenges() {
		return _sentChallenges;
	}

	public ArrayList<Challenge> getReceivedChallenges() {
		return _receivedChallenges;
	}

	public String getSentChallengesSensorName() {
		return SENT_CHALLENGES_SENSOR_NAME;
	}

	public String getReceivedChallengesSensorName() {
		return RECEIVED_CHALLENGES_SENSOR_NAME;
	}

	public boolean isUploaded() {
		for (Challenge challenge : _receivedChallenges) {
			if (!challenge.isUploaded()) {
				return false;
			}
		}

		for (Challenge challenge : _sentChallenges) {
			if (!challenge.isUploaded()) {
				return false;
			}
		}

		return true;
	}

	public boolean isReceivedChallengesUploaded() {
		for (Challenge challenge : _receivedChallenges) {
			if (!challenge.isUploaded()) {
				return false;
			}
		}
		return true;
	}

	public boolean isSentChallengesUploaded() {
		for (Challenge challenge : _sentChallenges) {
			if (!challenge.isUploaded()) {
				return false;
			}
		}
		return true;
	}

	public void setUploaded(boolean uploaded) {
		for (Challenge challenge : _receivedChallenges) {
			challenge.setUploaded(uploaded);
		}

		for (Challenge challenge : _sentChallenges) {
			challenge.setUploaded(uploaded);
		}
	}

}
