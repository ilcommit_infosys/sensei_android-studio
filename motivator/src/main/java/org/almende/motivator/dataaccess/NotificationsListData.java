package org.almende.motivator.dataaccess;

import android.util.Log;

import org.almende.motivator.models.Notification;

import java.util.ArrayList;

public class NotificationsListData {
	private String sensorName = "motivationNotifications";

	private ArrayList<Notification> list;

	public NotificationsListData() {
		list = new ArrayList<>();
	}

	public ArrayList<Notification> getNotifications() {
		return list;
	}

	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public boolean isUploaded() {
		for (Notification no : list) {
			if (!no.isUploaded()) {
				return false;
			}
		}
		return true;
	}

	public void setUploaded(boolean uploaded) {
		for (Notification notification : list) {
			notification.setUploaded(uploaded);
		}
	}

//	public boolean hasConversation(String partnerId) {
//		return list.containsKey(partnerId);
//	}

	public void addNotification(Notification notification) {
		list.add(notification);
	}

	public void updateNotification(Notification notification) {
		Log.i("lala", "lala");
	}

//	public Conversation getNotification(String partnerId) {
//		return list.get(partnerId);
//	}

//	public void addMessage(String partnerId, Message message) {
//		Conversation conversation = getConversation(partnerId);
//		conversation.addMessage(message);
//	}
}
