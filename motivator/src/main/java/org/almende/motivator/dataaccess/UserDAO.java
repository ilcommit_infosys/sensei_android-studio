package org.almende.motivator.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import org.almende.motivator.models.User;
import org.hva.createit.scl.dataaccess.AbstractDataAccessObject;
import org.hva.createit.scl.dataaccess.DatabaseHelper;

import java.util.List;

public class UserDAO extends AbstractDataAccessObject<User> {

	private static final String TAG = UserDAO.class.getCanonicalName();

	private String tableName;

	public UserDAO(Context context, String tableName) {
		super(DatabaseHelper.getHelper(context));

		this.tableName = tableName;

		open();
	}

	@Override
	public long getItemCount() {
		return 0;
	}

	@Override
	public void store(User user) {
		ContentValues values = userToContentValues(user);
		long id = database.insert(tableName, null, values);
		user.setDbId(id);
		user.setModified(false);
	}

	@Override
	public void update(User user) {
		ContentValues values = userToContentValues(user);
		database.update(tableName, values, User.FIELD_ID + "=?",
				new String[]{String.valueOf(user.getDbId())});
		user.setModified(false);
	}

	public User getUserData(int id) {
		User data = null;

		Cursor cursor = database.query(tableName, null,
				User.FIELD_ID + "=" + id,
				null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			if (!cursor.isAfterLast()) {
				data = cursorToUser(cursor);
			}
			cursor.close();
		}

		return data;
	}

//	public User getFriendData(int id) {
//		User data = null;
//
//		Cursor cursor = database.query(tableName, null,
//				User.FIELD_ID + "=" + id,
//				null, null, null, null);
//		if (cursor != null) {
//			cursor.moveToFirst();
//			data = cursorToFriend(cursor);
//			cursor.close();
//		}
//
//		return data;
//	}

	private ContentValues userToContentValues(User user) {
		ContentValues values = new ContentValues();

		// these fields are mandatory for any user
		values.put(User.FIELD_FACEBOOK_ID, user.getFacebookId());
		values.put(User.FIELD_NAME, user.getName());

		values.put(User.FIELD_UPLOADED, user.isUploaded());

//		// these fields are only present for the current user (profile)
//		if (user.has(User.FIELD_AGE)) {
//			values.put(User.FIELD_AGE, user.getAge());
//		}
//		if (user.has(User.FIELD_GOAL)) {
//			values.put(User.FIELD_GOAL, user.getGoal());
//		}
//		if (user.has(User.FIELD_CITY)) {
//			values.put(User.FIELD_CITY, user.getCity());
//		}
//		if (user.has(User.FIELD_SPORTS)) {
//			values.put(User.FIELD_SPORTS, user.getSports());
//		}
//		if (user.has(User.FIELD_ABOUT)) {
//			values.put(User.FIELD_ABOUT, user.getAbout());
//		}
//		if (user.has(User.FIELD_XP)) {
//			values.put(User.FIELD_XP, user.getXP());
//		}

		return values;
	}

//	public User cursorToUser(Cursor cursor) {
//
//		User user = new User();
//		if (cursor != null) {
//			// these fields are mandatory for any user
//			user.setDbId(cursor.getInt(cursor.getColumnIndexOrThrow(User.FIELD_ID)));
//			user.setFacebookId(cursor.getString(cursor.getColumnIndexOrThrow(User.FIELD_FACEBOOK_ID)));
//			user.setName(cursor.getString(cursor.getColumnIndexOrThrow(User.FIELD_NAME)));
//
//			user.setUploaded(cursor.getInt(cursor.getColumnIndexOrThrow(User.FIELD_UPLOADED)) == 1);
//
//			// these fields are only present for the current user (profile)
//			user.setAge(cursor.getInt(cursor.getColumnIndexOrThrow(User.FIELD_AGE)));
//			user.setGoal(cursor.getString(cursor.getColumnIndexOrThrow(User.FIELD_GOAL)));
//			user.setCity(cursor.getString(cursor.getColumnIndexOrThrow(User.FIELD_CITY)));
//			user.setSports(cursor.getString(cursor.getColumnIndexOrThrow(User.FIELD_SPORTS)));
//			user.setAbout(cursor.getString(cursor.getColumnIndexOrThrow(User.FIELD_ABOUT)));
//			user.setXP(cursor.getInt(cursor.getColumnIndexOrThrow(User.FIELD_XP)));
//
//			user.setModified(false);
//		}
//		return user;
//	}

	public User cursorToUser(Cursor cursor) {

		User user = new User();
		if (cursor != null) {
			// these fields are mandatory for any user
			user.setDbId(cursor.getInt(cursor.getColumnIndexOrThrow(User.FIELD_ID)));
			user.setFacebookId(cursor.getString(cursor.getColumnIndexOrThrow(User.FIELD_FACEBOOK_ID)));
			user.setName(cursor.getString(cursor.getColumnIndexOrThrow(User.FIELD_NAME)));

			// needs to be at the end
			user.setUploaded(cursor.getInt(cursor.getColumnIndexOrThrow(User.FIELD_UPLOADED)) == 1);
			user.setModified(false);
		}
		return user;
	}

	@Override
	public List<User> getItems(long fromIndex, long toIndex) {
		return null;
	}

}

