package org.almende.motivator.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import org.almende.motivator.models.Challenge;
import org.hva.createit.scl.dataaccess.AbstractDataAccessObject;
import org.hva.createit.scl.dataaccess.DatabaseHelper;

import java.util.Date;
import java.util.List;

public class ChallengeDAO extends AbstractDataAccessObject<Challenge> {

	private static final String TAG = ChallengeDAO.class.getCanonicalName();

	private String tableName;

	public ChallengeDAO(Context context, String tableName) {
		super(DatabaseHelper.getHelper(context));

		this.tableName = tableName;

		open();
	}

	@Override
	public long getItemCount() {
		return 0;
	}

	@Override
	public void store(Challenge acm) {
		ContentValues values = challengeToContentValues(acm);
		long id = database.insert(tableName, null, values);
		acm.setDbId(id);
		acm.setModified(false);
	}

	@Override
	public void update(Challenge acm) {
		ContentValues values = challengeToContentValues(acm);
		database.update(tableName, values, Challenge.FIELD_ID + "=?",
				new String[]{String.valueOf(acm.getDbId())});
		acm.setModified(false);
	}

	public Challenge getChallengeData(int id) {
		Challenge data = null;

		Cursor cursor = database.query(tableName, null,
				Challenge.FIELD_ID + "=" + id,
				null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			if (!cursor.isAfterLast()) {
				data = cursorToChallengeData(cursor);
			}
			cursor.close();
		}

		return data;
	}

//	private ContentValues challengeToContentValues(JSONObject json) {
//		ContentValues values = new ContentValues();
//		Iterator<?> keys = json.keys();
//		while (keys.hasNext()) {
//			String key = (String) keys.next();
//			try {
//				Object value = json.get(key);
//				if (value instanceof String) {
//					values.put(key, (String) value);
//				} else if (value instanceof Long) {
//					values.put(key, (Long) value);
//				} else if (value instanceof Integer) {
//					values.put(key, (Integer) value);
//				} else if (value instanceof Boolean) {
//					values.put(key, (Boolean) value);
//				} else {
//					Log.e(TAG, "unsupported value type!!!");
//				}
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//		}
//		return values;
//	}

	private ContentValues challengeToContentValues(Challenge challenge) {
		ContentValues values = new ContentValues();

		// these fields are mandatory and are filled at challenge creation
		values.put(Challenge.FIELD_START_DATE, challenge.getStartDate().getTime());
		values.put(Challenge.FIELD_TITLE, challenge.getTitle());
		values.put(Challenge.FIELD_CONTENT, challenge.getContent());
		values.put(Challenge.FIELD_REWARD, challenge.getReward());
		values.put(Challenge.FIELD_EVIDENCE_AMOUNT, challenge.getEvidenceAmount());
		values.put(Challenge.FIELD_EVIDENCE_TYPE, challenge.getEvidenceType());
		values.put(Challenge.FIELD_XP_REWARD, challenge.getXpReward());
		values.put(Challenge.FIELD_STATUS, challenge.getStatus());
		values.put(Challenge.FIELD_CHALLENGE_ID, challenge.getChallengeId());
		values.put(Challenge.FIELD_CHALLENGER, challenge.getChallenger());
		values.put(Challenge.FIELD_CHALLENGEE, challenge.getChallengee());
		values.put(Challenge.FIELD_CHALLENGER_NAME, challenge.getChallengerName());
		values.put(Challenge.FIELD_CHALLENGEE_NAME, challenge.getChallengeeName());

		values.put(Challenge.FIELD_UPDATE, challenge.hasUpdate());
		values.put(Challenge.FIELD_UPLOADED, challenge.isUploaded());

		// these fields are added as the challenge proceeds, so only add them if there is a value
		// added
		if (challenge.has(Challenge.FIELD_RATED_MESSAGE)) {
			values.put(Challenge.FIELD_RATED_MESSAGE, challenge.getRatedMessage());
		}
		if (challenge.has(Challenge.FIELD_RATED)) {
			values.put(Challenge.FIELD_RATED, challenge.getRated());
		}
		if (challenge.has(Challenge.FIELD_AMOUNT_HOURS)) {
			values.put(Challenge.FIELD_AMOUNT_HOURS, challenge.getAmountHours());
		}
		if (challenge.has(Challenge.FIELD_GPS)) {
			values.put(Challenge.FIELD_GPS, challenge.getGps());
		}
		if (challenge.has(Challenge.FIELD_LOGIN_DATE)) {
			values.put(Challenge.FIELD_LOGIN_DATE, challenge.getLoginDate().getTime());
		}
		if (challenge.has(Challenge.FIELD_END_DATE)) {
			values.put(Challenge.FIELD_END_DATE, challenge.getEndDate().getTime());
		}
//		if (challenge.has(Challenge.FIELD_SENSE_ID)) {
//			values.put(Challenge.FIELD_SENSE_ID, challenge.getSenseId());
//		}
		if (challenge.has(Challenge.FIELD_EVIDENCE)) {
			values.put(Challenge.FIELD_EVIDENCE, challenge.getEvidenceJson());
		}
		if (challenge.has(Challenge.FIELD_COMMENTS)) {
			values.put(Challenge.FIELD_COMMENTS, challenge.getCommentsJson());
		}
		return values;
	}

	public Challenge cursorToChallengeData(Cursor cursor) {

		Challenge data = new Challenge();
		if (cursor != null) {
			// these fields are mandatory and are filled at challenge creation
			data.setDbId(cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_ID)));
			data.setStartDate(new Date(cursor.getLong(cursor.getColumnIndexOrThrow(Challenge.FIELD_START_DATE))));
			data.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_TITLE)));
			data.setContent(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_CONTENT)));
			data.setReward(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_REWARD)));
			data.setEvidenceAmount(cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_EVIDENCE_AMOUNT)));
			data.setEvidenceType(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_EVIDENCE_TYPE)));
			data.setXpReward(cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_XP_REWARD)));
			data.setStatus(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_STATUS)));
			data.setChallengeId(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_CHALLENGE_ID)));
			data.setChallenger(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_CHALLENGER)));
			data.setChallengerName(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_CHALLENGER_NAME)));
			data.setChallengee(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_CHALLENGEE)));
			data.setChallengeeName(cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_CHALLENGEE_NAME)));

			data.setUpdate(cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_UPDATE)) == 1);

			// these fields are added as the challenge proceeds, so only add them if there is a meaningful value
			// stored in the DB
			String ratedMessage = cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_RATED_MESSAGE));
			if (ratedMessage != null) {
				data.setRatedMessage(ratedMessage);
			}
			String rated = cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_RATED));
			if (rated != null) {
				data.setRated(rated);
			}
			int amountHours = cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_AMOUNT_HOURS));
			if (amountHours > 0) {
				data.setAmountHours(amountHours);
			}
			String gps = cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_GPS));
			if (gps != null) {
				data.setGps(gps);
			}
			long loginDate = cursor.getLong(cursor.getColumnIndexOrThrow(Challenge.FIELD_LOGIN_DATE));
			if (loginDate != 0) {
				data.setLoginDate(new Date(loginDate));
			}
			long endDate = cursor.getLong(cursor.getColumnIndexOrThrow(Challenge.FIELD_END_DATE));
			if (endDate != 0) {
				data.setEndDate(new Date(endDate));
			}
//			String senseId = cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_SENSE_ID));
//			if (senseId != null) {
//				data.setSenseId(senseId);
//			}
			String evidence = cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_EVIDENCE));
			if (evidence != null) {
				data.setEvidenceJson(evidence);
			}
			String comments = cursor.getString(cursor.getColumnIndexOrThrow(Challenge.FIELD_COMMENTS));
			if (comments != null) {
				data.setCommentsJson(comments);
			}

			// needs to be at the end
			data.setUploaded(cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_UPLOADED)) == 1);
			data.setModified(false);
		}
		return data;
	}

	@Override
	public List<Challenge> getItems(long fromIndex, long toIndex) {
		return null;
	}

}

