package org.almende.motivator.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import org.almende.motivator.models.Conversation;
import org.almende.motivator.models.Message;
import org.hva.createit.scl.dataaccess.AbstractDataAccessObject;
import org.hva.createit.scl.dataaccess.Database;
import org.hva.createit.scl.dataaccess.DatabaseHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConversationListDAO extends AbstractDataAccessObject<ConversationsListData> {

	private String TAG = ConversationListDAO.class.getCanonicalName();

	private String myTable = Database.Conversations.TABLE_CONVERSATIONS;

	public ConversationListDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();
	}

	@Override
	public void store(ConversationsListData data) {
		update(data);
	}

	@Override
	public void update(ConversationsListData data) {
		for (Conversation conversation : data.getConversations()) {
			if (conversation.isModified()) {
				update(conversation);
			}
		}
	}

	private void update(Conversation conversation) {
		for (Message message : conversation.getMessages()) {

			if (message.getDbId() == -1) {
				store(conversation.getPartnerId(), conversation.getPartnerName(), message);
			} else if (message.isModified()) {
				update(conversation.getPartnerId(), conversation.getPartnerName(), message);
			}
		}
	}

	private void store(String partnerId, String partnerName, Message message) {
		ContentValues values = messageToContentValues(partnerId, partnerName, message);

		long id = database.insert(myTable, null, values);
		message.setDbId(id);
		message.setModified(false);
	}

	private void update(String partnerId, String partnerName, Message message) {
		ContentValues values = messageToContentValues(partnerId, partnerName, message);

		database.update(myTable, values, Message.FIELD_ID + "=?",
				new String[]{String.valueOf(message.getDbId())});
		message.setModified(false);
	}

//	public void update(Challenge challenge) {
//
//	}

	public ConversationsListData getConversations() {

		database.beginTransaction();
		ConversationsListData conversationsListData = null;
		try {
			conversationsListData = getConversationsListData();
//			conversationsListData = fillIfEmpty(conversationsListData);
			database.setTransactionSuccessful();
		} catch (NullPointerException e) {
			Log.e(TAG, e.toString());
		} finally {
			database.endTransaction();
		}
		return conversationsListData;
	}

	public boolean hasUnuploadedItems() {
		return DatabaseUtils.queryNumEntries(database, myTable, Message.FIELD_UPLOADED + "=?", new String[] { "0" }) > 0;
	}
	
	@Override
	public List<ConversationsListData> getUnuploadedItems() {
		ArrayList<ConversationsListData> result = new ArrayList<>();
		if (hasUnuploadedItems()) {
			result.add(getConversationsListData());
		}
		return result;
	}

	private ConversationsListData getConversationsListData() {
		ConversationsListData data = new ConversationsListData();

		Cursor cursor = database.query(myTable, null, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				String partnerId = cursor.getString(cursor.getColumnIndexOrThrow(Conversation.FIELD_PARTNER_ID));
				String partnerName = cursor.getString(cursor.getColumnIndexOrThrow(Conversation.FIELD_PARTNER_NAME));
				Message message = cursorToMessage(cursor);
				if (!data.hasConversation(partnerId)) {
					data.addConversation(new Conversation(partnerId, partnerName));
				}
				data.addMessage(partnerId, message);
				cursor.moveToNext();
			}
		}

		return data;
	}

	@Override
	public List<ConversationsListData> getItems(long fromIndex, long toIndex) {
		return null;
	}

	private ContentValues messageToContentValues(String partnerId, String partnerName, Message message) {
		ContentValues values = new ContentValues();

		values.put(Conversation.FIELD_PARTNER_ID, partnerId);
		values.put(Conversation.FIELD_PARTNER_NAME, partnerName);
		values.put(Message.FIELD_CATEGORY, message.getCatgeory());
		values.put(Message.FIELD_RECEIVER, message.getReceiver());
		values.put(Message.FIELD_AUTHOR, message.getAuthor());
		values.put(Message.FIELD_CONTENT, message.getContent());
		values.put(Message.FIELD_TITLE, message.getTitle());
		values.put(Message.FIELD_DATE, message.getDate().getTime());
		values.put(Message.FIELD_LIKED, message.getLiked());
		values.put(Message.FIELD_UPLOADED, message.isUploaded());

		return values;
	}

	private Message cursorToMessage(Cursor cursor) {

		Message message = new Message();
		if (cursor != null) {
			message.setDbId(cursor.getInt(cursor.getColumnIndexOrThrow(Message.FIELD_ID)));
			message.setCategory(cursor.getString(cursor.getColumnIndexOrThrow(Message.FIELD_CATEGORY)));
			message.setReceiver(cursor.getString(cursor.getColumnIndexOrThrow(Message.FIELD_RECEIVER)));
			message.setAuthor(cursor.getString(cursor.getColumnIndexOrThrow(Message.FIELD_AUTHOR)));
			message.setContent(cursor.getString(cursor.getColumnIndexOrThrow(Message.FIELD_CONTENT)));
			message.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(Message.FIELD_TITLE)));
			message.setDate(new Date(cursor.getLong(cursor.getColumnIndexOrThrow(Message.FIELD_DATE))));
			message.setLiked(cursor.getString(cursor.getColumnIndexOrThrow(Message.FIELD_LIKED)));

			// needs to be at the end
			message.setUploaded(cursor.getInt(cursor.getColumnIndexOrThrow(Message.FIELD_UPLOADED)) == 1);
			message.setModified(false);
		}
		return message;
	}

}
