package org.almende.motivator.dataaccess;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

//import org.hva.createit.scl.data.AffectData;
//import org.hva.createit.scl.data.MeasurementData;
//import org.almende.motivator.dataaccess.SentChallengesData;
import org.almende.motivator.models.Challenge;
import org.hva.createit.scl.dataaccess.AbstractDataAccessObject;
import org.hva.createit.scl.dataaccess.Database;
import org.hva.createit.scl.dataaccess.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class SentChallengesDAO extends AbstractDataAccessObject<SentChallengesData> {

	private String TAG = SentChallengesDAO.class.getCanonicalName();

	private String myTable = Database.Challenges.TABLE_SENTCHALLENGES;

	private ChallengeDAO cd;

	public SentChallengesDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();

		cd = new ChallengeDAO(context, myTable);
	}

	@Override
	public void store(SentChallengesData acm) {
//		for (Challenge challenge : acm.getChallenges()) {
//			cd.store(challenge);
//		}
		update(acm);
	}

	@Override
	public void update(SentChallengesData acm) {
		for (Challenge challenge : acm.getChallenges()) {
			if (challenge.getDbId() == -1) {
				cd.store(challenge);
			} else if (challenge.isModified()) {
				cd.update(challenge);
			}
		}
	}

//	public void update(Challenge challenge) {
//
//	}

	public SentChallengesData getSentChallenges() {

		database.beginTransaction();
		SentChallengesData sentChallengesData = null;
		try {
			sentChallengesData = getSentChallengesData();
//			sentChallengesData = fillIfEmpty(sentChallengesData);
			database.setTransactionSuccessful();
		} catch (NullPointerException e) {
			Log.e(TAG, e.toString());
		} finally {
			database.endTransaction();
		}
		return sentChallengesData;
	}

	public boolean hasUnuploadedItems() {
		return DatabaseUtils.queryNumEntries(database, myTable, Challenge.FIELD_UPLOADED + "=?", new String[] { "0" }) > 0;
	}
	
	@Override
	public List<SentChallengesData> getUnuploadedItems() {
		ArrayList<SentChallengesData> items = new ArrayList<>();
		if (hasUnuploadedItems()) {
			items.add(getSentChallengesData());
		}
		return items;
	}

	public SentChallengesData getSentChallengesData() {
		SentChallengesData data = new SentChallengesData();

		Cursor cursor = database.query(myTable, new String[]{ Challenge.FIELD_ID }, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				int id = cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_ID));
				data.addChallenge(cd.getChallengeData(id));
				cursor.moveToNext();
			}
		}

		return data;
	}

	@Override
	public List<SentChallengesData> getItems(long fromIndex, long toIndex) {
		return null;
	}

//	private SentChallengesData fillIfEmpty(SentChallengesData run) {
//		if (run.getAffect() == null) {
//			List<AffectData> af = new ArrayList<AffectData>();
//
//			try {
//				af.add(afd.getAffect(rdd.getRunDataStartBefore(
//						run.getStart_timestamp()).getTimestamp()));
//			} catch (NullPointerException e) {
//				Log.e(TAG, e.toString());
//				af.add(new AffectData(0, run.getStart_timestamp(), 0, 0, 0));
//			}
//			try {
//				af.add(afd.getAffect(rdd.getRunDataEndAfter(
//						run.getStop_timestamp()).getTimestamp()));
//			} catch (NullPointerException e) {
//				Log.e(TAG, e.toString());
//				af.add(new AffectData(0, run.getStop_timestamp(), 0, 0, 0));
//			}
//			run.setAffect(af);
//		}
//		if (run.getAverageHeartRateMeasurementData() == null) {
//			run.setAverageHeartRate(new MeasurementData(0, "averageHeartRate",
//					0, run.getStop_timestamp()));
//		}
//		if (run.getAverageStepFrequencyMeasurementData() == null) {
//			run.setAverageStepFrequency(new MeasurementData(0,
//					"averageStepFrequency", 0, run.getStop_timestamp()));
//		}
//		if (run.getAverageSpeedMeasurementData() == null) {
//			run.setAverageSpeed(new MeasurementData(0, "averageSpeed", 0, run
//					.getStop_timestamp()));
//		}
//		if (run.getDistanceMeasurementData() == null) {
//			run.setDistance(new MeasurementData(0, "distance", 0, run
//					.getStop_timestamp()));
//		}
//		if (run.getDurationMeasurementData() == null) {
//			run.setDuration(new MeasurementData(0, "duration", run
//					.getStop_timestamp() - run.getStart_timestamp(), run
//					.getStop_timestamp()));
//		}

//		return run;
//	}

//	@Override
//	public List<SentChallengesData> getItems(long fromIndex, long toIndex) {
//		return null;
//	}

//	private SentChallengesData getSentChallengesData() {
//		String[] values = { "1" };
//		Cursor cursor = database
//				.rawQuery(
//						"SELECT * FROM "
//								+ myTable
//								+ " ORDER BY "
//								+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP
//								+ " DESC " + "LIMIT ? " + ";", values);
//		SentChallengesData runOverview = null;
//		cursor.moveToFirst();
//		while (!cursor.isAfterLast()) {
//			runOverview = cursorToSentChallengesData(cursor);
//			cursor.moveToNext();
//		}
//		cursor.close();
//
//		return runOverview;
//	}

//	private SentChallengesData getSentChallengesData(int runId) {
//		String[] values = { "" + runId };
//		Cursor cursor = database
//				.rawQuery(
//						"SELECT * FROM "
//								+ myTable
//								+ " WHERE "
//								+ Database.MeasurementOverview._ID
//								+ " = ? "
//								+ " ORDER BY "
//								+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP
//								+ " DESC " + "LIMIT 1 " + ";", values);
//		SentChallengesData runOverview = null;
//		cursor.moveToFirst();
//		while (!cursor.isAfterLast()) {
//			runOverview = cursorToSentChallengesData(cursor);
//			cursor.moveToNext();
//		}
//		cursor.close();
//
//		return runOverview;
//	}

//	private List<SentChallengesData> getSentChallengesData(int limit) {
//		String[] values = { "" + limit };
//		Cursor cursor = database
//				.rawQuery(
//						"SELECT * FROM "
//								+ myTable
//								+ " ORDER BY "
//								+ Database.MeasurementOverview.COLUMN_MEASUREMENTOVERVIEW_START_TIMESTAMP
//								+ " DESC " + "LIMIT ? " + ";", values);
//		List<SentChallengesData> runOverview = new ArrayList<SentChallengesData>();
//		cursor.moveToFirst();
//		while (!cursor.isAfterLast()) {
//			runOverview.add(cursorToSentChallengesData(cursor));
//			cursor.moveToNext();
//		}
//		cursor.close();
//
//		return runOverview;
//	}

}
