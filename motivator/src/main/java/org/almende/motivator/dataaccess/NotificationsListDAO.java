package org.almende.motivator.dataaccess;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import org.almende.motivator.models.Notification;
import org.hva.createit.scl.dataaccess.AbstractDataAccessObject;
import org.hva.createit.scl.dataaccess.Database;
import org.hva.createit.scl.dataaccess.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class NotificationsListDAO extends AbstractDataAccessObject<NotificationsListData> {

	private String TAG = NotificationsListDAO.class.getCanonicalName();

	private String myTable = Database.Notifications.TABLE_NOTIFICATIONS;

	private NotificationDAO _notificationDAO;

	public NotificationsListDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();

		_notificationDAO = new NotificationDAO(context, myTable);
	}

	@Override
	public void store(NotificationsListData data) {
		update(data);
	}

	@Override
	public void update(NotificationsListData data) {
		for (Notification notification : data.getNotifications()) {
			if (notification.getDbId() == -1) {
				_notificationDAO.store(notification);
			} else if (notification.isModified()) {
				_notificationDAO.update(notification);
			}
		}
	}

	public NotificationsListData getNotifications() {

		database.beginTransaction();
		NotificationsListData notificationsListData = null;
		try {
			notificationsListData = getNotificationsListData();
//			notificationsListData = fillIfEmpty(notificationsListData);
			database.setTransactionSuccessful();
		} catch (NullPointerException e) {
			Log.e(TAG, e.toString());
		} finally {
			database.endTransaction();
		}
		return notificationsListData;
	}

	public boolean hasUnuploadedItems() {
		return DatabaseUtils.queryNumEntries(database, myTable, Notification.FIELD_UPLOADED + "=?", new String[] { "0" }) > 0;
	}

	@Override
	public List<NotificationsListData> getUnuploadedItems() {
		ArrayList<NotificationsListData> result = new ArrayList<>();
		if (hasUnuploadedItems()) {
			result.add(getNotificationsListData());
		}
		return result;
	}

	private NotificationsListData getNotificationsListData() {
		NotificationsListData data = new NotificationsListData();

		Cursor cursor = database.query(myTable, new String[]{ Notification.FIELD_ID }, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				int id = cursor.getInt(cursor.getColumnIndexOrThrow(Notification.FIELD_ID));
				data.addNotification(_notificationDAO.getNotificationData(id));
				cursor.moveToNext();
			}
		}

		return data;
	}

	@Override
	public List<NotificationsListData> getItems(long fromIndex, long toIndex) {
		return null;
	}

}
