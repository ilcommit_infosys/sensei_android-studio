package org.almende.motivator.dataaccess;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import org.almende.motivator.models.Challenge;
import org.hva.createit.scl.dataaccess.AbstractDataAccessObject;
import org.hva.createit.scl.dataaccess.Database;
import org.hva.createit.scl.dataaccess.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class ChallengesListDAO extends AbstractDataAccessObject<ChallengesListData> {

	private String TAG = ChallengesListDAO.class.getCanonicalName();

	private static final String SENT_CHALLENGES_TABLE = Database.Challenges.TABLE_SENTCHALLENGES;
	private static final String RECEIVED_CHALLENGES_TABLE = Database.ReceivedChallenges.TABLE_RECEIVEDCHALLENGES;

	private ChallengeDAO _sentDAO;
	private ChallengeDAO _receivedDAO;

	public ChallengesListDAO(Context context) {
		super(DatabaseHelper.getHelper(context));
		open();

		_sentDAO = new ChallengeDAO(context, SENT_CHALLENGES_TABLE);
		_receivedDAO = new ChallengeDAO(context, RECEIVED_CHALLENGES_TABLE);
	}

	@Override
	public void store(ChallengesListData data) {
//		for (Challenge challenge : acm.getChallenges()) {
//			_sentDAO.store(challenge);
//		}
		update(data);
	}

	@Override
	public void update(ChallengesListData data) {
		for (Challenge challenge : data.getReceivedChallenges()) {
			if (challenge.getDbId() == -1) {
				_receivedDAO.store(challenge);
			} else if (challenge.isModified()) {
				_receivedDAO.update(challenge);
			}
		}

		for (Challenge challenge : data.getSentChallenges()) {
			if (challenge.getDbId() == -1) {
				_sentDAO.store(challenge);
			} else if (challenge.isModified()) {
				_sentDAO.update(challenge);
			}
		}
	}

	public ChallengesListData getChallengesList() {

		database.beginTransaction();
		ChallengesListData challengesListData = null;
		try {
			challengesListData = getChallengesListData();
//			challengesListData = fillIfEmpty(challengesListData);
			database.setTransactionSuccessful();
		} catch (NullPointerException e) {
			Log.e(TAG, e.toString());
		} finally {
			database.endTransaction();
		}
		return challengesListData;
	}

	public boolean hasUnuploadedItems() {
		return DatabaseUtils.queryNumEntries(database, SENT_CHALLENGES_TABLE, Challenge.FIELD_UPLOADED + "=?", new String[] { "0" }) > 0
		    || DatabaseUtils.queryNumEntries(database, RECEIVED_CHALLENGES_TABLE, Challenge.FIELD_UPLOADED + "=?", new String[] { "0" }) > 0;

	}
	
	@Override
	public List<ChallengesListData> getUnuploadedItems() {
		ArrayList<ChallengesListData> items = new ArrayList<>();
		if (hasUnuploadedItems()) {
			items.add(getChallengesListData());
		}
		return items;
	}

	public ChallengesListData getChallengesListData() {
		ChallengesListData data = new ChallengesListData();
		fillSentChallengesData(data);
		fillReceivedChallengesData(data);
		return data;
	}

	public void fillSentChallengesData(ChallengesListData data) {

		Cursor cursor = database.query(SENT_CHALLENGES_TABLE, new String[]{ Challenge.FIELD_ID }, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				int id = cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_ID));
				data.addSentChallenge(_sentDAO.getChallengeData(id));
				cursor.moveToNext();
			}
		}
	}

	public void fillReceivedChallengesData(ChallengesListData data) {

		Cursor cursor = database.query(RECEIVED_CHALLENGES_TABLE, new String[]{ Challenge.FIELD_ID }, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				int id = cursor.getInt(cursor.getColumnIndexOrThrow(Challenge.FIELD_ID));
				data.addReceivedChallenge(_receivedDAO.getChallengeData(id));
				cursor.moveToNext();
			}
		}
	}

	@Override
	public List<ChallengesListData> getItems(long fromIndex, long toIndex) {
		return null;
	}

}
