package org.almende.motivator.utils;

import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 *
 * This is just a wrapper class for JSON objects used to store our models in the
 * local database and the Sense DB, in order to exclude certain fields from being
 * included in the Sense DB since they are only used for internal storage and upload
 * mechanisms.
 *
 * Could be done with Gson and transient fields, but that would add another dependency
 * I prefer to leave out for now. But maybe it is worth looking into.
 *
 * Created on 12-11-15
 *
 * @author Dominik Egger
 */
public abstract class JSONExcludeObject extends JSONObject {

	ArrayList<String> _excludeFields = new ArrayList<>();

	public JSONExcludeObject() {
		super();
		setExcludeFields();
	}

	public JSONExcludeObject(String json) throws JSONException {
		super(json);
		setExcludeFields();
	}

	private void setExcludeFields() {
		String[] fields = defineExcludeFields();
		if (fields != null) {
			_excludeFields.addAll(Arrays.asList(fields));
		}
	}

	@Nullable
	// return an array of field names with should be excluded when the
	// JSONObject is converted toString()
	// Note: return value can be null if no fields should be excluded,
	//   but then you can just as well use the JSONObject directly.
	protected abstract String[] defineExcludeFields();

	// Note: this overrides the writeTo function of the JSONObject
	//  even though there is no Override declaration, or anything...
	void writeTo(JSONStringer stringer) throws JSONException {
		stringer.object();
		Iterator<?> keys = keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (!_excludeFields.contains(key)) {
				Object value = get(key);
				stringer.key(key).value(value);
			}
		}
		stringer.endObject();
	}

}
