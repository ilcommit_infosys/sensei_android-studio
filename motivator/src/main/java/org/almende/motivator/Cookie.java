package org.almende.motivator;

//import com.facebook.model.GraphUser;

import org.almende.motivator.models.User;

import java.util.ArrayList;

/**
 * Created by AsterLaptop on 4/2/14.
 */
public class Cookie {

//    public String userEntryId;
    public ArrayList<User> facebookFriends = null;
//    public ArrayList<GraphUser> facebookFriends=null;

//    public String userName;
    public boolean internet = true;

    private static Cookie instance =null;

//    public User user;

    private Cookie(){}

    public static synchronized Cookie getInstance() {
        if(instance==null){
            instance=new Cookie();
        }
        return instance;
    }

}
