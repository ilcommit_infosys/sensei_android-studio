package org.almende.motivator;

import android.content.Context;
import android.content.SharedPreferences;

import org.almende.motivator.cfg.Config;
import org.almende.motivator.models.User;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 11-11-15
 *
 * @author Dominik Egger
 */
public class Profile {

	private static final String TAG = Profile.class.getCanonicalName();

	private static Profile instance;

	private static final String PREFS_NAME = "motivatorProfile";

	private static final String PREFS_FIELD_FACEBOOK_ID = "facebookId";
	private static final String PREFS_FIELD_NAME = "name";
	private static final String PREFS_FIELD_AGE = "age";
	private static final String PREFS_FIELD_GOAL = "goal";
	private static final String PREFS_FIELD_CITY = "city";
	private static final String PREFS_FIELD_SPORTS = "sports";
	private static final String PREFS_FIELD_ABOUT = "about";
	private static final String PREFS_FIELD_XP = "xp";
	private static final String PREFS_FIELD_UPLOADED = "uploaded";

	private SharedPreferences _settings;

	private boolean _downloadProfile = false;
	private boolean _profileInitialized = false;

	private User _thisUser;

	private Context context;
	private final SharedPreferences.Editor _editor;

	public static Profile getInstance(Context context) {
		if (instance == null || !instance._profileInitialized) {
			instance = new Profile(context);
		}

		return instance;
	}

	private Profile(Context context) {
		_settings = context.getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
		_editor = _settings.edit();

		_thisUser = new User();

		com.facebook.Profile fbProfile = com.facebook.Profile.getCurrentProfile();

		if (fbProfile == null) {
			com.facebook.Profile.fetchProfileForCurrentAccessToken();
			return;
		}

		String facebookId;
		if (_settings.contains(PREFS_FIELD_FACEBOOK_ID) &&
			fbProfile.getId().equals(facebookId = _settings.getString(PREFS_FIELD_FACEBOOK_ID, ""))) {
			// if something has been stored previously, retrieve data
			_thisUser.setFacebookId(facebookId);
			_thisUser.setName(_settings.getString(PREFS_FIELD_NAME, ""));
			_thisUser.setAge(_settings.getInt(PREFS_FIELD_AGE, 0));
			_thisUser.setGoal(_settings.getString(PREFS_FIELD_GOAL, ""));
			_thisUser.setCity(_settings.getString(PREFS_FIELD_CITY, ""));
			_thisUser.setSports(_settings.getString(PREFS_FIELD_SPORTS, ""));
			_thisUser.setAbout(_settings.getString(PREFS_FIELD_ABOUT, ""));
			_thisUser.setXP(_settings.getInt(PREFS_FIELD_XP, 0));
			_thisUser.setUploaded(_settings.getBoolean(PREFS_FIELD_UPLOADED, true));
			_thisUser.setModified(false);
			_downloadProfile = false;
		} else {
			// otherwise, get facebook profile, and initialize our profile with
			// facebook id and name
			if (fbProfile != null) {
				setFacebookId(fbProfile.getId());
				setName(fbProfile.getName());
			}
			_downloadProfile = true;
		}

		_profileInitialized = true;

	}

	public boolean isFirstUse() {
		return false;
//		return _settings.getBoolean(Config.PREFS_FIELD_FIRST_USE, true);
	}

	public void setFirstUse(boolean firstUse) {
		_editor.putBoolean(Config.PREFS_FIELD_FIRST_USE, firstUse);
		_editor.commit();
		_editor.clear();
	}

	public String getFacebookId() {
		return _thisUser.getFacebookId();
	}

	public void setFacebookId(String facebookId) {
		_thisUser.setFacebookId(facebookId);
		_editor.putString(PREFS_FIELD_FACEBOOK_ID, facebookId);
		_editor.commit();
		setUploaded(false);
	}

	public String getName() {
		return _thisUser.getName();
	}

	public void setName(String name) {
		_thisUser.setName(name);
		_editor.putString(PREFS_FIELD_NAME, name);
		_editor.commit();
		setUploaded(false);
	}

	public int getAge() {
		return _thisUser.getAge();
	}

	public void setAge(int age) {
		_thisUser.setAge(age);
		_editor.putInt(PREFS_FIELD_AGE, age);
		_editor.commit();
		setUploaded(false);
	}

	public String getGoal() {
		return _thisUser.getGoal();
	}

	public void setGoal(String goal) {
		_thisUser.setGoal(goal);
		_editor.putString(PREFS_FIELD_GOAL, goal);
		_editor.commit();
		setUploaded(false);
	}

	public String getCity() {
		return _thisUser.getCity();
	}

	public void setCity(String city) {
		_thisUser.setCity(city);
		_editor.putString(PREFS_FIELD_CITY, city);
		setUploaded(false);
	}

	public String getSports() {
		return _thisUser.getSports();
	}

	public void setSports(String sports) {
		_thisUser.setSports(sports);
		_editor.putString(PREFS_FIELD_SPORTS, sports);
		_editor.commit();
		setUploaded(false);
	}

	public String getAbout() {
		return _thisUser.getAbout();
	}

	public void setAbout(String about) {
		_thisUser.setAbout(about);
		_editor.putString(PREFS_FIELD_ABOUT, about);
		_editor.commit();
		setUploaded(false);
	}

	public int getXP() {
		return _thisUser.getXP();
	}

	public void setXP(int xp) {
		_thisUser.setXP(xp);
		_editor.putInt(PREFS_FIELD_XP, xp);
		_editor.commit();
		setUploaded(false);
	}

	public void incXP(int reward) {
		setXP(getXP() + reward);
	}

	public boolean isUploaded() {
		return _thisUser.isUploaded();
	}

	public void setUploaded(boolean uploaded) {
		_thisUser.setUploaded(uploaded);
		_editor.putBoolean(PREFS_FIELD_UPLOADED, uploaded);
		_editor.commit();
	}

	public User getUser() {
		return _thisUser;
	}

}
