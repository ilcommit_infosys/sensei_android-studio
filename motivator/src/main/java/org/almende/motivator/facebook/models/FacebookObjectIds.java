package org.almende.motivator.facebook.models;

/**
 * Copyright (c) 2016 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 2-3-16
 *
 * @author Dominik Egger
 */
public class FacebookObjectIds {

	public static final String CHALLENGE_ID              = "962377787179060";
	public static final String CHALLENGE_ACCEPT_ID       = "1109383602419582";
	public static final String CHALLENGE_APPROVE_ID      = "814074825385709";
	public static final String CHALLENGE_COMPLETE_ID     = "1303820572967237";
	public static final String CHALLENGE_DISAPPROVE_ID   = "828487250592944";
	public static final String CHALLENGE_MESSAGE_ID      = "1109270989096689";
	public static final String CHALLENGE_REJECT_ID       = "852472584875918";
	public static final String PERSONAL_MESSAGE_ID       = "1749565035265420";

//	public static final String CHALLENGE_ID              = "527476297417004";
//	public static final String CHALLENGE_ACCEPT_ID       = "1011972355531473";
//	public static final String CHALLENGE_APPROVE_ID      = "1223013634382336";
//	public static final String CHALLENGE_COMPLETE_ID     = "1508920792756449";
//	public static final String CHALLENGE_DISAPPROVE_ID   = "856753454440002";
//	public static final String CHALLENGE_MESSAGE_ID      = "897548933674946";
//	public static final String CHALLENGE_REJECT_ID       = "838797969552366";
//	public static final String PERSONAL_MESSAGE_ID       = "1052455514805444";

}
