package org.almende.motivator.facebook;

//import alm.motiv.AlmendeMotivator.misc.CustomCallback;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Log;
import android.widget.Toast;

import org.almende.motivator.Cookie;
import org.almende.motivator.misc.CustomCallback;
import org.almende.motivator.models.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.model.GameRequestContent;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.GameRequestDialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * FacebookManager controls connection with Facebook and gets requests from Facebook
 *
 * @author Thijs
 */

public class FacebookManager {

	private static final String TAG = FacebookManager.class.getCanonicalName();

	/**
	 * Fields
	 */
	private final String ME_HOME_PREFIX = "/me/home";
	private final String GET_ME_HOME = "message, from, link, likes, comments.limit(1).summary(true), created_time, id";

	private final Activity activity;

	//    private Session session;
	private final SharedPreferences sharedPreferences;


	/**
	 * Constructor
	 *
	 * @param activity
	 */
	public FacebookManager(Activity activity) {
		this.activity = activity;
		sharedPreferences = activity.getSharedPreferences("facebookManager",
				Context.MODE_PRIVATE);

	}

	public static void initialize(Context context) {
		FacebookSdk.sdkInitialize(context.getApplicationContext(), new FacebookSdk.InitializeCallback() {
			@Override
			public void onInitialized() {
				if (isLoggedIn()) {
//					Cookie cookie = Cookie.getInstance();
//					Profile profile = Profile.getCurrentProfile();
//					cookie.userEntryId = profile.getDbId();
//					cookie.userName = profile.getName();
				}
			}
		});
	}

	/**
	 * check if is logged in
	 */
	public static boolean isLoggedIn() {
		return FacebookSdk.isInitialized() && AccessToken.getCurrentAccessToken() != null;
	}

	public static void logout() {
		LoginManager.getInstance().logOut();
	}


	public boolean isSessionChanged() {
		if (isLoggedIn()) {
			long lastExpirationDateInMillis = sharedPreferences.getLong("lastExpirationdate", 0);
			if (lastExpirationDateInMillis == AccessToken.getCurrentAccessToken().getExpires().getTime()) {
				return false;
			}

			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putLong("lastExpirationdate", AccessToken.getCurrentAccessToken().getExpires().getTime());
			editor.commit();
			return true;
		}
		return false;
	}

	public static void getFriendsOfUser(final CustomCallback callback) {
		GraphRequest.newMyFriendsRequest(
				AccessToken.getCurrentAccessToken(),
				new GraphRequest.GraphJSONArrayCallback() {
					@Override
					public void onCompleted(JSONArray objects, GraphResponse response) {
						if (objects != null) {
							ArrayList<User> friends = new ArrayList<>();
							for (int i = 0; i < objects.length(); ++i) {
								JSONObject user;
								try {
									user = objects.getJSONObject(i);
									friends.add(new User(user.getString("id"), user.getString("name")));
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							Cookie.getInstance().facebookFriends = friends;
							callback.callback(objects);
						}
					}
				}
		).executeAsync();
	}

	public static void sendInvite(final Activity activity) {
		if (AppInviteDialog.canShow()) {
			AppInviteContent content = new AppInviteContent.Builder()
					.setApplinkUrl("https://fb.me/852445371541023")
					.setPreviewImageUrl("https://lh5.ggpht.com/0L_d88jX-65IwDe79yk5KXpZyclBgB0LYinPNi6lSdmfy_KtmDjAgnBMQNgfyNZ794A=w300")
					.build();
			AppInviteDialog.show(activity, content);
		}
	}

	public static void sendGameInvite(final Activity activity) {

		GameRequestDialog requestDialog = new GameRequestDialog(activity);

		GameRequestContent content = new GameRequestContent.Builder()
				.setMessage("I would like you to use ...!" +
						" It's a fun an interactive way to keep each other motivated to exercise.")
				.build();

		requestDialog.show(content);
	}

	public static void addDeleteRequest(Collection<GraphRequest> deleteRequests, JSONObject request) throws JSONException {
		deleteRequests.add(GraphRequest.newDeleteObjectRequest(
				AccessToken.getCurrentAccessToken(),
				request.getString("id"),
				new GraphRequest.Callback() {
					@Override
					public void onCompleted(GraphResponse response) {

					}
				}
		));
	}

   /* *//**
	 * Get timeline posts Async from the Logged in User
	 *//*
	public void getTimelineUserAsync(final CrCallBack completionCallback, final CrCallBack failCallback) {
		Session.openActiveSession(activity, false, new Session.StatusCallback() {

			// callback when session changes state
			@Override
			public void call(Session session, SessionState state,
							 Exception exception) {

				if (state.isOpened()) {
					// to the /me API
					Bundle params = new Bundle();
					params.putString("scope", "read_stream");
					params.putString("fields", GET_ME_HOME);

					// params.putString("limit", "20");
					Request request = Request.newGraphPathRequest(session,
							ME_HOME_PREFIX, new Request.Callback() {

						@Override
						public void onCompleted(Response response) {
							GraphObject object = response.getGraphObject();
							ArrayList<SocialMediaFeed> facebookFeeds = new ArrayList<SocialMediaFeed>();

							try {
								if (object != null) {
									JSONObject jsonobject = object.getInnerJSONObject();

									JSONArray data_array = jsonobject .getJSONArray("data");

									for (int i = 0; i < data_array.length(); i++) {
										SocialMediaFeed feed = new SocialMediaFeed(data_array.getJSONObject(i));
										if (!feed.getContent().equals("")) {
											facebookFeeds.add(feed);
										}
									}

									setFacebookFeeds(facebookFeeds);
								}

							} catch (JSONException e) {
								//e.printStackTrace();
							}

							if (facebookFeeds.size() != 0) {
								completionCallback.callback(facebookFeeds);
							}else{
								failCallback.callback(null);
							}

						}
					});
					request.setParameters(params);

					request.executeAsync();
				}else{
					failCallback.callback(null);
				}
			}
		});


	}*/

}
