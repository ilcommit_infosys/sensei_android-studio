package org.almende.motivator.facebook.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 28-10-15
 *
 * @author Dominik Egger
 */
public class FacebookRequestFactory {

	public static FacebookRequest fromString(String json) throws JSONException {
		JSONObject temp = new JSONObject(json);
		switch (temp.getInt("type")) {
			case FacebookRequest.NEW_CHALLENGE: {
				return new ChallengeRequestData(json);
			}
			case FacebookRequest.CHALLENGE_MESSAGE: {
				return new ChallengeRequestMessage(json);
			}
			case FacebookRequest.CHALLENGE_ACCEPT:
			case FacebookRequest.CHALLENGE_REJECT: {
				return new ChallengeRequestAnswer(json);
			}
			case FacebookRequest.CHALLENGE_APPROVE:
			case FacebookRequest.CHALLENGE_DISAPPROVE: {
				return new ChallengeRequestResult(json);
			}
			case FacebookRequest.CHALLENGE_COMPLETE: {
				return new ChallengeRequestComplete(json);
			}
			case FacebookRequest.PERSONAL_MESSAGE: {
				return new PersonalMessage(json);
			}
		}
		return null;
	}

}
