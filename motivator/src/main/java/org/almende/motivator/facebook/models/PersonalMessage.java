package org.almende.motivator.facebook.models;

import org.almende.motivator.models.Message;
import org.json.JSONException;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 28-10-15
 *
 * @author Dominik Egger
 */
public class PersonalMessage extends FacebookRequest {

//	public static final String FB_OBJECT_ID = "1052455514805444";

	public static final String FIELD_MESSAGE = "message";

	public PersonalMessage() throws JSONException {
		super(PERSONAL_MESSAGE);
	}

	public PersonalMessage(String json) throws JSONException {
		super(json);
	}

	public void setMessage(Message message) throws JSONException {
		put(FIELD_MESSAGE, message.toString());
	}

	public Message getMessage() throws JSONException {
		return new Message(getString(FIELD_MESSAGE));
	}

	public String getFbObjectId() {
		return FacebookObjectIds.PERSONAL_MESSAGE_ID;
	}
}
