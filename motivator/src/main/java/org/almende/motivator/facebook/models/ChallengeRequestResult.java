package org.almende.motivator.facebook.models;

import org.json.JSONException;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 28-10-15
 *
 * @author Dominik Egger
 */
public class ChallengeRequestResult extends ChallengeRequest {

//	public static final String FB_APPROVE_OBJECT_ID = "1223013634382336";
//	public static final String FB_DISAPPROVE_OBJECT_ID = "856753454440002";

	public static final String FIELD_MESSAGE = "message";

	public ChallengeRequestResult(boolean approved) throws JSONException {
		if (approved) {
			setType(CHALLENGE_APPROVE);
		} else {
			setType(CHALLENGE_DISAPPROVE);
		}
	}

	public ChallengeRequestResult(String json) throws JSONException {
		super(json);
	}

	public void setMessage(String message) throws JSONException {
		put(FIELD_MESSAGE, message);
	}

	public String getMessage() throws JSONException {
		return getString(FIELD_MESSAGE);
	}

	public String getFbObjectId() throws JSONException {
		if (getType() == CHALLENGE_APPROVE) {
			return FacebookObjectIds.CHALLENGE_APPROVE_ID;
		} else {
			return FacebookObjectIds.CHALLENGE_DISAPPROVE_ID;
		}
	}

}
