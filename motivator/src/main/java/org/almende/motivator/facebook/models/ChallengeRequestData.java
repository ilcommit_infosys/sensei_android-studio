package org.almende.motivator.facebook.models;

import org.almende.motivator.models.Challenge;
import org.json.JSONException;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 28-10-15
 *
 * @author Dominik Egger
 */
public class ChallengeRequestData extends ChallengeRequest {

//	public static final String FB_OBJECT_ID = "527476297417004";

	public static final String FIELD_CHALLENGE = "challenge";

	public ChallengeRequestData() throws JSONException {
		super(NEW_CHALLENGE);
	}

	public ChallengeRequestData(String json) throws JSONException {
		super(json);
	}

	public void setChallenge(Challenge challenge) throws JSONException {
		put("challenge", challenge.toString());
	}

	public Challenge getChallenge() throws JSONException {
		return new Challenge(getString(FIELD_CHALLENGE));
	}

	public String getFbObjectId() {
		return FacebookObjectIds.CHALLENGE_ID;
	}

	@Override
	public String getChallengeId() throws JSONException {
		return getChallenge().getChallengeId();
	}

}
