package org.almende.motivator.facebook.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 28-10-15
 *
 * @author Dominik Egger
 */
public abstract class FacebookRequest extends JSONObject {

	public static final int NEW_CHALLENGE = 0;
	public static final int CHALLENGE_ACCEPT = 1;
	public static final int CHALLENGE_REJECT = 2;
	public static final int CHALLENGE_COMPLETE = 3;
	public static final int CHALLENGE_APPROVE = 4;
	public static final int CHALLENGE_DISAPPROVE = 5;
	public static final int CHALLENGE_MESSAGE = 6;
	public static final int PERSONAL_MESSAGE = 7;

	public static final String FIELD_TYPE = "type";

	protected FacebookRequest() {
	}

	public FacebookRequest(int type) throws JSONException {
		setType(type);
	}

	public FacebookRequest(String json) throws JSONException {
		super(json);
	}

	public abstract String getFbObjectId() throws JSONException;

	public int getType() throws JSONException {
		return getInt(FIELD_TYPE);
	}

	protected void setType(int type) throws JSONException {
		put(FIELD_TYPE, type);
	}

}
