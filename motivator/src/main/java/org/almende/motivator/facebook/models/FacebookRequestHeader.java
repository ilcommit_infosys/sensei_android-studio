package org.almende.motivator.facebook.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 28-10-15
 *
 * @author Dominik Egger
 */
public class FacebookRequestHeader extends JSONObject {

	public static final String FIELD_FROM = "from";
	public static final String FIELD_TO = "to";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_ID = "id";

	protected FacebookRequestHeader() {
	}

	public FacebookRequestHeader(String json) throws JSONException {
		super(json);
	}

	public String getFromName() throws JSONException {
		return getJSONObject(FIELD_FROM).getString(FIELD_NAME);
	}

	public String getFromId() throws JSONException {
		return getJSONObject(FIELD_FROM).getString(FIELD_ID);
	}

	public String getToName() throws JSONException {
		return getJSONObject(FIELD_TO).getString(FIELD_NAME);
	}

	public String getToId() throws JSONException {
		return getJSONObject(FIELD_TO).getString(FIELD_ID);
	}

	public String getRequestId() throws JSONException {
		return getString(FIELD_ID).split("_")[0];
	}

}
