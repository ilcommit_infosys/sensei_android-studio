package org.almende.motivator.facebook.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 28-10-15
 *
 * @author Dominik Egger
 */
public abstract class ChallengeRequest extends FacebookRequest {

	public static final String FIELD_CHALLENGE_ID = "challengeId";

	protected ChallengeRequest() {
	}

	public ChallengeRequest(int type) throws JSONException {
		setType(type);
	}

	public ChallengeRequest(String json) throws JSONException {
		super(json);
	}

	public void setChallengeId(String challengeId) throws JSONException {
		put(FIELD_CHALLENGE_ID, challengeId);
	}

	public String getChallengeId() throws JSONException {
		return getString(FIELD_CHALLENGE_ID);
	}

}
