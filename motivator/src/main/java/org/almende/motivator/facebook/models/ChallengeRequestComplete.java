package org.almende.motivator.facebook.models;

import org.almende.motivator.models.Evidence;
import org.json.JSONException;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 28-10-15
 *
 * @author Dominik Egger
 */
public class ChallengeRequestComplete extends ChallengeRequest {

//	public static final String FB_OBJECT_ID = "1508920792756449";

	public static final String FIELD_EVIDENCE = "evidence";

	public ChallengeRequestComplete() throws JSONException {
		super(CHALLENGE_COMPLETE);
	}

	public ChallengeRequestComplete(String json) throws JSONException {
		super(json);
	}

	public void setEvidence(Evidence evidence) throws JSONException {
		put(FIELD_EVIDENCE, evidence.toString());
	}

	public Evidence getEvidence() throws JSONException {
		return new Evidence(getString(FIELD_EVIDENCE));
	}

	public String getFbObjectId() {
		return FacebookObjectIds.CHALLENGE_COMPLETE_ID;
	}
}
