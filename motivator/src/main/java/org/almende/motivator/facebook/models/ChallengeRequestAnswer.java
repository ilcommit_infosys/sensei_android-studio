package org.almende.motivator.facebook.models;

import org.json.JSONException;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 28-10-15
 *
 * @author Dominik Egger
 */
public class ChallengeRequestAnswer extends ChallengeRequest {

//	public static final String FB_ACCEPT_OBJECT_ID = "1011972355531473";
//	public static final String FB_REJECT_OBJECT_ID = "838797969552366";

	public ChallengeRequestAnswer(boolean accept) throws JSONException {
		if (accept) {
			setType(CHALLENGE_ACCEPT);
		} else {
			setType(CHALLENGE_REJECT);
		}
	}

	public ChallengeRequestAnswer(String json) throws JSONException {
		super(json);
	}

	public String getFbObjectId() throws JSONException {
		if (getType() == CHALLENGE_ACCEPT) {
			return FacebookObjectIds.CHALLENGE_ACCEPT_ID;
		} else {
			return FacebookObjectIds.CHALLENGE_REJECT_ID;
		}
	}
}
