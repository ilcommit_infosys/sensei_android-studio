package org.almende.motivator;

import android.content.Context;

import org.almende.motivator.dataaccess.ChallengesListDAO;
import org.almende.motivator.dataaccess.ChallengesListData;
import org.almende.motivator.dataaccess.ReceivedChallengesDAO;
import org.almende.motivator.dataaccess.ReceivedChallengesData;
import org.almende.motivator.dataaccess.SentChallengesDAO;
import org.almende.motivator.dataaccess.SentChallengesData;
import org.almende.motivator.models.Challenge;

import java.util.ArrayList;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 19-10-15
 *
 * @author Dominik Egger
 */
public class ChallengeDB {

	private static ChallengeDB instance = null;

	private final Context context;

	private ChallengesListDAO _challengesListDAO;
	private ChallengesListData _challengesListData;

	public ChallengeDB(Context context) {
		this.context = context;

		_challengesListDAO = new ChallengesListDAO(context);
		_challengesListData = _challengesListDAO.getChallengesList();
	}

	public static ChallengeDB getInstance(Context context) {
		if (instance == null) {
			instance = new ChallengeDB(context);
		}
		return instance;
	}

	public ArrayList<Challenge> getReceivedChallenges() {
		return _challengesListData.getReceivedChallenges();
	}

	public ArrayList<Challenge> getSentChallenges() {
		return _challengesListData.getSentChallenges();
	}

	public Challenge getChallenge(String challengeId) {
		return _challengesListData.getChallenge(challengeId);
	}

	public void updateChallenge(Challenge challenge) {
		_challengesListData.updateChallenge(challenge);
		_challengesListDAO.update(_challengesListData);
	}

	public void addNewSentChallenge(final Challenge challenge) {
		_challengesListData.addSentChallenge(challenge);
		_challengesListDAO.update(_challengesListData);
	}

	public void addNewReceived(final Challenge challenge) {
		_challengesListData.addReceivedChallenge(challenge);
		_challengesListDAO.update(_challengesListData);
	}

}
