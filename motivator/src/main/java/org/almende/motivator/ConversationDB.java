package org.almende.motivator;

import android.content.Context;

import org.almende.motivator.dataaccess.ConversationListDAO;
import org.almende.motivator.dataaccess.ConversationsListData;
import org.almende.motivator.models.Conversation;
import org.almende.motivator.models.Message;

import java.util.ArrayList;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 4-11-15
 *
 * @author Dominik Egger
 */
public class ConversationDB {

	private static ConversationDB instance;

	private final Context context;

	private ConversationListDAO _conversationListDAO;
	private ConversationsListData _conversationsListData;

	private ConversationDB(Context context) {
		this.context = context;

		_conversationListDAO = new ConversationListDAO(context);
		_conversationsListData = _conversationListDAO.getConversations();
	}

	public static ConversationDB getInstance(Context context) {
		if (instance == null) {
			instance = new ConversationDB(context);
		}
		return instance;
	}

	public boolean hasConversation(String partnerId) {
		return _conversationsListData.hasConversation(partnerId);
	}

	public Conversation createNewConversation(String partnerId, String partnerName) {
		if (!hasConversation(partnerId)) {
			Conversation conversation = new Conversation(partnerId, partnerName);
			_conversationsListData.addConversation(conversation);
			return conversation;
		} else {
			return null;
		}
	}

	public void addMessage(String partnerId, String partnerName, Message message) {
		if (!hasConversation(partnerId)) {
			_conversationsListData.addConversation(new Conversation(partnerId, partnerName));
		}
		_conversationsListData.addMessage(partnerId, message);
		_conversationListDAO.store(_conversationsListData);
	}

	public Conversation getConversation(String partnerId) {
			return _conversationsListData.getConversation(partnerId);
	}

	public ArrayList<Conversation> getConversations() {
		return _conversationsListData.getConversations();
	}
}
