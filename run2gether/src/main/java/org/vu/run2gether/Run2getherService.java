package org.vu.run2gether;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import org.hva.createit.scl.dataaccess.Run2GetherDAO;
import org.vu.run2gether.RunnerData;

import interdroid.swancore.swanmain.ExpressionManager;
import interdroid.swancore.swanmain.SwanException;
import interdroid.swancore.swanmain.ValueExpressionListener;
import interdroid.swancore.swansong.ExpressionFactory;
import interdroid.swancore.swansong.ExpressionParseException;
import interdroid.swancore.swansong.TimestampedValue;
import interdroid.swancore.swansong.ValueExpression;

public class Run2getherService extends Service {

    private final String TAG = "Run2getherService";
    public static final String ACTION_NEW_RUNNER_FOUND = "org.vu.run2gether.ui.ACTION_NEW_RUNNER_FOUND";
    public final int REQUEST_CODE = 2121121;
    private BroadcastReceiver r2gBcastReceiver;

    public Run2getherService() {}

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // register receiver for getting requests from fitness sensor
        r2gBcastReceiver = new Run2getherBroadcastReceiver(this);
        IntentFilter r2gIntentFilter = new IntentFilter();
        r2gIntentFilter.addAction(Run2getherBroadcastReceiver.ACTION_REQ_FITNESS_DATA);
        registerReceiver(r2gBcastReceiver, r2gIntentFilter);

        // register the fitness sensor
        registerFitnessSensor();

        return START_NOT_STICKY;
    }

    public void registerFitnessSensor() {
        String myExpression = "NEARBY@fitness:avg_speed$server_storage=false{ANY,0}";
        unregisterSWANSensor();
        registerSWANSensor(myExpression);
    }

    /* Register expression to SWAN */
    private void registerSWANSensor(String myExpression) {
        try {
            ExpressionManager.registerValueExpression(this, String.valueOf(REQUEST_CODE),
                    (ValueExpression) ExpressionFactory.parse(myExpression),
                new ValueExpressionListener() {

                    /* Registering a listener to process new values from the registered sensor*/
                    @Override
                    public void onNewValues(String id, TimestampedValue[] arg1) {
                        if (arg1.length > 0) {
                            String data = arg1[0].getValue().toString();
                            Log.d(TAG, "Received fitness data: " + data);

                            RunnerData runnerData = new RunnerData(data);
                            Run2GetherDAO run2GetherDAO = new Run2GetherDAO(Run2getherService.this);
                            RunnerData oldRunnerData = run2GetherDAO.getRunnerData(runnerData.getRunnerId());

                            if(oldRunnerData != null) {
                                run2GetherDAO.update(runnerData);
                            } else {
                                run2GetherDAO.store(runnerData);
                            }

                            // notify NearbyRunnersActivity that a new runner was found
                            Intent runnerFoundIntent = new Intent();
                            runnerFoundIntent.setAction(ACTION_NEW_RUNNER_FOUND);
                            sendBroadcast(runnerFoundIntent);
                        }
                    }
                });
        } catch (SwanException e) {
            e.printStackTrace();
        } catch (ExpressionParseException e) {
            e.printStackTrace();
        }
    }

    private void unregisterSWANSensor() {
        ExpressionManager.unregisterExpression(this, String.valueOf(REQUEST_CODE));
    }

    @Override
    public void onDestroy() {
        // stop receiver used by fitness sensor (run2gether)
        unregisterReceiver(r2gBcastReceiver);
        unregisterSWANSensor();

        super.onDestroy();
    }
}
