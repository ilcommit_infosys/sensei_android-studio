package org.vu.run2gether;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.scl.dataaccess.RunStatisticsDAO;

import java.util.List;

/**
 * Created by vladimir on 2/4/16.
 */
public class Run2getherBroadcastReceiver extends BroadcastReceiver {

    private final String TAG = "R2gBroadcastReceiver";
    public static final String ACTION_REQ_FITNESS_DATA = "interdroid.swan.crossdevice.swanplus.ACTION_REQUEST_FITNESS_DATA";
    public static final String ACTION_SEND_FITNESS_DATA = "interdroid.swan.crossdevice.swanplus.ACTION_SEND_FITNESS_DATA";
    public final int NUMBER_OF_RUNS_TO_SHARE = 1;

    Context context;

    public Run2getherBroadcastReceiver(Context context) {
        this.context = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (ACTION_REQ_FITNESS_DATA.equals(action)) {
            Intent replyIntent = new Intent();
            replyIntent.setAction(ACTION_SEND_FITNESS_DATA);
            replyIntent.putExtra("avg_speed", getRunningDataAsText());
            context.sendBroadcast(replyIntent);
            Log.d(TAG, "sent fitness data");

        }
    }

    public String getRunningDataAsText() {
        String data = "";
        double averageDistance = 0d;
        double averageSpeed = 0d;
        double averageDuration = 0d;
        RunStatisticsDAO runStatistics = new RunStatisticsDAO(context);
        List<RunOverviewData> runList = runStatistics.getLastRunOverviews(NUMBER_OF_RUNS_TO_SHARE);

        for(RunOverviewData runData : runList) {
            averageDistance += runData.getDistance();
            averageSpeed += runData.getAverageSpeed();
            averageDuration += runData.getDuration();
        }

        if(runList.size() > 0) {
            averageDistance /= runList.size();
            averageSpeed /= runList.size();
            averageDuration /= runList.size();
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String userName = sharedPref.getString(context.getString(R.string.pref_user_name), null);

        data += "username=" + userName;
        data += "&averageDistance=" + averageDistance;
        data += "&averageSpeed=" + averageSpeed;
        data += "&averageDuration=" + averageDuration;
        data += "&points=" + (int) (averageDuration / 1000);

        return data;
    }
}
