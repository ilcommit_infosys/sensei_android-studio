package com.askcs.android.affectbutton;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewParent;

public class AffectButton extends GLSurfaceView {

	private Affect mAffect;
	Features mFeatures;
	Face mFace;
	Renderer mRenderer;
	private Bitmap bmFace;

	int mWidth;
	int mHeight;
	int mSize;
	boolean mActive = true;
	float mOffsetX;
	float mOffsetY;

	private double mTouchX;
	private double mTouchY;

	private boolean screenshot = false;

	private List<AffectListener> mListeners;

	public AffectButton(Context context) {
		this(context, null);
	}

	public AffectButton(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public AffectButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.affectbutton);

		mActive = a.getBoolean(R.styleable.affectbutton_button_active, mActive);

		int c = attrs.getAttributeResourceValue(
				"http://schemas.android.com/apk/res/android", "background", 0);
		c = c > 0 ? context.getResources().getColor(c) : 0;
		if (((c >>> 24) & 0xff) == 0) {
			setZOrderOnTop(true);
			Log.i("Foo", "setting to transparent (" + Long.toHexString(c));
		} else {
			Settings.BG_COLOR = Settings.convert(c);
			this.setBackgroundColor(0);
			Log.i("Foo", "setting to color (" + Long.toHexString(c));
		}

		// TODO expose more settings to attributes ?

		setEGLContextClientVersion(2);

		setEGLConfigChooser(new MultisampleConfigChooser());
		getHolder().setFormat(PixelFormat.RGBA_8888);

		setAffect(new Affect());
		mFeatures = new Features(getAffect());
		mFace = new Face(getAffect(), mFeatures);
		mRenderer = new Renderer();
		mWidth = 1;
		mHeight = 1;
		setTouchX(0d);
		setTouchY(0d);

		setRenderer(mRenderer);
		setRenderMode(RENDERMODE_WHEN_DIRTY);

		mListeners = new LinkedList<AffectListener>();

		setPAD(0d, 0d, 0d);

	}

	public void addListener(AffectListener listener) {
		mListeners.add(listener);
	}

	public void removeListener(AffectListener listener) {
		mListeners.remove(listener);
	}

	@Override
	public void onMeasure(int widthSpec, int heightSpec) {
		int min = Math.min(widthSpec & MEASURED_SIZE_MASK, heightSpec
				& MEASURED_SIZE_MASK);
		setMeasuredDimension(min, min);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mActive) {
			/*
			 * Prevent parent controls from stealing our events once we've
			 * gotten a touch down
			 */
			if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
				ViewParent p = getParent();
				if (p != null)
					p.requestDisallowInterceptTouchEvent(true);
			}

			// TODO impose rate limit?
			// map screen coordinates to [-1, 1] range
			float x = Math.max(mOffsetX,
					Math.min(mOffsetX + mSize - 1, event.getX()));
			float y = Math.max(mOffsetY,
					Math.min(mOffsetY + mSize - 1, event.getY()));
			if (x < mOffsetX || y < mOffsetY || x > mOffsetX + mSize
					|| y > mOffsetY + mSize) {
				return false; // ?
			}

			setTouchX(2d * ((x - mOffsetX) / (double) (mSize - 1)) - 1d);
			setTouchX(Math.max(-1d, Math.min(1d, getTouchX())));
			setTouchY(1d - 2d * ((y - mOffsetY) / (double) (mSize - 1)));
			setTouchY(Math.max(-1d, Math.min(1d, getTouchY())));
			double pleasure = Settings.MULTIPLIER * getTouchX();
			double dominance = Settings.MULTIPLIER * getTouchY();
			double max = Math.max(Math.abs(pleasure), Math.abs(dominance));
			double arousal = 2d / (1d + Math.exp(Settings.SIGMOID_ZERO
					- Settings.SIGMOID_SLOPE * max)) - 1d;

			setPAD(pleasure, arousal, dominance);

			return true;
		} else {
			
			return false;
		}
	}

	public void setPAD(double pleasure, double arousal, double dominance) {
		pleasure = pleasure < -1D ? -1D : pleasure > 1D ? 1D : pleasure;
		arousal = arousal < -1D ? -1D : arousal > 1D ? 1D : arousal;
		dominance = dominance < -1D ? -1D : dominance > 1D ? 1D : dominance;
		Affect affect = getAffect();
		if (Settings.SWAP_AD) {
			affect.setPAD(pleasure, dominance, arousal);
		} else {
			affect.setPAD(pleasure, arousal, dominance);
		}
		mFeatures.setAffect(affect);
		mFace.setFace(affect, mFeatures, getTouchX(), getTouchY());
		requestRender();

		for (AffectListener listener : mListeners) {
			listener.onAffectChanged(affect);
		}
	}

	public double getTouchX() {
		return mTouchX;
	}

	public double getTouchY() {
		return mTouchY;
	}

	public void setTouchX(double touchX) {
		mTouchX = touchX;
	}

	public void setTouchY(double touchY) {
		mTouchY = touchY;
	}

	public double getPleasure() {
		return getAffect().getPleasure();
	}

	public double getArousal() {
		return getAffect().getArousal();
	}

	public double getDominance() {
		return getAffect().getDominance();
	}

	public Affect getAffect() {
		return mAffect;
	}

	public void setAffect(Affect mAffect) {
		this.mAffect = mAffect;
	}

	public void takeScreenshot() {
		this.screenshot = true;
		requestRender();
	}

	public Bitmap getBitmap() {
		if (bmFace == null) {
			takeScreenshot();
		}
		return bmFace;
	}

	@Override
	public void setClickable(boolean clickable) {
		super.setClickable(clickable);
		mActive = clickable;
		if (!clickable) {
			for (AffectListener listener : mListeners) {
				removeListener(listener);
			}
		}
	}

	class Renderer implements GLSurfaceView.Renderer {

		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			gl.glClearColor(0, 0, 0, 0);
			gl.glEnable(GL10.GL_CULL_FACE);
			gl.glShadeModel(GL10.GL_SMOOTH);
			gl.glDisable(GL10.GL_DEPTH_TEST);

			//
			mFace.init();
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {
			mWidth = width;
			mHeight = height;
			mSize = Math.min(mWidth, mHeight);
			mOffsetX = Math.max(0, (mWidth - mSize) / 2);
			mOffsetY = Math.max(0, (mHeight - mSize) / 2);
			mFace.resize(width, height);
			requestRender();
		}

		@Override
		public void onDrawFrame(GL10 gl) {
			mFace.draw();

			if (screenshot) {
				int width = mWidth;
				int height = mHeight;
				int screenshotSize = width * height;
				ByteBuffer bb = ByteBuffer.allocateDirect(screenshotSize * 4);
				bb.order(ByteOrder.nativeOrder());
				gl.glReadPixels(0, 0, width, height, GL10.GL_RGBA,
						GL10.GL_UNSIGNED_BYTE, bb);
				int pixelsBuffer[] = new int[screenshotSize];
				bb.asIntBuffer().get(pixelsBuffer);
				bb = null;

				for (int i = 0; i < screenshotSize; ++i) {
					// The alpha and green channels' positions are preserved
					// while the red and blue are swapped
					pixelsBuffer[i] = ((pixelsBuffer[i] & 0xff00ff00))
							| ((pixelsBuffer[i] & 0x000000ff) << 16)
							| ((pixelsBuffer[i] & 0x00ff0000) >> 16);
				}

				Bitmap bitmap = Bitmap.createBitmap(width, height,
						Bitmap.Config.ARGB_8888);
				bitmap.setPixels(pixelsBuffer, screenshotSize - width, -width,
						0, 0, width, height);
				bmFace = bitmap;
				screenshot = false;
			}
		}

	}

}
