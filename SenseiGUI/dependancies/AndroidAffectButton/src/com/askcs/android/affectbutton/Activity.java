package com.askcs.android.affectbutton;

import android.os.Bundle;
import android.widget.TextView;

public class Activity
extends android.app.Activity
implements AffectListener {
  
  static public final String TAG = "AffectButton";
  
  TextView mPleasure;
  TextView mArousal;
  TextView mDominance;
  AffectButton mAffectButton;
  
  @Override
  protected void onCreate( Bundle instanceState ) {
    super.onCreate( instanceState );
    setContentView( R.layout.main );
    mPleasure = (TextView) findViewById( R.id.pleasure );
    mArousal = (TextView) findViewById( R.id.arousal );
    mDominance = (TextView) findViewById( R.id.dominance );
    mAffectButton = (AffectButton) findViewById( R.id.affectbutton );
    mAffectButton.addListener( this );
  }
  
  @Override
  protected void onSaveInstanceState( Bundle instanceState ) {
    super.onSaveInstanceState( instanceState );
    instanceState.putDouble( "touchX", mAffectButton.getTouchX() );
    instanceState.putDouble( "touchY", mAffectButton.getTouchX() );
    instanceState.putDouble( "pleasure", mAffectButton.getAffect().getPleasure() );
    instanceState.putDouble( "arousal", mAffectButton.getAffect().getArousal() );
    instanceState.putDouble( "dominance", mAffectButton.getAffect().getDominance() );
  }
  
  @Override
  protected void onRestoreInstanceState( Bundle instanceState ) {
    super.onRestoreInstanceState( instanceState );
    mAffectButton.setTouchX( instanceState.getDouble( "touchX" ) );
    mAffectButton.setTouchY( instanceState.getDouble( "touchY" ) );
    mAffectButton.setPAD( instanceState.getDouble( "pleasure" ),
        instanceState.getDouble( "arousal" ),
        instanceState.getDouble( "dominance" ) );
  }
  
  @Override
  public void onAffectChanged( Affect affect ) {
    mPleasure.setText( Double.valueOf( affect.getPleasure() ).toString() );
    mArousal.setText( Double.valueOf( affect.getArousal() ).toString() );
    mDominance.setText( Double.valueOf( affect.getDominance() ).toString() );
  }
}
