# README #

The SenseiGUI project functions as a GUI layer for the SensorCollectionLibrary.
It overrides the standard Android theme properties like colour, and navigation behaviour.
Also it provides visualisation interfaces for the SensorCollectionLibrary DataObjects.
Last it contains the screens and navigation for the COMMIT/ Sensei application.