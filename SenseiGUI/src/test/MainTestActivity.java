/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package test;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.infosys.sensei.ui.LoginActivity;
import com.infy.ilcommit.intelirun.Infosys_SenseService;
import com.infy.ilcommit.intelirun.SendData;

import org.hva.createit.scl.SCLControllerActivity;
import org.hva.createit.scl.data.AccelerometerData;
import org.hva.createit.scl.data.DistanceData;
import org.hva.createit.scl.data.ExertionData;
import org.hva.createit.scl.data.HeartRateData;
import org.hva.createit.scl.data.LocationData;
import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.scl.data.StepfrequencyData;
import org.hva.createit.scl.dataaccess.AccelerometerDAO;
import org.hva.createit.scl.dataaccess.DatabaseHelper;
import org.hva.createit.scl.dataaccess.DistanceDAO;
import org.hva.createit.scl.dataaccess.ExertionDAO;
import org.hva.createit.scl.dataaccess.LocationDAO;
import org.hva.createit.scl.dataaccess.Run2GetherDAO;
import org.hva.createit.scl.dataaccess.RunStatisticsDAO;
import org.hva.createit.scl.dataaccess.StepfrequencyDAO;
import org.hva.createit.sensei.gui.R;
import org.vu.run2gether.RunnerData;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import nl.sense_os.platform.SenseApplication;

public class MainTestActivity extends SCLControllerActivity {
	// extends SCLControllerActivity{

	public static final String TAG = "MainServiceActivity";

	Button button1, button2;
	TextView logView;
	long timestamp;
	AccelerometerDAO ad;

	/**
	 * Start Infosys - Sense code
	 */
	boolean flag = false;
	String dataValue = "";
	private static SenseApplication mApplication;
	private Infosys_SenseService senseService;

	/**
	 * End Infosys - Sense code
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");

		/**
		 * Start Infosys - Sense code
		 */
		// the activity needs to be part of a SenseApplication so it can talk to
		// the SensePlatform
		mApplication = (SenseApplication) getApplication();

		senseService = new Infosys_SenseService(this);
		setContentView(R.layout.infosys_activity_main);
		/**
		 * End Infosys - Sense code
		 */

		// setContentView(R.layout.hva_activity_main);

		logView = (TextView) findViewById(R.id.text_view);

		Button backupButton = (Button) findViewById(R.id.button_backup);
		backupButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				backupDB();
			}
		});

		button1 = (Button) findViewById(R.id.button_start);
		button1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				button1.setEnabled(false);
				button1.setVisibility(View.GONE);

				button2.setVisibility(View.VISIBLE);
				button2.setEnabled(true);

				startSensors();
				// startLocationSensor();
			}
		});
		button2 = (Button) findViewById(R.id.button_stop);
		button2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				button1.setEnabled(true);
				button1.setVisibility(View.VISIBLE);
				button2.setEnabled(false);
				button2.setVisibility(View.GONE);

				stopSensors();
				// stopLocationSensor();
			}
		});

		Button button3 = (Button) findViewById(R.id.button_test);
		button3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				testCode();

			}
		});

		Button button4 = (Button) findViewById(R.id.button_cleardb);
		button4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				clearDB();
			}
		});


		Button button5 = (Button) findViewById(R.id.button_Upload);
		button5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				uploadDataToCloud();
			}
		});


		Button button6 = (Button) findViewById(R.id.button_login);
		button6.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(MainTestActivity.this, LoginActivity.class), 1, new Bundle());
			}
		});

		timestamp = System.currentTimeMillis();
		ad = new AccelerometerDAO(this);
	}

	// private void testCode() {
	// RunStatisticsDAO rdao = new RunStatisticsDAO(this);
	// RunOverviewData ro = rdao.getLastRunOverview();
	// Log.d(TAG, "Last Runstatistics "+ ro.getId() +" "+ro.getSensorName()+" "+
	// ro.getStart_timestamp()+" "+
	// ro.getStop_timestamp()+" "+ro.getDuration()+" "+ro.getAverageSpeed()+" "+ro.getDistance()+" "+ro.getAverageHeartRate()+" "+
	// ro.getAverageStepFrequency());
	//
	//
	//
	// StepfrequencyDAO sdao = new StepfrequencyDAO(this);
	// Log.d(TAG, "Unuploaded Measurements (StepfrequencyData) "
	// +sdao.getUnuploadedItemCount());
	// // List<StepfrequencyData> slist = sdao.getUnuploadedItems();
	// // for(StepfrequencyData sd : slist) {
	// // Log.d(TAG, "Unuploaded stepfrequency: "+sd.getStepFrequency());
	// // sd.setUploaded(true);
	// // sdao.update(sd);
	// // }
	//
	// LocationDAO lad = new LocationDAO(this);
	// Log.d(TAG, "Unuploaded Measurements (LocationData)" +
	// lad.getUnuploadedItemCount());
	// // List<LocationData> ll = lad.getUnuploadedItems();
	// // for(LocationData loc : ll) {
	// // Log.d(TAG, "Unuploaded location " +
	// loc.getLatitude().getValueAsDouble() + " " +
	// loc.getLongitude().getValueAsDouble() + " " +
	// loc.getSpeed().getValueAsFloat() + " " +
	// loc.getLatitude().getTimestamp());
	// // loc.getLatitude().setUploaded(true);
	// // lad.update(loc);
	// // }
	//
	//
	// AccelerometerDAO ad = new AccelerometerDAO(this);
	// Log.d(TAG,
	// "Unuploaded Measurements (AccelerometerData)"+ad.getUnuploadedItemCount());
	// // List<AccelerometerData> adL = ad.getUnuploadedItems();
	// // if(adL.size() == 0) {
	// // return;
	// // }
	// // AccelerometerData acm = adL.get(0);
	// // acm.getMeasurementX().setUploaded(true);
	// // acm.getMeasurementY().setUploaded(true);
	// //
	// // ad.update(acm);
	// // Log.d(TAG, "Unuploaded Measurements "+ad.getUnuploadedItemCount());
	// //
	// // acm.getMeasurementZ().setUploaded(true);
	// // ad.update(acm.getMeasurementZ());
	// //
	// // Log.d(TAG, "Unuploaded Measurements "+ad.getUnuploadedItemCount());
	//
	//
	// }

	private void clearDB() {
		LocationDAO lad = new LocationDAO(this);
		AccelerometerDAO ad = new AccelerometerDAO(this);
		StepfrequencyDAO sad = new StepfrequencyDAO(this);
		Log.d(TAG,
				"Unuploaded Measurements (StepfrequencyDAO)"
						+ sad.getUnuploadedItemCount());
		Log.d(TAG,
				"Unuploaded Measurements (LocationData)"
						+ lad.getUnuploadedItemCount());
		Log.d(TAG,
				"Unuploaded Measurements (AccelerometerData)"
						+ ad.getUnuploadedItemCount());

		lad.clearItems();
		Log.d(TAG,
				"Unuploaded Measurements (StepfrequencyDAO)"
						+ sad.getUnuploadedItemCount());
		Log.d(TAG,
				"Unuploaded Measurements (LocationData)"
						+ lad.getUnuploadedItemCount());
		Log.d(TAG,
				"Unuploaded Measurements (AccelerometerData)"
						+ ad.getUnuploadedItemCount());

	}

	// This method will be called when a MessageEvent is posted
	public void onEvent(StepfrequencyData event) {
		final StepfrequencyData eventCopy = event;
		MainTestActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Log.d(TAG,
						"Received StepFrequencyData event: "
								+ eventCopy.getStepFrequency());
				logView.setText(logView.getText()
						+ "\nReceived StepFrequencyData event: "
						+ eventCopy.getStepFrequency());
			}
		});
	}

	// This method will be called when a MessageEvent is posted
	public void onEvent(HeartRateData event) {
		Log.d(TAG, "Received HRData event " + event.getHeartRate() + " "
				+ event.getTimestamp());
	}

	// This method will be called when a MessageEvent is posted
	// public void onEvent(DistanceData event) {
	// final DistanceData eventCopy = event;
	// MainActivity.this.runOnUiThread(new Runnable() {
	// @Override
	// public void run() {
	// Log.d(TAG,
	// "Received DistanceData event: "
	// + eventCopy.getDistance());
	// logView.setText(logView.getText()
	// + "\nReceived DistanceData event: "
	// + eventCopy.getDistance());
	// }
	// });
	// }

	// This method will be called when a MessageEvent is posted
	// public void onEvent(final AccelerometerData event) {
	//
	// // if(System.currentTimeMillis() - timestamp > 1000) {
	// // timestamp = System.currentTimeMillis();
	// MainActivity.this.runOnUiThread(new Runnable() {
	// @Override
	// public void run() {
	// // Log.d(TAG, "Unuploaded Measurements " + ad.getUnuploadedItemCount());
	// Log.d(TAG, event.getTimeStamp()+" x:" + event.getX() );
	// }
	// });
	//
	// // }
	//
	// }

	// @Override
	// protected void processData(String data) {
	// if (mConnected) {
	// Log.d(TAG, "Hear rate data: " + data);
	// // if (heart_rate != null) {
	// // heart_rate.setText(data);
	// //
	// // if (recording) {
	// // hds.open();
	// // hds.addHeartRateSilent(new HeartRateData(Long
	// // .parseLong(data), System.currentTimeMillis(),
	// // accelerometerListener.run_id));
	// // hds.close();
	// // // new UDPThread().execute(data + ", " +
	// // // System.currentTimeMillis());
	// // }
	// // }
	// }
	// }

	private void backupDB() {
		String backupLocation = Environment.getExternalStorageDirectory()
				.getAbsolutePath()
				+ "/Sensei/backup"
				+ System.currentTimeMillis() + ".zip";

		ArrayList<String> uploadData = new ArrayList<String>();
		uploadData.add(backupLocation);
		makeZip mz = new makeZip(backupLocation);
		mz.addZipFile(getDatabasePath(DatabaseHelper.DATABASE_NAME)
				.getAbsolutePath());
		mz.closeZip();
	}

	public class makeZip {
		static final int BUFFER = 2048;

		ZipOutputStream out;
		byte data[];

		public makeZip(String name) {
			FileOutputStream dest = null;
			try {
				dest = new FileOutputStream(name);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out = new ZipOutputStream(new BufferedOutputStream(dest));
			data = new byte[BUFFER];
		}

		public void addZipFile(String name) {
			Log.v("addFile", "Adding: ");
			FileInputStream fi = null;
			try {
				fi = new FileInputStream(name);
				Log.v("addFile", "Adding: ");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.v("atch", "Adding: ");
			}
			BufferedInputStream origin = new BufferedInputStream(fi, BUFFER);
			ZipEntry entry = new ZipEntry(name);
			try {
				out.putNextEntry(entry);
				Log.v("put", "Adding: ");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int count;
			try {
				while ((count = origin.read(data, 0, BUFFER)) != -1) {
					out.write(data, 0, count);
					// Log.v("Write", "Adding: "+origin.read(data, 0, BUFFER));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.v("catch", "Adding: ");
			}
			try {
				origin.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void closeZip() {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Start Infosys - Sense code
	 */
	// public void onClick(View v) {
	// switch (v.getId()) {
	// case R.id.button_login:
	// startActivity(new Intent(this, LoginActivity.class));
	// break;
	// case R.id.button_register:
	// startActivity(new Intent(this, RegistrationActivity.class));
	// break;
	//
	// case R.id.button_Upload:
	// testCode();
	// // addToGroup();
	// break;
	//
	// default:
	// Log.w(TAG, "Unexpected button pressed: " + v);
	// }
	// }

	private void testCode() {

//		if (senseService.checkIfWifiWorking())
//			// senseService.sendUnuploadedData();
//			System.out.println();
//		else
//			Log.w(TAG, "Wifi not working..");

		ExertionDAO edao = new ExertionDAO(this);
		ExertionData ed = new ExertionData(0, 2, System.currentTimeMillis());
		Log.d(TAG, "ExertionDAO unuploaded count " + edao.getUnuploadedItemCount());
		edao.store(ed);
		Log.d(TAG, "ExertionDAO unuploaded count " + edao.getUnuploadedItemCount());
		ed.setUploaded(true);
		ed.setValue(4);
		ed.setId(3);
		edao.update(ed);
		Log.d(TAG, "ExertionDAO unuploaded count " + edao.getUnuploadedItemCount());
		List<ExertionData> eds = edao.getUnuploadedItems();
		ed = eds.get(0);
		ed.setUploaded(true);
		eds.set(0, ed);
		edao.update(ed);
		Log.d(TAG, "ExertionDAO unuploaded count " + edao.getUnuploadedItemCount());


		LocationDAO lao = new LocationDAO(this);
		List<LocationData> slist = new ArrayList<LocationData>();
		Log.d(TAG, "Unuploaded location items "+lao.getUnuploadedItemCount());
		slist = lao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded location items retrieved "+slist.size());

		for (LocationData loc : slist) {

			loc.getLatitude().setUploaded(true);
			loc.getLongitude().setUploaded(true);
			loc.getAltitude().setUploaded(true);
			loc.getAccuracy().setUploaded(true);
			loc.getSpeed().setUploaded(true);

			//new extra
			loc.getBearing().setUploaded(true);

			lao.update(loc);
		}
		Log.d(TAG, "Unuploaded location items "+lao.getUnuploadedItemCount());
		slist = lao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded location items retrieved "+slist.size());


		DistanceDAO dao = new DistanceDAO(this);
		List<DistanceData> dlist = new ArrayList<DistanceData>();
		dlist = dao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded distance items "+dao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded distance items retrieved " + dlist.size());
		for (DistanceData loc : dlist) {
			loc.setUploaded(true);
			dao.update(loc);
		}
		dlist = dao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded distance items "+dao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded distance items retrieved " + dlist.size());

		AccelerometerDAO alao = new AccelerometerDAO(this);

		List<AccelerometerData> aslist = new ArrayList<AccelerometerData>();
		aslist = alao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded accelerometer items "+alao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded accelerometer items retrieved " + aslist.size());

		for (AccelerometerData acc : aslist) {

			acc.getMeasurementX().setUploaded(true);
			acc.getMeasurementY().setUploaded(true);
			acc.getMeasurementZ().setUploaded(true);
			alao.update(acc);
		}

		aslist = alao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded accelerometer items "+alao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded accelerometer items retrieved " + aslist.size());


		RunStatisticsDAO rlao = new RunStatisticsDAO(this);
		List<RunOverviewData> rslist = new ArrayList<RunOverviewData>();
		//slist.add(lao.getLastRunOverview());
		rslist = rlao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded runstatistics items "+rlao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded runstatistics items retrieved " + rslist.size());

		for (RunOverviewData acc : rslist) {
			acc.setUploaded(true);
			acc.getAverageHeartRateMeasurementData().setUploaded(true);
			acc.getAverageSpeedMeasurementData().setUploaded(true);
			acc.getAverageStepFrequencyMeasurementData().setUploaded(
					true);
			acc.getDurationMeasurementData().setUploaded(true);
			acc.getDistanceMeasurementData().setUploaded(true);
			rlao.update(acc);
		}
		rslist = rlao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded runstatistics items "+rlao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded runstatistics items retrieved " + rslist.size());

		StepfrequencyDAO slao = new StepfrequencyDAO(this);
		List<StepfrequencyData> sslist = new ArrayList<StepfrequencyData>();
		sslist = slao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded stepfrequency items "+slao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded stepfrequency items retrieved " + sslist.size());

		for (StepfrequencyData sd : sslist) {

			sd.setUploaded(true);
			slao.update(sd);
		}
		sslist = slao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded stepfrequency items "+slao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded stepfrequency items retrieved " + sslist.size());

		ExertionDAO elao = new ExertionDAO(this);
		List<ExertionData> eslist = new ArrayList<ExertionData>();
		eslist = elao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded exertion items "+elao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded exertion items retrieved " + eslist.size());

		for (ExertionData sd : eslist) {

			sd.setUploaded(true);
			elao.update(sd);
		}
		eslist = elao.getUnuploadedItems();
		Log.d(TAG, "Unuploaded exertion items "+elao.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded exertion items retrieved " + eslist.size());

//		QuestionnaireDAO questionnaireDAO = new QuestionnaireDAO(this);
//		 List<QuestionnaireData> questions = new ArrayList<QuestionnaireData>();
//		 questions = questionnaireDAO.getUnuploadedItems();
//		 Log.d(TAG, "Unuploaded questionaire items "+questionnaireDAO.getUnuploadedItemCount());
//		 Log.d(TAG, "Unuploaded questionaire items retrieved " + questions.size());
//		 for(QuestionnaireData question : questions){ 
//			question.setUploaded(true); 
//			questionnaireDAO.update(question); 
//		}
//		 questions = questionnaireDAO.getUnuploadedItems();
//		 Log.d(TAG, "Unuploaded questionaire result should be 0 now");
//		 Log.d(TAG, "Unuploaded questionaire items "+questionnaireDAO.getUnuploadedItemCount());
//		 Log.d(TAG, "Unuploaded questionaire items retrieved " + questions.size());

		Run2GetherDAO run2GetherDAO = new Run2GetherDAO(this);
		List<RunnerData> runnerDatas = new ArrayList<RunnerData>();
		runnerDatas = run2GetherDAO.getUnuploadedItems();
		Log.d(TAG, "Unuploaded runnerData items "+run2GetherDAO.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded runnerData items retrieved " + runnerDatas.size());
		run2GetherDAO.store(new RunnerData("1", 100, 10, 10000, 2, new Date(System.currentTimeMillis()), false));
		runnerDatas = run2GetherDAO.getUnuploadedItems();
		Log.d(TAG, "Unuploaded runnerData items "+run2GetherDAO.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded runnerData items retrieved " + runnerDatas.size());
		for(RunnerData runner: runnerDatas){
			runner.setUploaded(true);
			run2GetherDAO.update(runner);
		}
		runnerDatas = run2GetherDAO.getUnuploadedItems();
		Log.d(TAG, "Unuploaded runnerData items "+run2GetherDAO.getUnuploadedItemCount());
		Log.d(TAG, "Unuploaded runnerData items retrieved " + runnerDatas.size());

		RunnerData currentRunner = run2GetherDAO.getCurrentRunnerDataStatistics();
		Log.d(TAG, "CurrentRunner stats " + currentRunner.getAverageDistance()+" "+currentRunner.getAverageSpeed() +" "+currentRunner.getAverageDuration());


	}

	public void uploadDataToCloud(){
		Intent serviceIntent = new Intent(this, SendData.class);
		startService(serviceIntent);
	}

	public static SenseApplication getmApplication() {
		return mApplication;
	}

	/**
	 * End Infosys - Sense code
	 */
}
