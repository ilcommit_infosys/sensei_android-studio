package org.hva.createit.sensei.ui;

import org.hva.createit.scl.data.StepfrequencyData;
import org.hva.createit.sensei.ui.view.DistanceUnit;
import org.hva.createit.sensei.ui.view.HeartrateUnit;
import org.hva.createit.sensei.ui.view.LocationUnit;
import org.hva.createit.sensei.ui.view.SpeedUnit;
import org.hva.createit.sensei.ui.view.StepfrequencyUnit;
import org.hva.createit.sensei.ui.view.TimerUnit;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class RunActivity extends SCLControllerBaseActivity {
	private StepfrequencyUnit spm;
	private LocationUnit location;
	private HeartrateUnit heartrate;
	private SpeedUnit speed;
	private DistanceUnit distance;
	private TimerUnit timer;

	// private double distanceDouble;

	@Override
	public void onBackPressed() {

	}

	@Override
	protected int getLayoutResource() {
		return R.layout.sensei_run_activity;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Button finishButton = (Button) findViewById(R.id.finishButton);
		finishButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				stopSensors();

				Intent intent = new Intent(RunActivity.this,
						AffectQuestionActivity.class);
				Bundle b = new Bundle();
				b.putInt("runstate", 1); // 1 == end
				intent.putExtras(b);
				startActivity(intent);
				finish();
			}
		});

		timer = (TimerUnit) findViewById(R.id.timer_unit);
		timer.startTimer(this);

		spm = (StepfrequencyUnit) findViewById(R.id.stepfrequency_unit);

		location = (LocationUnit) findViewById(R.id.location_unit);
		location.createMap(this);

		heartrate = (HeartrateUnit) findViewById(R.id.heartrate_unit);

		speed = (SpeedUnit) findViewById(R.id.speed_unit);

		distance = (DistanceUnit) findViewById(R.id.distance_unit);
		// distanceDouble = 0;

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				final Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						startSensors();
					}
				}, 1000);

			}
		});

		// setRandomVariables();
		//
		// updateTimer = new Timer();
		// updateTimer.schedule(new UpdateUnitTask(), 10, 1000);
	}

	// class UpdateUnitTask extends TimerTask {
	// @Override
	// public void run() {
	//
	// runOnUiThread(new Runnable() {
	// @Override
	// public void run() {
	// setRandomVariables();
	// }
	// });
	//
	// }
	// }
	//
	// private void setRandomVariables() {
	// Random r = new Random();
	// float speedInt = r.nextInt(9-7)+7;
	// spm.onEvent(new StepfrequencyData(0, r.nextInt(170-160)+160,
	// System.currentTimeMillis()));
	// location.onEvent(new LocationData("", "", 0, System.currentTimeMillis(),
	// 52 + (r.nextInt(3)*0.001), 4.9+ (r.nextInt(3)*0.001), 0, 0, 0, 0));
	// heartrate.onEvent(new HeartRateData(0, r.nextInt(140-130)+130,
	// System.currentTimeMillis()));
	// speed.onEvent(new LocationData("", "", 0, System.currentTimeMillis(),
	// 0,0, 0, 0, 0, speedInt));
	// distance.onEvent(new
	// DistanceData(0,distanceDouble,System.currentTimeMillis()));
	// distanceDouble += (speedInt / 60.0 / 60.0);
	// }

	@Override
	public void onPause() {
		super.onPause();
		distance.setAutoUpdate(false);
		speed.setAutoUpdate(false);
		// location.setAutoUpdate(false);
		spm.setAutoUpdate(false);
		heartrate.setAutoUpdate(false);
	}

	@Override
	public void onResume() {
		super.onResume();
		distance.setAutoUpdate(true);
		speed.setAutoUpdate(true);
		location.setAutoUpdate(true);
		spm.setAutoUpdate(true);
		heartrate.setAutoUpdate(true);
	}

	// This method will be called when a MessageEvent is posted
	public void onEvent(StepfrequencyData event) {
		final StepfrequencyData eventCopy = event;
		RunActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				spm.setInfo("" + eventCopy.getStepFrequency());
			}
		});
	}

	/*
	 * @Override public void onBackPressed() { // super.onBackPressed();
	 * finish(); }
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			// NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
