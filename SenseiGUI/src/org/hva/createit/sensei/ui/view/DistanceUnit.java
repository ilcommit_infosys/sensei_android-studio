package org.hva.createit.sensei.ui.view;

import org.hva.createit.scl.data.DistanceData;
import org.hva.createit.sensei.ui.R;

import android.content.Context;
import android.util.AttributeSet;

public class DistanceUnit extends BasicUnit {

	public DistanceUnit(Context context, AttributeSet attrs) {
		super(context, attrs);

		setHeader(getResources().getString(R.string.distance_header));
		setUnit(getResources().getString(R.string.distance_unit));

		info.setText("0.00");
	}

	public void onEvent(DistanceData event) {
		// convert distance from meters to km, with 0.00
		updateInfo("" + df.format(event.getDistance() / 1000));
	}

	/*
	 * Enter distance in meters
	 */
	public void setDistance(double distance) {
		updateInfo("" + df.format(distance / 1000));
	}
}
