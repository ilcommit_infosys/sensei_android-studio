package org.hva.createit.sensei.ui.view;

import org.hva.createit.scl.data.StepfrequencyData;
import org.hva.createit.sensei.ui.R;

import android.content.Context;
import android.util.AttributeSet;

public class StepfrequencyUnit extends BasicUnit {

	public StepfrequencyUnit(Context context, AttributeSet attrs) {
		super(context, attrs);

		setHeader(getResources().getString(R.string.stepfrequency_header));
		setUnit(getResources().getString(R.string.stepfrequency_unit));

		info.setText("0");
	}

	public void onEvent(StepfrequencyData event) {
		updateInfo("" + event.getStepFrequency());
	}

	public void setStepfrequency(float spm) {
		updateInfo("" + Math.round(spm));
	}
}
