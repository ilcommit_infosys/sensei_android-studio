package org.hva.createit.sensei.ui.view;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.hva.createit.sensei.ui.R;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class TimerUnit extends BasicUnit {
	Timer timer;
	Counter counter;
	Activity activity;

	private class Counter extends TimerTask {
		private Date startDateTime = new Date();

		@Override
		public void run() {

			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					updateTime((new Date()).getTime() - startDateTime.getTime());
				}
			});
		}

	}

	public TimerUnit(Context context, AttributeSet attrs) {
		super(context, attrs);

		setHeader(getResources().getString(R.string.duration));
		setUnit("");
		unit.setVisibility(View.INVISIBLE);
		info.setText("00:00:00");
	}

	public void startTimer(Activity activity) {
		this.activity = activity;

		// update database with starttime

		// starttimer
		this.counter = new Counter();

		this.timer = new Timer();
		this.timer.scheduleAtFixedRate(counter, delay, delay);

	}

	public void stopTimer() {
		// update datebase with stoptime
		// update database with duration time in seconds
		// stoptimer
		timer.cancel();
	}

	public void updateTime(long time) {
		long timeSeconds = time / 1000 % 60;
		long timeMinutes = time / (60 * 1000) % 60;
		long timeHours = time / (60 * 60 * 1000) % 24;
		if (timeHours == 0) {
			setInfo((timeMinutes < 10 ? "0" + timeMinutes : timeMinutes) + ":"
					+ (timeSeconds < 10 ? "0" + timeSeconds : timeSeconds));
		} else {
			setInfo((timeHours < 10 ? "0" + timeHours : timeHours) + ":"
					+ (timeMinutes < 10 ? "0" + timeMinutes : timeMinutes)
					+ ":"
					+ (timeSeconds < 10 ? "0" + timeSeconds : timeSeconds));
		}
	}
}
