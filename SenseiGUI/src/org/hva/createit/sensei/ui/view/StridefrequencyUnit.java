package org.hva.createit.sensei.ui.view;

import org.hva.createit.scl.data.StridefrequencyData;
import org.hva.createit.sensei.ui.R;

import android.content.Context;
import android.util.AttributeSet;

public class StridefrequencyUnit extends BasicUnit {

	public StridefrequencyUnit(Context context, AttributeSet attrs) {
		super(context, attrs);

		setHeader(getResources().getString(R.string.stridefrequency_header));
		setUnit(getResources().getString(R.string.stridefrequency_unit));

		info.setText("0");
	}

	public void onEvent(StridefrequencyData event) {
		updateInfo("" + event.getStridefrequency());
	}

	public void StridefrequencyData(float spm) {
		updateInfo("" + Math.round(spm));
	}
}
