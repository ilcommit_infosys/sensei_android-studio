package org.hva.createit.sensei.ui.view;

import java.util.ArrayList;
import java.util.List;

import org.hva.createit.scl.data.LocationData;
import org.hva.createit.sensei.ui.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class LocationUnit extends BasicUnit {
	private GoogleMap mMap;
	private Marker markerStart;
	private Marker markerFinish;
	private Activity activity;
	private Polyline polygon;
	private List<LatLng> positions;

	private int zoomFactor = 16;

	public LocationUnit(Context context, AttributeSet attrs) {
		super(context, attrs, R.layout.sensei_location_unit);

		setHeader(getResources().getString(R.string.location_header));
	}

	public void createMap(Activity activity) {
		this.activity = activity;
		if (setUpMapIfNeeded()) {
			setClickable(false);
			addPolyline();
		}
	}

	private void addPolyline() {
		PolylineOptions p = new PolylineOptions()
				.add(new LatLng(50.3598266, 4.9107966),
						new LatLng(50.3598266, 4.9107966)).width(20)
				.color(getResources().getColor(R.color.colorSecondaryLight));
		polygon = mMap.addPolyline(p);
		positions = new ArrayList<LatLng>();
	}

	/*
	 * Throws away the old polyline and draws a new one from the given location
	 * points
	 */
	public void setPositions(List<LocationData> locationDataList) {
		positions = new ArrayList<LatLng>();
		for (LocationData locationData : locationDataList) {
			positions.add(new LatLng(locationData.getLatitude()
					.getValueAsDouble(), locationData.getLongitude()
					.getValueAsDouble()));
		}

		polygon.setPoints(positions);
		if (positions.size() != 0) {
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
					positions.get(positions.size() - 1), 14);
			mMap.animateCamera(cameraUpdate);
			placeMarkerFinish(positions.get(0));
			placeMarkerStart(positions.get(positions.size() - 1));

		}
	}

	private void placeMarkerStart(LatLng position) {
		markerStart = mMap.addMarker(new MarkerOptions()
				.title("Start")
				.position(position)
				.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
	}

	private void placeMarkerFinish(LatLng position) {
		markerFinish = mMap.addMarker(new MarkerOptions().position(position)
				.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
	}

	public void onEvent(LocationData event) {
		if (setUpMapIfNeeded()) {
			LatLng latLng = new LatLng(event.getLatitude().getValueAsDouble(),
					event.getLongitude().getValueAsDouble());

			positions.add(latLng);
			polygon.setPoints(positions);

			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
					latLng, zoomFactor);
			mMap.animateCamera(cameraUpdate);
			if (markerStart == null) {
				placeMarkerStart(positions.get(0));
			}
			if (markerFinish == null) {
				placeMarkerFinish(latLng);
			} else {
				animateMarker(markerFinish, latLng, false);
			}
		}
	}

	public void animateMarker(final Marker marker, final LatLng toPosition,
			final boolean hideMarker) {
		final Handler handler = new Handler();
		final long start = SystemClock.uptimeMillis();
		Projection proj = mMap.getProjection();
		Point startPoint = proj.toScreenLocation(marker.getPosition());
		final LatLng startLatLng = proj.fromScreenLocation(startPoint);
		final long duration = 500;

		final LinearInterpolator interpolator = new LinearInterpolator();

		handler.post(new Runnable() {
			@Override
			public void run() {
				long elapsed = SystemClock.uptimeMillis() - start;
				float t = interpolator.getInterpolation((float) elapsed
						/ duration);
				double lng = t * toPosition.longitude + (1 - t)
						* startLatLng.longitude;
				double lat = t * toPosition.latitude + (1 - t)
						* startLatLng.latitude;
				marker.setPosition(new LatLng(lat, lng));

				if (t < 1.0) {
					// Post again 16ms later.
					handler.postDelayed(this, 16);
				} else {
					if (hideMarker) {
						marker.setVisible(false);
					} else {
						marker.setVisible(true);
					}
				}
			}
		});
	}

	private boolean setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null && activity != null) {
			mMap = ((MapFragment) activity.getFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				// The Map is verified. It is now safe to manipulate the map.
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	@Override
	public void setClickable(boolean clickable) {
		((MapFragment) activity.getFragmentManager().findFragmentById(R.id.map))
				.getView().setClickable(clickable);
	}

	public void captureMapScreen(SnapshotReadyCallback callback) {
		mMap.snapshot(callback);
	}

	public int[] getLocationOnScreen() {
		View map = activity.findViewById(R.id.map);
		int[] location = { 0, 0 };
		map.getLocationOnScreen(location);
		return location;
	}
}
