package org.hva.createit.sensei.ui.view;

import org.hva.createit.scl.data.HeartRateData;
import org.hva.createit.sensei.ui.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class HeartrateUnit extends BasicUnit {

	public HeartrateUnit(Context context, AttributeSet attrs) {
		super(context, attrs);

		setHeader(getResources().getString(R.string.heart_rate_header));
		setUnit(getResources().getString(R.string.heart_rate_unit_bpm));
		setIcon(getResources().getDrawable(R.drawable.heart));
		icon.setVisibility(View.VISIBLE);

		info.setText("0");
	}

	public void onEvent(HeartRateData event) {
		updateInfo("" + event.getHeartRate());
	}

	public void setHeartRate(int heartrate) {
		updateInfo("" + heartrate);
	}
}
