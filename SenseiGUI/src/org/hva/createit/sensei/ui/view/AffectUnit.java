package org.hva.createit.sensei.ui.view;

import java.util.List;

import org.hva.createit.scl.data.AffectData;
import org.hva.createit.sensei.ui.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;

import com.askcs.android.affectbutton.AffectButton;

public class AffectUnit extends BasicUnit {
	private AffectButton end;
	private AffectButton start;

	public AffectUnit(Context context, AttributeSet attrs) {

		super(context, attrs, R.layout.sensei_affect_unit);

		end = (AffectButton) findViewById(R.id.affectbuttonEnd);
		end.setPAD(0, 0, 0);

		start = (AffectButton) findViewById(R.id.affectbuttonStart);
		start.setPAD(0, 0, 0);
	}

	public void setAffectStart(AffectData affect) {
		start.setPAD(affect.getPleasure(), affect.getDominance(),
				affect.getArousal());
		start.takeScreenshot();
	}

	public void setAffectEnd(AffectData affect) {
		end.setPAD(affect.getPleasure(), affect.getDominance(),
				affect.getArousal());
		end.takeScreenshot();
	}

	/*
	 * AffectList should have only 2 items, a start and an end, more than 2
	 * items are ignored
	 */
	public void setAffect(List<AffectData> affectDataList) {
		if (affectDataList.get(0).getTimeStamp() > affectDataList.get(
				affectDataList.size() - 1).getTimeStamp()) {
			setAffectStart(affectDataList.get(affectDataList.size() - 1));
			setAffectEnd(affectDataList.get(0));
		} else {
			setAffectStart(affectDataList.get(0));
			setAffectEnd(affectDataList.get(affectDataList.size() - 1));
		}
	}

	public Bitmap getBitmap() {
		int[] locationStart = { 0, 0 };
		start.getLocationOnScreen(locationStart);

		int[] locationEnd = { 0, 0 };
		end.getLocationOnScreen(locationEnd);

		this.setDrawingCacheEnabled(true);
		Bitmap backBitmap = this.getDrawingCache();
		Canvas canvas = new Canvas(backBitmap);

		int[] locationView = { 0, 0 };
		this.getLocationOnScreen(locationView);

		Matrix startMatrix = new Matrix();
		startMatrix.postTranslate(locationStart[0] - locationView[0],
				locationStart[1] - locationView[1]);
		canvas.drawBitmap(start.getBitmap(), startMatrix, null);

		Matrix endMatrix = new Matrix();
		endMatrix.postTranslate(locationEnd[0] - locationView[0],
				locationEnd[1] - locationView[1]);
		canvas.drawBitmap(end.getBitmap(), endMatrix, null);

		return backBitmap;
	}

	@Override
	public void setClickable(boolean clickable) {
		super.setClickable(clickable);
		start.setClickable(clickable);
		end.setClickable(clickable);
	}
}
