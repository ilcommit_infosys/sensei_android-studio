package org.hva.createit.sensei.ui.view;

import org.hva.createit.sensei.ui.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;

import com.askcs.android.affectbutton.Affect;
import com.askcs.android.affectbutton.AffectButton;
import com.askcs.android.affectbutton.AffectListener;

public class AffectButtonView extends LinearLayout {
	private AffectButton mAffectButton;
	private boolean affect_confirm = false;
	public Button mConfirmButton;

	public AffectButtonView(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mInflater.inflate(R.layout.sensei_affectbutton, this, true);

		mConfirmButton = (Button) findViewById(R.id.confirm_button);
		// mConfirmButton.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// //save data and continue
		// }
		// });

		mAffectButton = (AffectButton) findViewById(R.id.affectbutton);
		mAffectButton.addListener(new AffectListener() {

			@Override
			public void onAffectChanged(Affect affect) {
				// set button to active
				affect_confirm = true;
				checkForConfirmButton();
			}
		});
	}

	private boolean checkForConfirmButton() {
		if (affect_confirm) {
			mConfirmButton.setEnabled(true);
			return true;
		} else if (!affect_confirm) {
			// please first rate using the affect button
		} else {
			// please first rate using the emotion buttons
		}
		return false;
	}

}
