package org.hva.createit.sensei.ui.view;

import org.hva.createit.scl.data.LocationData;
import org.hva.createit.sensei.ui.R;

import android.content.Context;
import android.util.AttributeSet;

public class SpeedUnit extends BasicUnit {

	public SpeedUnit(Context context, AttributeSet attrs) {
		super(context, attrs);

		setHeader(getResources().getString(R.string.speed_header));
		setUnit(getResources().getString(R.string.speed_unit));

		info.setText("0");
	}

	public void onEvent(LocationData event) {
		updateInfo("" + Math.round(event.getSpeed().getValueAsFloat() * 3.6));
	}

	/*
	 * speed in m/s
	 */
	public void setSpeed(float speed) {
		updateInfo("" + Math.round(speed * 3.6));
	}
}
