package org.hva.createit.sensei.ui;

import org.hva.createit.scl.data.QuestionnaireData;
import org.hva.createit.scl.dataaccess.QuestionnaireDAO;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

public class QuestionnaireInjuriesActivity extends BaseActivity {
	TextView heading2, remark1, remark2;
	LinearLayout injuries_layout_01, injuries_layout_02, injuries_layout_03,
			injuries_layout_04, injuries_layout_05, injuries_layout_06;
	String answers = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		answers = "";
		
		heading2 = (TextView) findViewById(R.id.injuries_heading_02);
		remark1 = (TextView) findViewById(R.id.injuries_remarks_01);
		remark2 = (TextView) findViewById(R.id.injuries_remarks_02);
		injuries_layout_01 = (LinearLayout) findViewById(R.id.sensei_injuries_questionnaire_question01);
		injuries_layout_02 = (LinearLayout) findViewById(R.id.sensei_injuries_questionnaire_question02);
		injuries_layout_03 = (LinearLayout) findViewById(R.id.sensei_injuries_questionnaire_question03);
		injuries_layout_04 = (LinearLayout) findViewById(R.id.sensei_injuries_questionnaire_question04);
		injuries_layout_05 = (LinearLayout) findViewById(R.id.sensei_injuries_questionnaire_question05);
		injuries_layout_06 = (LinearLayout) findViewById(R.id.sensei_injuries_questionnaire_question06);

		RadioGroup injuries_01 = (RadioGroup) injuries_layout_01.findViewById(R.id.injuries_01_answer);
		injuries_01
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch (checkedId) {
						case R.id.injuries_01_answer_01:
							storeAnswer(1, getString(R.string.injuries_01_question), getString(R.string.injuries_01_answer_01), 1);
							showPreviousInjuries();
							injuries_layout_02.setVisibility(View.GONE);
							break;
						case R.id.injuries_01_answer_02:
							storeAnswer(1, getString(R.string.injuries_01_question), getString(R.string.injuries_01_answer_02), 2);
							injuries_layout_02.setVisibility(View.VISIBLE);
							hidePreviousInjuries();
							break;
						case R.id.injuries_01_answer_03:
							storeAnswer(1, getString(R.string.injuries_01_question), getString(R.string.injuries_01_answer_03), 3);
							injuries_layout_02.setVisibility(View.VISIBLE);
							hidePreviousInjuries();
							break;
						}
					}
				});

		RadioGroup injuries_02 = (RadioGroup) injuries_layout_02.findViewById(R.id.injuries_02_answer);
		injuries_02
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch (checkedId) {
						case R.id.injuries_02_answer_01:
							storeAnswer(2, getString(R.string.injuries_02_question), getString(R.string.injuries_02_answer_01), 1);
							injuries_layout_03.setVisibility(View.VISIBLE);

							break;
						case R.id.injuries_02_answer_02:
							storeAnswer(2, getString(R.string.injuries_02_question), getString(R.string.injuries_02_answer_02), 2);
							injuries_layout_03.setVisibility(View.VISIBLE);

							break;
						}
					}
				});
		
		((EditText) injuries_layout_03.findViewById(R.id.injuries_03_answer_01))
		.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				storeAnswer(3, getString(R.string.injuries_03_question), s.toString() + " " + getString(R.string.injuries_03_answer_01), 1);
				showPreviousInjuries();
			}
		});

		RadioGroup injuries_04 = (RadioGroup) injuries_layout_04.findViewById(R.id.injuries_04_answer);
		injuries_04
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch (checkedId) {
						case R.id.injuries_04_answer_01:
							storeAnswer(4, getString(R.string.injuries_04_question), getString(R.string.injuries_04_answer_01), 1);
							remark2.setVisibility(View.GONE);
							injuries_layout_05.setVisibility(View.GONE);
							injuries_layout_06.setVisibility(View.GONE);
							end();
							break;
						case R.id.injuries_04_answer_02:
							storeAnswer(4, getString(R.string.injuries_04_question), getString(R.string.injuries_04_answer_02), 2);
							remark2.setVisibility(View.VISIBLE);
							injuries_layout_05.setVisibility(View.VISIBLE);
							injuries_layout_06.setVisibility(View.GONE);
							break;
						case R.id.injuries_04_answer_03:
							storeAnswer(4, getString(R.string.injuries_04_question), getString(R.string.injuries_04_answer_03), 3);
							remark2.setVisibility(View.VISIBLE);
							injuries_layout_05.setVisibility(View.VISIBLE);
							injuries_layout_06.setVisibility(View.GONE);
							break;
						}
					}
				});
		RadioGroup injuries_05 = (RadioGroup) injuries_layout_05.findViewById(R.id.injuries_02_answer);
		injuries_05
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch (checkedId) {
						case R.id.injuries_02_answer_01:
							storeAnswer(5, getString(R.string.injuries_02_question), getString(R.string.injuries_02_answer_01), 1);
							injuries_layout_06.setVisibility(View.VISIBLE);
							break;
						case R.id.injuries_02_answer_02:
							storeAnswer(5, getString(R.string.injuries_02_question), getString(R.string.injuries_02_answer_02), 2);
							injuries_layout_06.setVisibility(View.VISIBLE);
							break;
						}
					}
				});
		
		((EditText) injuries_layout_06.findViewById(R.id.injuries_03_answer_01))
		.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				storeAnswer(6, getString(R.string.injuries_03_question), s.toString() + " " + getString(R.string.injuries_03_answer_01), 1);
				end();
			}
		});
	}

	private void showPreviousInjuries() {
		heading2.setVisibility(View.VISIBLE);
		injuries_layout_04.setVisibility(View.VISIBLE);
	}
	
	private void hidePreviousInjuries() {
		heading2.setVisibility(View.GONE);
		injuries_layout_03.setVisibility(View.GONE);
		injuries_layout_04.setVisibility(View.GONE);
		injuries_layout_05.setVisibility(View.GONE);
		injuries_layout_06.setVisibility(View.GONE);
		
	}

	private void end() {
		Button submit_button = (Button) findViewById(R.id.submit_button);
		submit_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//submit results and go to next screen;
//				String questionnaire_result = "{questionnaire:'injuries',datetime:'"+System.currentTimeMillis()+"', questions:["+answers+"]}";
				QuestionnaireDAO qdao = new QuestionnaireDAO(QuestionnaireInjuriesActivity.this);
				qdao.store(new QuestionnaireData("injuries", answers, System.currentTimeMillis()));
				
				SharedPreferences prefs = QuestionnaireInjuriesActivity.this.getSharedPreferences(getString(R.string.pref_questionnaire),
						Context.MODE_PRIVATE);
				prefs.edit().putBoolean(getString(R.string.pref_questionnaire_injuries),false).commit();
				
				//go to next screen
				finish();
			}
		});
		submit_button.setVisibility(View.VISIBLE);
		
	}
	
	private void storeAnswer(int question_nr, String question, String answer, int option) {
		//"{questionnaire:'',datetime:'', questions:[{question_number:'', question:'"+question+"', answer:'"+answer+"', answer_option:"+option+", datetime:"+  System.currentTimeMillis()+"}]";
		if(answers.compareTo("") != 0) {
			answers +=",";
		}
		answers += "{question_number:'"+ question_nr+"', question:'"+question+"', answer:'"+answer+"', answer_option:"+option+", datetime:"+  System.currentTimeMillis()+"}";
	}
	
	@Override
	protected int getLayoutResource() {
		return R.layout.sensei_injuries_questionnaire;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
