package org.hva.createit.sensei.ui;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.scl.dataaccess.AffectDAO;
import org.hva.createit.scl.dataaccess.LocationDAO;
import org.hva.createit.scl.dataaccess.RunStatisticsDAO;
import org.hva.createit.sensei.ui.view.AffectUnit;
import org.hva.createit.sensei.ui.view.DistanceUnit;
import org.hva.createit.sensei.ui.view.HeartrateUnit;
import org.hva.createit.sensei.ui.view.LocationUnit;
import org.hva.createit.sensei.ui.view.SpeedUnit;
import org.hva.createit.sensei.ui.view.StepfrequencyUnit;
import org.hva.createit.sensei.ui.view.TimerUnit;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;

public class RunOverviewActivity extends BaseActivity {
	private final String TAG = "RunOverviewActivity";
	private TextView title;
	private StepfrequencyUnit spm;
	private LocationUnit location;
	private HeartrateUnit heartrate;
	private SpeedUnit speed;
	private DistanceUnit distance;
	private TimerUnit timer;
	private AffectUnit affect;
	private int runId = 0;
	private AffectDAO ad;
	private RunStatisticsDAO rsd;
	private LocationDAO ld;

	@Override
	protected int getLayoutResource() {
		return R.layout.sensei_run_overview_activity;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		title = (TextView) findViewById(R.id.runTitle);
		timer = (TimerUnit) findViewById(R.id.timer_unit);
		spm = (StepfrequencyUnit) findViewById(R.id.stepfrequency_unit);
		location = (LocationUnit) findViewById(R.id.location_unit);
		location.createMap(this);
		heartrate = (HeartrateUnit) findViewById(R.id.heartrate_unit);
		speed = (SpeedUnit) findViewById(R.id.speed_unit);
		distance = (DistanceUnit) findViewById(R.id.distance_unit);
		affect = (AffectUnit) findViewById(R.id.affect_unit);
		affect.setClickable(false);

		Button shareButton = (Button) findViewById(R.id.shareButton);
		shareButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				final LinearLayout mView = (LinearLayout) findViewById(R.id.run_overview_layout);

				SnapshotReadyCallback callback = new SnapshotReadyCallback() {

					@Override
					public void onSnapshotReady(Bitmap snapshot) {
						try {
							mView.setDrawingCacheEnabled(true);
							Bitmap backBitmap = mView.getDrawingCache();
							Bitmap bmOverlay = Bitmap.createBitmap(
									backBitmap.getWidth(),
									backBitmap.getHeight(),
									backBitmap.getConfig());
							Canvas canvas = new Canvas(bmOverlay);

							int[] locationScreen = { 0, 0 };
							mView.getLocationOnScreen(locationScreen);
							int[] locationMap = location.getLocationOnScreen();
							locationMap[0] = locationMap[0] - locationScreen[0];
							locationMap[1] = locationMap[1] - locationScreen[1];

							Matrix matrix = new Matrix();
							matrix.postTranslate(locationMap[0], locationMap[1]);

							canvas.drawBitmap(backBitmap, 0, 0, null);
							canvas.drawBitmap(snapshot, matrix, null);
							AffectUnit af = (AffectUnit) RunOverviewActivity.this
									.findViewById(R.id.affect_unit);
							Matrix afMatrix = new Matrix();
							int[] afLocation = { 0, 0 };
							af.getLocationOnScreen(afLocation);
							afMatrix.postTranslate(afLocation[0]
									- locationScreen[0], afLocation[1]
									- locationScreen[1]);
							canvas.drawBitmap(af.getBitmap(), afMatrix, null);

							String screenshotLocation = Environment
									.getExternalStorageDirectory()
									.getAbsolutePath()
									+ "/Sensei/Screenshot";

							Util.createFolderIfNeeded(screenshotLocation);

							screenshotLocation += "/Share"
									+ System.currentTimeMillis() + ".jpg";

							RunOverviewActivity.savePic(bmOverlay,
									screenshotLocation);

							Intent sharingIntent = new Intent(
									Intent.ACTION_SEND);
							Uri screenshotUri = Uri.parse("file://"
									+ screenshotLocation);
							sharingIntent.setType("*/*");// shareIntent.setType("*/*");
							sharingIntent
									.putExtra(Intent.EXTRA_TEXT,
											"I ran with intelligence with #COMMIT SENSEI");
							sharingIntent.putExtra(Intent.EXTRA_STREAM,
									screenshotUri);
							startActivity(Intent.createChooser(sharingIntent,
									"Share run using"));

						} catch (OutOfMemoryError e) {
							Intent sharingIntent = new Intent(
									Intent.ACTION_SEND);
							sharingIntent.setType("*/*");// shareIntent.setType("*/*");
							sharingIntent
									.putExtra(Intent.EXTRA_TEXT,
											"I ran with intelligence with #COMMIT SENSEI");
							startActivity(Intent.createChooser(sharingIntent,
									"Share run using"));
						}
					}
				};
				location.captureMapScreen(callback);

			}
		});

		ad = new AffectDAO(RunOverviewActivity.this);
		rsd = new RunStatisticsDAO(RunOverviewActivity.this);
		ld = new LocationDAO(RunOverviewActivity.this);

		Bundle b = getIntent().getExtras();
		if (b != null) {
			runId = b.getInt("runId");
		}

		new Handler().post(new Runnable() {
			@Override
			public void run() {
				displayLastRunData(runId);
			}
		});
	}

	private void displayLastRunData(int runId) {
		RunOverviewData rod;
		if (runId != 0) {
			rod = rsd.getRunOverview(runId);
		} else {
			rod = rsd.getLastRunOverview();
		}
		try {
			location.setPositions(ld.getItems(rod.getStart_timestamp(),
					rod.getStop_timestamp()));

			title.setText(rod.getStartDateAsString());
			timer.updateTime(rod.getDuration());
			distance.setDistance(rod.getDistance());
			speed.setSpeed(rod.getAverageSpeed());
			spm.setStepfrequency(rod.getAverageStepFrequency());
			heartrate.setHeartRate(rod.getAverageHeartRate());
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		// List<AffectData> affectDataList = ad.getLastAffectData();
		// affect.setAffect(affectDataList);
		affect.setAffect(rod.getAffect());
	}

	private static Bitmap takeScreenShot(Activity activity) {
		View view = activity.getWindow().getDecorView();
		view.setDrawingCacheEnabled(true);
		view.buildDrawingCache();
		Bitmap b1 = view.getDrawingCache();
		Rect frame = new Rect();
		activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
		int statusBarHeight = frame.top;
		Point size = new Point();
		activity.getWindowManager().getDefaultDisplay().getSize(size);
		int width = size.x;
		int height = size.y;

		Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height
				- statusBarHeight);
		view.destroyDrawingCache();
		return b;
	}

	private static void savePic(Bitmap b, String strFileName) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(strFileName);
			if (null != fos) {
				b.compress(Bitmap.CompressFormat.PNG, 90, fos);
				fos.flush();
				fos.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		NavUtils.navigateUpFromSameTask(this);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}
}
