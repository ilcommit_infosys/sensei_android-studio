package org.hva.createit.sensei.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;

public class SplashActvity extends Activity implements Eula.OnEulaAgreedTo {

	private boolean mAlreadyAgreedToEula = false;
	private SplashActvity sp;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.sensei_splash_activity);
		sp = this;
		startSplashThread();

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			// active = false;
		}
		return true;
	}

	/** {@inheritDoc} */
	@Override
	public void onEulaAgreedTo() {

		Intent newIntent = new Intent(SplashActvity.this, LoginActivity.class);
		// newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

		newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(newIntent);

	}

	private void startSplashThread() {

		SharedPreferences prefs = this.getSharedPreferences("LoginData",
				Context.MODE_PRIVATE);
		String storedUname = prefs.getString("Uname", null);
		if (storedUname != null) {
			mAlreadyAgreedToEula = true;
		} else {
			mAlreadyAgreedToEula = Eula.show(sp);
		}
		if (mAlreadyAgreedToEula) {

			Intent newIntent = new Intent(SplashActvity.this,
			// MainActivity.class);
					LoginActivity.class);
			newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			// newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(newIntent);

		}

	}

	@Override
	public void onResume() {
		super.onResume();
	}

}
