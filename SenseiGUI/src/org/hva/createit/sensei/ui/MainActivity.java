package org.hva.createit.sensei.ui;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.infy.ilcommit.intelirun.SendData;

import org.focuser.sendmelogs.LogCollector;

public class MainActivity extends BaseActivity implements View.OnClickListener,
        android.content.DialogInterface.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public static final int DIALOG_SEND_LOG = 345350;
	protected static final String TAG = "MainActivity";
    protected static final int DIALOG_PROGRESS_COLLECTING_LOG = 3255;
    protected static final int DIALOG_FAILED_TO_COLLECT_LOGS = 3535122;
    private static final int DIALOG_REPORT_FORCE_CLOSE = 3535788;
	private DrawerLayout drawer;
	private ListView mDrawerList;
	private int currentPosition = 1;
    private GoogleApiClient mGoogleApiClient;
	private LogCollector mLogCollector;

	// private BufferedWriter writer;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setActionBarIcon(R.drawable.ic_ab_drawer);
		/**
		 * Start Infosys - logger code
		 */
		mLogCollector = new LogCollector(this);
		CheckForceCloseTask task = new CheckForceCloseTask();
		task.execute();
		/**
		 * End Infosys - logger code
		 */

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(new Scope(Scopes.PLUS_LOGIN))
                .build();


		drawer = (DrawerLayout) findViewById(R.id.drawer);
		drawer.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
		mDrawerList = (ListView) findViewById(R.id.menu_drawer);

		// Set the adapter for the list view
        String mPlanetTitles[] = {"New run", "Overview", "Previous runs",
                "Challenges", "Settings", "Logout", "Send crash logs"};
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, mPlanetTitles));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		Button newRunButton = (Button) findViewById(R.id.newRunButton);
		newRunButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(MainActivity.this,
						AffectQuestionActivity.class));
				overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);

				// scheduleNotification(getNotification("30 second delay"),
				// 1000);

			}
		});
		
		Button newInjuriesQuestionnaireButton = (Button) findViewById(R.id.newInjuriesQuestionnaireButton);
		newInjuriesQuestionnaireButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(MainActivity.this,
						QuestionnaireInjuriesActivity.class));
				overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);

				// scheduleNotification(getNotification("30 second delay"),
				// 1000);

			}
		});
		
		Button newPersonalityQuestionnaireButton = (Button) findViewById(R.id.newPersonalityQuestionnaireButton);
		newPersonalityQuestionnaireButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(MainActivity.this,
						QuestionnairePersonalityActivity.class));
				overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);

				// scheduleNotification(getNotification("30 second delay"),
				// 1000);

			}
		});

		
		
		FragmentManager fragmentManager = getFragmentManager();

		FrameLayout frame = (FrameLayout) findViewById(R.id.extra_content_frame);
		frame.removeAllViewsInLayout();

		RunOverviewListFragment runOverviewListFragment = new RunOverviewListFragment();
		Bundle args = new Bundle();
		args.putInt("items", 3);
		runOverviewListFragment.setArguments(args);

		fragmentManager.beginTransaction()
				.replace(R.id.extra_content_frame, runOverviewListFragment)
				.commit();
		Intent serviceIntent = new Intent(this, SendData.class);

		startService(serviceIntent);

	}

	@Override
	public void onClick(View v) {

	}

	/**
	 * Start Infosys - logger code
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = null;
		switch (id) {
		case DIALOG_SEND_LOG:
		case DIALOG_REPORT_FORCE_CLOSE:
			Builder builder = new AlertDialog.Builder(this);
			String message;
			if (id == DIALOG_SEND_LOG)
				message = "Do you want to send me your logs?";
			else
				message = "It appears this app has been force-closed, do you want to report it to me?";
			builder.setTitle("Warning")
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setMessage(message).setPositiveButton("Yes", this)
					.setNegativeButton("No", this);
			dialog = builder.create();
			break;
		case DIALOG_PROGRESS_COLLECTING_LOG:
			ProgressDialog pd = new ProgressDialog(this);
			pd.setTitle("Progress");
			pd.setMessage("Collecting logs...");
			pd.setIndeterminate(true);
			dialog = pd;
			break;
		case DIALOG_FAILED_TO_COLLECT_LOGS:
			builder = new AlertDialog.Builder(this);
			builder.setTitle("Error").setMessage("Failed to collect logs.")
					.setNegativeButton("OK", null);
			dialog = builder.create();
		}
		return dialog;
	}

	/**
	 * End Infosys - logger code
	 */

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_main;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			drawer.openDrawer(Gravity.START);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	/** Swaps fragments in the main content view */
	private void selectItem(int position) {
		if (position == 3) {
			if (!Util.openApp(this, "alm.motiv.AlmendeMotivator")) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri
						.parse("market://details?id=alm.motiv.AlmendeMotivator"));
				startActivity(intent);
			}
			return;
		}
		if (currentPosition == position) {
			mDrawerList.setItemChecked(position, true);
			drawer.closeDrawer(mDrawerList);
			return;
		} else {
			currentPosition = position;
		}
		if (position == 0) {
			startActivity(new Intent(MainActivity.this,
					AffectQuestionActivity.class));
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			mDrawerList.setItemChecked(position, true);
			// setTitle("Settings");
			drawer.closeDrawer(mDrawerList);
		} else if (position == 1) {
			Intent newIntent = new Intent(MainActivity.this, MainActivity.class);
			newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(newIntent);

			mDrawerList.setItemChecked(position, true);
			// setTitle("Settings");
			drawer.closeDrawer(mDrawerList);
		} else if (position == 2) {
			// Insert the fragment by replacing any existing fragment
			FragmentManager fragmentManager = getFragmentManager();

			FrameLayout frame = (FrameLayout) findViewById(R.id.content_frame);
			frame.removeAllViewsInLayout();

			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, new RunOverviewListFragment())
					.commit();

			mDrawerList.setItemChecked(position, true);
			// setTitle("Settings");
			drawer.closeDrawer(mDrawerList);
		} else if (position == 4) {
			// Insert the fragment by replacing any existing fragment
			FragmentManager fragmentManager = getFragmentManager();

			FrameLayout frame = (FrameLayout) findViewById(R.id.content_frame);
			frame.removeAllViewsInLayout();

			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, new SettingsFragment())
					.commit();

			// Highlight the selected item, update the title, and close the
			// drawer
			mDrawerList.setItemChecked(position, true);
			// setTitle("Settings");
			drawer.closeDrawer(mDrawerList);
		}/**
		 * Start Infosys - logout code
		 */
		else if (position == 5) {

			SharedPreferences prefs = this.getSharedPreferences("LoginData",
					Context.MODE_PRIVATE);
			prefs.edit().putString("Uname", null).apply();
			prefs.edit().putString("Password", null).apply();

			stopService(new Intent(this, SendData.class));

            /**Fb logout begins**/
            LoginManager mLoginManager = LoginManager.getInstance();
            mLoginManager.logOut();
            /**fb logout ends**/


            /**google logout begins**/
            if (mGoogleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);

                Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                mGoogleApiClient.disconnect();
                                mGoogleApiClient.connect();
                            }
                        });
            }
            /**google logout ends**/

			Intent newIntent = new Intent(MainActivity.this,
					LoginActivity.class);
			newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(newIntent);

			mDrawerList.setItemChecked(position, true);
			// setTitle("Settings");
			drawer.closeDrawer(mDrawerList);
			/**
			 * End Infosys - logout code
			 */
		}
		/**
		 * Start Infosys - logger code
		 */
		else if (position == 6) {
			showDialog(DIALOG_SEND_LOG);
			/**
			 * End Infosys - logger code
			 */
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		// mTitle = title;
		getActionBar().setTitle(title);
	}

	private void scheduleNotification(Notification notification, int delay) {

		Intent notificationIntent = new Intent(this,
				NotificationPublisher.class);
		notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
		notificationIntent.putExtra(NotificationPublisher.NOTIFICATION,
				notification);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		long futureInMillis = SystemClock.elapsedRealtime() + delay;
		AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis,
				pendingIntent);
	}

	private Notification getNotification(String content) {
		Notification.Builder builder = new Notification.Builder(this);
		builder.setContentTitle("Scheduled Notification");
		builder.setContentText(content);
		builder.setSmallIcon(R.drawable.ic_launcher);
		return builder.build();
	}

	public class NotificationPublisher extends BroadcastReceiver {

		public static final String NOTIFICATION_ID = "notification-id";
		public static final String NOTIFICATION = "notification";

		@Override
		public void onReceive(Context context, Intent intent) {

			NotificationManager notificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);

			Notification notification = intent.getParcelableExtra(NOTIFICATION);
			int id = intent.getIntExtra(NOTIFICATION_ID, 0);
			notificationManager.notify(id, notification);

		}
	}

	
	@Override
	protected void onResume() {
		SharedPreferences prefs = this.getSharedPreferences(getString(R.string.pref_questionnaire),
				Context.MODE_PRIVATE);
//		Boolean storedUname = prefs.getBoolean(getString(R.string.pref_questionnaire_injuries), true);
//		if (!storedUname) {
//			Button newInjuriesQuestionnaireButton = (Button) findViewById(R.id.newInjuriesQuestionnaireButton);
//			newInjuriesQuestionnaireButton.setVisibility(View.GONE);
//		}
//		 storedUname = prefs.getBoolean(getString(R.string.pref_questionnaire_personality), true);
//		if (!storedUname) {
//			Button newPersonalityQuestionnaireButton = (Button) findViewById(R.id.newPersonalityQuestionnaireButton);
//			newPersonalityQuestionnaireButton.setVisibility(View.GONE);
//		}
		super.onResume();
	}
	
	
	/**
	 * Start Infosys - stopping service code
	 */
	@Override
	protected void onDestroy() {
		stopService(new Intent(this, SendData.class));

		super.onDestroy();

		Log.d("MainActivity", "Stop Service CALLED");
	}

	/**
	 * End Infosys - stopping service code
	 */
	@Override
	public void onBackPressed() {
		// super.onBackPressed();
		finish();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case DialogInterface.BUTTON_POSITIVE:
			new AsyncTask<Void, Void, Boolean>() {
				@Override
				protected Boolean doInBackground(Void... params) {
					return mLogCollector.collect();
				}

				@Override
				protected void onPreExecute() {
					showDialog(DIALOG_PROGRESS_COLLECTING_LOG);
				}

				@Override
				protected void onPostExecute(Boolean result) {
					dismissDialog(DIALOG_PROGRESS_COLLECTING_LOG);
					if (result)
						mLogCollector.sendLog("shruti_bansal01@Infosys.com",
								"Error Log", "Preface\nPreface line 2");
					else
						showDialog(DIALOG_FAILED_TO_COLLECT_LOGS);
				}

			}.execute();
		}
		dialog.dismiss();
	}

	/**
     * Start Infosys - google code
     */
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    /**
	 * End Infosys - logger code
	 */

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * Start Infosys - logger code
     */
    class CheckForceCloseTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            return mLogCollector.hasForceCloseHappened();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                showDialog(DIALOG_REPORT_FORCE_CLOSE);
            }
        }
    }

/**
 * End Infosys - google code
 */


}


