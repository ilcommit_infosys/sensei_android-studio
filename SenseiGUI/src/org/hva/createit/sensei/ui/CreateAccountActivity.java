package org.hva.createit.sensei.ui;

/**Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this 
 * Infosys proprietary software program ("Program"), this Program is protected 
 * by copyright laws, international treaties and other pending or existing 
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation 
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and 
 * will be prosecuted to the maximum extent possible under the law
 **/

import nl.sense_os.service.ISenseServiceCallback;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.infy.ilcommit.intelirun.Infosys_SenseService;

/**
 * @author shruti_bansal01@Infosys.com
 * @description Activity to register new Sense user, set user profile,
 * add new user to sensei_group and login with new credentials.
 *
 */
public class CreateAccountActivity extends Activity {
	SharedPreferences sharedPref;
	private Button tvDisplayDate;
	// private DatePicker dpResult;
	private int year;
	private int month;
	private int day;
	static final int DATE_DIALOG_ID = 999;
	private static final String TAG = "RegistrationActivity";

	// Values for email and password at the time of the registration attempt.
	// private String mEmail;
	Infosys_SenseService Infosys_SenseService;
	// private String mPassword;
	// private String mPasswordConfirmation;

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mRegistrationFormView;
	private EditText mConfirmPasswordView;
	private TextView mRegistrationStatusMessageView;
	private View mRegistrationStatusView;
	String mEmail;
	String mPassword ;
	private boolean mBusy;

	private ISenseServiceCallback mSenseCallback = new ISenseServiceCallback.Stub() {

		@Override
		public void onChangeLoginResult(int result) throws RemoteException {
			// not used
		}

		@Override
		public void onRegisterResult(int result) throws RemoteException {
			mBusy = false;

			if (result == -2) {
				// registration forbidden
				onRegistrationFailure(true);

			} else if (result == -1) {
				// registration failed
				onRegistrationFailure(false);

			} else {
				mEmail = ((EditText) findViewById(R.id.email)).getText()
						.toString();
				mPassword = ((EditText) findViewById(R.id.password)).getText()
						.toString();
				onRegistrationSuccess();
			}
		}

		@Override
		public void statusReport(int status) throws RemoteException {
			// not used
		}
	};

	@Override
	public void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView();
		Infosys_SenseService = new Infosys_SenseService(this);
		// Set up the registration form.
		mEmailView = (EditText) findViewById(R.id.email);

		mPasswordView = (EditText) findViewById(R.id.password);
		mConfirmPasswordView = (EditText) findViewById(R.id.confirm_password);
		mRegistrationFormView = findViewById(R.id.registration_form);
		mRegistrationStatusView = findViewById(R.id.registration_status);
		mRegistrationStatusMessageView = (TextView) findViewById(R.id.registration_status_message);

		/** Start Hva Registration code**/
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

		((EditText) findViewById(R.id.name))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(getString(R.string.pref_user_name),
								s.toString());
						editor.commit();
					}
				});

		((EditText) findViewById(R.id.password))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(
								getString(R.string.pref_user_password),
								s.toString());
						editor.commit();
					}
				});

		((EditText) findViewById(R.id.height))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(getString(R.string.pref_user_height),
								s.toString());
						editor.commit();
					}
				});
		((EditText) findViewById(R.id.weight))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(getString(R.string.pref_user_weight),
								s.toString());
						editor.commit();
					}
				});
		/** End Hva Registration code **/

		// dpResult = ((DatePicker) findViewById(R.id.date_of_birth));
		tvDisplayDate = ((Button) findViewById(R.id.tvDate));
		tvDisplayDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				showDialog(DATE_DIALOG_ID);

			}

		});

		RadioGroup rg = (RadioGroup) findViewById(R.id.gender_group);

		rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				SharedPreferences.Editor editor = sharedPref.edit();

				switch (checkedId) {
				case R.id.gender_female:
					editor.putString(getString(R.string.pref_user_gender),
							getString(R.string.gender_female_label));
					break;
				case R.id.gender_male:
					editor.putString(getString(R.string.pref_user_gender),
							getString(R.string.gender_male_label));
					break;
				}
				editor.commit();
			}
		});

		findViewById(R.id.registration_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptRegistration();
					}
				});

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			return new DatePickerDialog(this, datePickerListener, year, month,
					day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// set selected date into textview
			tvDisplayDate.setText(new StringBuilder().append(month + 1)
					.append("-").append(day).append("-").append(year)
					.append(" "));

			// set selected date into datepicker also
			// dpResult.init(year, month, day, null);

		}
	};

	public void setContentView() {
		setContentView(R.layout.sensei_user_registration_activity);
	}

	/**
	 * Attempts to register the account specified by the registration form. If
	 * there are form errors (invalid email, missing fields, etc.), the errors
	 * are presented and no actual registration attempt is made.
	 */
	public void attemptRegistration() {
		// mEmail, password, email, address, zipCode, country, firstName,
		// surname, mobileNumber, callback
		mEmailView = ((EditText) findViewById(R.id.email));
		mPasswordView = ((EditText) findViewById(R.id.password));
		mConfirmPasswordView = ((EditText) findViewById(R.id.confirm_password));
		String email = mEmailView.getText().toString();
		;
		String password = mPasswordView.getText().toString();

		String mPasswordConfirmation = mConfirmPasswordView.getText()
				.toString();
		if (mBusy) {
			return;
		}
		Log.d(TAG, "inside attemptRegistration");
		Log.d(TAG, "inside attemptRegistration");
		// Reset errors.
		boolean cancel = false;
		View focusView = null;
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Check for a valid password.
		if (TextUtils.isEmpty(password)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid password confirmation.
		if (TextUtils.isEmpty(mPasswordConfirmation)) {
			mConfirmPasswordView
					.setError(getString(R.string.error_field_required));
			focusView = mConfirmPasswordView;
			cancel = true;
		} else if (!password.equals(mPasswordConfirmation)) {
			mConfirmPasswordView
					.setError(getString(R.string.error_password_match));
			focusView = mConfirmPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(email)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}
		if (!(android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
		mEmailView.setError("Invalid Email Address");
		focusView = mEmailView;
		cancel = true;
		}

		
		if (cancel) {
			// There was an error; don't attempt registration and focus the
			// first form field with an
			// error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user
			// registration attempt.
			mRegistrationStatusMessageView
					.setText(R.string.progress_registering);
			showProgress(true);

			// register using SensePlatform

			try {
				Log.d(TAG, "attempting Registration inside run()");
				mSenseCallback.onChangeLoginResult(Infosys_SenseService
						.attemptRegistrationAtCloud(email, password, email,
								null, null, null, email, null, null,
								mSenseCallback));

				// join group sensei_group
				// SenseApi.joinGroup(mApplication.getSensePlatform().getContext(),"23110");

				// this is an asynchronous call, we get a callback when the
				// registration is complete
				mBusy = true;
			} catch (IllegalStateException e) {
				Log.w(TAG, "Failed to register at SensePlatform!", e);
				onRegistrationFailure(false);
			} catch (RemoteException e) {
				Log.w(TAG, "Failed to register at SensePlatform!", e);
				onRegistrationFailure(false);
			}
		}
		/*
		 * catch (JSONException e) { Log.w(TAG,
		 * "Failed to join group sensei_group!", e);
		 * onRegistrationFailure(false); } catch (IOException e) { Log.w(TAG,
		 * "Failed to join group sensei_group!", e);
		 * onRegistrationFailure(false); }
		 */
	}

	private void onRegistrationFailure(final boolean forbidden) {

		// update UI
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				showProgress(false);

				/*
				 * if (forbidden) { mPasswordView.setError(getString(R.string.
				 * error_incorrect_password)); mPasswordView.requestFocus(); }
				 * else {
				 */
				Toast.makeText(CreateAccountActivity.this,
						R.string.register_failure, Toast.LENGTH_LONG).show();
				// }
			}
		});

	}

	protected void onRegistrationSuccess() {
		Log.d(TAG, "inside onRegistrationSuccess");

		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		String mEmail = this.mEmail;
		String mPassword = this.mPassword;
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(getString(R.string.pref_user_email), mEmail);
		editor.putString(getString(R.string.pref_user_password), mPassword);
		editor.commit();
		// update UI
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				showProgress(false);
				Toast.makeText(CreateAccountActivity.this,
						R.string.register_success, Toast.LENGTH_LONG).show();
			}
		});
		// finish registration activity
		setResult(RESULT_OK);
		finish();
	}

	

	/**
	 * Shows the progress UI and hides the registration form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy
		// animations. If available, use these APIs to fade-in the progress
		// spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mRegistrationStatusView.setVisibility(View.VISIBLE);
			mRegistrationStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mRegistrationStatusView
									.setVisibility(show ? View.VISIBLE
											: View.GONE);
						}
					});

			mRegistrationFormView.setVisibility(View.VISIBLE);
			mRegistrationFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mRegistrationFormView
									.setVisibility(show ? View.GONE
											: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant
			// UI components.
			mRegistrationStatusView.setVisibility(show ? View.VISIBLE
					: View.GONE);
			mRegistrationFormView
					.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

}
