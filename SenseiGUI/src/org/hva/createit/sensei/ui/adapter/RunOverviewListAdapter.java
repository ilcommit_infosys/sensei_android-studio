package org.hva.createit.sensei.ui.adapter;

import java.util.List;

import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.sensei.ui.R;
import org.hva.createit.sensei.ui.view.AffectUnit;
import org.hva.createit.sensei.ui.view.DistanceUnit;
import org.hva.createit.sensei.ui.view.HeartrateUnit;
import org.hva.createit.sensei.ui.view.SpeedUnit;
import org.hva.createit.sensei.ui.view.StepfrequencyUnit;
import org.hva.createit.sensei.ui.view.TimerUnit;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RunOverviewListAdapter extends BaseAdapter {
	private List<RunOverviewData> runs;
	private Context context;
	private LayoutInflater inflater;
	private final String TAG = "RunOverviewListAdapter";

	public RunOverviewListAdapter(Context context, List<RunOverviewData> runs) {
		this.runs = runs;
		this.context = context;
		this.inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return runs.size();
	}

	@Override
	public RunOverviewData getItem(int position) {
		return runs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return runs.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.sensei_previous_run,
					parent, false);
			holder = new ViewHolder();
			holder.runTitle = (TextView) convertView
					.findViewById(R.id.runTitle);
			holder.distanceUnit = (DistanceUnit) convertView
					.findViewById(R.id.distance_unit);
			holder.stepfrequencyUnit = (StepfrequencyUnit) convertView
					.findViewById(R.id.stepfrequency_unit);
			holder.timerUnit = (TimerUnit) convertView
					.findViewById(R.id.timer_unit);

			holder.speedUnit = (SpeedUnit) convertView
					.findViewById(R.id.speed_unit);
			holder.affectUnit = (AffectUnit) convertView
					.findViewById(R.id.affect_unit);
			holder.affectUnit.setClickable(false);
			holder.heartrateUnit = (HeartrateUnit) convertView
					.findViewById(R.id.heartrate_unit);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		try {
			RunOverviewData run = runs.get(position);
			holder.runTitle.setText(run.getStartDateAsString());
			holder.distanceUnit.setDistance(run.getDistance());
			holder.stepfrequencyUnit.setStepfrequency(run
					.getAverageStepFrequency());
			holder.timerUnit.updateTime(run.getDuration());
			holder.speedUnit.setSpeed(run.getAverageSpeed());
			holder.affectUnit.setAffect(run.getAffect());
			holder.heartrateUnit.setHeartRate(run.getAverageHeartRate());
			convertView.setTag(holder);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}

		return convertView;
	}

	private class ViewHolder {
		TextView runTitle;
		DistanceUnit distanceUnit;
		StepfrequencyUnit stepfrequencyUnit;
		SpeedUnit speedUnit;
		TimerUnit timerUnit;
		AffectUnit affectUnit;
		HeartrateUnit heartrateUnit;
	}

}
