package org.hva.createit.sensei.ui;

import java.util.ArrayList;
import java.util.List;

import org.hva.createit.scl.ble.BluetoothLeService;
import org.hva.createit.scl.ble.GattAttributes;
import org.hva.createit.scl.sensor.SensorService;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import de.greenrobot.event.EventBus;

//test
public class SCLControllerBaseActivity extends BaseActivity {
	// BluetoothHeartRateActivity{
	public static final String TAG = "SensorCollectionLibraryController";

	SensorServiceReceiver sensorReceiver = null;
	Intent sensorIntent;
	Handler startupHandler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");

		sensorIntent = new Intent(this, SensorService.class);
		startService(sensorIntent);

		// start HR components
		startHeartRateComponents();

	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "onResume/registering receiver");

		sensorReceiver = new SensorServiceReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(SensorService.NEW_MEASUREMENT);
		registerReceiver(sensorReceiver, intentFilter);

		registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
		if (mBluetoothLeService != null) {
			if (!mBluetoothLeService.isConnected()) {
				final boolean result = mBluetoothLeService
						.connect(mDeviceAddress);
				Log.d(TAG, "Connect request result=" + result);
			}
		}

		EventBus.getDefault().register(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d(TAG, "onPause/unregistering receiver");
		if (sensorReceiver != null) {
			unregisterReceiver(sensorReceiver);
		}
		sensorReceiver = null;

		unregisterReceiver(mGattUpdateReceiver);

		EventBus.getDefault().unregister(this);

	}

	@Override
	protected void onStart() {
		super.onStart();
		// Bind to the service
		bindService(new Intent(this, SensorService.class), mConnection,
				Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop");

		if (sensorReceiver != null) {
			unregisterReceiver(sensorReceiver);
		}
		// Unbind from the service
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
		stopService(sensorIntent);

		unbindService(mServiceConnection);
		mBluetoothLeService = null;
	}

	private class SensorServiceReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context arg0, Intent arg1) {

		}
	}

	/** Messenger for communicating with the service. */
	Messenger mService = null;

	/** Flag indicating whether we have called bind on the service. */
	boolean mBound;

	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the object we can use to
			// interact with the service. We are communicating with the
			// service using a Messenger, so here we get a client-side
			// representation of that from the raw IBinder object.
			mService = new Messenger(service);
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			mService = null;
			mBound = false;
		}
	};

	public void startSensors() {
		startService(sensorIntent);

		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null, SensorService.MSG_START_SENSORS, 0,
				0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}

	}

	public void stopSensors() {
		if (!mBound) {
			bindService(new Intent(this, SensorService.class), mConnection,
					Context.BIND_AUTO_CREATE);
		}
		// if (!mBound) {
		// return;
		// }
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message
				.obtain(null, SensorService.MSG_STOP_SENSORS, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		stopService(sensorIntent);
	}

	public void startLocationSensor() {
		startService(sensorIntent);

		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null,
				SensorService.MSG_START_SENSOR_LOCATION, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	public void stopLocationSensor() {
		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null,
				SensorService.MSG_STOP_SENSOR_LOCATION, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		stopService(sensorIntent);
	}

	public void startStepFrequencySensor() {
		startService(sensorIntent);

		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null,
				SensorService.MSG_START_SENSOR_STEPFREQUENCY, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	public void stopStepFrequencySensor() {
		if (!mBound)
			return;
		// Create and send a message to the service, using a supported 'what'
		// value
		Message msg = Message.obtain(null,
				SensorService.MSG_STOP_SENSOR_STEPFREQUENCY, 0, 0);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		// stopService(sensorIntent);
	}

	@Override
	protected int getLayoutResource() {
		return 0;
	}

	public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
	public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

	// private TextView mConnectionState;
	// private TextView mDataField;
	private String mDeviceName;
	public String mDeviceAddress;
	private String mDefaultDeviceName = "Mio Link";
	public String mDefaultDeviceAddress = "C3:4D:F2:BD:3B:63"; // mio

	private SharedPreferences sharedPref;
	private BluetoothLeService mBluetoothLeService;
	private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
	private boolean mConnected = false;
	private BluetoothGattCharacteristic mNotifyCharacteristic;

	private final String LIST_NAME = "NAME";
	private final String LIST_UUID = "UUID";

	// Code to manage Service lifecycle.
	private final ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,
				IBinder service) {
			mBluetoothLeService = ((BluetoothLeService.LocalBinder) service)
					.getService();
			if (!mBluetoothLeService.initialize()) {
				Log.e(TAG, "Unable to initialize Bluetooth");
				finish();
			}
			// Automatically connects to the device upon successful start-up
			// initialization.
			mBluetoothLeService.connect(mDeviceAddress);
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBluetoothLeService = null;
		}
	};

	// Handles various events fired by the Service.
	// ACTION_GATT_CONNECTED: connected to a GATT server.
	// ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
	// ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
	// ACTION_DATA_AVAILABLE: received data from the device. This can be a
	// result of read
	// or notification operations.
	private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
				mConnected = true;
				updateConnectionState(R.string.connected);
				invalidateOptionsMenu();
			} else if (BluetoothLeService.ACTION_GATT_DISCONNECTED
					.equals(action)) {
				mConnected = false;
				updateConnectionState(R.string.disconnected);
				invalidateOptionsMenu();
				clearUI();
			} else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED
					.equals(action)) {
				// Show all the supported services and characteristics on the
				// user interface.
				displayGattServices(mBluetoothLeService
						.getSupportedGattServices());
			} else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
				// displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
			}
		}
	};

	// If a given GATT characteristic is selected, check for supported features.
	// This sample
	// demonstrates 'Read' and 'Notify' features. See
	// http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for
	// the complete
	// list of supported characteristic features.
	private final ExpandableListView.OnChildClickListener servicesListClickListner = new ExpandableListView.OnChildClickListener() {
		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			if (mGattCharacteristics != null) {
				final BluetoothGattCharacteristic characteristic = mGattCharacteristics
						.get(groupPosition).get(childPosition);
				return listenToCharacteristic(characteristic);
			}
			return false;
		}
	};

	private void clearUI() {
		// mDataField.setText(R.string.no_data);
	}

	private void startHeartRateComponents() {
		// if(mDeviceAddress == null || mDeviceName == null) {
		// sharedPref = this.getPreferences(Context.MODE_PRIVATE);
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		mDeviceAddress = sharedPref.getString(
				getString(R.string.pref_ble_device_address),
				mDefaultDeviceAddress);
		mDeviceName = sharedPref.getString(
				getString(R.string.pref_ble_device_name), mDefaultDeviceName);

		// }

		if (mDeviceAddress == null || mDeviceName == null) {
			// no heartrate sensor added dont display hr data
			// TO-DO
		}
		Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
		bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
	}

	// Demonstrates how to iterate through the supported GATT
	// Services/Characteristics.
	// In this sample, we populate the data structure that is bound to the
	// ExpandableListView
	// on the UI.
	private void displayGattServices(List<BluetoothGattService> gattServices) {
		if (gattServices == null)
			return;
		String uuid = null;
		// String unknownServiceString =
		// getResources().getString(R.string.unknown_service);
		// String unknownCharaString =
		// getResources().getString(R.string.unknown_characteristic);
		// ArrayList<HashMap<String, String>> gattServiceData = new
		// ArrayList<HashMap<String, String>>();
		// ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
		// = new ArrayList<ArrayList<HashMap<String, String>>>();
		mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

		BluetoothGattCharacteristic foundCharacteristic = null;

		// Loops through available GATT Services.
		for (BluetoothGattService gattService : gattServices) {
			// HashMap<String, String> currentServiceData = new HashMap<String,
			// String>();
			// uuid = gattService.getUuid().toString();
			// currentServiceData.put(
			// LIST_NAME, SampleGattAttributes.lookup(uuid,
			// unknownServiceString));
			// currentServiceData.put(LIST_UUID, uuid);
			// gattServiceData.add(currentServiceData);

			// ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
			// new ArrayList<HashMap<String, String>>();
			List<BluetoothGattCharacteristic> gattCharacteristics = gattService
					.getCharacteristics();
			// ArrayList<BluetoothGattCharacteristic> charas =
			// new ArrayList<BluetoothGattCharacteristic>();
			// Loops through available Characteristics.
			for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
				// charas.add(gattCharacteristic);
				// HashMap<String, String> currentCharaData = new
				// HashMap<String, String>();
				uuid = gattCharacteristic.getUuid().toString();
				// currentCharaData.put(
				// LIST_NAME, SampleGattAttributes.lookup(uuid,
				// unknownCharaString));
				// currentCharaData.put(LIST_UUID, uuid);
				// gattCharacteristicGroupData.add(currentCharaData);

				if (uuid.equals(GattAttributes.HEART_RATE_MEASUREMENT)) {
					foundCharacteristic = gattCharacteristic;
					// stop loop, no need to proceed
					// return;
				}
			}
			// mGattCharacteristics.add(charas);
			// gattCharacteristicData.add(gattCharacteristicGroupData);

		}
		if (foundCharacteristic != null) {
			listenToCharacteristic(foundCharacteristic);
		}

	}

	private static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
		intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
		intentFilter
				.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
		intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
		return intentFilter;
	}

	private boolean listenToCharacteristic(
			BluetoothGattCharacteristic characteristic) {
		final int charaProp = characteristic.getProperties();
		if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
			// If there is an active notification on a characteristic, clear
			// it first so it doesn't update the data field on the user
			// interface.
			if (mNotifyCharacteristic != null) {
				mBluetoothLeService.setCharacteristicNotification(
						mNotifyCharacteristic, false);
				mNotifyCharacteristic = null;
			}
			mBluetoothLeService.readCharacteristic(characteristic);
		}
		if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
			mNotifyCharacteristic = characteristic;
			mBluetoothLeService.setCharacteristicNotification(characteristic,
					true);
		}
		return true;
	}

	private void updateConnectionState(final int resourceId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// mConnectionState.setText(resourceId);
			}
		});
	}

}
