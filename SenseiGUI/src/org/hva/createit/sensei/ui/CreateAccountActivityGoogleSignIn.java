package org.hva.createit.sensei.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.askcs.android.affectbutton.Activity;
import com.infy.ilcommit.intelirun.Infosys_SenseService;

import nl.sense_os.service.ISenseServiceCallback;

/**
 * Created by Nilanajan_Nath on 12/24/2015.
 */
public class CreateAccountActivityGoogleSignIn extends Activity
{
    private static final String TAG = "GoogleSignIn" ;
    private TextView mRegistrationStatusMessageView;
    private boolean mBusy;
    String mEmail;
    String mPassword ;
    private View mRegistrationStatusView;


    private ISenseServiceCallback mSenseCallback = new ISenseServiceCallback.Stub() {

        @Override
        public void onChangeLoginResult(int result) throws RemoteException {
            // not used
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRegisterResult(int result) throws RemoteException {
            mBusy = false;

            if (result == -2) {
                Log.w(TAG,"attemp reg level..4");
                onRegistrationFailure(true);
            }
            else if (result == -1) {
                Log.w(TAG,"attemp reg level..5");
                onRegistrationFailure(false);
            }
            else {
                Log.w(TAG,"attemp reg level..6");
                onRegistrationSuccess();
            }
        }

        @Override
        public void statusReport(int status) throws RemoteException {
            // not used
        }
    };

    @Override
    protected void onCreate(Bundle instanceState)
    {
        super.onCreate(instanceState);
        setContentView(R.layout.sensei_user_registration_google_activity);
        mRegistrationStatusView = findViewById(R.id.registration_status);
        mRegistrationStatusMessageView = (TextView) findViewById(R.id.registration_status_message);
        Log.w(TAG,"Create activity");
        attemptRegistration();
    }

    public void attemptRegistration() {

        mRegistrationStatusMessageView.setText(R.string.progress_registering);
        showProgress(true);
        // register using SensePlatform
        try {
            Log.w(TAG,"attemp reg level..1");
            SharedPreferences sharedPreferences = getSharedPreferences("org.hva.createit.sensegui.login.googleSignin", MODE_PRIVATE);
            mEmail = sharedPreferences.getString("email", null) ;
            String userName = sharedPreferences.getString("userName",null);
            mPassword = "12345" ;
            Log.w(TAG, "attemp reg level..2 " + mEmail + " " + userName);
            Infosys_SenseService Infosys_SenseService=new Infosys_SenseService(this);
            mSenseCallback.onRegisterResult(
                    Infosys_SenseService.attemptRegistrationAtCloud(userName, mPassword, mEmail,
                            null, null, null, null, null, null,
                            mSenseCallback));
            Log.w(TAG, "attemp reg level..3");
            // join group sensei_group
            //SenseApi.joinGroup(mApplication.getSensePlatform().getContext(), "23110");
            // this is an asynchronous call, we get a callback when the
            // registration is complete
            mBusy = true;
        }
        catch (IllegalStateException e) {
                onRegistrationFailure(false);
        }
        catch (RemoteException e) {
                onRegistrationFailure(false);
        }

		/*
		 * catch (JSONException e) { Log.w(TAG,
		 * "Failed to join group sensei_group!", e);
		 * onRegistrationFailure(false); } catch (IOException e) { Log.w(TAG,
		 * "Failed to join group sensei_group!", e);
		 * onRegistrationFailure(false); }
		 */
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {

        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy
        // animations. If available, use these APIs to fade-in the progress
        // spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);

            mRegistrationStatusView.setVisibility(View.VISIBLE);
            mRegistrationStatusView.animate().setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mRegistrationStatusView
                                    .setVisibility(show ? View.VISIBLE
                                            : View.GONE);
                        }
                    });
        }
        else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant
            // UI components.
            mRegistrationStatusView.setVisibility(show ? View.VISIBLE
                    : View.GONE);
        }
    }

    private void onRegistrationFailure(final boolean forbidden) {

        // update UI
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                showProgress(false);
                Toast.makeText(CreateAccountActivityGoogleSignIn.this,
                        R.string.register_failure, Toast.LENGTH_LONG).show();
            }
        });

    }

    protected void onRegistrationSuccess() {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String mEmail = this.mEmail;
        String mPassword = this.mPassword;
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.pref_user_email), mEmail);
        editor.putString(getString(R.string.pref_user_password), mPassword);
        editor.commit();
        // update UI
        Log.w(TAG,"attemp reg level..7");
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                showProgress(false);
                Log.w(TAG, "attemp reg level..8");
                Toast.makeText(CreateAccountActivityGoogleSignIn.this,
                        R.string.register_success, Toast.LENGTH_LONG).show();
            }
        });
        // finish registration activity
        setResult(RESULT_OK);
        finish();
    }



}
