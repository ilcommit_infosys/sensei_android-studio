package org.hva.createit.sensei.ui;

import java.util.List;

import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.scl.dataaccess.RunStatisticsDAO;
import org.hva.createit.sensei.ui.adapter.RunOverviewListAdapter;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class RunOverviewListFragment extends Fragment {
	private final String TAG = "RunOverviewFragment";
	private List<RunOverviewData> runs;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Bundle args = this.getArguments();
		int items = 5;
		if (args != null) {
			items = args.getInt("items", items);
		}

		RunStatisticsDAO rsd = new RunStatisticsDAO(
				RunOverviewListFragment.this.getActivity());
		runs = rsd.getLastRunOverviews(items);

		ListView runList = (ListView) RunOverviewListFragment.this
				.getActivity().findViewById(R.id.previousRunsList);
		RunOverviewListAdapter runOverviewListAdapter = new RunOverviewListAdapter(
				RunOverviewListFragment.this.getActivity(), runs);
		runList.setAdapter(runOverviewListAdapter);
		runList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.d(TAG,
						"item pressed: " + position + " id: "
								+ runs.get(position).getId());

				Intent intent = new Intent(RunOverviewListFragment.this
						.getActivity(), RunOverviewActivity.class);
				Bundle b = new Bundle();
				b.putInt("runId", runs.get(position).getId()); // 1 == end
				intent.putExtras(b); // Put your id to your next Intent

				startActivity(intent);
				RunOverviewListFragment.this.getActivity()
						.overridePendingTransition(R.anim.slide_in_right,
								R.anim.slide_out_left);
			}

		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater
				.inflate(R.layout.sensei_previous_runs, container, false);
	}
}