package org.hva.createit.sensei.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class UserSettingsActivity extends BaseActivity {
	private static Spinner dateSpinner;
	private SharedPreferences sharedPref;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dateSpinner = (Spinner) this.findViewById(R.id.date_of_birth);

		View.OnTouchListener Spinner_OnTouch = new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					showDatePickerDialog(v);
				}
				return true;
			}
		};
		View.OnKeyListener Spinner_OnKey = new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
					showDatePickerDialog(v);
					return true;
				} else {
					return false;
				}
			}
		};

		dateSpinner.setOnTouchListener(Spinner_OnTouch);
		dateSpinner.setOnKeyListener(Spinner_OnKey);
		setSpinnerDate("00 - 00 - 0000");

		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		String userName = sharedPref.getString(
				getString(R.string.pref_user_name), null);
		((EditText) findViewById(R.id.name)).setText(userName);
		String userEmail = sharedPref.getString(
				getString(R.string.pref_user_email), null);
		((EditText) findViewById(R.id.email)).setText(userEmail);
		String userPassword = sharedPref.getString(
				getString(R.string.pref_user_password), null);
		((EditText) findViewById(R.id.password)).setText(userPassword);
		String userHeight = sharedPref.getString(
				getString(R.string.pref_user_height), null);
		((EditText) findViewById(R.id.height)).setText(userHeight);
		String userWeight = sharedPref.getString(
				getString(R.string.pref_user_weight), null);
		((EditText) findViewById(R.id.weight)).setText(userWeight);
		String userDateOfBirth = sharedPref.getString(
				getString(R.string.pref_user_birth_date), null);
		if (userDateOfBirth != null) {
			setSpinnerDate(userDateOfBirth);
		}

		String userGender = sharedPref.getString(
				getString(R.string.pref_user_gender), null);
		if (userGender!=null&&userGender.equals(getString(R.string.gender_female_label))) {
			((RadioButton) findViewById(R.id.gender_female)).setChecked(true);
		} else if (userGender!=null&&userGender.equals(getString(R.string.gender_male_label))) {
			((RadioButton) findViewById(R.id.gender_male)).setChecked(true);
		}

		((EditText) findViewById(R.id.name))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(getString(R.string.pref_user_name),
								s.toString());
						editor.commit();
					}
				});

		((EditText) findViewById(R.id.email))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(getString(R.string.pref_user_email),
								s.toString());
						editor.commit();
					}
				});

		((EditText) findViewById(R.id.password))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(
								getString(R.string.pref_user_password),
								s.toString());
						editor.commit();
					}
				});

		((EditText) findViewById(R.id.height))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(getString(R.string.pref_user_height),
								s.toString());
						editor.commit();
					}
				});
		((EditText) findViewById(R.id.weight))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(getString(R.string.pref_user_weight),
								s.toString());
						editor.commit();
					}
				});
		Spinner dob = ((Spinner) findViewById(R.id.date_of_birth));
		dob.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				String item = (String) parent.getItemAtPosition(position);
				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putString(getString(R.string.pref_user_birth_date), item);
				editor.commit();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		RadioGroup rg = (RadioGroup) findViewById(R.id.gender_group);

		rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				SharedPreferences.Editor editor = sharedPref.edit();

				switch (checkedId) {
				case R.id.gender_female:
					editor.putString(getString(R.string.pref_user_gender),
							getString(R.string.gender_female_label));
					break;
				case R.id.gender_male:
					editor.putString(getString(R.string.pref_user_gender),
							getString(R.string.gender_male_label));
					break;
				}
				editor.commit();
			}
		});
	}

	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "Date");
	}

	private void setSpinnerDate(String date) {
		List<String> list = new ArrayList<String>();
		list.add(date);

		ArrayAdapter<String> adp = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, list);
		dateSpinner.setAdapter(adp);
	}

	private class DatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			// Do something with the date chosen by the user
			setSpinnerDate(day + " - " + month + " - " + year);

		}
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.sensei_user_details_activity;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
