package org.hva.createit.sensei.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

public class SettingsFragment extends Fragment {
	private Spinner dateSpinner;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		final ListView listview = (ListView) this.getActivity().findViewById(
				R.id.listview);
		String[] values = new String[] { "User settings", "Heart rate",
				"Backup DB", "About Sensei" };

		final ArrayList<String> list = new ArrayList<String>();
		for (String value : values) {
			list.add(value);
		}
		final StableArrayAdapter adapter = new StableArrayAdapter(
				this.getActivity(), android.R.layout.simple_list_item_1, list);
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				switch (position) {
				case 0:
					// // Insert the fragment by replacing any existing fragment
					// FragmentManager fragmentManager = getFragmentManager();
					//
					// FrameLayout frame = (FrameLayout) SettingsFragment.this
					// .getActivity().findViewById(R.id.content_frame);
					// frame.removeAllViewsInLayout();
					//
					// fragmentManager
					// .beginTransaction()
					// .replace(R.id.content_frame,
					// new UserSettingsFragment()).commit();
					startActivity(new Intent(SettingsFragment.this
							.getActivity(), UserSettingsActivity.class));
					SettingsFragment.this.getActivity()
							.overridePendingTransition(R.anim.slide_in_right,
									R.anim.slide_out_left);
					break;
				case 1:

					startActivity(new Intent(SettingsFragment.this
							.getActivity(), HeartrateSettingsActivity.class));
					SettingsFragment.this.getActivity()
							.overridePendingTransition(R.anim.slide_in_right,
									R.anim.slide_out_left);
					break;
				case 2:
					// backup db
					Util.backupDB(SettingsFragment.this.getActivity());
					break;
				case 3:
					startActivity(new Intent(SettingsFragment.this
							.getActivity(), AboutActivity.class));
					SettingsFragment.this.getActivity()
							.overridePendingTransition(R.anim.slide_in_right,
									R.anim.slide_out_left);
					;
				}
			}

		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.sensei_settings, container, false);
	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}

}
