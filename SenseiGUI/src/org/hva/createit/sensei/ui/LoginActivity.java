package org.hva.createit.sensei.ui;

/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this 
 * Infosys proprietary software program ("Program"), this Program is protected 
 * by copyright laws, international treaties and other pending or existing 
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation 
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and 
 * will be prosecuted to the maximum extent possible under the law
 **/

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.infy.ilcommit.intelirun.Infosys_SenseService;

import org.json.JSONException;

import java.io.IOException;

import nl.sense_os.service.ISenseServiceCallback;
import nl.sense_os.service.SenseServiceStub;


/**
 * @author Shruti_Bansal01@Infosys.com, Kushagra_Kapoor01@Infosys.com, Nilanajan_Nath@Infosys.com
 * @description Activity to login given user credentials
 *
 */
public class LoginActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, ResultCallback<People.LoadPeopleResult> {


    public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";
    private static final int RC_SIGN_IN = 0;
    private static final int REQ_SIGN_IN_REQUIRED = 55664;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean signedInUser;
    private ConnectionResult mConnectionResult;
    private SignInButton signinButton;
    private TextView username, emailLabel;
    private LinearLayout profileFrame;
    private ScrollView logInForm;
    private int googleFlag = 0;
	private int CREATE_ACCOUNT_CODE = 42;
	private Infosys_SenseService Infosys_senseService;
	private String TAG = "LoginActivity";
	private SenseServiceStub mSenseService;

	// Values for email and password at the time of the login attempt
	private String mEmail;
	private String mPassword;
    private String accessToken;
    private String openPassword = "FBLogin";

	// UI references
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

    //for facebook login
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private boolean busy;

    //call back for login attempt with Sense credentials
	private ISenseServiceCallback mServiceCallback = new ISenseServiceCallback.Stub() {

		@Override
		public void onChangeLoginResult(int result) throws RemoteException {

			busy = false;

			if (result == -2) {
				// login forbidden
				onLoginFailure(true);

			} else if (result == -1) {
				// login failed
				onLoginFailure(false);

			} else {
				onLoginSuccess();

			}
		}

		@Override
		public void onRegisterResult(int result) throws RemoteException {


			/*if (result == -2) {
                // user exists already
				//onRegistrationFailure(true);

			} else */
            if (result == -1) {
                Log.e(TAG, "Registration failed for fb user:" + mEmail);


            } else {
                try {

                    Infosys_senseService.attemptLogin(mEmail, openPassword,
                            mServiceCallback);
				/*
				 * mServiceCallback .onChangeLoginResult(Infosys_senseService
				 * .attemptLogin(mEmail, mPassword));
				 */
                    // this is an asynchronous call, we get a callback when
                    // the
                    // login is complete
                    busy = true;

                } catch (IllegalStateException e) {
                    Log.w(TAG, "Failed to log in at SensePlatform!", e);
                    onLoginFailure(false);
                } catch (RemoteException e) {
                    Log.w(TAG, "Failed to log in at SensePlatform!", e);
                    onLoginFailure(false);
		}
            }

        }


		@Override
		public void statusReport(int status) throws RemoteException {
			// not used
		}
	};

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

	/**
	 * Tries to log in at CommonSense using the supplied username and password.
	 * After login, the service remembers the username and password.
	 * 
     * @param user
	 *            Username for login
     * @param password
	 *            Hashed password for login
	 * @param callback
	 *            Interface to receive callback when login is completed
	 * @throws IllegalStateException
	 *             If the Sense service is not bound yet
	 * @throws RemoteException
	 */
	public void login(String user, String password,
			ISenseServiceCallback callback) throws IllegalStateException,
			RemoteException {
		// checkSenseService();
		mSenseService.changeLogin(user, password, callback);
	}

    //method to initiate view for LoginActivity
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

        //initialise facebook sdk
        FacebookSdk.sdkInitialize(getApplicationContext());

		setContentView(R.layout.sensei_login_activity);

        //fb callback
        callbackManager = CallbackManager.Factory.create();

        //facebook login button
        loginButton = (LoginButton) findViewById(R.id.fb_login_button);

        //callback on fb login button press
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(TAG, "Successfully logged with facebook");
                busy = true;
                setmEmail(loginResult.getAccessToken().getUserId());
                setAccessToken(loginResult.getAccessToken().getToken());
                attemptRegistrationOnOpenLogin("fb");


            }

            @Override
            public void onCancel() {

                Log.w(TAG, "Login with facebook cancelled");
                onLoginFailure(false);
            }

            @Override
            public void onError(FacebookException error) {
                Log.w(TAG, "Error occured while logging in with facebook");
                onLoginFailure(false);
            }
        });

		SharedPreferences prefs = this.getSharedPreferences("LoginData",
				Context.MODE_PRIVATE);
		String storedUname = prefs.getString("Uname", null);
		if (storedUname != null) {
			Intent newIntent = new Intent(LoginActivity.this,
					MainActivity.class);
			newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			// newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(newIntent);

		}

        //initiate Google client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(new Scope(Scopes.PLUS_LOGIN))
                .build();

        //Google signin button
        signinButton = (SignInButton) findViewById(R.id.signin);
        setGooglePlusButtonText(signinButton, "Login in with Google");
        signinButton.setOnClickListener(this);
        username = (TextView) findViewById(R.id.username);
        emailLabel = (TextView) findViewById(R.id.email1);
        profileFrame = (LinearLayout) findViewById(R.id.profileFrame);
        logInForm = (ScrollView) findViewById(R.id.login_form);


		Infosys_senseService = new Infosys_SenseService(this);
		mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mEmailView = (EditText) findViewById(R.id.email);
		if (mEmail != null) {
			mEmailView.setText(mEmail);
		}
		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {

							attemptLogin();
							return true;
						}

						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.create_account_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						// startactivity for results
						startActivityForResult(new Intent(LoginActivity.this,
								CreateAccountActivity.class),
								CREATE_ACCOUNT_CODE);
						// on result account created login
						// on result no account created do nothing.
					}
				});
		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});


    }

    //first time automated registration at cloud for user signing up through fb or google
    private void attemptRegistrationOnOpenLogin(String login) {
        try {
            busy = true;
            //Individualising email ids used with fb or google
            setmEmail(login + mEmail);
            setmPassword(openPassword);

            Infosys_SenseService Infosys_SenseService = new Infosys_SenseService(this);
            Log.d(TAG, "attempting Registration inside run()");
            mServiceCallback.onChangeLoginResult(Infosys_SenseService
                    .attemptRegistrationAtCloud(mEmail, mPassword, mEmail,
                            null, null, null, mEmail, null, null,
                            mServiceCallback));

        } catch (IllegalStateException e) {
            Log.w(TAG, "Failed to register at SensePlatform!", e);

        } catch (RemoteException e) {
            Log.w(TAG, "Failed to register at SensePlatform!", e);

        }

	}

	private void addToGroup() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Infosys_senseService.afterLoginSuccess();
				} catch (JSONException e) {
					Log.w(TAG, "Failed to join group sensei_group!", e);
					onLoginFailure(false);
				} catch (IOException e) {
					Log.w(TAG, "Failed to join group sensei_group!", e);
					onLoginFailure(false);
				}
			}
		}).start();
	}

    private void getUserDetails() {

    }

	protected void onLoginSuccess() {
		addToGroup();
		// update UI
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				showProgress(false);
				Toast.makeText(LoginActivity.this, R.string.login_success,
						Toast.LENGTH_LONG).show();
			}
		});
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        if (mEmail != null)
            editor.putString(getString(R.string.pref_user_name), mEmail);
        System.out.println(mEmail);
        if (mPassword != null)
            editor.putString(getString(R.string.pref_user_password), mPassword);
        System.out.println(mPassword);
        if (accessToken != null)
            editor.putString(getString(R.string.pref_fbAccessToken), accessToken);
        System.out.println(accessToken);

        editor.commit();
		SharedPreferences prefs = this.getSharedPreferences("LoginData",
				Context.MODE_PRIVATE);
		prefs.edit().putString("Uname", mEmail).apply();
		prefs.edit().putString("Password", mPassword).apply();

		Intent newIntent = new Intent(LoginActivity.this, MainActivity.class);
		newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		// newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		startActivity(newIntent);
		setResult(RESULT_OK);
		finish();
	}

	private void onLoginFailure(final boolean forbidden) {

		// update UI
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				showProgress(false);

				if (forbidden) {
					mPasswordView
							.setError(getString(R.string.error_incorrect_password));
					mPasswordView.requestFocus();
				} else {
					Toast.makeText(LoginActivity.this, R.string.login_failure,
							Toast.LENGTH_LONG).show();
				}
			}
		});

	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	protected void attemptLogin() {
		if (busy) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);

			// log in (you only need to do this once, Sense will remember the
			// login)

			try {

				Infosys_senseService.attemptLogin(mEmail, mPassword,
						mServiceCallback);
				/*
				 * mServiceCallback .onChangeLoginResult(Infosys_senseService
				 * .attemptLogin(mEmail, mPassword));
				 */
				// this is an asynchronous call, we get a callback when
				// the
				// login is complete
				busy = true;

			} catch (IllegalStateException e) {
				Log.w(TAG, "Failed to log in at SensePlatform!", e);
				onLoginFailure(false);
			} catch (RemoteException e) {
				Log.w(TAG, "Failed to log in at SensePlatform!", e);
				onLoginFailure(false);
			}
		}

	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		Log.d(TAG, "Inside Show Progress");
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			Log.d(TAG, "Inside if");
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							Log.d(TAG, "Inside onAnimationEnd");
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
			Log.d(TAG, "Before mLoginFormView");
			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			Log.d(TAG, "Inside else of show progress");
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((googleFlag == 1) && (requestCode == RC_SIGN_IN)) {
            Log.i(TAG, "Signing in using google");
            if (resultCode == RESULT_OK) {
                signedInUser = false;
            }
            mIntentInProgress = false;
            if (!mGoogleApiClient.isConnecting()) {

                mGoogleApiClient.connect();
            }
        } else {
            Log.i(TAG, "Signing in using facebook");
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    /**methods implemented for google start here**/
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }


    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }


    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {

            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }

        if (!mIntentInProgress) {
            // store mConnectionResult
            mConnectionResult = result;

            if (signedInUser) {

                resolveSignInError();
            }
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(this);
        getProfileInformation();
        Toast.makeText(this, "Connected to google as " + mEmail, Toast.LENGTH_LONG).show();
        Log.i(TAG, "Connected to google as " + mEmail);
        attemptRegistrationOnOpenLogin("google");

    }


    private void getProfileInformation() {
        try {

            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

                Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

                SharedPreferences sharedPreferences = getSharedPreferences("org.hva.createit.sensegui.login.googleSignin", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("userName", personName);
                editor.putString("email", email);
                editor.commit();
                username.setText(personName);
                emailLabel.setText(email);
                setmEmail(email);

                new RetrieveTokenTask().execute(email);


                //new LoadProfileImage(image).execute(personPhotoUrl);

                // update profile frame with new info about Google Account
                // profile
                updateProfile(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateProfile(boolean isSignedIn) {
        if (isSignedIn) {
            logInForm.setVisibility(View.GONE);
            profileFrame.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Connected", Toast.LENGTH_LONG).show();
            //gotoRegistration() ;

        } else {
            logInForm.setVisibility(View.VISIBLE);
            profileFrame.setVisibility(View.GONE);
        }
    }

    private void gotoRegistration() {
        Intent intent = new Intent(getApplicationContext(), CreateAccountActivityGoogleSignIn.class);
        startActivity(intent);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
        updateProfile(false);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.signin:
                showProgress(true);
                googleFlag = 1;
                googlePlusLogin();
                break;
        }
    }


    public void signIn(View v) {
        googlePlusLogin();
    }

    public void logout(View v) {
        googlePlusLogout();
    }

    private void googlePlusLogin() {

        if (!mGoogleApiClient.isConnecting()) {
            //GoogleSignInOptions signInOptions =
            signedInUser = true;
            resolveSignInError();
        }
    }

    private void googlePlusLogout() {
        if (mGoogleApiClient.isConnected()) {

            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
            updateProfile(false);
        }
    }


    @Override
    public void onResult(People.LoadPeopleResult peopleData) {
//		if (peopleData.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {
//			PersonBuffer personBuffer = peopleData.getPersonBuffer();
//			try {
//				int count = personBuffer.getCount();
//				for (int i = 0; i < count; i++) {
//					Log.d(TAG, "Display name: " + personBuffer.get(i).getDisplayName());
//				}
//			} finally {
//				personBuffer.release();
//			}
//		} else {
//			Log.e(TAG, "Error requesting people data: " + peopleData.getStatus());
//		}
    }

    protected void setGooglePlusButtonText(SignInButton signInButton, String buttonText) {
        // Find the TextView that is inside of the SignInButton and set its text
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setText(buttonText);
                return;
            }
        }
    }

    private class RetrieveTokenTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String accountName = params[0];
            String scopes = "oauth2:profile email";
            String token = null;
            try {
                token = GoogleAuthUtil.getToken(getApplicationContext(), accountName, scopes);
                Log.i(TAG, "Retrieved access token from google: " + token);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            } catch (UserRecoverableAuthException e) {
                startActivityForResult(e.getIntent(), REQ_SIGN_IN_REQUIRED);
            } catch (GoogleAuthException e) {
                Log.e(TAG, e.getMessage());
            }
            return token;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            setAccessToken(s);
        }
    }


    /**methods implemented for google end**/
}
