package org.hva.createit.sensei.ui;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public abstract class BaseActivity extends ActionBarActivity {

	private Toolbar toolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResource());
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);

			// Set an OnMenuItemClickListener to handle menu item clicks
			// toolbar.setOnMenuItemClickListener(new
			// Toolbar.OnMenuItemClickListener() {
			// @Override
			// public boolean onMenuItemClick(MenuItem item) {
			// // Handle the menu item
			// return true;
			// }
			// });

			// Inflate a menu to be displayed in the toolbar
			// toolbar.inflateMenu(R.menu.menu);

		}

	}

	protected abstract int getLayoutResource();

	protected void setActionBarIcon(int iconRes) {
		toolbar.setNavigationIcon(iconRes);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
