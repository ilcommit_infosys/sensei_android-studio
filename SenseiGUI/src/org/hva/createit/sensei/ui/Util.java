package org.hva.createit.sensei.ui;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.hva.createit.scl.dataaccess.DatabaseHelper;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;

public class Util {
	public static boolean createFolderIfNeeded(String path) {
		File folder = new File(path);
		boolean success = true;
		if (!folder.exists()) {
			success = folder.mkdirs();
		}
		return success;
	}

	public static void backupDB(Context context) {

		String backupLocation = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/Sensei/Backup";
		Util.createFolderIfNeeded(backupLocation);
		backupLocation += "/backup" + System.currentTimeMillis() + ".zip";

		ArrayList<String> uploadData = new ArrayList<String>();
		uploadData.add(backupLocation);
		makeZip mz = new makeZip(backupLocation);
		mz.addZipFile(context.getDatabasePath(DatabaseHelper.DATABASE_NAME)
				.getAbsolutePath());
		mz.closeZip();
	}

	public static class makeZip {
		static final int BUFFER = 2048;

		ZipOutputStream out;
		byte data[];

		public makeZip(String name) {
			FileOutputStream dest = null;
			try {
				dest = new FileOutputStream(name);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out = new ZipOutputStream(new BufferedOutputStream(dest));
			data = new byte[BUFFER];
		}

		public void addZipFile(String name) {
			Log.v("addFile", "Adding: ");
			FileInputStream fi = null;
			try {
				fi = new FileInputStream(name);
				Log.v("addFile", "Adding: ");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.v("atch", "Adding: ");
			}
			BufferedInputStream origin = new BufferedInputStream(fi, BUFFER);
			ZipEntry entry = new ZipEntry(name);
			try {
				out.putNextEntry(entry);
				Log.v("put", "Adding: ");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int count;
			try {
				while ((count = origin.read(data, 0, BUFFER)) != -1) {
					out.write(data, 0, count);
					// Log.v("Write", "Adding: "+origin.read(data, 0,
					// BUFFER));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.v("catch", "Adding: ");
			}
			try {
				origin.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void closeZip() {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Open another app.
	 * 
	 * @param context
	 *            current Context, like Activity, App, or Service
	 * @param packageName
	 *            the full package name of the app to open
	 * @return true if likely successful, false if unsuccessful
	 */
	public static boolean openApp(Context context, String packageName) {
		PackageManager manager = context.getPackageManager();

		Intent i = manager.getLaunchIntentForPackage(packageName);
		if (i == null) {
			return false;
			// throw new PackageManager.NameNotFoundException();
		}
		i.addCategory(Intent.CATEGORY_LAUNCHER);
		context.startActivity(i);
		return true;
	}
}
