package org.hva.createit.sensei.ui;

import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;

/**
 * Created by Shruti_Bansal01 on 1/8/2016.
 */
public class googleClientBuilder
{
    public static GoogleApiClient mGoogleApiClient ;


    private googleClientBuilder()
    {

    }

    public static GoogleApiClient getInstance()
    {
        return mGoogleApiClient ;
    }
}