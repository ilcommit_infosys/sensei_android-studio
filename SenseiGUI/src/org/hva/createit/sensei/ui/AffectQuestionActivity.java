package org.hva.createit.sensei.ui;

import org.hva.createit.scl.data.AffectData;
import org.hva.createit.scl.data.RunData;
import org.hva.createit.scl.dataaccess.AffectDAO;
import org.hva.createit.scl.dataaccess.RunDataDAO;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Button;

import com.askcs.android.affectbutton.Affect;
import com.askcs.android.affectbutton.AffectButton;
import com.askcs.android.affectbutton.AffectListener;

public class AffectQuestionActivity extends BaseActivity {
	private DrawerLayout drawer;
	private AffectButton mAffectButton;
	private boolean affect_confirm = false;
	private Button mConfirmButton;
	private int runstate = 0;
	private AffectDAO ad;
	private RunDataDAO rdd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ad = new AffectDAO(this);
		rdd = new RunDataDAO(this);

		Bundle b = getIntent().getExtras();
		if (b != null) {
			runstate = b.getInt("runstate");
		}

		mConfirmButton = (Button) findViewById(R.id.confirm_button);
		mConfirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				final AffectData afdat = new AffectData(0, System
						.currentTimeMillis(), mAffectButton.getPleasure(),
						mAffectButton.getDominance(), mAffectButton
								.getArousal());
				new Handler().post(new Runnable() {
					@Override
					public void run() {
						ad.store(afdat);
					}
				});

				if (runstate == 0) {
					new Handler().post(new Runnable() {
						@Override
						public void run() {
							rdd.store(new RunData(0,
									RunData.RUNDATA_TYPE_RUNSTART, afdat
											.getTimeStamp()));
						}
					});

					startActivity(new Intent(AffectQuestionActivity.this,
							RunActivity.class));

				} else {
					new Handler().post(new Runnable() {
						@Override
						public void run() {
							rdd.store(new RunData(0,
									RunData.RUNDATA_TYPE_RUNEND, afdat
											.getTimeStamp()));
						}
					});
					startActivity(new Intent(AffectQuestionActivity.this,
							RunOverviewActivity.class));
				}
				finish();
			}
		});

		mAffectButton = (AffectButton) findViewById(R.id.affectbutton);
		mAffectButton.addListener(new AffectListener() {

			@Override
			public void onAffectChanged(Affect affect) {
				// set button to active
				affect_confirm = true;
				checkForConfirmButton();
			}
		});
	}

	private boolean checkForConfirmButton() {
		if (affect_confirm) {
			mConfirmButton.setEnabled(true);
			return true;
		} else if (!affect_confirm) {
			// please first rate using the affect button
		} else {
			// please first rate using the emotion buttons
		}
		return false;
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.sensei_affect_question_activity;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		NavUtils.navigateUpFromSameTask(this);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}
}
