package org.hva.createit.sensei.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;

public class UserSettingsFragment extends Fragment {
	private Spinner dateSpinner;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		dateSpinner = (Spinner) this.getActivity().findViewById(
				R.id.date_of_birth);

		View.OnTouchListener Spinner_OnTouch = new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					showDatePickerDialog(v);
				}
				return true;
			}
		};
		View.OnKeyListener Spinner_OnKey = new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
					showDatePickerDialog(v);
					return true;
				} else {
					return false;
				}
			}
		};

		dateSpinner.setOnTouchListener(Spinner_OnTouch);
		dateSpinner.setOnKeyListener(Spinner_OnKey);
		setSpinnerDate("00 - 00 - 0000");

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.sensei_user_details, container, false);
	}

	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "Date");
	}

	private void setSpinnerDate(String date) {
		List<String> list = new ArrayList<String>();
		list.add(date);

		ArrayAdapter<String> adp = new ArrayAdapter<String>(this.getActivity(),
				android.R.layout.simple_list_item_1, list);
		dateSpinner.setAdapter(adp);
	}

	public class DatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			// Do something with the date chosen by the user
			setSpinnerDate(day + " - " + month + " - " + year);

		}
	}
}
