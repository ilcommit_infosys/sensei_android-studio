/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;

import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.scl.dataaccess.LocationDAO;
import org.hva.createit.scl.dataaccess.RunStatisticsDAO;
import org.hva.createit.sensei.ui.AffectUnit;
import org.hva.createit.sensei.ui.BaseActivity;
import org.hva.createit.sensei.ui.DistanceUnit;
import org.hva.createit.sensei.ui.DurationUnit;
import org.hva.createit.sensei.ui.HeartrateUnit;
import org.hva.createit.sensei.ui.LocationUnit;
import org.hva.createit.sensei.ui.SpeedUnit;
import org.hva.createit.sensei.ui.StepfrequencyUnit;
import org.hva.createit.sensei.ui.Util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Shruti Bansal <shruti_bansal01@Infosys.com>, Nilanjan Nath <nilanajan_nath@Infosys.com>
 * @description Activity to display overview of previous or a selected workout activity
 */
public class RunSummaryActivity extends BaseActivity {
    private final String TAG = "RunSummaryActivity";
    private StepfrequencyUnit spm;
    private LocationUnit location;
    private HeartrateUnit heartrate;
    private SpeedUnit speed;
    private DistanceUnit distance;
    private DurationUnit timer;
    private int runId = 0;
    private RunStatisticsDAO rsd;
    private LocationDAO ld;
    private AffectUnit affect;
    private ImageButton nextBtn;

    /**
     * @description Save Run summary screenshot to phone memory
     * @param bitmap
     * @param strFileName
     */
    private static void savePic(Bitmap bitmap, String strFileName) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(strFileName);
            if (null != fos) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
                fos.flush();
                fos.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description getter for corresponsind layout resource
     * @return layout
     */
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_run_summary;
    }

    /**
     * @description Initiate all screen widgets
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        timer = (DurationUnit) findViewById(R.id.duration_unit);
        spm = (StepfrequencyUnit) findViewById(R.id.stepfrequency_unit);
        location = (LocationUnit) findViewById(R.id.location_unit);
        location.createMap(this);
        heartrate = (HeartrateUnit) findViewById(R.id.heartrate_unit);
        speed = (SpeedUnit) findViewById(R.id.speed_unit);
        distance = (DistanceUnit) findViewById(R.id.distance_unit);
        affect = (AffectUnit) findViewById(R.id.emotion_unit);
        rsd = new RunStatisticsDAO(RunSummaryActivity.this);
        ld = new LocationDAO(RunSummaryActivity.this);



        Intent i = getIntent();
        Bundle b = i.getExtras();
        runId = b.getInt("runId");
        if (runId == 0) {
            ImageButton homeBtn = (ImageButton) findViewById(R.id.house_btn);
            homeBtn.setImageResource(R.drawable.home_button);
            homeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }
            });
            nextBtn = (ImageButton) findViewById(R.id.next_btn);
            nextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), Last5RunsActivity.class);
                    startActivity(i);
                }
            });
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    displayLastRunData();
                }
            });
        } else {
            ImageButton homeBtn = (ImageButton) findViewById(R.id.house_btn);
            homeBtn.setImageResource(R.drawable.back_arrow);
            homeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), Last5RunsActivity.class);
                    startActivity(i);
                }
            });
            nextBtn = (ImageButton) findViewById(R.id.next_btn);
            nextBtn.setVisibility(View.GONE);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    displaySelectedRunData(runId);
                }
            });
        }

        //share button
        ImageButton shareButton = (ImageButton) findViewById(R.id.btnShare);
        shareButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final LinearLayout mView = (LinearLayout) findViewById(R.id.activity_run_summary);

                SnapshotReadyCallback callback = new SnapshotReadyCallback() {

                    @Override
                    public void onSnapshotReady(Bitmap snapshot) {
                        try {
                            mView.setDrawingCacheEnabled(true);
                            Bitmap backBitmap = mView.getDrawingCache();
                            Bitmap bmOverlay = Bitmap.createBitmap(
                                    backBitmap.getWidth(),
                                    backBitmap.getHeight(),
                                    backBitmap.getConfig());
                            Canvas canvas = new Canvas(bmOverlay);

                            int[] locationScreen = {0, 0};
                            mView.getLocationOnScreen(locationScreen);
                            int[] locationMap = location.getLocationOnScreen();
                            locationMap[0] = locationMap[0] - locationScreen[0];
                            locationMap[1] = locationMap[1] - locationScreen[1];

                            Matrix matrix = new Matrix();
                            matrix.postTranslate(locationMap[0], locationMap[1]);

                            canvas.drawBitmap(backBitmap, 0, 0, null);
                            canvas.drawBitmap(snapshot, matrix, null);
                            String screenshotLocation = Environment
                                    .getExternalStorageDirectory()
                                    .getAbsolutePath()
                                    + "/intelirun/Screenshot";

                            Util.createFolderIfNeeded(screenshotLocation);

                            screenshotLocation += "/Share"
                                    + System.currentTimeMillis() + ".jpg";

                            RunSummaryActivity.savePic(bmOverlay,
                                    screenshotLocation);

                            Intent sharingIntent = new Intent(
                                    Intent.ACTION_SEND);
                            Uri screenshotUri = Uri.parse("file://"
                                    + screenshotLocation);
                            sharingIntent.setType("*/*");// shareIntent.setType("*/*");
                            sharingIntent
                                    .putExtra(Intent.EXTRA_TEXT,
                                            "I ran with intelligence with #COMMIT INTELiRun");
                            sharingIntent.putExtra(Intent.EXTRA_STREAM,
                                    screenshotUri);
                            startActivity(Intent.createChooser(sharingIntent,
                                    "Share run using"));

                        } catch (OutOfMemoryError e) {
                            Intent sharingIntent = new Intent(
                                    Intent.ACTION_SEND);
                            sharingIntent.setType("*/*");// shareIntent.setType("*/*");
                            /*sharingIntent
                                    .putExtra(Intent.EXTRA_TEXT,
                                            "I ran with intelligence with #COMMIT INTELiRun");*/
                            startActivity(Intent.createChooser(sharingIntent,
                                    "Share run using"));
                        }
                    }
                };
                location.captureMapScreen(callback);

            }
        });



    }

    /**
     * @description display selected run workout data in RunSummaryActivity from given list in last%RunsActivity
     */
    private void displaySelectedRunData(int runId) {
        RunOverviewData rod;
        rod = rsd.getRunOverview(runId);
        try {
            location.setPositions(ld.getItems(rod.getStart_timestamp(),
                    rod.getStop_timestamp()));
            timer.updateTime(rod.getDuration());
            distance.setDistance(rod.getDistance());
            distance.setHeader("Total Distance");
            speed.setSpeed(rod.getAverageSpeed());
            speed.setHeader("Avg. Speed");
            spm.setStepfrequency(rod.getAverageStepFrequency());
            spm.setHeader("Avg. Step Frequency");
            heartrate.setHeartRate(rod.getAverageHeartRate());
            heartrate.setHeader("Avg. Heart Rate");
            affect.setAffect(rod.getAffect());
        } catch (Exception e) {
            Log.e(TAG,e.toString());
        }
    }

    /**
     * @description display latest run workout data in RunSummaryActivity
     */
    private void displayLastRunData() {
        RunOverviewData rod;
        rod = rsd.getLastRunOverview();
        try {
            location.setPositions(ld.getItems(rod.getStart_timestamp(),
                    rod.getStop_timestamp()));
            timer.updateTime(rod.getDuration());
            distance.setDistance(rod.getDistance());
            distance.setHeader("Total Distance");
            speed.setSpeed(rod.getAverageSpeed());
            speed.setHeader("Avg. Speed");
            spm.setStepfrequency(rod.getAverageStepFrequency());
            spm.setHeader("Avg. Step Frequency");
            heartrate.setHeartRate(rod.getAverageHeartRate());
            heartrate.setHeader("Avg. Heart Rate");
            affect.setAffect(rod.getAffect());
        } catch (Exception e) {
            Log.e(TAG,e.toString());
        }
    }

    /**
     * @description leads to MainActivity when back_arrow button pressed
     */
    @Override
    public void onBackPressed() {

            Intent intent = new Intent(RunSummaryActivity.this,
                    MainActivity.class);
            startActivity(intent);

    }


}