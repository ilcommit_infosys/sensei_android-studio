/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.hva.createit.scl.data.RunOverviewData;

import java.util.List;

/**
 * @author Nilanjan Nath <nilanajan_nath@Infosys.com>
 * @description ListAdapter class for displaying list of last 5 runs' timestamp
 */
public class RunOverviewListAdapter extends BaseAdapter {
    private final String TAG = "RunOverviewListAdapter";
    private List<RunOverviewData> runs;
    private Context context;
    private LayoutInflater inflater;
    private View.OnClickListener callback;

    /**
     * @description initiate widgets
     * @param context
     * @param runs
     * @param callback
     */
    public RunOverviewListAdapter(Context context, List<RunOverviewData> runs, View.OnClickListener callback) {
        this.runs = runs;
        this.context = context;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.callback = callback;
    }

    /**
     * @description getter for list size count
     * @return size of runList
     */
    @Override
    public int getCount() {
        return runs.size();
    }

    /**
     * @description getter for selected item in the list
     * @param position
     * @return RunOverviewData object
     */
    @Override
    public RunOverviewData getItem(int position) {
        return runs.get(position);
    }

    /**
     * @description getter for ItemId at given position
     * @param position
     * @return itemId
     */
    @Override
    public long getItemId(int position) {
        return runs.get(position).getId();
    }

    /**
     * @description getter for ListAdapter view
     * @param position
     * @param convertView
     * @param parent
     * @return View
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        RunOverviewData run = runs.get(position);

        ViewHolder holder;
        if (convertView == null) {
            Log.w(TAG, "item : " + position + " id: " + runs.get(position).getId());
            convertView = inflater.inflate(R.layout.runlistlayout, null);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.last5runtext);
            holder.button = (Button) convertView.findViewById(R.id.last5runbutton);
        } else {
            Log.w(TAG, "item : " + position + " id: "
                    + runs.get(position).getId());
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textView.setText(run.getStartDateAsString());
        holder.button.setText("Details >>");
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w(TAG, "item pressed: " + position + " id: " + runs.get(position).getId());

                int id = runs.get(position).getId();
                Bundle b = new Bundle();
                b.putInt("runId", id); // 1 == end
                Intent intent = new Intent(context, RunSummaryActivity.class);
                intent.putExtras(b); // Put your id to your next Intent
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        convertView.setTag(holder);
        return convertView;
    }

    /**
     * @description Inner class for ViewHolder
     */
    private class ViewHolder {
        TextView textView;
        Button button;
    }

}
