/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.meiru.voicerecording.ExertionDetection;

import org.hva.createit.scl.data.ExertionData;
import org.hva.createit.scl.dataaccess.ExertionDAO;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Shruti Bansal <shruti_bansal01@Infosys.com>
 * @description Activity to record voice sample from user and initiate exertion detection.
 */
public class ExertionActivity extends ExertionDetection {
    private String gender;
    private ExertionDAO exertionDAO;
    private ImageButton btn;
    private ImageView mic_overlay;
    private Animation animation;
    private int color = -1;
    private String inFileName = null;
    private SharedPreferences sharedPref;

    /**
     * @description Initiate all screen widgets
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exertion);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        gender = sharedPref.getString(
                getString(R.string.pref_user_gender), "male");
        setGender(gender);

        btn = (ImageButton) findViewById(R.id.btnRecording);

        mic_overlay = (ImageView) findViewById(R.id.overlay_blink);

        btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Drawable bMap = getResources().getDrawable(R.drawable.blue_circle);
                    btn.setBackground(bMap);
                    mic_overlay.setVisibility(View.VISIBLE);
                    animation = blinkImage(mic_overlay);
                    startRec();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ExertionActivity.this, "Start recording", Toast.LENGTH_LONG).show();
                        }
                    });
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    Drawable bMap = getResources().getDrawable(R.drawable.red_circle);
                    btn.setBackground(bMap);
                    animation.cancel();
                    mic_overlay.setVisibility(View.GONE);
                    View buttons = findViewById(R.id.buttons_exertion);
                    buttons.setVisibility(View.GONE);

                    View status = findViewById(R.id.progress_exertion);
                    status.setVisibility(View.VISIBLE);
                    stopRec();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ExertionActivity.this, "Stop recording", Toast.LENGTH_LONG).show();
                        }
                    });
                    detectFatigue();
                    return true;
                } else
                    return false;
            }
        });

        exertionDAO = new ExertionDAO(this);

    }

    /**
     * @description initiate fatigue detection from VoiceAnalysis library, read result, store in DAO
     * and render corresponding color to screen background blink
     *
     */
    private void detectFatigue() {
        Thread fatigueDetectionThread = new Thread(new Runnable() {
            @Override
            public void run() {

                HashMap map = fatigueDetection();
                Iterator iter = map.entrySet().iterator();

                while (iter.hasNext()) {
                    Map.Entry mEntry = (Map.Entry) iter.next();
                    inFileName = mEntry.getKey().toString();
                    color = Integer.parseInt(mEntry.getValue().toString());

                    Log.i("color", "mapResult in EA class:" + mEntry.getKey() + " : " + color);
                }
                ExertionData exertionData = new ExertionData( color, inFileName, Long.parseLong(inFileName.substring(0, inFileName.indexOf(".pcm") - 1)));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        intent.putExtra("color", color);
                        setResult(1, intent);
                        finish();
                    }
                });

                new StoreExertionData().doInBackground(exertionData);
            }
        });

        fatigueDetectionThread.start();


    }


    /**
     * @description blink given image for specified times on screen
     *
     */

    private Animation blinkImage(ImageView image) {
        final Animation animation = new AlphaAnimation((float) 0.5, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        // animation
        // rate
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        // infinitely
        animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the
        // end so the button will
        // fade back_arrow in
        image.startAnimation(animation);
        return animation;
    }

    /**
     * @description AsyncTask to store ExertionData object
     */
    private class StoreExertionData extends
            AsyncTask<ExertionData, Void, String> {

        @Override
        protected void onPreExecute() {
        }

        ;

        /**
         * The system calls this to perform work in a worker thread and delivers
         * it the parameters given to AsyncTask.execute()
         *
         * @return
         */
        @Override
        protected String doInBackground(ExertionData... acm) {
            exertionDAO.store(acm[0]);
            return "";
        }

        /**
         * The system calls this to perform work in the UI thread and delivers
         * the result from doInBackground()
         */
        @Override
        protected void onPostExecute(String result) {
        }
    }


}