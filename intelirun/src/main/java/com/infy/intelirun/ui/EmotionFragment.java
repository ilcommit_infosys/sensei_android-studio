/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import org.hva.createit.scl.data.AffectData;

/**
 * @author Shruti Bansal <shruti_bansal01@Infosys.com>
 * @description Fragment to capture emoticon in Mainactivity and RunActivity for before and after run
 */
public class EmotionFragment extends Fragment {
    private String emotion = "";
    private ImageButton depressed, angry, crying, sad, happy, excited, delighted;

    /**
     * @description Initialises fragment widgets
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return Fragment View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_emotion, container,
                false);


        depressed = (ImageButton) v.findViewById(R.id.depressed);
        angry = (ImageButton) v.findViewById(R.id.angry);
        crying = (ImageButton) v.findViewById(R.id.crying);
        sad = (ImageButton) v.findViewById(R.id.sad);
        happy = (ImageButton) v.findViewById(R.id.happy);
        excited = (ImageButton) v.findViewById(R.id.excited);
        delighted = (ImageButton) v.findViewById(R.id.delighted);


        depressed.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                resetAllButtons(v);
                //frustrated
                if (!emotion.equals("depressed")) {
                    emotion = "depressed";
                    setAffectDatatoActivity(emotion);

                    Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.depressed_blue);
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, depressed.getHeight(), depressed.getWidth(), true);
                    depressed.setImageBitmap(bMapScaled);
                } else
                    emotion = "";

            }
        });

        angry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllButtons(v);
                //frustrated
                if (!emotion.equals("angry")) {
                    emotion = "angry";
                    setAffectDatatoActivity(emotion);
                    Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.angry_blue);
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, angry.getHeight(), angry.getWidth(), true);
                    angry.setImageBitmap(bMapScaled);
                } else
                    emotion = "";
            }
        });

        crying.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllButtons(v);
                //frustrated
                if (!emotion.equals("crying")) {
                    emotion = "crying";
                    setAffectDatatoActivity(emotion);
                    Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.crying_blue);
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, crying.getHeight(), crying.getWidth(), true);
                    crying.setImageBitmap(bMapScaled);
                } else
                    emotion = "";
            }
        });

        sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllButtons(v);
                //frustrated
                if (!emotion.equals("sad")) {
                    emotion = "sad";
                    setAffectDatatoActivity(emotion);
                    Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.sad_blue);
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, sad.getHeight(), sad.getWidth(), true);
                    sad.setImageBitmap(bMapScaled);
                } else
                    emotion = "";
            }
        });

        happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllButtons(v);
                //frustrated
                if (!emotion.equals("happy")) {
                    emotion = "happy";
                    setAffectDatatoActivity(emotion);
                    Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.happy_blue);
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, happy.getHeight(), happy.getWidth(), true);
                    happy.setImageBitmap(bMapScaled);
                } else
                    emotion = "";
            }
        });

        excited.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllButtons(v);
                //frustrated
                if (!emotion.equals("excited")) {
                    emotion = "excited";
                    setAffectDatatoActivity(emotion);
                    Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.excited_blue);
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, excited.getHeight(), excited.getWidth(), true);
                    excited.setImageBitmap(bMapScaled);
                } else
                    emotion = "";
            }
        });

        delighted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAllButtons(v);
                //frustrated
                if (!emotion.equals("delighted")) {
                    emotion = "delighted";
                    setAffectDatatoActivity(emotion);
                    Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.delighted_blue);
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, delighted.getHeight(), delighted.getWidth(), true);
                    delighted.setImageBitmap(bMapScaled);
                } else
                    emotion = "";
            }
        });


        return v;
    }

    /**
     * @description return AffectData object to parent activity
     * @param emotion
     */
    private void setAffectDatatoActivity(String emotion) {
        try {
            MainActivity activity = (MainActivity) getActivity();
            activity.setAffectData(returnAffect(emotion));

        } catch (ClassCastException e) {
            RunActivity activity = (RunActivity) getActivity();
            activity.setAffectData(returnAffect(emotion));

        }
    }

    /**
     * @description reset all emoticons once an emoticon is selected
     * @param view
     */
    private void resetAllButtons(View view) {
        //emotion="";
        Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.depressed);
        Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, depressed.getHeight(), depressed.getWidth(), true);
        depressed.setImageBitmap(bMapScaled);

        bMap = BitmapFactory.decodeResource(getResources(), R.drawable.angry);
        bMapScaled = Bitmap.createScaledBitmap(bMap, angry.getHeight(), angry.getWidth(), true);
        angry.setImageBitmap(bMapScaled);

        bMap = BitmapFactory.decodeResource(getResources(), R.drawable.crying);
        bMapScaled = Bitmap.createScaledBitmap(bMap, crying.getHeight(), crying.getWidth(), true);
        crying.setImageBitmap(bMapScaled);

        bMap = BitmapFactory.decodeResource(getResources(), R.drawable.sad);
        bMapScaled = Bitmap.createScaledBitmap(bMap, sad.getHeight(), sad.getWidth(), true);
        sad.setImageBitmap(bMapScaled);

        bMap = BitmapFactory.decodeResource(getResources(), R.drawable.happy);
        bMapScaled = Bitmap.createScaledBitmap(bMap, happy.getHeight(), happy.getWidth(), true);
        happy.setImageBitmap(bMapScaled);

        bMap = BitmapFactory.decodeResource(getResources(), R.drawable.excited);
        bMapScaled = Bitmap.createScaledBitmap(bMap, excited.getHeight(), excited.getWidth(), true);
        excited.setImageBitmap(bMapScaled);

        bMap = BitmapFactory.decodeResource(getResources(), R.drawable.delighted);
        bMapScaled = Bitmap.createScaledBitmap(bMap, delighted.getHeight(), delighted.getWidth(), true);
        delighted.setImageBitmap(bMapScaled);
    }

    /**
     * @description return AffectData object corresponding to given emotion string
     * @param str
     * @return AffectData object
     */
    private AffectData returnAffect(String str) {
        AffectData afdat = null;
        //  int deviceId=getDefaultDeviceUuid(getActivity());
        int deviceId = 0;
        switch (str) {
            case "depressed":
                //frustrated
                afdat = new AffectData(deviceId, System
                        .currentTimeMillis(), -1, 1, -1);
                break;
            case "angry":
                //angry

                afdat = new AffectData(deviceId, System
                        .currentTimeMillis(), -1, 1, 1);
                break;
            case "crying":
                //afraid
                afdat = new AffectData(deviceId, System
                        .currentTimeMillis(), -1, -1, 1);
                break;
            case "sad":
                //sad
                afdat = new AffectData(deviceId, System
                        .currentTimeMillis(), -1, -1, -1);
                break;
            case "happy":
                //relaxed (1,-1,-1)
                afdat = new AffectData(deviceId, System
                        .currentTimeMillis(), 1, -1, -1);
                break;
            case "excited":
                //content (1,1,-1)
                afdat = new AffectData(deviceId, System
                        .currentTimeMillis(), 1, 1, -1);
                break;
            case "delighted":
                //elated
                afdat = new AffectData(deviceId, System
                        .currentTimeMillis(), 1, 1, 1);
                break;
        }
        return afdat;
    }


}
