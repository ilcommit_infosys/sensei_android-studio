package com.infy.intelirun.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.hva.createit.scl.data.DistanceData;
import org.hva.createit.scl.data.HeartRateData;
import org.hva.createit.scl.data.MeasurementData;
import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.scl.data.StepfrequencyData;
import org.hva.createit.scl.dataaccess.Database;
import org.hva.createit.scl.dataaccess.RunStatisticsDAO;

import java.text.DecimalFormat;
import java.util.List;

/**
 * @author Shruti Bansal <shruti_bansal01@Infosys.com>, Nilanjan Nath <nilanajan_nath@Infosys.com>
 * @description Activity to render aggregate values from db on workoutStats screen
 */
public class HistorySummaryActivity extends Activity
        //implements AdapterView.OnItemSelectedListener
{
    private final String TAG = "History";
    private int flag=0;
 //   private HistoryTabs ht ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_summary);

        /*Spinner spinner = (Spinner) findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.history_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(HistorySummaryActivity.this);
        AsyncTaskRunner runner = new AsyncTaskRunner();
        runner.execute(item);
        */

        RunOverviewData runOverviewData = getRunStats();

        TextView averageStepsTV =(TextView) findViewById(R.id.history_averageSteps);
        TextView averageSpeedTV=(TextView) findViewById(R.id.history_averageSpeed);
        TextView averageHrTV=(TextView) findViewById(R.id.history_averageHr);
        TextView totalDistanceTV =(TextView) findViewById(R.id.history_totalDistance);
        TextView totalDurationTV=(TextView) findViewById(R.id.history_totalDuration);

        if( runOverviewData!=null) {
            DecimalFormat df = new DecimalFormat("##0.##");
            // Double num=Math.round(runOverviewData.getDistance()* 10.0) / 10.0;
            totalDistanceTV.setText(df.format(runOverviewData.getDistance() / 1000));

            //Double num=Math.round(runOverviewData.getAverageSpeedMeasurementData().getValueAsFloat()* 10.0) / 10.0;
            averageSpeedTV.setText(Math.round(runOverviewData.getAverageSpeedMeasurementData().getValueAsFloat())+"");
            Log.i(TAG,"Speed: getValuesAsFloat:"+Math.round(runOverviewData.getAverageSpeedMeasurementData().getValueAsFloat())+"");
            //Log.i(TAG,"Speed: getValuesAsDouble:"+Math.round(runOverviewData.getAverageSpeedMeasurementData().getValueAsDouble())+"");
            Log.i(TAG,"Speed: getValuesAsFloat in df:"+df.format(runOverviewData.getAverageSpeedMeasurementData().getValueAsFloat()));


            double val= runOverviewData.getDurationMeasurementData().getValueAsDouble();
            totalDurationTV.setText(returnTime(val));

            val= runOverviewData.getAverageHeartRateMeasurementData().getValueAsDouble();
            int intVal=(int)val;
            averageHrTV.setText(intVal+"");


            val= runOverviewData.getAverageStepFrequencyMeasurementData().getValueAsDouble();
            intVal=(int)val;
            averageStepsTV.setText(intVal+"");
        }



        ImageButton back = (ImageButton) findViewById(R.id.history_back_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        /*AsyncTaskRunner runner = new AsyncTaskRunner();
        runner.execute();*/
    }




    public RunOverviewData getRunStats(){
        RunStatisticsDAO statsDAO=new RunStatisticsDAO(this);
        List<RunOverviewData> runs = statsDAO.getLastRunOverviews((int)statsDAO.getItemCount());
        DistanceData totalDistance = new DistanceData(0, 0, 0);
        MeasurementData totalDuration = new MeasurementData(0,"", 0.0,0);
        MeasurementData averageSpeed = new MeasurementData(0, "", 0.0, 0);
        MeasurementData averageStepFrequency=new MeasurementData(0, "",0.0, 0);
        MeasurementData averageHeartRate=new MeasurementData(0, "",0.0, 0);

        int i = 0;
        for(RunOverviewData run: runs){
            totalDistance.setDistance(totalDistance.getDistance() + run.getDistance());
            totalDuration.setValue(totalDuration.getValueAsDouble() + run.getDuration());
            averageSpeed.setValue((averageSpeed.getValueAsFloat() * i + (run.getAverageSpeed())) / (i + 1));

            averageStepFrequency.setValue((averageStepFrequency.getValueAsDouble() * i + ((double)run.getAverageStepFrequency())) / (i + 1));

            averageHeartRate.setValue((averageHeartRate.getValueAsDouble() * i + ((double)run.getAverageHeartRate())) / (i + 1));
            i++;
        }
        RunOverviewData thisRun = new RunOverviewData(0,0,System.currentTimeMillis());
        thisRun.setAverageSpeed(averageSpeed);
        thisRun.setDistance(totalDistance);
        thisRun.setDuration(totalDuration);
        thisRun.setAverageHeartRate(averageHeartRate);
        thisRun.setAverageStepFrequency(averageStepFrequency);
        return thisRun;

    }
    private String returnTime(double time) {
        int timeSeconds = (int)time / 1000 % 60;
        int timeMinutes =(int) time / (60 * 1000) % 60;
        int timeHours = (int)time / (60 * 60 * 1000) % 24;
        /*if (timeHours == 0) {
            return((timeMinutes < 10 ? "0" + timeMinutes : timeMinutes) + ":"
                    + (timeSeconds < 10 ? "0" + timeSeconds : timeSeconds));
        } else {*/
            return((timeHours < 10 ? "0" + timeHours : timeHours) + ":"
                    + (timeMinutes < 10 ? "0" + timeMinutes : timeMinutes)
                    + ":"
                    + (timeSeconds < 10 ? "0" + timeSeconds : timeSeconds));
        //}
    }



/*

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        String item = (String)parent.getItemAtPosition(position);
        Log.w(TAG, item+" pressed for "+position+" with "+id);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        *//*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         *//*

        @Override
        protected void onPreExecute()
        {
            runOnUiThread(new Runnable() {

                @Override
                public void run()
                {
                    Toast.makeText(getApplicationContext(), "Trying to connect to cloud", Toast.LENGTH_LONG).show() ;
                }
            });
        }
        @Override
        protected String doInBackground(String... params)
        {
            final String arg = params[0] ;

            try
            {
                publishProgress("Starting"); // Calls onProgressUpdate()

                boolean useLive = true; // Specifying whether you want to use live server or stagint server
                Log.w(TAG, "level 1 ") ;

                // SenseStatisticsQuery query = new SenseStatisticsQuery().setLimit(100);
                SenseStatisticsAPI statisticsAPI = new SenseStatisticsAPI(useLive);
                statisticsAPI.setSessionId(SenseApi.getSessionId(getApplicationContext()));

                Log.w(TAG, "level 2 ") ;

                // get available Ids in user context
                JSONArray arrayOfContextIds = statisticsAPI.getContextIds(SenseStatisticsContext.USER);

                String length = String.valueOf(arrayOfContextIds.length());

                Log.w(TAG, "arrayOfContextIds : "+length) ;







                for(int i=0;i<arrayOfContextIds.length();i++)
                {
                    int contextId = (int)arrayOfContextIds.get(i);
                    String ci = String.valueOf(contextId);
                    Log.w(TAG, "Context Id :"+ci);

                    JSONArray arrayOfMeasurementTypes =
                            statisticsAPI.getAvailableMeasurementType(SenseStatisticsContext.USER, contextId);
                    Log.i(TAG,"Available measurement types:");
                    for(int k=0;k<arrayOfMeasurementTypes.length();k++) {
                        Log.i(TAG,(String)arrayOfMeasurementTypes.get(k));
                    }


                    for(int k=0;k<arrayOfMeasurementTypes.length();k++)
                    {
                        String measurement = (String)arrayOfMeasurementTypes.get(k);

                        //  Log.w(TAG, "Measurement Type :"+measurement);

                        try
                        {
                            if(arg.equals("Month"))
                            {
                                Log.w(TAG, "Duration/Type : Month");


                                if(measurement.equals("totalDuration")) {

                                    JSONArray statistics = statisticsAPI.getStatistics(SenseStatisticsContext.USER,
                                            contextId,
                                            AggregationType.SUM,
                                            Period.MONTH,
                                            measurement);
                                    // for(int j=0;j<statistics.length();j++) {
                                    try {
                                        JSONObject json = (JSONObject) statistics.get(0);
                                        Log.w(TAG, "totalduration#####$$$$$*****$$$$$##########$$$$$*****$$$$$##########$$$$$*****$$$$$#####");
                                        Log.w(TAG, statistics.toString());
                                        // ht.totalDuration=json.getString("value");
                                    } catch (NullPointerException e) {
                                        Log.w(TAG, "NullPointer caught.."+e.getMessage());
                                    }
                                    //   }
                                }
                                else
                                if(measurement.equals("totalDistance")) {

                                    JSONArray statistics = statisticsAPI.getStatistics(SenseStatisticsContext.USER,
                                            contextId,
                                            AggregationType.SUM,
                                            Period.MONTH,
                                            measurement);
                                    //for(int j=0;j<statistics.length();j++) {
                                    try {
                                        JSONObject json = (JSONObject) statistics.get(0);
                                        Log.w(TAG, "totalDistance#####$$$$$*****$$$$$##########$$$$$*****$$$$$##########$$$$$*****$$$$$#####");
                                        Log.w(TAG, statistics.toString());
                                        //ht.totalDistance=json.getString("value");
                                    } catch (NullPointerException e) {
                                        Log.w(TAG, "NullPointer caught.."+e.getMessage());
                                    }
                                    //   }
                                }
                                else
                                if(measurement.equals("totalDayRunning")) {

                                    JSONArray statistics = statisticsAPI.getStatistics(SenseStatisticsContext.USER,
                                            contextId,
                                            AggregationType.SUM,
                                            Period.MONTH,
                                            measurement);
                                    //for(int j=0;j<statistics.length();j++) {
                                    try {
                                        JSONObject json = (JSONObject) statistics.get(0);
                                        Log.w(TAG, "totalDayRunning#####$$$$$*****$$$$$##########$$$$$*****$$$$$##########$$$$$*****$$$$$#####");
                                        Log.w(TAG, statistics.toString());
                                        // ht.totalDayRunning=json.getString("value");
                                    } catch (NullPointerException e) {
                                        Log.w(TAG, "NullPointer caught.."+e.getMessage());
                                    }
                                    //}
                                }
                                else
                                if(measurement.equals("averageSpeed")) {

                                    JSONArray statistics = statisticsAPI.getStatistics(SenseStatisticsContext.USER,
                                            contextId,
                                            AggregationType.SUM,
                                            Period.MONTH,
                                            measurement);
                                    //for(int j=0;j<statistics.length();j++) {
                                    try {
                                        // JSONObject json = (JSONObject) statistics.get(0);
                                        Log.w(TAG, "avergaeSpeed#####$$$$$*****$$$$$##########$$$$$*****$$$$$##########$$$$$*****$$$$$#####");
                                        Log.w(TAG, statistics.toString());
                                        // ht.totalDayRunning=json.getString("value");
                                    } catch (NullPointerException e) {
                                        Log.w(TAG, "NullPointer caught.."+e.getMessage());
                                    }
                                    //}
                                }

                            }
                            else if(arg.equals("Week"))
                            {
                                Log.w(TAG, "Duration/Type : Week");


                                if(measurement.equals("totalDuration")) {

                                    JSONArray statistics = statisticsAPI.getStatistics(SenseStatisticsContext.USER,
                                            contextId,
                                            AggregationType.SUM,
                                            Period.WEEK,
                                            measurement);
                                    //for(int j=0;j<statistics.length();j++) {
                                    try {
                                        JSONObject json = (JSONObject) statistics.get(0);
                                        Log.w(TAG, "totalDuration#####$$$$$*****$$$$$##########$$$$$*****$$$$$##########$$$$$*****$$$$$#####");
                                        Log.w(TAG, statistics.toString());
                                        //ht.totalDuration=json.getString("value");
                                    } catch (NullPointerException e) {
                                        Log.w(TAG, "NullPointer caught.."+e.getMessage());
                                    }
                                    //}
                                }
                                else
                                if(measurement.equals("totalDistance")) {

                                    JSONArray statistics = statisticsAPI.getStatistics(SenseStatisticsContext.USER,
                                            contextId,
                                            AggregationType.SUM,
                                            Period.WEEK,
                                            measurement);
                                    //for(int j=0;j<statistics.length();j++) {
                                    try {
                                        JSONObject json = (JSONObject) statistics.get(0);
                                        Log.w(TAG, "totalDistance#####$$$$$*****$$$$$##########$$$$$*****$$$$$##########$$$$$*****$$$$$#####");
                                        Log.w(TAG, statistics.toString());
                                        //ht.totalDistance=json.getString("value");
                                    } catch (NullPointerException e) {
                                        Log.w(TAG, "NullPointer caught.."+e.getMessage());
                                    }
                                    //}
                                }
                                else
                                if(measurement.equals("totalDayRunning")) {

                                    JSONArray statistics = statisticsAPI.getStatistics(SenseStatisticsContext.USER,
                                            contextId,
                                            AggregationType.SUM,
                                            Period.WEEK,
                                            measurement);
                                    //for(int j=0;j<statistics.length();j++) {
                                    try {
                                        JSONObject json = (JSONObject) statistics.get(0);
                                        Log.w(TAG, "totalDayRunning#####$$$$$*****$$$$$##########$$$$$*****$$$$$##########$$$$$*****$$$$$#####");
                                        Log.w(TAG, statistics.toString());
                                        //  ht.totalDayRunning=json.getString("value");
                                    } catch (NullPointerException e) {
                                        Log.w(TAG, "NullPointer caught.."+e.getMessage());
                                    }
                                    //}
                                }
                            }
                        }
                        catch (SenseResponseException e)
                        {
                            flag=0;
                            Log.w(TAG, "Error msg :"+e.getMessage());
                            e.printStackTrace() ;
                        }
                    }
                }
                flag=1;











            }
            catch (Exception e)
            {
                flag=0;
                LOG.e(TAG, e.toString());
            }
            *//*new Thread(new Runnable()
            {
                @Override
                public void run()
                {

                }
            }).start();*//*

            return null;
        }

        *//*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         *//*
        @Override
        protected void onPostExecute(String aVoid)
        {
            super.onPostExecute(aVoid);
            if(flag==1)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Toast.makeText(getApplicationContext(),
                                "successful", Toast.LENGTH_LONG).show();
                        try
                        {
                            if(null!=ht.totalDayRunning && null!=ht.totalDistance & null!=ht.totalDuration)
                            {
                                TextView tv1 = (TextView)findViewById(R.id.history_totalDistance);
                                tv1.setText(ht.totalDistance);
                                TextView tv2 = (TextView)findViewById(R.id.history_totalDuration);
                                tv2.setText(ht.totalDuration);
                                *//*TextView tv3 = (TextView)findViewById(R.id.history_totalDayRunning);
                                tv3.setText(ht.totalDayRunning);*//*
                            }
                        }
                        catch (NullPointerException e)
                        {
                            Log.w(TAG, "m Here...."+e.getMessage());
                        }
                    }
                });
            }
            else
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Toast.makeText(getApplicationContext(),
                                "unsuccessful", Toast.LENGTH_LONG).show();
                    }
                });
            }
        }


        *//*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         *//*

        @Override
        protected void onProgressUpdate(String... text) {
            Log.w(TAG, text[0]);
            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }

    public class HistoryTabs
    {
        public String  totalDuration ;
        public String totalDistance ;
        public String totalDayRunning ;
    }*/
}

