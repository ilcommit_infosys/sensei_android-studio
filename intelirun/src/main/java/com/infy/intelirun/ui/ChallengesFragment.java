/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.ui.ChallengeOverviewActivity;


/**
 * @author Shruti Bansal <shruti_bansal01@Infosys.com>, Dominik Egger <dominik@almende.org>
 * @description Clickable fragment appearing on MainActivity displaying no. of new challenges received.
 */
public class ChallengesFragment extends Fragment implements View.OnClickListener {

    private TextView _receivedChallenges;

    private ChallengeDB _challengesDB;

    /**
     * @description Initialises fragment widgets
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return Fragment View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        _challengesDB = ChallengeDB.getInstance(getActivity());

        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_challenges, container,
                false);

        v.setOnClickListener(this);

        //ImageButton challenges = (ImageButton) v.findViewById(R.id.image_challenges);
        //challenges.setOnClickListener(this);

        _receivedChallenges = (TextView) v.findViewById(R.id.receivedChallenges);
        updateReceivedChallenges();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateReceivedChallenges();
    }

    /**
     * @description set/update no of received challenges on TextView
     */
    private void updateReceivedChallenges() {
        _receivedChallenges.setText(String.valueOf(_challengesDB.getReceivedChallenges().size()));
    }

    @Override
    public void onClick(View v) {
        ChallengeOverviewActivity.show(getActivity());
    }
}
