/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.infy.senseCommunication.Constants;
import com.infy.senseCommunication.Infosys_SenseUserDataAPI;
import com.infy.senseCommunication.SenseResponseCallback;

import org.apache.cordova.LOG;
import org.hva.createit.sensei.ui.BaseActivity;
import org.json.JSONObject;

/**
 * @author Shruti Bansal <shruti_bansal01@Infosys.com>
 * @description Activity to enable user edit his details supplied during registration
 */
public class UserSettingsActivity extends BaseActivity {
    private String TAG = "UserSettingsActivity";
    private View mRegistrationFormView;
    private View mRegistrationStatusView;
    private SharedPreferences sharedPref;
    private String userName, lastName, userEmail, userHeight, userWeight, userPassword, userGender, userAge;
    private EditText userNameEdit, lastNameEdit, userEmailEdit, userHeightEdit, userWeightEdit, userAgeEdit;
    private SenseResponseCallback callback = new SenseResponseCallback() {
        @Override
        public void getResponse(JSONObject response) {

            try {
                Log.i(TAG, response.toString());
                userName = response.getString(Constants.name);
                SharedPreferences.Editor editor;
                if(userName!=null&&!userName.equals("")) {
                    editor = sharedPref.edit();
                    editor.putString(getString(R.string.pref_user_name), userName);
                    editor.commit();
                }

                lastName = response.getString(Constants.last_name);
                if(lastName!=null&&!lastName.equals("")) {
                    editor = sharedPref.edit();
                    editor.putString(getString(R.string.pref_user_last_name), lastName);
                    editor.commit();
                }

                userEmail = response.getString(Constants.email);
                if(userEmail!=null&&!userEmail.equals("")) {
                    editor = sharedPref.edit();
                    editor.putString(getString(R.string.pref_user_email), userEmail);
                    editor.commit();
                }

                userHeight = response.getString(Constants.height);
                if(userHeight!=null&&!userHeight.equals("")) {
                    editor = sharedPref.edit();
                    editor.putString(getString(R.string.pref_user_height), userHeight);
                    editor.commit();
                }

                userWeight = response.getString(Constants.weight);
                if(userWeight!=null&&!userWeight.equals("")) {
                    editor = sharedPref.edit();
                    editor.putString(getString(R.string.pref_user_weight), userWeight);
                    editor.commit();
                }

                /*userPassword = response.getString(Constants.password);
                editor = sharedPref.edit();
                editor.putString(getString(R.string.pref_user_password), userPassword);
                editor.commit();*/

                userGender = response.getString(Constants.gender);
                if(userGender!=null&&!userGender.equals("")) {
                    editor = sharedPref.edit();
                    editor.putString(getString(R.string.pref_user_gender), userGender);
                    editor.commit();
                }

                userAge = response.getString(Constants.age);
                if(userAge!=null&&!userAge.equals("")) {
                    editor = sharedPref.edit();
                    editor.putString(getString(R.string.pref_user_age), userAge);
                    editor.commit();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showProgress(false);
                        setData();
                    }
                });
            } catch (Exception e) {

                LOG.e(TAG, "Error while retrieving profile info from cloud:" + e.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(UserSettingsActivity.this, "Error retrieving profile info from cloud. Retrieving from local data.", Toast.LENGTH_LONG).show();
                        showProgress(false);
                        setData();
                    }
                });


            }

        }
    };
    private RadioGroup rg;
    private TextView status;

    /**
     * @param savedInstanceState
     * @description OnCreate method for UserSettingsActivity
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userNameEdit = ((EditText) findViewById(R.id.first_name));
        lastNameEdit = ((EditText) findViewById(R.id.last_name));
        //userPasswordEdit = (EditText) findViewById(R.id.password);
        userHeightEdit = ((EditText) findViewById(R.id.height));
        userEmailEdit = ((EditText) findViewById(R.id.email));
        userWeightEdit = ((EditText) findViewById(R.id.weight));
        userAgeEdit = ((EditText) findViewById(R.id.age));
        rg = (RadioGroup) findViewById(R.id.gender_group);

        View pwdLayout=findViewById(R.id.passwordLayout);
        pwdLayout.setVisibility(View.GONE);
        userEmailEdit.setEnabled(false);

        ImageButton backBtn = (ImageButton) findViewById(R.id.userprofile_back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });

        mRegistrationFormView = findViewById(R.id.registration_form);
        mRegistrationStatusView = findViewById(R.id.registration_status);

        status = (TextView) findViewById(R.id.registration_status_message);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        //retrieve user profile data from Sense cloud and populate in the screen, if not available, populate from shared preferences
        receiveData();


        userNameEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.pref_user_name),
                        s.toString());
                editor.commit();
            }
        });
        lastNameEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.pref_user_last_name),
                        s.toString());
                editor.commit();
            }
        });
        userEmailEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.pref_user_email),
                        s.toString());
                editor.commit();
            }
        });

        /*((EditText) findViewById(R.id.password))
                .addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(
                                getString(R.string.pref_user_password),
                                s.toString());
                        editor.commit();
                    }
                });*/

        userHeightEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.pref_user_height),
                        s.toString());
                editor.commit();
            }
        });
        userWeightEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.pref_user_weight),
                        s.toString());
                editor.commit();
            }
        });

        userAgeEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.pref_user_age),
                        s.toString());
                editor.commit();
            }
        });

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                SharedPreferences.Editor editor = sharedPref.edit();
                switch (checkedId) {
                    case R.id.gender_female:
                        editor.putString(getString(R.string.pref_user_gender),
                                getString(R.string.gender_female_label));
                        break;
                    case R.id.gender_male:
                        editor.putString(getString(R.string.pref_user_gender),
                                getString(R.string.gender_male_label));
                        break;
                }
                editor.commit();
            }
        });
        Button create = (Button) findViewById(R.id.registration_button);
        create.setVisibility(View.GONE);
        Button save = (Button) findViewById(R.id.save_button);
        save.setVisibility(View.VISIBLE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendData().execute();


            }
        });
    }

    /**
     * @description method to retrieve set preferences for user profile data
     */
    private void getPref() {
        userName = sharedPref.getString(
                getString(R.string.pref_user_name), null);
        lastName = sharedPref.getString(
                getString(R.string.pref_user_last_name), null);
        userEmail = sharedPref.getString(
                getString(R.string.pref_user_email), null);
        userPassword = sharedPref.getString(
                getString(R.string.pref_user_password), null);
        userHeight = sharedPref.getString(
                getString(R.string.pref_user_height), null);
        userWeight = sharedPref.getString(
                getString(R.string.pref_user_weight), null);
        userAge = sharedPref.getString(
                getString(R.string.pref_user_age), null);
        userGender = sharedPref.getString(
                getString(R.string.pref_user_gender), null);

    }

    /**
     * @description method to retrieve preferences and set user profile data on the screen widgets
     */

    private void setData() {
        getPref();
        userNameEdit.setText(userName);
        lastNameEdit.setText(lastName);
        userEmailEdit.setText(userEmail);
        //userPasswordEdit.setText(userPassword);
        userHeightEdit.setText(userHeight);
        userWeightEdit.setText(userWeight);
        userAgeEdit.setText(userAge);
        if (userGender != null && userGender.equals(getString(R.string.gender_female_label))) {
            ((RadioButton) findViewById(R.id.gender_female)).setChecked(true);
        } else if (userGender != null && userGender.equals(getString(R.string.gender_male_label))) {
            ((RadioButton) findViewById(R.id.gender_male)).setChecked(true);
        }


    }

    /**
     * @description Shows the progress UI and hides the registration form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);

            mRegistrationStatusView.setVisibility(View.VISIBLE);
            mRegistrationStatusView.animate().setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mRegistrationStatusView
                                    .setVisibility(show ? View.VISIBLE
                                            : View.GONE);
                        }
                    });

            mRegistrationFormView.setVisibility(View.VISIBLE);
            mRegistrationFormView.animate().setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mRegistrationFormView
                                    .setVisibility(show ? View.GONE
                                            : View.VISIBLE);
                        }
                    });
        } else {
            mRegistrationStatusView.setVisibility(show ? View.VISIBLE
                    : View.GONE);
            mRegistrationFormView
                    .setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * @return activity layout
     * @description getter for user profile layout
     */
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_user_profile;
    }

    /**
     * @description OnBackPressed for userSettingsActivity
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    /**
     * @description execute SendData AsyncTask
     */
    protected void sendData() {
        new SendData().execute();
    }

    /**
     * method to initiate profile data retrieval from Sense cloud
     */
    private void receiveData() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setText("Retrieving data");
                showProgress(true);
                Toast.makeText(UserSettingsActivity.this, "Trying to receive profile info from cloud", Toast.LENGTH_LONG).show();
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject data = new Infosys_SenseUserDataAPI().getUserData(getApplicationContext());

                    callback.getResponse((JSONObject) data.get(Constants.userData));

                } catch (Exception e) {
                    LOG.e(TAG, e.toString());

                }

            }
        }).start();

    }

    /**
     * @description AsyncTask to send updated user details to cloud
     */
    private class SendData extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {


        }

        @Override
        protected Void doInBackground(Void... params) {

            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    status.setText("Saving data...");
                    showProgress(true);
                    Toast.makeText(UserSettingsActivity.this, "Trying to send profile info", Toast.LENGTH_LONG).show();
                }
            });
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        getPref();
                        new Infosys_SenseUserDataAPI().sendUserData(getApplicationContext(), userName, lastName, userEmail, userHeight, userWeight, userPassword, userAge, userGender);
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(UserSettingsActivity.this,
                                        "Profile info sending successful", Toast.LENGTH_LONG).show();
                                showProgress(false);

                            }
                        });
                    } catch (Exception e) {
                        LOG.e(TAG, e.toString());
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(UserSettingsActivity.this,
                                        "Profile info sending unsuccessful. Try again!", Toast.LENGTH_LONG).show();
                                showProgress(false);

                            }
                        });

                    }
                }
            }).start();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }


    }


}
