package com.infy.intelirun.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;


/**
 * @author Nilanjan Nath <nilanajan_nath@Infosys.com>
 * @description Activity to list timestamps of last 5 runs from database for user selection
 */
public class Last5RunsActivity extends Activity {
    /**
     * @description Initiate all screen widgets
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last5runs);
        ImageButton back = (ImageButton) findViewById(R.id.Last5runs_back_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        RunOverviewListFragment runOverviewListFragment = new RunOverviewListFragment();
        Bundle args = new Bundle();
        args.putInt("items", 3);
        runOverviewListFragment.setArguments(args);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.extra_content_frame, runOverviewListFragment)
                .commit();
        }
}
