/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.hva.createit.scl.data.AffectData;
import org.hva.createit.scl.data.RunData;
import org.hva.createit.scl.data.StepfrequencyData;
import org.hva.createit.scl.dataaccess.AffectDAO;
import org.hva.createit.scl.dataaccess.RunDataDAO;
import org.hva.createit.sensei.ui.DistanceUnit;
import org.hva.createit.sensei.ui.DurationUnit;
import org.hva.createit.sensei.ui.HeartrateUnit;
import org.hva.createit.sensei.ui.LocationUnit;
import org.hva.createit.sensei.ui.SCLControllerBaseActivity;
import org.hva.createit.sensei.ui.SpeedUnit;
import org.hva.createit.sensei.ui.StepfrequencyUnit;

public class RunActivity extends SCLControllerBaseActivity {
    int requestCode = 1;
    AffectData aftdat;
    int color = Color.WHITE;
    private StepfrequencyUnit spm;
    private LocationUnit location;
    private HeartrateUnit heartrate;
    private SpeedUnit speed;
    private DistanceUnit distance;
    private DurationUnit timer;
    private ImageButton finishButton;
    // private ImageButton pauseButton;
    private ImageButton exertionButton;
    // private double distanceDouble;
    private AffectDAO ad;
    private RunDataDAO rdd;

    @Override
    public void onBackPressed() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_run;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rdd = new RunDataDAO(this);

        ad = new AffectDAO(this);
        finishButton = (ImageButton) findViewById(R.id.btnStop);
//pauseButton =(ImageButton) findViewById(R.id.btnPause);
        exertionButton = (ImageButton) findViewById(R.id.btnExertion);
        timer = (DurationUnit) findViewById(R.id.duration_unit);
        timer.startTimer(this);

        spm = (StepfrequencyUnit) findViewById(R.id.stepfrequency_unit);

        location = (LocationUnit) findViewById(R.id.location_unit);
        location.createMap(this);

        heartrate = (HeartrateUnit) findViewById(R.id.heartrate_unit);

        speed = (SpeedUnit) findViewById(R.id.speed_unit);

        distance = (DistanceUnit) findViewById(R.id.distance_unit);
        // distanceDouble = 0;

        /*new Handler().post(new Runnable() {
            @Override
            public void run() {
                rdd.store(new RunData(0,
                        RunData.RUNDATA_TYPE_RUNSTART, System
                        .currentTimeMillis()));
            }
        });*/

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startSensors();
                    }
                }, 1000);

            }
        });


/*

        pauseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onPause();
            }
        });*/


        finishButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                stopSensors();
                final long timeStamp = System.currentTimeMillis();
                //aftdat.setTimestamp(timeStamp);
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (aftdat != null)
                            ad.store(aftdat);
                        rdd.store(new RunData(0,
                                RunData.RUNDATA_TYPE_RUNEND, timeStamp));
                    }
                });

                Intent intent = new Intent(RunActivity.this,
                        RunSummaryActivity.class);
                Bundle b = new Bundle();
                b.putInt("runstate", 1); // 1 == end
                intent.putExtras(b);
                startActivity(intent);
                finish();
                /*} else

                    finishRun(timeStamp);*/
            }
        });

        exertionButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RunActivity.this,
                        ExertionActivity.class);
                startActivityForResult(intent, requestCode);
            }
        });

    }

    public void finishRun(final long time) {

    }

    public AffectData getAffectData() {
        return this.aftdat;
    }

    public void setAffectData(AffectData aftdat) {
        if (aftdat != null)
            System.out.println("@@@@@@@@@@@@@@@@@@@@" + aftdat.getArousal() + "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        this.aftdat = aftdat;
    }

    @Override
    public void onPause() {
        super.onPause();
        distance.setAutoUpdate(false);
        speed.setAutoUpdate(false);
        // location.setAutoUpdate(false);
        spm.setAutoUpdate(false);
        heartrate.setAutoUpdate(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        distance.setAutoUpdate(true);
        speed.setAutoUpdate(true);
        location.setAutoUpdate(true);
        spm.setAutoUpdate(true);
        heartrate.setAutoUpdate(true);
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(StepfrequencyData event) {
        final StepfrequencyData eventCopy = event;
        RunActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                spm.setInfo("" + eventCopy.getStepFrequency());
            }
        });
    }

    /*
     * @Override public void onBackPressed() { // super.onBackPressed();
     * finish(); }
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                // NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        try {
            if (requestCode == 1) {
                color = data.getIntExtra("color", Color.WHITE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(RunActivity.this, "Exertion level: " + decodeColor(color), Toast.LENGTH_LONG).show();
                    }
                });
                setActivityBackgroundColor(color);

            /*String color = data.getStringExtra("color");
            setActivityBackgroundColor(Color.parseColor(color));*/
            }
        }
        catch (NullPointerException e){
            color=Color.WHITE;
        }
    }

    private void setActivityBackgroundColor(int color) {
        final LinearLayout layout = (LinearLayout) findViewById(R.id.exertion_color_container);
        final Animation anim = new AlphaAnimation((float) 0.5, 0);
        layout.setVisibility(View.VISIBLE);
        ColorDrawable c = new ColorDrawable(color);
        int duration = 500;
        c.setAlpha(95);
        layout.setBackground(c);
        anim.setDuration(duration);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(20);
        layout.setAnimation(anim);
        layout.postDelayed(new Runnable() {
            @Override
            public void run() {
                layout.setVisibility(View.GONE);
            }
        }, duration);
        //layout.setBackground(null);
    }

    private String decodeColor(int color) {
        String c = "";
        if (color == Color.WHITE)
            c = "";
        else if (color == Color.RED)
            c = "high";
        else if (color == Color.YELLOW)
            c = "medium";
        else if (color == Color.GREEN)
            c = "low";
        return c;
    }

}
