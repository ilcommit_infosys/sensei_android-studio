/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.facebook.login.LoginManager;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.infy.dataSending.SendData;

import org.almende.motivator.ui.ChallengeOverviewActivity;
import org.focuser.sendmelogs.LogCollector;
import org.hva.createit.scl.data.AffectData;
import org.hva.createit.scl.data.RunData;
import org.hva.createit.scl.dataaccess.AffectDAO;
import org.hva.createit.scl.dataaccess.RunDataDAO;
import org.hva.createit.sensei.ui.AboutActivity;
import org.hva.createit.sensei.ui.QuestionnaireInjuriesActivity;
import org.hva.createit.sensei.ui.QuestionnairePersonalityActivity;
import org.hva.createit.sensei.ui.UserProfileCarouselActivity;
import org.hva.createit.sensei.ui.Util;
import org.hva.createit.sensei.ui.sweetblue.SelectHeartRateActivity;
import org.vu.run2gether.Run2getherService;

import me.grantland.widget.AutofitTextView;
import uni.ut.behaviouralmotivation.MessageSelector;
/**
 * @author Shruti Bansal <shruti_bansal01@Infosys.com>, Nilanjan Nath <nilanajan_nath@Infosys.com>
 * @description MainActivity encompassing all features of the app
 */
public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, android.content.DialogInterface.OnClickListener {

    public static final int DIALOG_SEND_LOG = 345350;
    protected static final int DIALOG_PROGRESS_COLLECTING_LOG = 3255;
    protected static final int DIALOG_FAILED_TO_COLLECT_LOGS = 3535122;
    private static final int DIALOG_REPORT_FORCE_CLOSE = 3535788;
    private AffectData aftdat;
    private AffectDAO ad;
    private RunDataDAO rdd;
    private int runState = 0;
    private DrawerLayout drawer;
    private boolean mSlideState = false;
    private long timeStamp;
    private LogCollector mLogCollector;
    private GoogleApiClient mGoogleApiClient;
    private NavigationView mNavigationView;
    private SharedPreferences prefs;
    private AutofitTextView tv;
    private GoogleApiClient client;
private boolean welcome = true;

    /**
     * @description Initiate all screen widgets
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        prefs = this.getSharedPreferences(getString(R.string.pref_user_settings), Context.MODE_PRIVATE);
        welcome = prefs.getBoolean(getString(R.string.pref_welcome), welcome);
//        welcome = true;//debug
        if(welcome) {
            welcome = false;
            SharedPreferences prefs = this.getSharedPreferences(getString(R.string.pref_user_settings),
                    Context.MODE_PRIVATE);
            prefs.edit().putBoolean(getString(R.string.pref_welcome), welcome).apply();

            startActivity(new Intent(this, UserProfileCarouselActivity.class));
        }


        setContentView(R.layout.activity_main);
        addMapFragment();
        ad = new AffectDAO(this);
        rdd = new RunDataDAO(this);
        final Button newRunButton = (Button) findViewById(R.id.startRun);
        newRunButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                timeStamp = System.currentTimeMillis();
                {
                    //aftdat.setTimestamp(timeStamp);
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            if (aftdat != null) {
                                aftdat.setTimestamp(timeStamp);
                                ad.store(aftdat);
                            }
                            rdd.store(new RunData(0,
                                    RunData.RUNDATA_TYPE_RUNSTART, timeStamp));
                        }
                    });

                }

                startActivity(new Intent(MainActivity.this,
                        RunActivity.class));
            }
        });
        /**
         * Start Infosys - logger code
         */
        mLogCollector = new LogCollector(this);
        CheckForceCloseTask task = new CheckForceCloseTask();
        task.execute();
        /**
         * End Infosys - logger code
         */
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(new Scope(Scopes.PLUS_LOGIN))
                .build();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerListener(new ActionBarDrawerToggle(this,
                drawer, R.drawable.hamburger, 0, 0) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                mSlideState = false;//is Closed
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mSlideState = true;//is Opened
            }
        });


        ImageButton hamburgerBtn = (ImageButton) findViewById(R.id.hamburger_btn);
        hamburgerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSlideState) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }

            }
        });
        prefs = this.getSharedPreferences("LoginData", Context.MODE_PRIVATE);
        mNavigationView = (NavigationView) findViewById(R.id.menu);
        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        switch (menuItem.getItemId()) {
                            case R.id.nav_item_Logout: {
                                prefs.edit().putString(getString(R.string.pref_user_email), null).apply();
                                prefs.edit().putString("Password", null).apply();

                                stopService(new Intent(getApplicationContext(), SendData.class));

                                /**Fb logout begins**/
                                LoginManager mLoginManager = LoginManager.getInstance();
                                mLoginManager.logOut();
                                /**fb logout ends**/


                                /**google logout begins**/
                                if (mGoogleApiClient.isConnected()) {
                                    Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);

                                    Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                                            .setResultCallback(new ResultCallback<Status>() {
                                                @Override
                                                public void onResult(Status status) {
                                                    mGoogleApiClient.disconnect();
                                                    mGoogleApiClient.connect();
                                                }
                                            });
                                }
                                /**google logout ends**/

                                Intent newIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(newIntent);
                                /**
                                 * End Infosys - logout code
                                 */
                                //drawer.closeDrawer(Gravity.LEFT);
                                return true;
                            }
                            case R.id.nav_item_CrashReports: {
                                /**
                                 * Start Infosys - logger code
                                 */
                                showDialog(DIALOG_SEND_LOG);
                                //drawer.closeDrawer(Gravity.LEFT);
                                return true;
                            }
                            case R.id.nav_item_Home: {
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                return true;
                            }
                            case R.id.nav_item_QuickRun: {
                                timeStamp = System.currentTimeMillis();
                                {
                                    //aftdat.setTimestamp(timeStamp);
                                    new Handler().post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (aftdat != null)
                                                ad.store(aftdat);
                                            rdd.store(new RunData(0,
                                                    RunData.RUNDATA_TYPE_RUNSTART, timeStamp));
                                        }
                                    });

                                }

                                startActivity(new Intent(getApplicationContext(), RunActivity.class));
                                return true;
                            }

                            case R.id.nav_item_BluetoothPairing: {
//                                startActivity(new Intent(getApplicationContext(), HeartrateSettingsActivity.class));
                                startActivity(new Intent(getApplicationContext(), SelectHeartRateActivity.class));

                                return true;
                            }
                            case R.id.nav_item_about: {
                                startActivity(new Intent(getApplicationContext(), AboutActivity.class));

                                return true;
                            }
                            /*case R.id.nav_item_Run2gether: {
                                startActivity(new Intent(getApplicationContext(), NearbyRunnersActivity.class));
                                return true;
                            }*/
                            case R.id.nav_item_UserProfile: {
                                startActivity(new Intent(getApplicationContext(), UserSettingsActivity.class));
                                return true;
                            }
                            case R.id.nav_item_challenges: {
                                ChallengeOverviewActivity.show(MainActivity.this);
                                return true;
                            }
			                case R.id.nav_item_history: {
                                startActivity(new Intent(getApplicationContext(), HistorySummaryActivity.class));
                                return true;
                            }
                            case R.id.nav_InjuriesQuestionnaire: {
                                startActivity(new Intent(getApplicationContext(), QuestionnaireInjuriesActivity.class));
                                return true;
                            }
                            case R.id.nav_PersonalityQuestionnaire: {
                                startActivity(new Intent(getApplicationContext(), QuestionnairePersonalityActivity.class));
                                return true;
                            }

                            case R.id.nav_Last5runs: {
                                startActivity(new Intent(getApplicationContext(), Last5RunsActivity.class));
                                return true;
                            }

                            case R.id.nav_backup_db: {
                                Util.backupDB(MainActivity.this);
                                return true;
                            }

                            default:
                                //return super.onOptionsItemSelected(item);
                        }
                        drawer.closeDrawers();
                        return true;
                    }
                });

        Intent serviceIntent = new Intent(this, SendData.class);
        startService(serviceIntent);

        tv = (AutofitTextView) findViewById(R.id.mqtextView);

        Handler handler = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                Bundle reply = msg.getData();
                String quote = reply.getString("MotivationalMsg", "");
                tv.setText(quote);
                Log.w("MSS_A", quote);
            }
        };

        Intent intent = new Intent(this, MessageSelector.class);
        intent.putExtra("messenger", new Messenger(handler));
        startService(intent);

      //  ComponentName c = startService(new Intent(this, Run2getherService.class));
    }


    /**
     * Start Infosys - logger code
     */

    /**
     * @description create Dialog box for sendLogs
     * @param id
     * @return Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case DIALOG_SEND_LOG:
            case DIALOG_REPORT_FORCE_CLOSE:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                String message;
                if (id == DIALOG_SEND_LOG)
                    message = "You will be redirected to a new mail draft to our developers, you can add your feedback at the end of this email. Do you wish to allow INTELiRun to collect your logs?";
                else
                    message = "It appears this app has been force-closed, do you want to report it to me?";
                builder.setTitle("Warning")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(message).setPositiveButton("Yes", (DialogInterface.OnClickListener) this)
                        .setNegativeButton("No", (DialogInterface.OnClickListener) this);
                dialog = builder.create();
                break;
            case DIALOG_PROGRESS_COLLECTING_LOG:
                ProgressDialog pd = new ProgressDialog(this);
                pd.setTitle("Progress");
                pd.setMessage("Collecting logs...");
                pd.setIndeterminate(true);
                dialog = pd;
                break;
            case DIALOG_FAILED_TO_COLLECT_LOGS:
                builder = new AlertDialog.Builder(this);
                builder.setTitle("Error").setMessage("Failed to collect logs.")
                        .setNegativeButton("OK", null);
                dialog = builder.create();
        }
        return dialog;
    }

    /**
     * @description onClickListener for crash report dialog
     * @param dialog
     * @param which
     */
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... params) {
                        return mLogCollector.collect();
                    }

                    @Override
                    protected void onPreExecute() {
                        showDialog(DIALOG_PROGRESS_COLLECTING_LOG);
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        dismissDialog(DIALOG_PROGRESS_COLLECTING_LOG);
                        if (result) {

                            mLogCollector.sendLog("commit.intelirun@gmail.com;shruti_bansal01@Infosys.com;j.h.f.van.der.bie@hva.nl;j.m.dallinga@hva.nl",
                                    "INTELiRun logs", "App logs start here....");

                        }
                        else
                            showDialog(DIALOG_FAILED_TO_COLLECT_LOGS);
                    }

                }.execute();
        }
        dialog.dismiss();
    }

    /**
     * End Infosys - logger code
     */

    /**
     * @description setter for selected AffectData object i.e. emotion before starting a run
     * @param aftdat
     */
    public void setAffectData(AffectData aftdat) {
        this.aftdat = aftdat;
    }

    /**
     * @description dynamically add Map fragment to display current location
     */
    private void addMapFragment() {

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment mapFragment = MapFragment.newInstance();
        transaction.add(mapFragment, "map");
    }

    /**
     * Start Infosys - stopping service code
     */

    /**
     * @description onDestroy method for activity
     */
    @Override
    protected void onDestroy() {
        stopService(new Intent(this, SendData.class));
        stopService(new Intent(this, Run2getherService.class));

        super.onDestroy();
        Log.d("MainActivity", "Stop Service CALLED");
    }

    /**
     * End Infosys - stopping service code
     */

    /**
     * @description OnStart for GoogleClient
     */
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }


    /**
     * @description OnDestroy for GoogleClient
     */
    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    /**
     * @description OnBackPressed for GoogleClient
     */
    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        finish();
    }


    /**
     * @description OnConnected for GoogleClient
     */
    @Override
    public void onConnected(Bundle bundle) {

    }


    /**
     * @description OnConnectionSuspended for GoogleClient
     */
    @Override
    public void onConnectionSuspended(int i) {

    }


    /**
     * @description OnConnectionFailed for GoogleClient
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * Start Infosys - logger code
     */

    /**
     * @description Check if app was force closed to initiate crash report sending
     */
    class CheckForceCloseTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            return mLogCollector.hasForceCloseHappened();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                showDialog(DIALOG_REPORT_FORCE_CLOSE);
            }
        }
    }

/**
 * End Infosys - google code
 */

}