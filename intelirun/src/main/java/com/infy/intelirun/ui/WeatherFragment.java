/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.infy.weather.Location;
import com.infy.weather.Weather;
import com.infy.weather.WeatherHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Shruti Bansal <shruti_bansal01@Infosys.com>
 * @description Fragment to capture and render current weather information on MainActivity
 */
public class WeatherFragment extends Fragment {


    //private TextView temp_view, sunny_view, wind_view, humidity_view, min_temp_view, max_temp_view;
    private TextView temp_view, sunny_view;
    private ImageView weather_image_view;
    private long temp, rain_percentage, min_temp, max_temp;
    private String sunny;
    private double wind_speed;
    private Bitmap image;
    private View v;
    private View weatherStatus, weatherForm;
    private String data;
    //private Bitmap img;
    private Bitmap img;

    /**
     * @description Initialises fragment widgets
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return Fragment View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_weather, container,
                false);

        weatherForm = (View) v.findViewById(R.id.weather_form);
        weatherStatus = (View) v.findViewById(R.id.weather_status);
        weather_image_view = (ImageView) v.findViewById(R.id.weather_image_view);
        getCurrentWeather();

        return v;
    }

    /**
     * @description get current location coordinates and initiate weather capturing AsyncTask
     */
    private void getCurrentWeather() {
        try {
            Location loc = new Location(getActivity().getApplicationContext());
            String city = loc.getContextValues();
            CurrentWeather task = new CurrentWeather();
            task.execute(new String[]{city, "en"});
        } catch (Exception e) {
            Log.e("WeatherFragment", "Weather cannot be retrieved");
        }
    }

    /**
     * @description getter for captured weather data
     * @return data string
     */
    public String getData() {
        return data;
    }

    /**
     * @description Shows the progress UI and hides the registration form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy
        // animations. If available, use these APIs to fade-in the progress
        // spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);
            weatherForm.setVisibility(View.VISIBLE);
            weatherForm.animate().setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            weatherForm
                                    .setVisibility(show ? View.GONE
                                            : View.VISIBLE);
                        }
                    });
            weatherStatus.setVisibility(View.VISIBLE);
            weatherStatus.animate().setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            weatherStatus
                                    .setVisibility(show ? View.VISIBLE
                                            : View.GONE);
                        }
                    });


        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant
            // UI components.
           // weatherStatus.setVisibility(show ? View.VISIBLE: View.GONE);
            weatherForm
                    .setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * @description AsyncTask to initiate weather capturing in background and then populate corresponding values in various data holders in the fragement
     */
    public class CurrentWeather extends AsyncTask<String, Void, JSONObject> {
        JSONObject jobj;

        protected void onPreExecute() {
            showProgress(true);


        }

        @Override
        protected JSONObject doInBackground(String... params) {
          //  showProgress(true);
            try {
                WeatherHttpClient client = new WeatherHttpClient();
                data = (client.getWeatherData(params[0], params[1]));
                //showProgress(true);
                jobj = new JSONObject(data);

                // retrieve the icon
                JSONArray jArr = jobj.getJSONArray("weather");
                JSONObject JSONWeather = jArr.getJSONObject(0);
                Log.i("icon", getString("icon", JSONWeather));
                String img_name = getString("icon", JSONWeather) + ".png";


              // img = client.getImage(img_name);
                img=client.getImage(img_name);
                Log.i("print", jobj.toString(2));


            } catch (Exception e) {
                Log.e("WeatherFragment", e.toString());
            }
            return jobj;

        }

        @Override
        protected void onPostExecute(JSONObject jobj) {
            super.onPostExecute(jobj);
            try {
                //showProgress(false);
                JSONObject mainObj = getObject("main", jobj);
                temp = Math.round((getFloat("temp", mainObj) - 275.15));
                min_temp = Math.round(getFloat("temp_min", mainObj) - 275.15);
                max_temp = Math.round(getFloat("temp_max", mainObj) - 275.15);
                rain_percentage = Math.round(getFloat("humidity", mainObj));

                //weather
                JSONArray jArr = jobj.getJSONArray("weather");
                JSONObject JSONWeather = jArr.getJSONObject(0);
                sunny = getString("description", JSONWeather);

                // Wind
                JSONObject wObj = getObject("wind", jobj);
                wind_speed = 3.6 * getInt("speed", wObj);


                showProgress(false);

                temp_view = (TextView) v.findViewById(R.id.temp_view);
                temp_view.setText(temp + "");


                //icon
               if (true) {
                    //Bitmap image = BitmapFactory.decodeByteArray(img, 0, img.length);
                    //weather_image_view.setImageBitmap(image);

                    /*InputStream is = new ByteArrayInputStream(img);
                    Bitmap b = BitmapFactory.decodeStream(new FlushedInputStream(is));
                    weather_image_view.setImageBitmap(b);*/

                    /*InputStream is = new ByteArrayInputStream(img);
                    Bitmap b = BitmapFactory.decodeStream(new BufferedInputStream(is));*/


                    weather_image_view.setImageBitmap(img);
                }
                sunny_view = (TextView) v.findViewById(R.id.sunny_view);
                //sunny=sunny.toUpperCase();
                sunny_view.setText(sunny);

              /*  wind_view = (TextView) v.findViewById(R.id.wind_view);
                wind_view.setText(wind_speed + "");

                humidity_view = (TextView) v.findViewById(R.id.humidity_view);
                humidity_view.setText(rain_percentage + "");*/

                /*min_temp_view = (TextView) v.findViewById(R.id.min_temp_view);
                min_temp_view.setText(min_temp + "");

                max_temp_view = (TextView) v.findViewById(R.id.max_temp_view);
                max_temp_view.setText(max_temp + "");*/

            } catch (JSONException e) {
                Log.e("WeatherFragment", e.getMessage());
            }
            catch (Exception e) {
                Log.e("WeatherFragment", e.getMessage());
            }
            //delegate.processFinish(jobj);
        }

        private JSONObject getObject(String tagName, JSONObject jObj) throws JSONException {
            JSONObject subObj = jObj.getJSONObject(tagName);
            return subObj;
        }

        private float getFloat(String tagName, JSONObject jObj) throws JSONException {
            return (float) jObj.getDouble(tagName);
        }

        private int getInt(String tagName, JSONObject jObj) throws JSONException {
            return jObj.getInt(tagName);
        }

        private String getString(String tagName, JSONObject jObj) throws JSONException {
            return jObj.getString(tagName);
        }

    }


    /// Loads a Bitmap from a byte array



}
