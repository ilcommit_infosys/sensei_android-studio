/**
 * Copyright &copy; 2015 Infosys Limited, Bangalore, India. All rights reserved.
 * Except for any open source software components embedded in this
 * Infosys proprietary software program ("Program"), this Program is protected
 * by copyright laws, international treaties and other pending or existing
 * intellectual property rights in India, the United States and other countries.
 * Except as expressly permitted, any unauthorized reproduction, storage,
 * transmission in any form or by any means (including without limitation
 * electronic, mechanical, printing, photocopying, recording or otherwise),
 * or any distribution of this Program, or any portion of it,
 * may result in severe civil and criminal penalties, and
 * will be prosecuted to the maximum extent possible under the law
 **/
package com.infy.intelirun.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.hva.createit.scl.data.RunOverviewData;
import org.hva.createit.scl.dataaccess.RunStatisticsDAO;

import java.util.List;

/**
 * @author Nilanjan Nath <nilanajan_nath@Infosys.com>
 * @description Fragment for displaying list of last 5 runs' timestamp inside a ListAdapter
 */
public class RunOverviewListFragment extends Fragment implements View.OnClickListener {
    ListView runList;
    private final String TAG = "RunOverviewListFragment" ;
    private List<RunOverviewData> runs;

    /**
     * @description Initiate activity creation and widgets
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle args = this.getArguments();
        int items = 5;
         if (args != null) {
            items = args.getInt("items", items);
        }
        RunStatisticsDAO rsd = new RunStatisticsDAO(getActivity());
        runs = rsd.getLastRunOverviews(items);
        runList = (ListView) getView().findViewById(R.id.previousRunsList);
        String[] runArray = new String[runs.size()];

        for (int i = 0; i < runs.size(); i++) {
            runArray[i] = runs.get(i).getStartDateAsString();
        }

        Log.i(TAG, "runs size :" + runs.size());
        RunOverviewListAdapter itemsAdapter = new RunOverviewListAdapter(getActivity().getApplicationContext(), runs, this);
        Log.i(TAG, "runs size :" + runArray.length);
        runList.setAdapter(itemsAdapter);

    }

    /**
     * @description OnClickListener for RunOverview List
     * @param view
     */
    @Override
    public void onClick(View view) {

    }

    /**
     * @description onCreateView for RunOverviewListFragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return Fragment View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sensei_previous_runs, container, false);
    }


}

