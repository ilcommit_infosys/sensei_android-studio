package org.hva.createit.sensei.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.infy.intelirun.ui.R;

/**
 * Created by biejh on 03-07-16.
 */
public class QuestionnaireCarouselActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Button save = (Button) findViewById(R.id.save_button);
        save.setText(R.string.next);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HeartRateCarouselActivity.class);
                startActivity(i);

            }
        });
        Button injuries = (Button) findViewById(R.id.button_questionnaire_injuries);
        injuries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), QuestionnaireInjuriesActivity.class));
            }
        });

        Button per = (Button) findViewById(R.id.button_questionnaire_personality);
        per.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), QuestionnairePersonalityActivity.class));
            }
        });


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_questionnaire_carousel;
    }
}
