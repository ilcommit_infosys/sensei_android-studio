package org.hva.createit.sensei.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.infy.intelirun.ui.R;
import com.infy.intelirun.ui.UserSettingsActivity;

/**
 * Created by biejh on 03-07-16.
 */
public class UserProfileCarouselActivity extends UserSettingsActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout welcome = (LinearLayout) findViewById(R.id.welcome_text_layout);
        welcome.setVisibility(View.VISIBLE);


        TextView header = (TextView) findViewById(R.id.activity_header);
        header.setText(R.string.label_welcome_header);

        Button save = (Button) findViewById(R.id.save_button);
        save.setText(R.string.next);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
                Intent i = new Intent(getApplicationContext(), QuestionnaireCarouselActivity.class);
                startActivity(i);
            }
        });

        ImageButton backBtn = (ImageButton) findViewById(R.id.userprofile_back_btn);
        backBtn.setVisibility(View.GONE);

    }
}
