/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.sensei.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;

import com.infy.intelirun.ui.LoginActivity;
import com.infy.intelirun.ui.R;

public class SplashActvity extends Activity implements Eula.OnEulaAgreedTo {

	private boolean mAlreadyAgreedToEula = false;
	private SplashActvity sp;
	private static final String PREFERENCE_EULA_ACCEPTED = "eula.accepted";
	private static final String PREFERENCES_EULA = "eula";
	private  SharedPreferences preferences;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		preferences = this.getSharedPreferences(PREFERENCES_EULA, Activity.MODE_PRIVATE);

		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		sp = this;
		startSplashThread();

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			// active = false;
		}
		return true;
	}

	/** {@inheritDoc} */
	@Override
	public void onEulaAgreedTo() {

		Intent newIntent = new Intent(SplashActvity.this, LoginActivity.class);
		// newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

		newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(newIntent);

	}

	private void startSplashThread() {

		SharedPreferences prefs = this.getSharedPreferences("LoginData",
				Context.MODE_PRIVATE);
		String storedUname = prefs.getString("Uname", null);
		/*if (storedUname != null) {
			mAlreadyAgreedToEula = true;
		} */
		mAlreadyAgreedToEula=preferences.getBoolean(PREFERENCE_EULA_ACCEPTED, false);
		if (mAlreadyAgreedToEula) {

			Intent newIntent = new Intent(SplashActvity.this,
			// MainActivity.class);
					LoginActivity.class);
			newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			// newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(newIntent);

		}
		else {
			mAlreadyAgreedToEula = Eula.show(sp);
		}


	}

	@Override
	public void onResume() {
		super.onResume();
	}

}
