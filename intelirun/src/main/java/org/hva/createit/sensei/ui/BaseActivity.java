/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.sensei.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toolbar;

import com.step.frequency.SFService;

public abstract class BaseActivity extends Activity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        /*toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);

			// Set an OnMenuItemClickListener to handle menu item clicks
			// toolbar.setOnMenuItemClickListener(new
			// Toolbar.OnMenuItemClickListener() {
			// @Override
			// public boolean onMenuItemClick(MenuItem item) {
			// // Handle the menu item
			// return true;
			// }
			// });

			// Inflate a menu to be displayed in the toolbar
			// toolbar.inflateMenu(R.menu.menu);

		}*/

    }

    protected abstract int getLayoutResource();

	/*//protected void setActionBarIcon(int iconRes) {
		toolbar.setNavigationIcon(iconRes);
	}*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
		/*switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			return true;
		}*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(getBaseContext(), SFService.class));
        super.onDestroy();
    }
}
