/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.sensei.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.infy.intelirun.ui.R;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

public class DurationUnit extends RelativeLayout {
    public TextView info;
    protected long delay = 1000;
    protected long previousUpdate = 0;
    protected DecimalFormat df = new DecimalFormat("##0.##");
    protected int mode = 0;
    protected int size = 0;
    Timer timer;
    Counter counter;
    Activity activity;
    String TAG = "DurationUnit";
    private Context context;

    public DurationUnit(Context context, AttributeSet attrs) {
        super(context, attrs);
        // , defStyleAttr, defStyleRes);

        this.context = context;
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.duration_unit, this, true);

        TypedArray a = context
                .obtainStyledAttributes(attrs, R.styleable.intelirun);

        size = a.getInteger(R.styleable.intelirun_unit_size, size);


        info = (TextView) findViewById(R.id.base_info);
        delay = getResources().getInteger(R.integer.pref_delay_millis);

        mode = a.getInteger(R.styleable.intelirun_unit_mode, mode);
    }

    public DurationUnit(Context context, AttributeSet attrs, int layout) {
        super(context, attrs);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // mInflater.inflate(R.layout.sensei_location_unit, this, true);
        mInflater.inflate(layout, this, true);

        TypedArray a = context
                .obtainStyledAttributes(attrs, R.styleable.intelirun);

        size = a.getInteger(R.styleable.intelirun_unit_size, size);

        delay = getResources().getInteger(R.integer.pref_delay_millis);

        mode = a.getInteger(R.styleable.intelirun_unit_mode, mode);

        info.setText("00:00");
    }

    public void startTimer(Activity activity) {
        this.activity = activity;

        // update database with starttime

        // starttimer
        this.counter = new Counter();

        this.timer = new Timer();
        this.timer.scheduleAtFixedRate(counter, delay, delay);

    }

    public void stopTimer() {
        // update datebase with stoptime
        // update database with duration time in seconds
        // stoptimer
        timer.cancel();
    }

    public void updateTime(long time) {
        long timeSeconds = time / 1000 % 60;
        long timeMinutes = time / (60 * 1000) % 60;
        long timeHours = time / (60 * 60 * 1000) % 24;
        if (timeHours == 0) {
            setInfo((timeMinutes < 10 ? "0" + timeMinutes : timeMinutes) + ":"
                    + (timeSeconds < 10 ? "0" + timeSeconds : timeSeconds));
        } else {
            setInfo((timeHours < 10 ? "0" + timeHours : timeHours) + ":"
                    + (timeMinutes < 10 ? "0" + timeMinutes : timeMinutes)
                    + ":"
                    + (timeSeconds < 10 ? "0" + timeSeconds : timeSeconds));
        }
    }

    public void setInfo(final String info) {
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DurationUnit.this.info.setText(info);
            }
        });
    }

    public void updateInfo(String info) {
        long currentUpdate = System.currentTimeMillis();
        if (currentUpdate - previousUpdate > delay) {
            setInfo(info);
            currentUpdate = previousUpdate;
        }
    }

    public void setAutoUpdate(boolean on) {
        if (on) {
            if (!EventBus.getDefault().isRegistered(this)) {
                Log.d(TAG, "Registering EventBus");
                EventBus.getDefault().register(this);
            } else {
                Log.d(TAG, "EventBus service already started");
            }
        } else {
            EventBus.getDefault().unregister(this);
        }
    }

    private class Counter extends TimerTask {
        private Date startDateTime = new Date();

        @Override
        public void run() {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateTime((new Date()).getTime() - startDateTime.getTime());
                }
            });
        }

    }

}
