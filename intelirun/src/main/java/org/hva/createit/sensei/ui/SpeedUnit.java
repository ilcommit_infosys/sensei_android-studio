/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.sensei.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.infy.intelirun.ui.R;

import org.hva.createit.scl.data.LocationData;

public class SpeedUnit extends BasicUnit {

    public SpeedUnit(Context context, AttributeSet attrs) {
        super(context, attrs);

        setHeader(getResources().getString(R.string.speed_header));
        setUnit(getResources().getString(R.string.speed_unit));
       // setIcon(getResources().getDrawable(R.drawable.speed));
        info.setText("0");
    }

    public void onEvent(LocationData event) {
        updateInfo("" + Math.round(event.getSpeed().getValueAsFloat() * 3.6));
    }

    /*
     * speed in m/s
     */
    public void setSpeed(float speed) {
        updateInfo("" + Math.round(speed * 3.6));
    }
}
