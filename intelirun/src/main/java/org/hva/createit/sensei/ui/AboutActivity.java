package org.hva.createit.sensei.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.infy.intelirun.ui.BuildConfig;
import com.infy.intelirun.ui.R;

public class AboutActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TextView app_version = (TextView) findViewById(R.id.app_version);
		app_version.setText("Version "+ BuildConfig.VERSION_NAME);
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_about;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
