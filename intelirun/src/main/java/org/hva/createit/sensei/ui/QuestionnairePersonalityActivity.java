/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.sensei.ui;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.infy.dataSending.Constants;
import com.infy.intelirun.ui.R;

import org.hva.createit.scl.data.QuestionnaireData;
import org.hva.createit.scl.dataaccess.QuestionnaireDAO;
import org.json.JSONException;
import org.json.JSONObject;


public class QuestionnairePersonalityActivity extends BaseActivity {
	TextView heading2, remark1, remark2;
	LinearLayout personality_questionnaire_layout,state_of_change_questionnaire_layout,
	injuries_layout_02, injuries_layout_03,
			injuries_layout_04, injuries_layout_05, injuries_layout_06;
	String answers = "" ;
	SharedPreferences prefs ;
	JSONObject jsonObject = new JSONObject();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ImageButton backBtn = (ImageButton) findViewById(R.id.personalityquestionnaire_back_btn);
		backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				Intent i = new Intent(getApplicationContext(), MainActivity.class);
//				startActivity(i);
				onBackPressed();
			}
		});

		answers = "" ;
		prefs = QuestionnairePersonalityActivity.this.getSharedPreferences(getString(R.string.pref_questionnaire), Context.MODE_PRIVATE);

		state_of_change_questionnaire_layout = (LinearLayout) findViewById(R.id.sensei_state_of_change_questionnaire_question01);
		personality_questionnaire_layout = (LinearLayout) findViewById(R.id.sensei_personality_questionnaire_question02);

		RadioGroup state_of_change_01 = (RadioGroup) state_of_change_questionnaire_layout.findViewById(R.id.state_of_change_01_answer);
		state_of_change_01
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						switch (checkedId) {
						case R.id.state_of_change_01_answer_01:
							storeAnswer(0, getString(R.string.state_of_change_01_question), getString(R.string.state_of_change_01_answer_01), 1);
							break;
						case R.id.state_of_change_01_answer_02:
							storeAnswer(0, getString(R.string.state_of_change_01_question), getString(R.string.state_of_change_01_answer_02), 2);
							break;
						case R.id.state_of_change_01_answer_03:
							storeAnswer(0, getString(R.string.state_of_change_01_question), getString(R.string.state_of_change_01_answer_03), 3);
							break;
						case R.id.state_of_change_01_answer_04:
							storeAnswer(0, getString(R.string.state_of_change_01_question), getString(R.string.state_of_change_01_answer_04), 4);
							break;
						case R.id.state_of_change_01_answer_05:
							storeAnswer(0, getString(R.string.state_of_change_01_question), getString(R.string.state_of_change_01_answer_05), 5);
							break;
						}
						personality_questionnaire_layout.setVisibility(View.VISIBLE);
						
					}
				});

		RadioGroup personality_01_01_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_01_answer);
		personality_01_01_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(1, getString(R.string.personality_01_01_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(1, getString(R.string.personality_01_01_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(1, getString(R.string.personality_01_01_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(1, getString(R.string.personality_01_01_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(1, getString(R.string.personality_01_01_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
			}
		});
		
		
		RadioGroup personality_01_02_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_02_answer);
		personality_01_02_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(2, getString(R.string.personality_01_02_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(2, getString(R.string.personality_01_02_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(2, getString(R.string.personality_01_02_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(2, getString(R.string.personality_01_02_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(2, getString(R.string.personality_01_02_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
			}
		});
		
		RadioGroup personality_01_03_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_03_answer);
		personality_01_03_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(3, getString(R.string.personality_01_03_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(3, getString(R.string.personality_01_03_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(3, getString(R.string.personality_01_03_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(3, getString(R.string.personality_01_03_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(3, getString(R.string.personality_01_03_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
			}
		});
		
		RadioGroup personality_01_04_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_04_answer);
		personality_01_04_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(4, getString(R.string.personality_01_04_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(4, getString(R.string.personality_01_04_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(4, getString(R.string.personality_01_04_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(4, getString(R.string.personality_01_04_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(4, getString(R.string.personality_01_04_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
			}
		});
		
		RadioGroup personality_01_05_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_05_answer);
		personality_01_05_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(5, getString(R.string.personality_01_05_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(5, getString(R.string.personality_01_05_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(5, getString(R.string.personality_01_05_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(5, getString(R.string.personality_01_05_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(5, getString(R.string.personality_01_05_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
			}
		});
		
		RadioGroup personality_01_06_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_06_answer);
		personality_01_06_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(6, getString(R.string.personality_01_06_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(6, getString(R.string.personality_01_06_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(6, getString(R.string.personality_01_06_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(6, getString(R.string.personality_01_06_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(6, getString(R.string.personality_01_06_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
			}
		});
		
		RadioGroup personality_01_07_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_07_answer);
		personality_01_07_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(7, getString(R.string.personality_01_07_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(7, getString(R.string.personality_01_07_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(7, getString(R.string.personality_01_07_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(7, getString(R.string.personality_01_07_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(7, getString(R.string.personality_01_07_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
			}
		});
		
		RadioGroup personality_01_08_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_08_answer);
		personality_01_08_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(8, getString(R.string.personality_01_08_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(8, getString(R.string.personality_01_08_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(8, getString(R.string.personality_01_08_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(8, getString(R.string.personality_01_08_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(8, getString(R.string.personality_01_08_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
			}
		});
		
		RadioGroup personality_01_09_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_09_answer);
		personality_01_09_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(9, getString(R.string.personality_01_09_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(9, getString(R.string.personality_01_09_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(9, getString(R.string.personality_01_09_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(9, getString(R.string.personality_01_09_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(9, getString(R.string.personality_01_09_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
			}
		});
		
		RadioGroup personality_01_10_question = (RadioGroup) personality_questionnaire_layout.findViewById(R.id.sensei_personality_questionnaire_01_10_answer);
		personality_01_10_question
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.personality_01_answer_01:
					storeAnswer(10, getString(R.string.personality_01_10_question), getString(R.string.personality_01_answer_01), 1);
					break;
				case R.id.personality_01_answer_02:
					storeAnswer(10, getString(R.string.personality_01_10_question), getString(R.string.personality_01_answer_02), 2);
					break;
				case R.id.personality_01_answer_03:
					storeAnswer(10, getString(R.string.personality_01_10_question), getString(R.string.personality_01_answer_03), 3);
					break;
				case R.id.personality_01_answer_04:
					storeAnswer(10, getString(R.string.personality_01_10_question), getString(R.string.personality_01_answer_04), 4);
					break;
				case R.id.personality_01_answer_05:
					storeAnswer(10, getString(R.string.personality_01_10_question), getString(R.string.personality_01_answer_05), 5);
					break;
				}
				end();
			}
		});
		
		
	}


	private void end() {
		Button submit_button = (Button) findViewById(R.id.submit_button);
		submit_button.setVisibility(View.VISIBLE);
		Log.d("QPA", "end");

		submit_button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				//submit results and go to next screen;
//				String questionnaire_result = "{questionnaire:'injuries',datetime:'"+System.currentTimeMillis()+"', questions:["+answers+"]}";
				QuestionnaireDAO qdao = new QuestionnaireDAO(QuestionnairePersonalityActivity.this);
				qdao.store(new QuestionnaireData(Constants.personality, answers, System.currentTimeMillis()));
				prefs.edit().putBoolean(getString(R.string.pref_questionnaire_personality), false).commit();
				finish();
//				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//				startActivity(intent);
//				Log.i("QPA", "Main activity started");
			}
		});

	}
	
	

	
	private void storeAnswer(int question_nr, String question, String answer, int option) {
		//"{questionnaire:'',datetime:'', questions:[{question_number:'', question:'"+question+"', answer:'"+answer+"', answer_option:"+option+", datetime:"+  System.currentTimeMillis()+"}]";
		if(answers.compareTo("") != 0)
		{
			answers +=",";
		}
		answers += "{question_number:'"+ question_nr+"', question:'"+question+"', answer:'"+answer+"', answer_option:"+option+", datetime:"+  System.currentTimeMillis()+"}";
		try
		{
			Log.d("QPA", "store answer");
			String jsonAns = prefs.getString("QuestionnairePersonalityAns", "");
			//JSONObject jsonObject = new JSONObject(jsonAns);
			//Log.d("QPA", "store answer");
			jsonObject.put(String.valueOf(question_nr), String.valueOf(option));
			String ansJson = jsonObject.toString();
			prefs.edit().putString("QuestionnairePersonalityAns", ansJson).commit();
			//go to next screen
			Log.d("QPA", ansJson);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	protected int getLayoutResource() {
		return R.layout.sensei_personality_questionnaire;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

