/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.sensei.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.infy.intelirun.ui.R;

import org.hva.createit.scl.data.HeartRateData;

public class HeartrateUnit extends BasicUnit {

    public HeartrateUnit(Context context, AttributeSet attrs) {
        super(context, attrs);

        setHeader(getResources().getString(R.string.heart_rate_header));
        setUnit(getResources().getString(R.string.heart_rate_unit_bpm));
        //setIcon(getResources().getDrawable(R.drawable.heartbeat));


        info.setText("0");
    }

    public void onEvent(HeartRateData event) {
        updateInfo("" + event.getHeartRate());
    }

    public void setHeartRate(int heartrate) {
        updateInfo("" + heartrate);
    }
}
