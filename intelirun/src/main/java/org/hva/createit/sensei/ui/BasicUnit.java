/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.sensei.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.infy.intelirun.ui.R;

import java.text.DecimalFormat;

import de.greenrobot.event.EventBus;

public class BasicUnit extends RelativeLayout {
    private final String TAG = "BasicUnit";
    public TextView header;
    public TextView info;
    public TextView unit;
    public ImageView icon;
    protected long delay = 1000;
    protected long previousUpdate = 0;
    protected DecimalFormat df = new DecimalFormat("##0.##");
    /*
     * unit-mode attribute: 0 default, active attribute, used to display current
     * run parameters 1 inactive attribute, used to display old parameters 2
     * interactive attribute, used to set run parameters
     */
    protected int mode = 0;
    /*
     * unit-size attribute: 0 default, Full display of unit 1 full display of
     * unit minus header 2 small text, minus header
     */
    protected int size = 0;
    private Context context;

    public BasicUnit(Context context, AttributeSet attrs) {
        // , int defStyleAttr,
        // int defStyleRes) {
        super(context, attrs);
        // , defStyleAttr, defStyleRes);

        this.context = context;
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.base_unit, this, true);

        TypedArray a = context
                .obtainStyledAttributes(attrs, R.styleable.intelirun);

        header = (TextView) findViewById(R.id.base_header);
        size = a.getInteger(R.styleable.intelirun_unit_size, size);
        if (size > 0) {
            header.setVisibility(View.GONE);
        }

        info = (TextView) findViewById(R.id.base_info);
        unit = (TextView) findViewById(R.id.base_unit);
        if (size == 2) {
            // info.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            // unit.setTextSize(R.dimen.textsize_small);
        }

      //  icon = (ImageView) findViewById(R.id.base_icon);

        delay = getResources().getInteger(R.integer.pref_delay_millis);

        mode = a.getInteger(R.styleable.intelirun_unit_mode, mode);
    }

    public BasicUnit(Context context, AttributeSet attrs, int layout) {
        super(context, attrs);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // mInflater.inflate(R.layout.sensei_location_unit, this, true);
        mInflater.inflate(layout, this, true);

        TypedArray a = context
                .obtainStyledAttributes(attrs, R.styleable.intelirun);

        header = (TextView) findViewById(R.id.base_header);
        size = a.getInteger(R.styleable.intelirun_unit_size, size);
        if (size > 0) {
            header.setVisibility(View.GONE);
        }
        delay = getResources().getInteger(R.integer.pref_delay_millis);

        mode = a.getInteger(R.styleable.intelirun_unit_mode, mode);
    }

    public void setHeader(String header) {
        this.header.setText(header);
    }

    public void setUnit(String unit) {
        this.unit.setText(unit);
    }

    public void setInfo(final String info) {
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                BasicUnit.this.info.setText(info);
            }
        });
    }

    public void updateInfo(String info) {
        long currentUpdate = System.currentTimeMillis();
        if (currentUpdate - previousUpdate > delay) {
            setInfo(info);
            currentUpdate = previousUpdate;
        }
    }

    /*public void setIcon(Drawable icon) {
        this.icon.setImageDrawable(icon);
    }*/

    public void setAutoUpdate(boolean on) {
        if (on) {
            if (!EventBus.getDefault().isRegistered(this)) {
                Log.d(TAG, "Registering EventBus");
                EventBus.getDefault().register(this);
            } else {
                Log.d(TAG, "EventBus service allready started");
            }
        } else {
            EventBus.getDefault().unregister(this);
        }
    }

}
