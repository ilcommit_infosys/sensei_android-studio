/** Copyright © 2016 the Amsterdam University of Applied Sciences, Amsterdam, The Netherlands.
 *
 * All rights reserved.
 * Except for any open source software components embedded in this program,
 * no part of this publication may be reproduced, distributed,
 * or transmitted in any form or by any means, including photocopying, recording,
 * or other electronic or mechanical methods, without the prior written permission of the author.
 * For permission requests contact the author.
 *
 * @author Joey van der Bie - j.h.f.van.der.bie@hva.nl
 **/
package org.hva.createit.sensei.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;

import com.infy.intelirun.ui.R;

import org.hva.createit.scl.data.AffectData;

import java.util.List;

public class AffectUnit extends BasicUnit {

    int height = dpToPx(40);
    int width = dpToPx(40);;
    private ImageView to;
    private ImageView from;
    private ImageView arrow;

    public AffectUnit(Context context, AttributeSet attrs) {

        super(context, attrs, R.layout.affect_unit);
        to = (ImageView) findViewById(R.id.endAffect);
        from = (ImageView) findViewById(R.id.startAffect);
        arrow = (ImageView) findViewById(R.id.black_arrow);
        /*end = (AffectButton) findViewById(R.id.affectbuttonEnd);
		end.setPAD(0, 0, 0);

		start = (AffectButton) findViewById(R.id.affectbuttonStart);
		start.setPAD(0, 0, 0);*/
    }
    private int dpToPx(int dp)
    {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
    private void setAffectStart(AffectData affect) {
        //start.setPAD(affect.getPleasure(), affect.getDominance(),affect.getArousal());
        from.setImageBitmap(decodeAffect(affect));
        //start.takeScreenshot();
    }

    public void setAffectEnd(AffectData affect) {
        //end.setPAD(affect.getPleasure(), affect.getDominance(),affect.getArousal());
        to.setImageBitmap(decodeAffect(affect));
        //end.takeScreenshot();
    }

    /*
     * AffectList should have only 2 items, a start and an end, more than 2
     * items are ignored
     */
    public void setAffect(List<AffectData> affectDataList) {

//        System.out.println("******************************************" + affectDataList.get(0).getArousal());
//        AffectData after;
//        AffectData before = affectDataList.get(0);
//        if(affectDataList.size() == 1) {
//         after = new AffectData(0,System.currentTimeMillis(), 0,0,0);
//        }else{
//            after=affectDataList.get(1);
//        }
//
//        if(before.getArousal()!=0&&after.getArousal()!=0){
//            setAffectStart(before);
//            setAffectEnd(after);
//            /*from.setVisibility(VISIBLE);
//            to.setVisibility(VISIBLE);
//            arrow.setVisibility(VISIBLE);*/
//        }else  if(before.getArousal()==0&&after.getArousal()!=0){
//            //setAffectStart(before);
//            setAffectEnd(after);
//           /* from.setVisibility(GONE);
//            to.setVisibility(VISIBLE);
//            arrow.setVisibility(GONE);*/
//        }
//        else  if(before.getArousal()!=0&&after.getArousal()==0){
//            setAffectStart(before);
//            //setAffectEnd(after);
//            /*from.setVisibility(VISIBLE);
//            to.setVisibility(GONE);
//            arrow.setVisibility(GONE);*/
//        }



        if (affectDataList.size() > 1) {

            from.setVisibility(VISIBLE);
            if (affectDataList.get(0).getTimestamp() > affectDataList.get(1)
                    .getTimestamp()) {
                setAffectStart(affectDataList.get(1));
                setAffectEnd(affectDataList.get(0));
            } else {
                setAffectStart(affectDataList.get(0));
                setAffectEnd(affectDataList.get(1));
            }
            from.setVisibility(VISIBLE);
            to.setVisibility(VISIBLE);
            arrow.setVisibility(VISIBLE);

        } else if (affectDataList.size() == 1) {
            setAffectStart(affectDataList.get(0));
            from.setVisibility(VISIBLE);
            to.setVisibility(GONE);
            arrow.setVisibility(GONE);
        }
    }

    @Override
    public void setClickable(boolean clickable) {
        super.setClickable(clickable);
    }

    private Bitmap decodeAffect(AffectData aftdat) {
        Bitmap bMap = null, bMapScaled = null;
//PDA
        if (aftdat.getPleasure() == -1 && aftdat.getDominance() == 1 && aftdat.getArousal() == -1) {
            bMap = BitmapFactory.decodeResource(getResources(), R.drawable.depressed);
        } else if (aftdat.getPleasure() == -1 && aftdat.getDominance() == 1 && aftdat.getArousal() == 1) {
            bMap = BitmapFactory.decodeResource(getResources(), R.drawable.angry);
        } else if (aftdat.getPleasure() == -1 && aftdat.getDominance() == -1 && aftdat.getArousal() == 1) {
            bMap = BitmapFactory.decodeResource(getResources(), R.drawable.crying);
        } else if (aftdat.getPleasure() == -1 && aftdat.getDominance() == -1 && aftdat.getArousal() == -1) {
            bMap = BitmapFactory.decodeResource(getResources(), R.drawable.sad);
        } else if (aftdat.getPleasure() == 1 && aftdat.getDominance() == -1 && aftdat.getArousal() == -1) {
            bMap = BitmapFactory.decodeResource(getResources(), R.drawable.happy);
        } else if (aftdat.getPleasure() == 1 && aftdat.getDominance() == 1 && aftdat.getArousal() == -1) {
            bMap = BitmapFactory.decodeResource(getResources(), R.drawable.excited);
        } else if (aftdat.getPleasure() == 1 && aftdat.getDominance() == 1 && aftdat.getArousal() == 1) {
            bMap = BitmapFactory.decodeResource(getResources(), R.drawable.delighted);
        }

        if (bMap != null)
            bMapScaled = Bitmap.createScaledBitmap(bMap, height, width, true);
        return bMapScaled;

    }

}