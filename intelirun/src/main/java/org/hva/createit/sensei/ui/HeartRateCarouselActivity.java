package org.hva.createit.sensei.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.infy.intelirun.ui.MainActivity;
import com.infy.intelirun.ui.R;

import org.hva.createit.sensei.ui.sweetblue.SelectHeartRateActivity;

/**
 * Created by biejh on 03-07-16.
 */
public class HeartRateCarouselActivity extends SelectHeartRateActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ImageButton injuries = (ImageButton) findViewById(R.id.bluetooth_back_btn);
        injuries.setVisibility(View.GONE);


        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10, 10, 10, 10);

        Button finish = new Button(this);
        finish.setLayoutParams(lp);
        finish.setText(R.string.label_start_running);
        finish.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        finish.setTextColor(Color.WHITE);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = HeartRateCarouselActivity.this.getSharedPreferences(getString(R.string.pref_user_settings),
                        Context.MODE_PRIVATE);
                prefs.edit().putBoolean(getString(R.string.pref_welcome), false).apply();

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

            }
        });

        RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        relativeParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        relativeParams.setMargins(10, 10, 10, 10);


        RelativeLayout drawer = (RelativeLayout) findViewById(R.id.main_heartrate_layout);
        drawer.addView(finish, relativeParams);


         }

}
