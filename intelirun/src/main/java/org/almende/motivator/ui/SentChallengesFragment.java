package org.almende.motivator.ui;

/**
 * Created by AsterLaptop on 4/13/14.
 */

//import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.adapters.Entry;
import org.almende.motivator.adapters.EntryAdapter;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.service.EventListener;
import org.almende.motivator.service.MotivatorService;

import java.util.ArrayList;

public class SentChallengesFragment extends Fragment implements OnItemClickListener, EventListener {

	private static final String TAG = SentChallengesFragment.class.getCanonicalName();

	private ArrayList<Entry> items = new ArrayList<Entry>();
	private ListView listview = null;
	private Button btnCreate;

	private ChallengeDB _challengeDB;

	private ArrayList<Challenge> sentChallenges = new ArrayList<>();
//	private ArrayList<Challenge> receivedChallenges = new ArrayList<>();
	private EntryAdapter adapter;

	private MotivatorService _service;
	private ServiceConnection _connection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MotivatorService.MotivatorBinder binder = (MotivatorService.MotivatorBinder) service;
			_service = binder.getService();
			_service.registerEventListener(SentChallengesFragment.this);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {

		}
	};

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		//if we don't have an agreement yet on the terms of use, we redirect the user
//        if (!redirect) {
//            Intent intent = new Intent(this, TermsActivity.class);
//            finish();
//            startActivity(intent);
//        } else
		// todo: DE add again
//		if (Profile.getInstance(getActivity()).isFirstUse()) {
//			getActivity().getFragmentManager()
//					.beginTransaction()
//					.add(R.id.content_frame, new FirstUseFragment())
//					.commit();
//		}

		listview = (ListView) getActivity().findViewById(R.id.lvChallenges);
		adapter = new EntryAdapter(getActivity(), items);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(this);

//		btnCreate = (Button) getActivity().findViewById(R.id.btnCreate);
//		btnCreate.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				startActivity(new Intent(getActivity(), ChallengeCreateActivity.class));
//				getActivity().overridePendingTransition(R.anim.slide_in_right,
//						R.anim.slide_out_left);
//			}
//		});

		_challengeDB = ChallengeDB.getInstance(getActivity().getApplicationContext());

//		receivedChallenges = _challengeDB.getReceivedChallenges();
		sentChallenges = _challengeDB.getSentChallenges();

		Intent intent = new Intent(getActivity(), MotivatorService.class);
		getActivity().bindService(intent, _connection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onResume() {
		super.onResume();

		updateListView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().unbindService(_connection);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater
				.inflate(R.layout.list_challenges, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onEvent(Event event) {
		switch (event) {
			case CHALLENGE_UPDATE:
				updateListView();
				break;
		}
	}

	public void updateListView() {

		items.clear();
//		items.add(new ChallengeHeader("Challenges you sent"));

		if (!sentChallenges.isEmpty()) {
			items.addAll(sentChallenges);
//		} else {
//			items.add(new ChallengeHeader("No challenges sent"));
		}
//
//		items.add(new ChallengeHeader("Challenges you received"));

//		if (!receivedChallenges.isEmpty()) {
//			items.addAll(receivedChallenges);
//		} else {
//			items.add(new ChallengeHeader("No challenges received"));
//		}

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					adapter.notifyDataSetChanged();
				}
			});
		}
	}

	@Override
	public void onItemClick(AdapterView arg0, View arg1, int position, long arg3) {
		Challenge item = (Challenge) adapter.getItem(position);

		if (item.getStatus().equals(Challenge.COMPLETED)) {
			ChallengeApproveActivity.show(getActivity(), item.getChallengeId());
		} else {
			ChallengeDetailsActivity.show(getActivity(), item.getChallengeId());
		}

//		//Open the challengeViewActivity and give the current selected Challenge to the activity
//		Intent intent = new Intent(getActivity(), ChallengeViewActivity.class);
//		intent.putExtra("challengeId", item.getChallengeId());
//		this.startActivity(intent);
	}

}
