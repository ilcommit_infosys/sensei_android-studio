package org.almende.motivator.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Target;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.Profile;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.utils.RoundedImageView;

import java.util.Date;

/**
 * Created by Kevin on 02/04/2014.
 */
public class ChallengeDetailsActivity extends FacebookActivity {
	private static final String TAG = ChallengeDetailsActivity.class.getCanonicalName();

	public static final String EXTRA_CHALLENGE_ID = "challengeId";

	private ChallengeDB _challengeDB;
	private Challenge _challenge;

	private RoundedImageView _imgPicture;
	private TextView _txtName;
	private TextView _txtChallengeDate;
	private TextView _txtChallengeStatus;
	private TextView _txtTitle;
	private TextView _txtMessage;
	private TextView _txtEvidence;
	private TextView _txtRewards;

	private Target[] _imageTarget = new Target[1];

	private RelativeLayout _layButtons;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		_challengeDB = ChallengeDB.getInstance(getApplicationContext());

		String challengeId = getIntent().getStringExtra(EXTRA_CHALLENGE_ID);
		if (challengeId != null) {
			_challenge = _challengeDB.getChallenge(challengeId);
			_challenge.setUpdate(false);
		} else {
			Log.e(TAG, "challenge id not set!!");
			onBackPressed();
		}

		initUI();
		assignChallenge();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (_challenge.isModified()) {
			_challengeDB.updateChallenge(_challenge);
		}
	}

	private void initUI() {
		setContentView(R.layout.challenge_details);

		ImageView btnClose = (ImageView) findViewById(R.id.detailsChallengeClose);
		btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button btnCancel = (Button) findViewById(R.id.detailsChallengeCancel);
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button btnComplete = (Button) findViewById(R.id.detailsChallengeComplete);
		btnComplete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showCompleteScreen();
			}
		});

		_imgPicture = (RoundedImageView) findViewById(R.id.imgDetailsChallengeChallengerImage);
		_txtName = (TextView) findViewById(R.id.txtDetailsChallengeChallengerName);
		_txtChallengeDate = (TextView) findViewById(R.id.txtDetailsChallengeDate);
		_txtChallengeStatus = (TextView) findViewById(R.id.txtDetailsChallengeStatus);
		_txtTitle = (TextView) findViewById(R.id.txtDetailsChallengeTitle);
		_txtMessage = (TextView) findViewById(R.id.txtDetailsChallengeMessage);
		_txtEvidence = (TextView) findViewById(R.id.txtDetailsChallengeEvidence);
		_txtRewards = (TextView) findViewById(R.id.txtDetailsChallengeRewards);

		_layButtons = (RelativeLayout) findViewById(R.id.layButtons);
		_layButtons.setVisibility(View.GONE);
	}

	private void showCompleteScreen() {
		ChallengeCompleteActivity.show(this, _challenge.getChallengeId());
		finish();
	}

	private boolean isOwnChallenge() {
		String facebookId = Profile.getInstance(this).getFacebookId();
		return _challenge.getChallenger().equals(facebookId);
	}

	private void assignChallenge() {

		if (isOwnChallenge()) {
			loadFbImage(_challenge.getChallengee(), _imageTarget, _imgPicture);
			_txtName.setText(_challenge.getChallengeeName());
		} else {
			loadFbImage(_challenge.getChallenger(), _imageTarget, _imgPicture);
			_txtName.setText(_challenge.getChallengerName());
		}

		long dt = new Date().getTime() - _challenge.getStartDate().getTime();
		long mins = dt / 1000 / 60;
		long hours = mins / 60;
		long days = hours / 24;

		if (days > 0) {
			_txtChallengeDate.setText(String.format("%d d ago", days));
		} else if (hours > 0) {
			_txtChallengeDate.setText(String.format("%d hr ago", hours));
		} else {
			_txtChallengeDate.setText(String.format("%d min ago", mins));
		}

		_txtChallengeStatus.setText(_challenge.getStatus());
		_txtTitle.setText(_challenge.getTitle());
		_txtMessage.setText(_challenge.getContent());
		_txtEvidence.setText(String.format("%d %s", _challenge.getEvidenceAmount(), _challenge.getEvidenceType()));
		_txtRewards.setText(String.format("%s xp points\n+\n%s", _challenge.getXpReward(), _challenge.getReward()));

		if (!isOwnChallenge() &&
			_challenge.getStatus().equals(Challenge.ACCEPTED)) {
			_layButtons.setVisibility(View.VISIBLE);
		}
	}

	public static void show(Activity activity, String challengeId) {
		Intent intent = new Intent(activity, ChallengeDetailsActivity.class);
		intent.putExtra(EXTRA_CHALLENGE_ID, challengeId);
		activity.startActivity(intent);
	}

	@Override
	protected String getTAG() {
		return ChallengeDetailsActivity.class.getCanonicalName();
	}

}
