package org.almende.motivator.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;
import com.facebook.share.widget.GameRequestDialog;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.FriendsHelper;
import org.almende.motivator.Validation;
import org.almende.motivator.adapters.FriendsAdapterNew;
import org.almende.motivator.adapters.SendToGridAdapter;
import org.almende.motivator.facebook.models.ChallengeRequestData;
import org.almende.motivator.misc.CustomCallback;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.models.User;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import com.google.analytics.tracking.android.EasyTracker;
//import com.google.analytics.tracking.android.MapBuilder;

/**
 * Created by Kevin on 02/04/2014.
 */
public class ChallengeCreateActivity extends FacebookActivity {
	private static final boolean DEBUG = false;

	private static final String TAG = ChallengeCreateActivity.class.getCanonicalName();

	private GridView _gvSendTo;
	private Spinner _spSendTo;
	private EditText _edtTitle;
	private EditText _edtMessage;
	private EditText _edtReward;
	private ImageView userPic;
//	private TextView xp;

	//	private String challengee;
	private int _evidenceAmount;
	private String _evidenceType;
	private int _xpPoints;
//	private boolean challengeeSelected = false;
//	private String challengeeName;

	private User currentUser;

//	private String[] facebookFriendsId = {"loading..."};
//	private String[] facebookFriendsName = {"no friends found"};

//	private EasyTracker easyTracker;

	private ChallengeDB _challengeDB;
	private Spinner _spEvidenceAmount;
	private Spinner _spEvidenceType;
	private TextView _txtXpPoints;
	private Spinner _spDifficultyLevel;
	private String _difficultyLevel;

	private Challenge _challenge;

	private ArrayList<User> _sendToList;

	private boolean hasFriends = false;
	private SendToGridAdapter _gridAdapter;
	private ArrayList<User> _friends = new ArrayList<>();
	private FriendsAdapterNew _friendsAdapter;
	private EditText _edtCreateChallengeSendTo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		fetchUserInfo();

		_challengeDB = ChallengeDB.getInstance(getApplicationContext());

		_challenge = new Challenge();
		_challenge.setStartDate(new Date());
		_challenge.setStatus(Challenge.NEW);

		_sendToList = new ArrayList<>();

		showStartScreen();
		getFriends();

//		new DatabaseThread().execute();
	}

	private void getFriends() {

//		_friends = FriendsDB.getInstance(getApplicationContext()).getFollowedFriends();

		FriendsHelper.getInstance(this).retreiveFacebookFriends(new CustomCallback() {
			@Override
			public Object callback(Object object) {

				_friends.clear();
				_friends.add(new User());
				_friends.addAll((ArrayList<User>)object);
				_friendsAdapter.notifyDataSetChanged();

				if (!_friends.isEmpty()) {
					hasFriends = true;
				} else {
					Toast.makeText(getApplicationContext(), "You need to follow at least one friend first.", Toast.LENGTH_SHORT).show();
				}

				return null;
			}
		});

	}

	private void showStartScreen() {
		setContentView(R.layout.challenge_create_1);

		ImageView btnClose = (ImageView) findViewById(R.id.createChallengeClose);
		btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button btnCancel = (Button) findViewById(R.id.createChallengeCancel);
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button btnNext = (Button) findViewById(R.id.createChallengeNext);
		btnNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (verifyStartScreen()) {
					saveStartScreen();
					showNextScreen();
				}
			}
		});

		_edtTitle = (EditText) findViewById(R.id.edtCreateChallengeTitle);
		_edtMessage = (EditText) findViewById(R.id.edtCreateChallengeMessage);

		_spSendTo = (Spinner) findViewById(R.id.spCreateChallengeSendTo);
//		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, facebookFriendsName);
		_friendsAdapter = new FriendsAdapterNew(this, _friends);
		_friendsAdapter.setIgnoreFirst(true);
		_spSendTo.setAdapter(_friendsAdapter);
		_spSendTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (position != 0) {
//					challengee = facebookFriendsId[position];
//					challengeeName = facebookFriendsName[position];
//					challengeeSelected = true;

					if (!_sendToList.contains(_friends.get(position))) {
//						if (!_sendToList.contains(_friends.get(position-1))) {
//						_sendToList.add(_friends.get(position - 1));
						_sendToList.add(_friends.get(position));
						_gridAdapter.notifyDataSetChanged();
					}
					_spSendTo.setSelection(0);
				} else {
//					challengeeSelected = false;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
//				challengeeSelected = false;
			}
		});

//		_edtCreateChallengeSendTo = (EditText) findViewById(R.id.edtCreateChallengeSendTo);
//		_edtCreateChallengeSendTo.setOnTouchListener(new View.OnTouchListener() {
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				_spSendTo.performClick();
//				return true;
//			}
//		});

		_gvSendTo = (GridView) findViewById(R.id.gvCreateChallengeSendTo);
		_gridAdapter = new SendToGridAdapter(this, _sendToList);
		_gvSendTo.setAdapter(_gridAdapter);

//		_spSendTo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				challengee = facebookFriendsId[position];
//				challengeeName = facebookFriendsName[position];
//				challengeeSelected = true;
//			}
//		});

		if (DEBUG) {
			_edtTitle.setText("test123");
			_edtMessage.setText("try this");
		}

		String temp;
		if ((temp = _challenge.getTitle()) != "") {
			_edtTitle.setText(temp);
		}
		if ((temp = _challenge.getContent()) != "") {
			_edtMessage.setText(temp);
		}
//		if ((temp = _challenge.getChallengee()) != "") {
//			_spSendTo.setSelection(temp);
//		}

	}

	private boolean verifyStartScreen() {

		boolean success = true;
		if (!Validation.isTitle(_edtTitle, true)) success = false;
		if (!Validation.isLetters(_edtMessage, true)) success = false;

		Boolean noChallengee = false;

//		if (!challengeeSelected || challengee == null || challengee.equals("loading...")) {
		if (_sendToList.isEmpty()) {
			success = false;
			noChallengee = true;
		}
		if (!success) {
			if (noChallengee) {
				Toast.makeText(getApplicationContext(), "You forgot to select a challengee", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getApplicationContext(), "Not everything is filled in correctly.", Toast.LENGTH_SHORT).show();
			}
		}

		return success;
	}

	private void saveStartScreen() {
		_challenge.setTitle(_edtTitle.getText().toString());
		_challenge.setContent(_edtMessage.getText().toString());

//		_challenge.setChallengee();

	}

	private void showNextScreen() {
		setContentView(R.layout.challenge_create_2);

		ImageView btnClose = (ImageView) findViewById(R.id.createChallengeClose);
		btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button btnBack = (Button) findViewById(R.id.createChallengeBack);
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showStartScreen();
			}
		});

		Button btnSend = (Button) findViewById(R.id.createChallengeSend);
		btnSend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (validateNextScreen()) {
					saveNextScreen();
					sendChallenge(_challenge);
				}
			}
		});

		_spEvidenceAmount = (Spinner) findViewById(R.id.spCreateChallengeEvidenceAmount);
		_spEvidenceAmount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				_evidenceAmount = Integer.parseInt(parent.getSelectedItem().toString());
				int xpAmount = Integer.parseInt(parent.getSelectedItem().toString()) * 300;
				_txtXpPoints.setText(xpAmount + "XP");
				_xpPoints = xpAmount;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//Isn't possible so, do nothing
			}
		});

		_spEvidenceType = (Spinner) findViewById(R.id.spCreateChallengeEvidenceType);
		_spEvidenceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				_evidenceType = parent.getSelectedItem().toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//Isn't possible so, do nothing
			}
		});

		_edtReward = (EditText) findViewById(R.id.edtCreateChallengeReward);
		_txtXpPoints = (TextView) findViewById(R.id.txtCreateChallengeXpPoints);

		_spDifficultyLevel = (Spinner) findViewById(R.id.spCreateChallengeDifficultyLevel);
		_spDifficultyLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				_difficultyLevel = parent.getSelectedItem().toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//Isn't possible so, do nothing
			}
		});

		if (DEBUG) {
			_edtReward.setText("Beer");
		}
	}

	private void saveNextScreen() {

		_challenge.setReward(_edtReward.getText().toString());
		_challenge.setXpReward(_xpPoints);
		_challenge.setEvidenceAmount(_evidenceAmount);
		_challenge.setEvidenceType(_evidenceType);

	}

	private boolean validateNextScreen() {

		if (!Validation.isLetters(_edtReward, false)) {
			Toast.makeText(getApplicationContext(), "You forgot to fill in a reward.", Toast.LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

//	@Override
//	protected String getTAG() {
//		return TAG;
//	}

//	@Override
//	protected int getLayoutResource() {
//		return R.layout.sensei_challenge_create;
//	}

//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);

//		fetchUserInfo();

//		_challengeDB = ChallengeDB.getInstance(getApplicationContext());

//		Spinner spinnerAmount = (Spinner) findViewById(R.id.spinner_evidence_amount);
//		spinnerAmount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//			@Override
//			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//				_evidenceAmount = Integer.parseInt(adapterView.getSelectedItem().toString());
//				xp = (TextView) findViewById(R.id.txtExperiencePoints);
//				int xpAmount = Integer.parseInt(adapterView.getSelectedItem().toString()) * 300;
//				xp.setText(xpAmount + "XP");
//				_xpPoints = xpAmount;
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> adapterView) {
//				//Isn't possible so, do nothing
//			}
//		});

//		Spinner spinnerType = (Spinner) findViewById(R.id.spinner_evidence_type);
//		spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//			@Override
//			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//				_evidenceType = adapterView.getSelectedItem().toString();
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> adapterView) {
//				//Isn't possible so, do nothing
//			}
//		});

//		_edtTitle = (EditText) findViewById(R.id.txtChallengeName);
//		_edtMessage = (EditText) findViewById(R.id.txtChallengeContent);
//		_edtReward = (EditText) findViewById(R.id.txtReward);

//		Button btnCreateChallenge = (Button) findViewById(R.id.btnCreateChallenge);
//		btnCreateChallenge.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View view) {
//
//				// May return null if a EasyTracker has not yet been initialized with a
//				// property ID.
//
//				// MapBuilder.createEvent().build() returns a Map of event fields and values
//				// that are set and sent with the hit.
////				easyTracker.send(MapBuilder
////								.createEvent("ui_action",     // Event category (required)
////										"button_press",  // Event action (required)
////										"create_challenge",   // Event label
////										null)            // Event value
////								.build()
////				);
//
//				createChallenge();
//			}
//		});

//		new DatabaseThread().execute();
//
//	}

//	@Override
//	protected void onResume() {
//		super.onResume();
//
//		if (DEBUG) {
//			_edtTitle.setText("test123");
//			_edtMessage.setText("try this");
//			_edtReward.setText("Beer");
//		}
//
//	}

//	public void createChallenge() {
//		if (validation()) {
//
//			String title = _edtTitle.getText().toString();
//			String content = _edtMessage.getText().toString();
//			String reward = _edtReward.getText().toString();
//
////			Challenge challenge = new Challenge();
////			challenge.setStartDate(new Date());
////			challenge.setTitle(title);
////			challenge.setContent(content);
////			challenge.setReward(reward);
////			challenge.setEvidenceAmount(_evidenceAmount);
////			challenge.setEvidenceType(_evidenceType);
////			challenge.setXpReward(_xpPoints);
////			challenge.setStatus("new");
//
//			sendChallenge(challenge);
//		}
//	}

	private void sendChallenge(final Challenge challenge) {

		try {
			ChallengeRequestData data = new ChallengeRequestData();

			// send only the minimum challenge over FB (limit is 255)
			data.setChallenge(challenge);

			List<String> recipients = new ArrayList<>();
			for (User user : _sendToList) {
				recipients.add(user.getFacebookId());
			}
			sendRequest(recipients,
					"Send Challenge",
					"I challenge you to: \"" + challenge.getContent() + "\".\r\n " +
							"Your reward will be " + _xpPoints + " XP + " + challenge.getReward(),
					data,
					new CustomCallback() {
						@Override
						public Object callback(Object object) {
							GameRequestDialog.Result result = (GameRequestDialog.Result) object;

							for (User user : _sendToList) {
								// set all the other challenge fields once the request was made
								Challenge challenge = null;
								try {
									challenge = _challenge.copy();

									challenge.setChallengeId(result.getRequestId());
									challenge.setChallenger(currentUser.getFacebookId());
									challenge.setChallengerName(currentUser.getName());
									challenge.setChallengee(user.getFacebookId());
									challenge.setChallengeeName(user.getName());

									_challengeDB.addNewSentChallenge(challenge);

								} catch (JSONException e) {
									e.printStackTrace();
								}
							}

							onBackPressed();
							return null;
						}
					}
			);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

//	public boolean validation() {
//		boolean success = true;
//		if (!Validation.isTitle(_edtTitle, true)) success = false;
//		if (!Validation.isLetters(_edtMessage, true)) success = false;
//		if (!Validation.isLetters(_edtReward, false)) success = false;
//
//		Boolean noChallengee = false;
//
//		if (!challengeeSelected || challengee == null || challengee.equals("loading...")) {
//			success = false;
//			noChallengee = true;
//		}
//		if (!success) {
//			if (noChallengee) {
//				Toast.makeText(getApplicationContext(), "You forgot to select a challengee", Toast.LENGTH_SHORT).show();
//			} else {
//				Toast.makeText(getApplicationContext(), "Not everything is filled in correctly.", Toast.LENGTH_SHORT).show();
//			}
//		}
//
//		return success;
//	}

//	public void updateUI() {
//		if (currentUser.getName() != null) {
//			TextView txtChallenger = (TextView) findViewById(R.id.txtChallenger);
//			txtChallenger.setText(currentUser.getName());
//
//			String imgId = "https://graph.facebook.com/" + currentUser.getFacebookId() + "/picture?type=normal&height=200&width=200";
//			userPic = (ImageView) findViewById(R.id.imgChallenger);
//			Picasso.with(getApplicationContext()).load(imgId).into(userPic);
//			userPic.setMinimumHeight(300);
//		}
//	}

//	public void updatePicture(String id, String name) {
//
//		if(!name.equals("loading... please try again")){
//			TextView txtChallengee = (TextView) findViewById(R.id.txtChallengee);
//			txtChallengee.setText(name);
//
//			String imgId = "https://graph.facebook.com/" + id + "/picture?type=normal&height=200&width=200";
//			userPic = (ImageView) findViewById(R.id.imgChallengee);
//			Picasso.with(getApplicationContext()).load(imgId).into(userPic);
//			userPic.setMinimumHeight(300);
//		}
//
//	}

	private void fetchUserInfo() {
		Profile profile = Profile.getCurrentProfile();
		if (profile != null) {
			currentUser = new User(profile.getId(), profile.getName());
//			updateUI();
		} else {
			currentUser = null;
		}
	}

//	public void onSelectFriendsPressed(View v) {
//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
//		builder.setTitle("Choose the challengee")
//
//				.setItems(facebookFriendsName, new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int which) {
//						challengee = facebookFriendsId[which];
//						updatePicture(challengee, facebookFriendsName[which]);
//						challengeeSelected = true;
//						challengeeName = facebookFriendsName[which];
//					}
//				});
//		builder.create();
//		builder.show();
//	}

	public static void show(Activity activity) {
		activity.startActivity(new Intent(activity, ChallengeCreateActivity.class));
	}

	@Override
	protected String getTAG() {
		return "ChallengeCreateActivity";
	}

//	public static void show(Context context) {
//		final Dialog dialog = new Dialog(context, R.style.AppTheme_Dialog);
//
//		dialog.setContentView(R.layout.challenge_create_1);
//		dialog.setCancelable(true);
//
//		ImageView btnClose = (ImageView) dialog.findViewById(R.id.createChallengeClose);
//		btnClose.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				dialog.dismiss();
//			}
//		});
//
//		Button btnCancel = (Button) dialog.findViewById(R.id.createChallengeCancel);
//		btnCancel.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				dialog.dismiss();
//			}
//		});
//
//		Button btnNext = (Button) dialog.findViewById(R.id.createChallengeNext);
//		btnNext.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				dialog.dismiss();
//			}
//		});
//
//		dialog.show();
//	}

//	@Override
//	public void onStart() {
//		super.onStart();
//		//google analytics
//		easyTracker = EasyTracker.getInstance(this);  // Add this method.
//		easyTracker.activityStart(this);
//
//	}

//	@Override
//	public void onStop() {
//		super.onStop();
//		//google analytics
//		//EasyTracker.getInstance(this).activityStop(this);  // Add this method.
//		easyTracker.activityStop(this);
//	}

//	class DatabaseThread extends AsyncTask<String, String, String> {
//		private ProgressDialog simpleWaitDialog;
//
//		protected String doInBackground(String... args) {
//			if (Cookie.getInstance().internet) {
//
//				ArrayList<User> _friends = FriendsDB.getInstance(getApplicationContext()).getFollowedFriends();
//
//				if (!_friends.isEmpty()) {
//					String[] facebookFriendsTemp = new String[_friends.size()];
//					String[] facebookFriendsNameTemp = new String[_friends.size()];
//
//					for (int i = 0; i < _friends.size(); ++i) {
//						User friend = _friends.get(i);
//						facebookFriendsTemp[i] = friend.getFacebookId();
//						facebookFriendsNameTemp[i] = friend.getName();
//					}
//
//					facebookFriendsId = facebookFriendsTemp;
//					facebookFriendsName = facebookFriendsNameTemp;
//
//					if (DEBUG) {
//						challengee = facebookFriendsId[0];
//						runOnUiThread(new Runnable() {
//							@Override
//							public void run() {
//								updatePicture(challengee, facebookFriendsName[0]);
//							}
//						});
//						challengeeSelected = true;
//						challengeeName = facebookFriendsName[0];
//					}
//				} else {
//					facebookFriendsName[0] = "You need to follow at least one friend first.";
//				}
//			}
//			return null;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			simpleWaitDialog = ProgressDialog.show(this,
//					"Please wait", "Loading");
//		}
//
//		protected void onPostExecute(String result) {
//			try {
//				simpleWaitDialog.dismiss();
//				simpleWaitDialog = null;
//			} catch (Exception e) {
//				// nothing
//			}
//		}
//	}

//	@Override
//	public void onBackPressed() {
//		super.onBackPressed();
//		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//	}

//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//			// Respond to the action bar's Up/Home button
//			case android.R.id.home:
//				onBackPressed();
//				return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
}
