package org.almende.motivator.ui;

//import android.app.Fragment;
import android.app.Activity;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.NotificationDB;
import org.almende.motivator.adapters.Entry;
import org.almende.motivator.facebook.FacebookManager;
import org.almende.motivator.service.EventListener;
import org.almende.motivator.service.MotivatorService;

import java.util.ArrayList;

public class ChallengeOverviewActivity extends FacebookActivity implements EventListener {

	private FragmentTabHost _tabHost;

	private static final String TAG = ChallengeOverviewActivity.class.getCanonicalName();

	private ArrayList<Entry> items = new ArrayList<Entry>();
	private ListView listview = null;
	private Button btnCreate;

	private ChallengeDB _challengeDB;
	private ImageButton _chatButton;
	private TextView _notificationButton;

	private NavigationView _sideBar;

	private Fragment _overlay = null;
	private TextView _title;
	private ImageButton _btnCreateNewChallenge;

	private NotificationDB _notificationDB;
	private TextView _unreadMessages;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_challenges);

		_notificationDB = NotificationDB.getInstance(this);

		initUI();

		Intent intent = new Intent(this, MotivatorService.class);
		bindService(intent, _connection, Context.BIND_AUTO_CREATE);
	}

	private void initUI() {
		_title = (TextView) findViewById(android.R.id.title);
		_title.setText("CHALLENGES");

		_tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		_tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
//		_tabHost.setBackgroundColor(getResources().getColor(R.color.white));

		_tabHost.getTabWidget().setDividerDrawable(R.drawable.divider_vertical);

		setupTab(ReceivedChallengesFragment.class, "Received");
		setupTab(SentChallengesFragment.class, "Sent");

		_chatButton = (ImageButton) findViewById(R.id.chat_button);
		_chatButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (_overlay instanceof RecentChatsFragment) {
					removeOverlay();
				} else {
					addOverlay(new RecentChatsFragment());
				}
			}
		});

		_notificationButton = (TextView) findViewById(R.id.notification_button);
		_notificationButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (_overlay instanceof NotificationsFragment) {
					removeOverlay();
				} else {
					addOverlay(new NotificationsFragment());
				}
			}
		});
		updateNotificationsButton();

		_btnCreateNewChallenge = (ImageButton) findViewById(R.id.create_new_challenge);
		_btnCreateNewChallenge.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ChallengeCreateActivity.show(ChallengeOverviewActivity.this);
			}
		});

		_unreadMessages = (TextView) findViewById(R.id.unread_messages);
	}

	private MotivatorService _service;
	private ServiceConnection _connection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MotivatorService.MotivatorBinder binder = (MotivatorService.MotivatorBinder) service;
			_service = binder.getService();
			_service.registerEventListener(ChallengeOverviewActivity.this);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {

		}
	};

	private void addOverlay(Fragment fragment) {
		if (_overlay != null) {
			removeOverlay();
		}
		_overlay = fragment;
		getFragmentManager()
				.beginTransaction()
				.add(R.id.content_frame, _overlay)
				.commit();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	private void removeOverlay() {
		getFragmentManager()
				.beginTransaction()
				.remove(_overlay)
				.commit();
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		_overlay = null;
	}

	@Override
	public void onBackPressed() {
		if (_overlay != null) {
			removeOverlay();
		} else {
			super.onBackPressed();
		}
	}

	//	private ArrayList<Challenge> sentChallenges = new ArrayList<>();
//	private ArrayList<Challenge> receivedChallenges = new ArrayList<>();
//	private EntryAdapter adapter;

//	private MotivatorService _service;
//	private ServiceConnection _connection = new ServiceConnection() {
//
//		@Override
//		public void onServiceConnected(ComponentName name, IBinder service) {
//			MotivatorService.MotivatorBinder binder = (MotivatorService.MotivatorBinder) service;
//			_service = binder.getService();
//			_service.registerEventListener(ChallengeOverviewFragment.this);
//		}
//
//		@Override
//		public void onServiceDisconnected(ComponentName name) {
//
//		}
//	};

	private void setupTab(Class cls, final String tag) {
		View tabview = createTabView(_tabHost.getContext(), tag);
		TabHost.TabSpec setContent = _tabHost.newTabSpec(tag).setIndicator(tabview);
		_tabHost.addTab(setContent, cls, null);
	}

	private static View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
		TextView tv = (TextView) view.findViewById(R.id.tabTitle);
		tv.setText(text);
		return view;
	}

	@Override
	public void onActivityReenter(int resultCode, Intent data) {
		super.onActivityReenter(resultCode, data);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResumeFragments() {
		super.onResumeFragments();
	}

	@Override
	public void onResume() {
		super.onResume();

		if (!FacebookManager.isLoggedIn()) {
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.content_frame, new FacebookLoginFragment())
					.commit();
			return;
		}

		updateNotificationsButton();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unbindService(_connection);
	}

	@Override
	public void onStart() {
		super.onStart();
		//google analytics
	}

	@Override
	public void onStop() {
		super.onStop();
		//google analytics
	}

	@Override
	protected String getTAG() {
		return TAG;
	}

	public static void show(Activity activity) {
		activity.startActivity(new Intent(activity, ChallengeOverviewActivity.class));
	}

	@Override
	public void onEvent(Event event) {
		switch (event) {
			case NOTIFICATION_UPDATE:
				updateNotificationsButton();
		}
	}

	private void updateNotificationsButton() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				int numNotifications = _notificationDB.getNumUnreadNotifications();
				if (numNotifications == 0) {
					_notificationButton.setVisibility(View.GONE);
				} else {
					_notificationButton.setVisibility(View.VISIBLE);
					_notificationButton.setText(String.valueOf(numNotifications));
				}
			}
		});
	}

}
