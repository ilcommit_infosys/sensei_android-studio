package org.almende.motivator.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.infy.intelirun.ui.R;

import org.almende.motivator.facebook.FacebookManager;

import java.util.Arrays;

public class FacebookLoginFragment extends Fragment {

	private static final String TAG = FacebookLoginFragment.class.getCanonicalName();

	private CallbackManager _callbackManager;

	@Override
	public void onResume() {
		super.onResume();

		if (FacebookManager.isLoggedIn()) {
			getFragmentManager().beginTransaction()
					.remove(FacebookLoginFragment.this)
					.commit();
			getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		_callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
		_callbackManager = CallbackManager.Factory.create();

		LoginButton authButton = (LoginButton) getActivity().findViewById(R.id.authButton);
		authButton.setFragment(this);
		authButton.setReadPermissions(Arrays.asList("public_profile", "user_friends"));

	}

	@Override
	public View onCreateView(LayoutInflater inflater,
							 ViewGroup container,
							 Bundle savedInstanceState) {
		return inflater.inflate(R.layout.base_facebook_login, container, false);
	}

}
