package org.almende.motivator.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.model.GameRequestContent;
import com.facebook.share.widget.GameRequestDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.almende.motivator.facebook.models.FacebookRequest;
import org.almende.motivator.misc.CustomCallback;
import org.hva.createit.sensei.ui.BaseActivity;
import org.json.JSONException;

import java.util.List;

/**
 * Copyright (c) 2015 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 5-11-15
 *
 * @author Dominik Egger
 */
public abstract class FacebookActivity extends ActionBarActivity {

	protected GameRequestDialog requestDialog;
	protected CallbackManager callbackManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//initialise facebook sdk
		FacebookSdk.sdkInitialize(getApplicationContext());
		super.onCreate(savedInstanceState);

		callbackManager = CallbackManager.Factory.create();
		requestDialog = new GameRequestDialog(this);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	protected abstract String getTAG();

	protected void sendRequest(List<String> recipients, String title, String message,
							   FacebookRequest request, final CustomCallback callback) throws JSONException {
		GameRequestContent gamerequest = new GameRequestContent.Builder()
				.setMessage(message)
				.setActionType(GameRequestContent.ActionType.SEND)
				.setObjectId(request.getFbObjectId())
				.setTitle(title)
				.setRecipients(recipients)
				.setData(request.toString())
				.build();

		requestDialog.registerCallback(callbackManager, new FacebookCallback<GameRequestDialog.Result>() {
			@Override
			public void onSuccess(GameRequestDialog.Result result) {
				Log.d(getTAG(), "success: " + result.getRequestId());
				callback.callback(result);
			}

			@Override
			public void onCancel() {
				Log.w(getTAG(), "cancel");
			}

			@Override
			public void onError(FacebookException error) {
				Log.e(getTAG(), "error");
			}
		});
		requestDialog.show(gamerequest);
	}

	protected void sendRequest(String receiverId, String title, String message,
							   FacebookRequest request, final CustomCallback callback) throws JSONException {
		GameRequestContent gamerequest = new GameRequestContent.Builder()
				.setMessage(message)
				.setActionType(GameRequestContent.ActionType.SEND)
				.setObjectId(request.getFbObjectId())
				.setTitle(title)
				.setTo(receiverId)
				.setData(request.toString())
				.build();

		requestDialog.registerCallback(callbackManager, new FacebookCallback<GameRequestDialog.Result>() {
			@Override
			public void onSuccess(GameRequestDialog.Result result) {
				Log.d(getTAG(), "success: " + result.getRequestId());
				callback.callback(result);
			}

			@Override
			public void onCancel() {
				Log.w(getTAG(), "cancel");
			}

			@Override
			public void onError(FacebookException error) {
				Log.e(getTAG(), "error");
			}
		});
		requestDialog.show(gamerequest);
	}

	protected void loadFbImage(String fbId, Target[] target, final ImageView image) {
		//todo cache images

		String imgId = "https://graph.facebook.com/" + fbId + "/picture";

		// imgId = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash1/t1.0-1/c0.33.200.200/p200x200/248489_10150308474960968_2461155_n.jpg";

		//Log.d("facebook", "url = " + imgId);
		target[0] = new Target() {
			@Override
			public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
				image.setImageBitmap(bitmap);
			}

			@Override
			public void onBitmapFailed(Drawable drawable) {
				Log.e(getTAG(), "onBitmapFailed");
			}

			@Override
			public void onPrepareLoad(Drawable drawable) {
				Log.i(getTAG(), "onPrepareLoad");
			}
		};

		Picasso.with(this).load(imgId).into(target[0]);
	}
}
