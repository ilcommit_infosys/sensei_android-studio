package org.almende.motivator.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.widget.GameRequestDialog;
import com.squareup.picasso.Target;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.facebook.models.ChallengeRequestAnswer;
import org.almende.motivator.misc.CustomCallback;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.utils.RoundedImageView;
import org.json.JSONException;

import java.util.Date;

/**
 * Created by Kevin on 02/04/2014.
 */
public class ChallengeAcceptActivity extends FacebookActivity {
	private static final String TAG = ChallengeAcceptActivity.class.getCanonicalName();

	public static final String EXTRA_CHALLENGE_ID = "challengeId";

	private ChallengeDB _challengeDB;
	private Challenge _challenge;

	private RoundedImageView _imgChallenger;
	private TextView _txtChallengerName;
	private TextView _txtChallengeDate;
	private TextView _txtChallengeStatus;
	private TextView _txtTitle;
	private TextView _txtMessage;
	private TextView _txtEvidence;
	private TextView _txtRewards;

	private Target[] _imageTarget = new Target[1];

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		_challengeDB = ChallengeDB.getInstance(getApplicationContext());

		String challengeId = getIntent().getStringExtra(EXTRA_CHALLENGE_ID);
		if (challengeId != null) {
			_challenge = _challengeDB.getChallenge(challengeId);
			_challenge.setUpdate(false);
		} else {
			Log.e(TAG, "challenge id not set!!");
			onBackPressed();
		}

		initUI();
		assignChallenge();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (_challenge.isModified()) {
			_challengeDB.updateChallenge(_challenge);
		}
	}

	private void initUI() {
		setContentView(R.layout.challenge_accept);

		ImageView btnClose = (ImageView) findViewById(R.id.acceptChallengeClose);
		btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button btnCancel = (Button) findViewById(R.id.acceptChallengeReject);
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				rejectChallenge();
			}
		});

		Button btnNext = (Button) findViewById(R.id.acceptChallengeAccept);
		btnNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				acceptChallenge();
			}
		});

		_imgChallenger = (RoundedImageView) findViewById(R.id.imgAcceptChallengeChallengerImage);
		_txtChallengerName = (TextView) findViewById(R.id.txtAcceptChallengeChallengerName);
		_txtChallengeDate = (TextView) findViewById(R.id.txtAcceptChallengeDate);
		_txtChallengeStatus = (TextView) findViewById(R.id.txtAcceptChallengeStatus);
		_txtTitle = (TextView) findViewById(R.id.txtAcceptChallengeTitle);
		_txtMessage = (TextView) findViewById(R.id.txtAcceptChallengeMessage);
		_txtEvidence = (TextView) findViewById(R.id.txtAcceptChallengeEvidence);
		_txtRewards = (TextView) findViewById(R.id.txtAcceptChallengeRewards);

	}

	private void assignChallenge() {

		loadFbImage(_challenge.getChallenger(), _imageTarget, _imgChallenger);
		_txtChallengerName.setText(_challenge.getChallengerName());

		long dt = new Date().getTime() - _challenge.getStartDate().getTime();
		long mins = dt / 1000 / 60;
		long hours = mins / 60;
		long days = hours / 24;

		if (days > 0) {
			_txtChallengeDate.setText(String.format("%d d ago", days));
		} else if (hours > 0) {
			_txtChallengeDate.setText(String.format("%d hr ago", hours));
		} else {
			_txtChallengeDate.setText(String.format("%d min ago", mins));
		}

		_txtChallengeStatus.setText(_challenge.getStatus());
		_txtTitle.setText(_challenge.getTitle());
		_txtMessage.setText(_challenge.getContent());
		_txtEvidence.setText(String.format("%d %s", _challenge.getEvidenceAmount(), _challenge.getEvidenceType()));
		_txtRewards.setText(String.format("%s xp points\n+\n%s", _challenge.getXpReward(), _challenge.getReward()));
	}

	private void acceptChallenge() {

		try {
			ChallengeRequestAnswer data = new ChallengeRequestAnswer(true);

			data.setChallengeId(_challenge.getChallengeId());

			sendRequest(_challenge.getChallenger(), "Accept Challenge", "I accept your challenge", data,
					new CustomCallback() {
						@Override
						public Object callback(Object object) {
							GameRequestDialog.Result result = (GameRequestDialog.Result) object;

							Log.d(TAG, "success: " + result.getRequestId());

							_challenge.setStatus(Challenge.ACCEPTED);
							_challenge.updateLoginDate();

							onBackPressed();
							return null;
						}
					}
			);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void rejectChallenge() {

		try {
			ChallengeRequestAnswer data = new ChallengeRequestAnswer(false);

			data.setChallengeId(_challenge.getChallengeId());

			sendRequest(_challenge.getChallenger(), "Reject Challenge", "I reject your challenge", data,
					new CustomCallback() {
						@Override
						public Object callback(Object object) {
							GameRequestDialog.Result result = (GameRequestDialog.Result) object;

							Log.d(TAG, "success: " + result.getRequestId());

							_challenge.setStatus(Challenge.REJECTED);
							_challenge.updateLoginDate();

							onBackPressed();
							return null;
						}
					}
			);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static void show(Activity activity, String challengeId) {
		Intent intent = new Intent(activity, ChallengeAcceptActivity.class);
		intent.putExtra(EXTRA_CHALLENGE_ID, challengeId);
		activity.startActivity(intent);
	}

	@Override
	protected String getTAG() {
		return "ChallengeAcceptActivity";
	}

}
