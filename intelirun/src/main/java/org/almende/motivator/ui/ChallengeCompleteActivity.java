package org.almende.motivator.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.widget.GameRequestDialog;
import com.squareup.picasso.Target;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.MyLocationListener;
import org.almende.motivator.adapters.RequiredEvidenceGridAdapter;
import org.almende.motivator.facebook.models.ChallengeRequestComplete;
import org.almende.motivator.misc.CustomCallback;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.models.Evidence;
import org.almende.motivator.utils.RoundedImageView;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Kevin on 02/04/2014.
 */
public class ChallengeCompleteActivity extends FacebookActivity {
	private static final String TAG = ChallengeCompleteActivity.class.getCanonicalName();

	public static final String EXTRA_CHALLENGE_ID = "challengeId";

	//keep track of camera capture intent
	public static final int CAMERA_CAPTURE = 1;

	//keep track of camera capture intent
	public static final int SELECT_PICTURE = 2;

	//keep track of share action capture intent
	public static final int SHARE_REQUEST_CODE = 3;


	private ChallengeDB _challengeDB;
	private Challenge _challenge;

	private RoundedImageView _imgChallenger;
	private TextView _txtChallengerName;
	private TextView _txtChallengeDate;
	private TextView _txtChallengeStatus;
	private TextView _txtEvidence;

	private Target[] _imageTarget = new Target[1];

	private EditText _edtEffort;
	private GridView _gvRequiredEvidence;

	private Uri _picUri;

	//the picture uri collection
	private ArrayList<Uri> pictureUriList;
//	private ArrayList<File> tempFiles;

	private RequiredEvidenceGridAdapter _gridAdapter;
	private int _currentPostion;
	private TextView _txtTitle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		_challengeDB = ChallengeDB.getInstance(getApplicationContext());

		String challengeId = getIntent().getStringExtra(EXTRA_CHALLENGE_ID);
		if (challengeId != null) {
			_challenge = _challengeDB.getChallenge(challengeId);
			_challenge.setUpdate(false);
		} else {
			Log.e(TAG, "challenge id not set!!");
			onBackPressed();
		}

		pictureUriList = new ArrayList<>();
		for (int i = 0; i < _challenge.getEvidenceAmount(); ++i) {
			pictureUriList.add(null);
		}

		initUI();
		assignChallenge();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (_challenge.isModified()) {
			_challengeDB.updateChallenge(_challenge);
		}
	}

	private void initUI() {
		setContentView(R.layout.challenge_complete);

		ImageView btnClose = (ImageView) findViewById(R.id.completeChallengeClose);
		btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button btnCancel = (Button) findViewById(R.id.completeChallengeBack);
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ChallengeDetailsActivity.show(ChallengeCompleteActivity.this, _challenge.getChallengeId());
				finish();
			}
		});

		Button btnNext = (Button) findViewById(R.id.completeChallengeComplete);
		btnNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				completeChallenge();
			}
		});

		_imgChallenger = (RoundedImageView) findViewById(R.id.imgCompleteChallengeChallengerImage);
		_txtChallengerName = (TextView) findViewById(R.id.txtCompleteChallengeChallengerName);
		_txtChallengeDate = (TextView) findViewById(R.id.txtCompleteChallengeDate);
		_txtChallengeStatus = (TextView) findViewById(R.id.txtCompleteChallengeStatus);
		_txtTitle = (TextView) findViewById(R.id.txtCompleteChallengeTitle);
		_txtEvidence = (TextView) findViewById(R.id.txtCompleteChallengeEvidence);

		_edtEffort = (EditText) findViewById(R.id.edtCompleteChallengeEffort);

		_gvRequiredEvidence = (GridView) findViewById(R.id.gvCompleteChallengeRequiredEvidence);
		_gvRequiredEvidence.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (_gridAdapter.getItem(position) == null) {
					addImage(position);
				} else {
//					changeImage(position);
				}
			}
		});

		_gridAdapter = new RequiredEvidenceGridAdapter(this, pictureUriList);
		_gvRequiredEvidence.setAdapter(_gridAdapter);
	}

	private void assignChallenge() {

		loadFbImage(_challenge.getChallenger(), _imageTarget, _imgChallenger);
		_txtChallengerName.setText(_challenge.getChallengerName());

		long dt = new Date().getTime() - _challenge.getStartDate().getTime();
		long mins = dt / 1000 / 60;
		long hours = mins / 60;
		long days = hours / 24;

		if (days > 0) {
			_txtChallengeDate.setText(String.format("%d d ago", days));
		} else if (hours > 0) {
			_txtChallengeDate.setText(String.format("%d hr ago", hours));
		} else {
			_txtChallengeDate.setText(String.format("%d min ago", mins));
		}

		_txtChallengeStatus.setText(_challenge.getStatus());
		_txtTitle.setText(_challenge.getTitle());
		_txtEvidence.setText(String.format("%d %s", _challenge.getEvidenceAmount(), _challenge.getEvidenceType()));
	}

	public static void show(Activity activity, String challengeId) {
		Intent intent = new Intent(activity, ChallengeCompleteActivity.class);
		intent.putExtra(EXTRA_CHALLENGE_ID, challengeId);
		activity.startActivity(intent);
	}

	@Override
	protected String getTAG() {
		return "ChallengeAcceptActivity";
	}

	private void addImage(int position) {
		_currentPostion = position;

		final Dialog dialog = new Dialog(this);
		dialog.setCancelable(true);
		dialog.setContentView(getLayoutInflater().inflate(R.layout.popup_evidence, null));

		Button browse = (Button) dialog.findViewById(org.almende.motivator.R.id.browsePicturseBtn);
		browse.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					Intent browseIntent = new Intent(Intent.ACTION_GET_CONTENT);
					browseIntent.setType("image/*");
					startActivityForResult(Intent.createChooser(browseIntent, "Select a picture"), SELECT_PICTURE);
					dialog.dismiss();
				} catch (ActivityNotFoundException anfe) {
					String errorMessage = "Whoops - can't open your images!";
					Toast toast = Toast.makeText(ChallengeCompleteActivity.this, errorMessage, Toast.LENGTH_SHORT);
					toast.show();
				}
			}
		});

		Button takePicture = (Button) dialog.findViewById(org.almende.motivator.R.id.captureBtn);
		takePicture.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					//use standard intent to capture an image
					Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

					File photoFile = null;
					try {
						photoFile = createImageFile();
					} catch (IOException e) {
						e.printStackTrace();
					}

					if (photoFile != null) {
						_picUri = Uri.fromFile(photoFile);
						captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, _picUri);
						//we will handle the returned data in onActivityResult
						startActivityForResult(captureIntent, CAMERA_CAPTURE);
					}
					dialog.dismiss();
				} catch (ActivityNotFoundException anfe) {
					//display an error message
					String errorMessage = "Whoops - your device doesn't support capturing images!";
					Toast toast = Toast.makeText(ChallengeCompleteActivity.this, errorMessage, Toast.LENGTH_SHORT);
					toast.show();
				}
			}
		});

		dialog.show();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == SHARE_REQUEST_CODE) {
			// came back from evidence upload. proceed with facebook request
			// Note: Share Intent will never return a valid resultCode, so we can't check
			// for RESULT_OK
			sendChallengeCompleted();
		} else {
			if (resultCode == RESULT_OK) {
				if (requestCode == CAMERA_CAPTURE) {//user is returning from taking the image
					addReference(_picUri);
				} else if (requestCode == SELECT_PICTURE) {
					_picUri = data.getData();
					addReference(_picUri);
				}
			}
		}
	}

	private void addReference(Uri picUri) {
		pictureUriList.set(_currentPostion, _picUri);
		_gridAdapter.notifyDataSetChanged();
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
//		File storageDir = Environment.getExternalStoragePublicDirectory(
//				Environment.DIRECTORY_PICTURES);
		File storageDir = this.getExternalCacheDir();
		File image = File.createTempFile(
				imageFileName,  /* prefix */
				".jpg",         /* suffix */
				storageDir      /* directory */
		);

//		tempFiles.add(image);

		return image;
	}

	private void sharePhoto() {
		Intent shareIntent = new Intent();
		shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
		shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, pictureUriList);
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Evidence for Challenge: " + _challenge.getTitle());
		shareIntent.setType("image/*");
		startActivityForResult(Intent.createChooser(shareIntent, "Send To"), SHARE_REQUEST_CODE);
	}

	public String getGPS() {
		try {
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			MyLocationListener locationListener = new MyLocationListener();
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
			locationListener.onLocationChanged(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));

			return locationListener.getLocation();
		} catch (Exception e) {
			return "";
		}
	}

	private void completeChallenge() {

		boolean allowed = true;
		if (_edtEffort.getText().toString().equals("")) {
			Toast.makeText(getApplicationContext(), "You forgot to enter the effort (in hours)!", Toast.LENGTH_LONG).show();
			allowed = false;
//		} else if (_edtEffort.getText().toString().length() > 2) {
//			Toast.makeText(getApplicationContext(), "The amount of hours can only be 2 decimals big", Toast.LENGTH_LONG).show();
//			allowed = false;
		} else if (Integer.parseInt(_edtEffort.getText().toString()) == 0) {
			Toast.makeText(getApplicationContext(), "Effort can't be 0", Toast.LENGTH_LONG).show();
			allowed = false;
		}
		if (allowed) {
			sharePhoto();
		}

	}

	private void sendChallengeCompleted() {

		final Evidence evidence = new Evidence();

		evidence.setGps(getGPS());
		evidence.setAmountHours(Integer.valueOf(_edtEffort.getText().toString()));
		evidence.setEndDate(new Date());
		evidence.setChallengeId(_challenge.getChallengeId());

		try {
			ChallengeRequestComplete data = new ChallengeRequestComplete();

			data.setChallengeId(_challenge.getChallengeId());
			data.setEvidence(evidence);

			sendRequest(_challenge.getChallenger(), "Challenge Completed", "I completed your challenge", data,
					new CustomCallback() {
						@Override
						public Object callback(Object object) {
							GameRequestDialog.Result result = (GameRequestDialog.Result) object;

							Log.d(TAG, "success: " + result.getRequestId());

							// update the currentChallenge only once the request was made
							_challenge.updateLoginDate();
							_challenge.setStatus(Challenge.COMPLETED);
							_challenge.setGps(evidence.getGps());
							_challenge.setAmountHours(evidence.getAmountHours());
							_challenge.setEndDate(evidence.getEndDate());
//							_challenge.setEvidence((ArrayList<Uri>) pictureUriList.clone());

							onBackPressed();
							return null;
						}
					}
			);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
