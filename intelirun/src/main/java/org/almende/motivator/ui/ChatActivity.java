package org.almende.motivator.ui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ConversationDB;
import org.almende.motivator.Profile;
import org.almende.motivator.adapters.ChatAdapter;
import org.almende.motivator.facebook.models.PersonalMessage;
import org.almende.motivator.misc.CustomCallback;
import org.almende.motivator.models.Conversation;
import org.almende.motivator.models.Message;
import org.almende.motivator.service.EventListener;
import org.almende.motivator.service.MotivatorService;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;

/**
 * Copyright (c) 2016 Dominik Egger <dominik@dobots.nl>. All rights reserved.
 * <p/>
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as
 * published by the Free Software Foundation.
 * <p/>
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * <p/>
 * Created on 30-3-16
 *
 * @author Dominik Egger
 */
public class ChatActivity extends FacebookActivity implements EventListener {

	public static final String PARTNER_ID = "partnerId";
	public static final String PARTNER_NAME = "partnerName";

	private ListView _lvChat;
	private EditText _edtMessage;
	private ImageView _btnSend;
	private TextView _title;

	private ConversationDB _conversationDB;
	private Conversation _conversation;
	private ChatAdapter _adapter;
	private String _partnerId;
	private String _partnerName;

	ArrayList<Message> _messages = new ArrayList<>();
	private ImageButton _backButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		_conversationDB = ConversationDB.getInstance(getApplicationContext());

		_partnerId = getIntent().getStringExtra(PARTNER_ID);
		_partnerName = getIntent().getStringExtra(PARTNER_NAME);
		if (_partnerId != null) {
			if (_conversationDB.hasConversation(_partnerId)) {
				_conversation = _conversationDB.getConversation(_partnerId);
			} else {
				_conversation = _conversationDB.createNewConversation(_partnerId, _partnerName);
			}
		} else {
			Log.e(getTAG(), "challenge id not set!!");
			onBackPressed();
		}

		initUI();
		fillChatView();

		Intent intent = new Intent(this, MotivatorService.class);
		bindService(intent, _connection, Context.BIND_AUTO_CREATE);

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(_connection);
	}

	private MotivatorService _service;
	private ServiceConnection _connection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MotivatorService.MotivatorBinder binder = (MotivatorService.MotivatorBinder) service;
			_service = binder.getService();
			_service.registerEventListener(ChatActivity.this);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {

		}
	};

	@Override
	public void onEvent(Event event) {
		switch(event) {
			case CONVERSATION_UPDATE:
				if (_conversation.isModified()) {
					updateChatView();
				}
				break;
		}
	}

	private void fillChatView() {
//		_messages.addAll(_conversation.getMessages());
//		_adapter = new ChatAdapter(this, _messages);
		_adapter = new ChatAdapter(this, _conversation.getMessages());
		_adapter.setOwnName(Profile.getInstance(this).getName());
		_lvChat.setAdapter(_adapter);
	}

	private void updateChatView() {
//		_messages.clear();
//		_messages.addAll(_conversation.getMessages());

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				_adapter.notifyDataSetChanged();
			}
		});
	}

	private void initUI() {
		setContentView(R.layout.activity_chat);

		_title = (TextView) findViewById(android.R.id.title);
		_title.setText(_conversation.getPartnerName());

		_backButton = (ImageButton) findViewById(R.id.back_button);
		_backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		_edtMessage = (EditText) findViewById(R.id.edtMessage);

		_btnSend = (ImageView) findViewById(R.id.btnSend);
		_btnSend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendMessage();
			}
		});

		_lvChat = (ListView) findViewById(R.id.lvChat);

	}

	private void sendMessage() {
		try {
			final Message message = new Message();
			message.setAuthor(Profile.getInstance(this).getName());
			message.setReceiver(_conversation.getPartnerName());

			String content = _edtMessage.getText().toString();
			message.setContent(content);

			message.setDate(new Date());

			PersonalMessage data = new PersonalMessage();
			data.setMessage(message);

			sendRequest(_partnerId, "Message", content, data, new CustomCallback() {
				@Override
				public Object callback(Object object) {
					_conversationDB.addMessage(_conversation.getPartnerId(), _conversation.getPartnerName(), message);
					_adapter.notifyDataSetChanged();
					_edtMessage.setText("");
					return null;
				}
			});

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static void show(Activity activity, String partnerId, String partnerName) {
		Intent intent = new Intent(activity, ChatActivity.class);
		intent.putExtra(PARTNER_ID, partnerId);
		intent.putExtra(PARTNER_NAME, partnerName);
		activity.startActivity(intent);
	}

	@Override
	protected String getTAG() {
		return ChatActivity.class.getCanonicalName();
	}

}
