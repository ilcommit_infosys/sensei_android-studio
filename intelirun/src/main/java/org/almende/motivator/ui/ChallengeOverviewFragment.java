package org.almende.motivator.ui;

/**
 * Created by AsterLaptop on 4/13/14.
 */

//import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.adapters.Entry;

import java.util.ArrayList;

public class ChallengeOverviewFragment extends Fragment {

	private FragmentTabHost _tabHost;

	private static final String TAG = ChallengeOverviewFragment.class.getCanonicalName();

	private ArrayList<Entry> items = new ArrayList<Entry>();
	private ListView listview = null;
	private Button btnCreate;

	private ChallengeDB _challengeDB;

//	private ArrayList<Challenge> sentChallenges = new ArrayList<>();
//	private ArrayList<Challenge> receivedChallenges = new ArrayList<>();
//	private EntryAdapter adapter;

//	private MotivatorService _service;
//	private ServiceConnection _connection = new ServiceConnection() {
//
//		@Override
//		public void onServiceConnected(ComponentName name, IBinder service) {
//			MotivatorService.MotivatorBinder binder = (MotivatorService.MotivatorBinder) service;
//			_service = binder.getService();
//			_service.registerEventListener(ChallengeOverviewFragment.this);
//		}
//
//		@Override
//		public void onServiceDisconnected(ComponentName name) {
//
//		}
//	};

	private void setupTab(Class cls, final String tag) {
		View tabview = createTabView(_tabHost.getContext(), tag);
		TabHost.TabSpec setContent = _tabHost.newTabSpec(tag).setIndicator(tabview);
		_tabHost.addTab(setContent, cls, null);
	}

	private static View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
		TextView tv = (TextView) view.findViewById(R.id.tabTitle);
		tv.setText(text);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

//		_tabHost = (FragmentTabHost) getActivity().findViewById(android.R.id.tabhost);
//		_tabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);
//
//		setupTab(ReceivedChallengesFragment.class, "Received");
//		setupTab(SentChallengesFragment.class, "Sent");

//		_tabHost.addTab(_tabHost.newTabSpec("Received").setIndicator("Received"),
//				ReceivedChallengesFragment.class, null);
//		_tabHost.addTab(_tabHost.newTabSpec("Sent").setIndicator("Sent"),
//				SentChallengesFragment.class, null);

		//if we don't have an agreement yet on the terms of use, we redirect the user
//        if (!redirect) {
//            Intent intent = new Intent(this, TermsActivity.class);
//            finish();
//            startActivity(intent);
//        } else
		// todo: DE add again
//		if (Profile.getInstance(getActivity()).isFirstUse()) {
//			getActivity().getFragmentManager()
//					.beginTransaction()
//					.add(R.id.content_frame, new FirstUseFragment())
//					.commit();
//		}

//		listview = (ListView) getActivity().findViewById(R.id.lvReceivedChallenges);
//		adapter = new EntryAdapter(getActivity(), items);
//		listview.setAdapter(adapter);
//		listview.setOnItemClickListener(this);

//		btnCreate = (Button) getActivity().findViewById(R.id.btnCreate);
//		btnCreate.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				startActivity(new Intent(getActivity(), ChallengeCreateActivity.class));
//				getActivity().overridePendingTransition(R.anim.slide_in_right,
//						R.anim.slide_out_left);
//			}
//		});

//		_challengeDB = ChallengeDB.getInstance(getActivity().getApplicationContext());

//		receivedChallenges = _challengeDB.getReceivedChallenges();
//		sentChallenges = _challengeDB.getSentChallenges();
//
//		Intent intent = new Intent(getActivity(), MotivatorService.class);
//		getActivity().bindService(intent, _connection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onResume() {
		super.onResume();

//		_tabHost.setup(getActivity(), getChildFragmentManager(), R.id.content_frame);

//		updateListView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
//		getActivity().unbindService(_connection);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
//		_tabHost = null;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

//		_tabHost = new FragmentTabHost(getActivity());
//		_tabHost.setup(getActivity(), getChildFragmentManager(), R.id.content_frame);
//		_tabHost.setBackgroundColor(getResources().getColor(R.color.white));
//
//		_tabHost.getTabWidget().setDividerDrawable(R.drawable.divider_vertical);
//
//		setupTab(ReceivedChallengesFragment.class, "Received");
//		setupTab(SentChallengesFragment.class, "Sent");
//
//		return _tabHost;
//
		return inflater
				.inflate(R.layout.base_challengeoverview, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();
		//google analytics
	}

	@Override
	public void onStop() {
		super.onStop();
		//google analytics
	}

//	@Override
//	public void onEvent(Event event) {
//		switch (event) {
//			case CHALLENGE_UPDATE:
//				updateListView();
//				break;
//		}
//	}

//	public void updateListView() {
//
//		items.clear();
//		items.add(new ChallengeHeader("Challenges you sent"));
//
//		if (!sentChallenges.isEmpty()) {
//			items.addAll(sentChallenges);
//		} else {
//			items.add(new ChallengeHeader("No challenges sent"));
//		}
//
//		items.add(new ChallengeHeader("Challenges you received"));
//
//		if (!receivedChallenges.isEmpty()) {
//			items.addAll(receivedChallenges);
//		} else {
//			items.add(new ChallengeHeader("No challenges received"));
//		}
//
//		if (getActivity() != null) {
//			getActivity().runOnUiThread(new Runnable() {
//				@Override
//				public void run() {
//					adapter.notifyDataSetChanged();
//				}
//			});
//		}
//	}

//	@Override
//	public void onItemClick(AdapterView arg0, View arg1, int position, long arg3) {
//		Challenge item = (Challenge) items.get(position);
//
//		//Open the challengeViewActivity and give the current selected Challenge to the activity
//		Intent intent = new Intent(getActivity(), ChallengeViewActivity.class);
//		intent.putExtra("challengeId", item.getChallengeId());
//		this.startActivity(intent);
//	}

}
