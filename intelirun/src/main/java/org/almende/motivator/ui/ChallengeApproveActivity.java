package org.almende.motivator.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.widget.GameRequestDialog;
import com.squareup.picasso.Target;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ChallengeDB;
import org.almende.motivator.Profile;
import org.almende.motivator.Validation;
import org.almende.motivator.facebook.models.ChallengeRequestResult;
import org.almende.motivator.misc.CustomCallback;
import org.almende.motivator.models.Challenge;
import org.almende.motivator.utils.RoundedImageView;
import org.json.JSONException;

import java.util.Date;

public class ChallengeApproveActivity extends FacebookActivity {
	private static final String TAG = ChallengeApproveActivity.class.getCanonicalName();

	public static final String EXTRA_CHALLENGE_ID = "challengeId";

	private ChallengeDB _challengeDB;
	private Challenge _challenge;

	private RoundedImageView _imgChallengee;
	private TextView _txtChallengeeName;
	private TextView _txtChallengeDate;
	private TextView _txtChallengeStatus;
	private TextView _txtTitle;
	private TextView _txtEvidence;

	private Target[] _imageTarget = new Target[1];
	private EditText _edtComments;

	private Profile _profile;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		_challengeDB = ChallengeDB.getInstance(getApplicationContext());

		String challengeId = getIntent().getStringExtra(EXTRA_CHALLENGE_ID);
		if (challengeId != null) {
			_challenge = _challengeDB.getChallenge(challengeId);
			_challenge.setUpdate(false);
		} else {
			Log.e(TAG, "challenge id not set!!");
			onBackPressed();
		}

		_profile = Profile.getInstance(this);

		initUI();
		assignChallenge();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (_challenge.isModified()) {
			_challengeDB.updateChallenge(_challenge);
		}
	}

	private void initUI() {
		setContentView(R.layout.challenge_approve);

		ImageView btnClose = (ImageView) findViewById(R.id.approveChallengeClose);
		btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button btnCancel = (Button) findViewById(R.id.approveChallengeDisapprove);
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				disapproveChallenge();
			}
		});

		Button btnNext = (Button) findViewById(R.id.approveChallengeApprove);
		btnNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				approveChallenge();
			}
		});

		_imgChallengee = (RoundedImageView) findViewById(R.id.imgapproveChallengeChallengeeImage);
		_txtChallengeeName = (TextView) findViewById(R.id.txtapproveChallengeChallengerName);
		_txtChallengeDate = (TextView) findViewById(R.id.txtapproveChallengeDate);
		_txtChallengeStatus = (TextView) findViewById(R.id.txtapproveChallengeStatus);
		_txtTitle = (TextView) findViewById(R.id.txtapproveChallengeTitle);
		_txtEvidence = (TextView) findViewById(R.id.txtapproveChallengeEvidence);
		_edtComments = (EditText) findViewById(R.id.edtApproveChallengeComments);
	}

	private void assignChallenge() {

		loadFbImage(_challenge.getChallengee(), _imageTarget, _imgChallengee);
		_txtChallengeeName.setText(_challenge.getChallengeeName());

		long dt = new Date().getTime() - _challenge.getStartDate().getTime();
		long mins = dt / 1000 / 60;
		long hours = mins / 60;
		long days = hours / 24;

		if (days > 0) {
			_txtChallengeDate.setText(String.format("%d d ago", days));
		} else if (hours > 0) {
			_txtChallengeDate.setText(String.format("%d hr ago", hours));
		} else {
			_txtChallengeDate.setText(String.format("%d min ago", mins));
		}

		_txtChallengeStatus.setText(_challenge.getStatus());
		_txtTitle.setText(_challenge.getTitle());
		_txtEvidence.setText(String.format("%d %s", _challenge.getEvidenceAmount(), _challenge.getEvidenceType()));
	}

	private void approveChallenge() {
		if (validateApprovement()) {
			sendChallengeApprove();
		}
	}

	private boolean validateApprovement() {
		Boolean success = true;

		if (!Validation.hasText(_edtComments)) {
			success = false;
		}

		if (!success) {
			Toast.makeText(ChallengeApproveActivity.this, "Please fill in everything", Toast.LENGTH_LONG).show();
			return false;
		} else {
//			Message message = new Message();
//			message.setAuthor(_profile.getName());
//			message.setTitle("Evidence approvement");
//			message.setReceiver(_challenge.getChallengeeName());
//			message.setLiked("false");
//			message.setContent(_edtComments.getText().toString());
//			message.setDate(new Date());
			// todo: DE what to do with the message?
			return true;
		}
	}

	private void sendChallengeApprove() {

		try {
			ChallengeRequestResult data = new ChallengeRequestResult(true);

			data.setChallengeId(_challenge.getChallengeId());
			data.setMessage(_edtComments.getText().toString());

			sendRequest(_challenge.getChallenger(), "Approve Challenge", "I approve your evidence", data,
					new CustomCallback() {
						@Override
						public Object callback(Object object) {
							GameRequestDialog.Result result = (GameRequestDialog.Result) object;

							Log.d(TAG, "success: " + result.getRequestId());

							_challenge.setRated(Challenge.APPROVED);
							_challenge.setStatus(Challenge.CLOSED);
							_challenge.setRatedMessage(_edtComments.getText().toString());
							_challenge.updateLoginDate();

							onBackPressed();
							return null;
						}
					}
			);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void disapproveChallenge() {
		if (validateApprovement()) {
			sendChallengeDisapprove();
		}
	}

	private void sendChallengeDisapprove() {

		try {
			ChallengeRequestResult data = new ChallengeRequestResult(false);

			data.setChallengeId(_challenge.getChallengeId());
			data.setMessage(_edtComments.getText().toString());

			sendRequest(_challenge.getChallenger(), "Disapprove Challenge", "I disapprove your evidence", data,
					new CustomCallback() {
						@Override
						public Object callback(Object object) {
							GameRequestDialog.Result result = (GameRequestDialog.Result) object;

							Log.d(TAG, "success: " + result.getRequestId());

							_challenge.setRated(Challenge.DISAPPROVED);
							_challenge.setStatus(Challenge.CLOSED);
							_challenge.setRatedMessage(_edtComments.getText().toString());
							_challenge.updateLoginDate();

							onBackPressed();
							return null;
						}
					}
			);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static void show(Activity activity, String challengeId) {
		Intent intent = new Intent(activity, ChallengeApproveActivity.class);
		intent.putExtra(EXTRA_CHALLENGE_ID, challengeId);
		activity.startActivity(intent);
	}

	@Override
	protected String getTAG() {
		return ChallengeApproveActivity.class.getCanonicalName();
	}

}
