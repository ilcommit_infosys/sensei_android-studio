package org.almende.motivator.ui;

/**
 * Created by AsterLaptop on 4/13/14.
 */

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.infy.intelirun.ui.R;

import org.almende.motivator.NotificationDB;
import org.almende.motivator.adapters.NotificationsAdapter;
import org.almende.motivator.models.Notification;
import org.almende.motivator.service.EventListener;
import org.almende.motivator.service.MotivatorService;

import java.util.ArrayList;

public class NotificationsFragment extends Fragment implements OnItemClickListener, EventListener {

	private static final String TAG = NotificationsFragment.class.getCanonicalName();

	private ArrayList<Notification> items = new ArrayList<>();
	private ListView listview = null;
	private View _background;

	private NotificationDB _notificationDB;

	private ArrayList<Notification> _notifications = new ArrayList<>();
	private NotificationsAdapter adapter;

	private MotivatorService _service;
	private ServiceConnection _connection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MotivatorService.MotivatorBinder binder = (MotivatorService.MotivatorBinder) service;
			_service = binder.getService();
			_service.registerEventListener(NotificationsFragment.this);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {

		}
	};

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		_background = getActivity().findViewById(R.id.background);
		_background.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getFragmentManager().beginTransaction()
						.remove(NotificationsFragment.this)
						.commit();
				getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
			}
		});

		listview = (ListView) getActivity().findViewById(R.id.lvNotifications);
		adapter = new NotificationsAdapter(getActivity(), items);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(this);

		_notificationDB = NotificationDB.getInstance(getActivity().getApplicationContext());

		_notifications = _notificationDB.getNotifications();

		Intent intent = new Intent(getActivity(), MotivatorService.class);
		getActivity().bindService(intent, _connection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onResume() {
		super.onResume();

		updateListView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		for (Notification notification : items) {
			if (notification.isNew()) {
				notification.setNew(false);
			}
		}
		_notificationDB.updateDB();

		getActivity().unbindService(_connection);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater
				.inflate(R.layout.base_notifications, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();
		//google analytics
	}

	@Override
	public void onStop() {
		super.onStop();
		//google analytics
	}

	@Override
	public void onEvent(Event event) {
		switch (event) {
			case NOTIFICATION_UPDATE:
				updateListView();
				break;
		}
	}

	public void updateListView() {

		items.clear();
//		items.add(new ChallengeHeader("Challenges you sent"));

//		if (!sentChallenges.isEmpty()) {
//			items.addAll(sentChallenges);
//		} else {
//			items.add(new ChallengeHeader("No challenges sent"));
//		}
//
//		items.add(new ChallengeHeader("Challenges you received"));

		if (!_notifications.isEmpty()) {
			items.addAll(_notifications);
		}

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					adapter.notifyDataSetChanged();
				}
			});
		}
	}

	@Override
	public void onItemClick(AdapterView arg0, View arg1, int position, long arg3) {
		Notification item = adapter.getItem(position);

//		if (item.isNewChallenge()) {
//			ChallengeAcceptActivity.show(getActivity(), item.getChallengeId());
//		} else {
//			//Open the challengeViewActivity and give the current selected Challenge to the activity
//			Intent intent = new Intent(getActivity(), ChallengeViewActivity.class);
//			intent.putExtra("challengeId", item.getChallengeId());
//			this.startActivity(intent);
//		}
	}

}
