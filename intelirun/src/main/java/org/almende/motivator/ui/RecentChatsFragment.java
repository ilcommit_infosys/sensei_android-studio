package org.almende.motivator.ui;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Toast;

import com.infy.intelirun.ui.R;

import org.almende.motivator.ConversationDB;
import org.almende.motivator.FriendsDB;
import org.almende.motivator.FriendsHelper;
import org.almende.motivator.adapters.FriendsAdapterNew;
import org.almende.motivator.adapters.RecentChatsAdapterNew;
import org.almende.motivator.misc.CustomCallback;
import org.almende.motivator.models.Conversation;
import org.almende.motivator.models.User;
import org.almende.motivator.service.EventListener;
import org.almende.motivator.service.MotivatorService;

import java.util.ArrayList;

//import com.google.analytics.tracking.android.EasyTracker;

//import com.facebook.model.GraphUser;

// todo: DE how to solve friends profiles? If they are stored in the sense DB, they can't be accessed
// by another user. only if the sensor is shared. but then need to distinguish between public profile
// and private profile data

public class RecentChatsFragment extends Fragment implements EventListener {

	private static final String TAG = RecentChatsFragment.class.getCanonicalName();

	private RecentChatsAdapterNew _recentChatsAdapter;
	private boolean manageFriends = true;
	private int positionSelectedFriend = 0;

	private FriendsDB _friendsDB;
//	private ArrayList<User> _followedFriends = new ArrayList<>();
	private User _selectedFriend;

	private AutoCompleteTextView _edtSearch;

	private ArrayList<User> _friends = new ArrayList<>();
//	private String[] facebookFriendsId;
//	private String[] facebookFriendsName;
	private ListView _lvRecentChats;

	private ArrayList<Conversation> _recentChats = new ArrayList<>();
	private ConversationDB _conversationDB;
	private FriendsAdapterNew _autoCompleteAdapter;

	private View _background;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		_conversationDB = ConversationDB.getInstance(getActivity());

		_friendsDB = FriendsDB.getInstance(getActivity().getApplicationContext());

		initUI();
		getFriends();

		Intent intent = new Intent(getActivity(), MotivatorService.class);
		getActivity().bindService(intent, _connection, Context.BIND_AUTO_CREATE);

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().unbindService(_connection);
	}

	private void initUI() {

		_background = getActivity().findViewById(R.id.background);
		_background.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				close();
			}
		});

		_edtSearch = (AutoCompleteTextView) getActivity().findViewById(R.id.edtFriendsSearch);
//		ArrayAdapter<String> autoCompleteAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, facebookFriendsName);
		_autoCompleteAdapter = new FriendsAdapterNew(getActivity(), _friends);
		_edtSearch.setAdapter(_autoCompleteAdapter);
		_edtSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				startActivity(new Intent(getActivity(), MessageCreateActivity.class));
//				getActivity().overridePendingTransition(R.anim.slide_in_right,
//						R.anim.slide_out_left);
				User user = _autoCompleteAdapter.getItem(position);
				close();
				ChatActivity.show(getActivity(), user.getFacebookId(), user.getName());
			}
		});

		_lvRecentChats = (ListView) getActivity().findViewById(R.id.lvFriendsRecentChats);
//		_recentChatsAdapter = new RecentChatsAdapterNew(getActivity());

		_recentChatsAdapter = new RecentChatsAdapterNew(getActivity(), _recentChats);
		_lvRecentChats.setAdapter(_recentChatsAdapter);
		_lvRecentChats.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Conversation conversation = _recentChatsAdapter.getItem(position);

//				Intent intent = new Intent(getActivity(), MessageViewActivity.class);
//				intent.putExtra("partnerId", conversation.getPartnerId());
//				startActivity(intent);
				close();
				ChatActivity.show(getActivity(), conversation.getPartnerId(), conversation.getPartnerName());
			}
		});
	}

	private MotivatorService _service;
	private ServiceConnection _connection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MotivatorService.MotivatorBinder binder = (MotivatorService.MotivatorBinder) service;
			_service = binder.getService();
			_service.registerEventListener(RecentChatsFragment.this);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {

		}
	};

	@Override
	public void onEvent(Event event) {
		switch(event) {
			case CONVERSATION_UPDATE:
				updateRecentChats();
				break;
		}
	}

	private void close() {
		getFragmentManager().beginTransaction()
				.remove(RecentChatsFragment.this)
				.commit();
		getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	private void getFriends() {

//		_friends = FriendsDB.getInstance(getActivity().getApplicationContext()).getFollowedFriends();
		FriendsHelper.getInstance(getActivity()).retreiveFacebookFriends(new CustomCallback() {
			@Override
			public Object callback(Object object) {
				_friends.addAll((ArrayList<User>)object);
				_autoCompleteAdapter.notifyDataSetChanged();

//				_autoCompleteAdapter = new FriendsAdapterNew(getActivity(), _friends);
//				_edtSearch.setAdapter(_autoCompleteAdapter);

				if (!_friends.isEmpty()) {
//					hasFriends = true;
				} else {
					Toast.makeText(getActivity().getApplicationContext(), "You need to follow at least one friend first.", Toast.LENGTH_SHORT).show();
				}


				return null;
			}
		});

	}

	@Override
	public void onResume() {
		super.onResume();
		updateRecentChats();
	}

	private void updateRecentChats() {
		_recentChats.clear();
		_recentChats.addAll(_conversationDB.getConversations());

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					_recentChatsAdapter.notifyDataSetChanged();
				}
			});
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater
				.inflate(R.layout.base_recent_chats, container, false);
	}

//	@Override
//	public void onStart() {
//		super.onStart();
//		//google analytics
//		EasyTracker.getInstance(getActivity()).activityStart(getActivity());  // Add this method.
//	}

//	@Override
//	public void onStop() {
//		super.onStop();
//		//google analytics
//		EasyTracker.getInstance(getActivity()).activityStop(getActivity());  // Add this method.
//	}

//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//	}

//	private AlertDialog helpDialog;

//	private void showPopUpUnfollow() {
//		LayoutInflater inflater = getActivity().getLayoutInflater();
//
//		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(getActivity());
//		View v = inflater.inflate(R.layout.popup_friend, null);
//		v.findViewById(R.id.btnUnfollowFriend).setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				onUnfollowFriendPressed(v);
//			}
//		});
//		v.findViewById(R.id.btnVisitProfile).setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				onVisitProfilePressed(v);
//			}
//		});
//		helpBuilder.setView(v);
//
//		helpDialog = helpBuilder.create();
//		helpDialog.show();
//	}

//	public void onVisitProfilePressed(View v) {
//		Intent displayFriend = new Intent(getActivity(), ProfileActivity.class);
//		displayFriend.putExtra("viewFriendProfile", true);
//        displayFriend.putExtra("facebookIdFriend", _recentChatsAdapter.getItem(positionSelectedFriend).getFacebookId());
//
//		startActivity(displayFriend);
//		getActivity().overridePendingTransition(R.anim.slide_in_right,
//				R.anim.slide_out_left);
//		helpDialog.dismiss();
//	}

//	public void onUnfollowFriendPressed(View v) {
//		_friendsDB.removeFriend(_selectedFriend);
//		_recentChatsAdapter.removeModel(positionSelectedFriend);
//		helpDialog.dismiss();
//	}

   /* private void showPopUp(final int position) {
		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		helpBuilder.setTitle(friend.getName());
		helpBuilder.setMessage("Do you want to follow " + friend.getName());
		helpBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						_recentChatsAdapter.removeModel(position);
						new DatabaseThread().execute("insert");
					}
				});

		helpBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do nothing
			}
		});

		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();
	}*/

	/*
	public ArrayList compareFriends() {
		ArrayList<GraphUser> facebookFriends = Cookie.getInstance().facebookFriends;
		ArrayList<BasicDBObject> currentFriends = new ArrayList<BasicDBObject>();
		try {
			currentFriends = user.getFriends();
		} catch (Exception e) {
			System.out.println(e);
			return currentFriends;
		}

		ArrayList<GraphUser> result = new ArrayList<GraphUser>();

		if (currentFriends != null) {
			Set<String> set = new HashSet<String>();
			//fill our set, we are going to compare strings
			for (BasicDBObject aFriend : currentFriends) {
				set.add((String) aFriend.get("facebookID"));
			}

			if (manageFriends) {
				//we want only to show the facebookfriends that the user follows so that he can manage it
				for (GraphUser facebookFriend : facebookFriends) {
					if (set.contains(facebookFriend.getChallengeId())) {
						result.add(facebookFriend);
					}
				}
				Collections.sort(result, sortUsers);
				return result;
			}

			//compare strings and put the facebook user that's not yet followed by the user in result
			for (GraphUser facebookFriend : facebookFriends) {
				if (!set.contains(facebookFriend.getChallengeId())) {
					result.add(facebookFriend);
				}
			}
			Collections.sort(result, sortUsers);
			return result;
		}
		return currentFriends;

	}

	public ArrayList<GraphUser> hasSportopiaAccount(ArrayList<GraphUser> facebookFriends) {

		//we use this method so that in the friendlist only the facebookfriends with a sportopia account are showed
		ArrayList<GraphUser> result = new ArrayList<GraphUser>();

		if (facebookFriends != null) {
			Set<String> set = new HashSet<String>();
			//fill our set, we are going to compare strings
			for (DBObject aFriend : allUsers) {
				set.add((String) aFriend.get("facebookID"));
			}

			//compare strings and put the facebook user that also has a sportopia account in the array
			for (GraphUser facebookFriend : facebookFriends) {
				if (set.contains(facebookFriend.getChallengeId())) {
					result.add(facebookFriend);
				}
			}
			return result;
		}
		return result;
	}*/

//	public void onFollowFriendsPressed(View v) {
//		startActivity(new Intent(getActivity(), FollowFriendActivity.class));
//		getActivity().overridePendingTransition(R.anim.slide_in_right,
//				R.anim.slide_out_left);
//	}

//	public void onInvitePressed(View v){
//		if (Cookie.getInstance().internet) {
//			FacebookManager.sendInvite(getActivity());
////			FacebookManager.sendGameInvite(getActivity());
//		}
//	}

}
