package org.vu.run2gether.ui;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.infy.intelirun.ui.R;

import org.hva.createit.scl.dataaccess.Run2GetherDAO;
import org.vu.run2gether.Run2getherService;
import org.vu.run2gether.RunnerData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NearbyRunnersActivity extends ListActivity {

    private final String TAG = "NearbyRunnersActivity";
    private static SimpleDateFormat sdf = new SimpleDateFormat("MMM d, HH:mm");

    List<RunnerData> nearbyRunners = new ArrayList<RunnerData>();
    NearbyRunnersAdapter runnersAdapter = new NearbyRunnersAdapter();

    IntentFilter newRunnersIntentFilter = new IntentFilter();
    BroadcastReceiver newRunnersReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (Run2getherService.ACTION_NEW_RUNNER_FOUND.equals(action)) {
                updateRunnersData();
            }
        }
    };

    class NearbyRunnersAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return nearbyRunners.size();
        }

        @Override
        public Object getItem(int position) {
            return nearbyRunners.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @SuppressWarnings("deprecation")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(NearbyRunnersActivity.this)
                        .inflate(R.layout.run2gether_list_item, null);
            }

            ((TextView) (convertView.findViewById(R.id.username)))
                    .setText(formatUsername((RunnerData) getItem(position)));
            ((TextView) (convertView.findViewById(R.id.points)))
                    .setText(((RunnerData) getItem(position)).getPoints() + " points");
            ((TextView) (convertView.findViewById(R.id.encountered)))
                    .setText(formatLastSeen((RunnerData) getItem(position)));

            return convertView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.run2gether_activity_main);
        setListAdapter(runnersAdapter);
        newRunnersIntentFilter.addAction(Run2getherService.ACTION_NEW_RUNNER_FOUND);
        updateRunnersData();
    }

    private void updateRunnersData() {
        Run2GetherDAO run2GetherDAO = new Run2GetherDAO(this);
        List<RunnerData> allRunnerData = run2GetherDAO.getAllRunnerData();

        Collections.sort(allRunnerData, new Comparator<RunnerData>() {
            @Override
            public int compare(RunnerData runnerData1, RunnerData runnerData2) {
                return -runnerData1.getLastSeen().compareTo(runnerData2.getLastSeen());
            }
        });

        nearbyRunners.clear();
        nearbyRunners.addAll(allRunnerData);
        runnersAdapter.notifyDataSetChanged();
    }

    private String formatUsername(RunnerData runnerData) {
        String username = runnerData.getRunnerId();
        return username.replaceFirst("@.*", "");
    }

    private String formatLastSeen(RunnerData runnerData) {
        return "Last seen on " + sdf.format(runnerData.getLastSeen());
    }

    @Override
    protected void onStart() {
        registerReceiver(newRunnersReceiver, newRunnersIntentFilter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(newRunnersReceiver);
        super.onStop();
    }

}
