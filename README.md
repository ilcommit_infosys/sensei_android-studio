**Sensei_AndroidStudio** project is the gradle version of SensorCollectionLibrary and SenseiGUI projects. 

*The project contains:*


* *SenseiGUI*: GUI layer containing screens and navigation for COMMIT/SENSeI app. Depends on AndroidAffectButton, SensorCollectionLibrary and sendmelogs.
* *SensorCollectionLibrary*: Mobile library for sensor data collection, storage and cloud communication. Depends on sense-android-library, Android-ReactiveLocation, common-math3, eventbus

You can directly download and set up the given repository as gradle workspace in Android Studio.


Use **key.jks** included in the repository for generating apk. The key and sha are already registered at facebook and google developer page. 

Project information available at this [link](http://www.commit-nl.nl/projects/sensei-sensor-based-engagement-for-improved-health). 


Watch COMMIT/SENSeI on [YouTube](https://www.youtube.com/watch?v=bq7Hwk2z82I).