package uni.ut.behaviouralmotivation;

import java.util.Map;

public class User {
	int userNumber;
	Map<String, Integer> personality; //the summed answer scores for the personality questions, index 0 = Openness, index 1 = Conscientiousness etc.
    int stageOfChange;
    Messagelist messageListForUser;
    int condition;

// here we define what a user is, it has a number, personality scores, a stage of change score, and a personalized message list and a condition, to test stuff.
// and a setter
    
    User(int userNumber, Map<String, Integer> personality, int stageOfChange, Messagelist messageListForUser, int condition) {
        this.userNumber = userNumber;
        this.personality = personality;
        this.stageOfChange = stageOfChange;
        this.messageListForUser = messageListForUser;
        this.condition = condition;
    }
    
    public int getUserNumber() {
        return this.userNumber;
    }

    public Map<String, Integer> getPersonality() {
        return this.personality;
    }

    public int getStageOfChange() {
        return this.stageOfChange;
    }

    public Messagelist getMessagelist() {
        return this.messageListForUser;
    }

    public void setUser(int userNumber, Map<String, Integer> personality, int stageOfChange, Messagelist messageListForUser, int condition) {
    	this.userNumber = userNumber;
        this.personality = personality;
        this.stageOfChange = stageOfChange;
        this.messageListForUser = messageListForUser;
    }
    public int getCondition() {
        return this.condition;
    }
}
