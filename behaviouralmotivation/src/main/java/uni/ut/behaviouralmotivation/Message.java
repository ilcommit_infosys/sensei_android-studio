package uni.ut.behaviouralmotivation;

public class Message {

    int number;
    String message;
    String processType;
    boolean ifSent;
    int probabilityNumber;

// here we define what a message is, it has a number, a string message, and string type of process, and a boolean (which is not used yet).
// and a setter
    
    Message(int number, String message, String processType, boolean ifSent, int probabilityNumber) {
        this.number = number;
        this.message = message;
        this.processType = processType;
        this.ifSent = ifSent;
        this.probabilityNumber = probabilityNumber;
    }
    
    public int getNumber() {
        return this.number;
    }

    public String getMessage() {
        return this.message;
    }

    public String getProcessType() {
        return this.processType;
    }

    public boolean getSent() {
        return this.ifSent;
    }
    
    public int getProbabilityNumber() {
    	return this.probabilityNumber;
    }
    public void addProbabilityNumber(int number) {
    	this.probabilityNumber = this.probabilityNumber + number;
    }
    public void setMessage(int number, String message, String processType, boolean ifSent, int probabilityNumber) {
        this.number = number;
        this.message = message;
        this.processType = processType;
        this.ifSent = ifSent;
        this.probabilityNumber = probabilityNumber;
    }
}