package uni.ut.behaviouralmotivation;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Nilanajan_Nath on 3/30/2016.
 */
public class MessageSelector extends IntentService
{
    static private Random random = new Random();
    static private Userlist userList = new Userlist();
    // create full messagelist
    static private Messagelist messageListFull = new Messagelist();
    private Handler mHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
        Log.d("MSS ", "created");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(getApplicationContext(), "Message Selector service started", Toast.LENGTH_LONG).show();
            }
        });
        Log.d("MSS ", "started");
        onCreate();
        return super.onStartCommand(intent, flags, startId);
    }

    public MessageSelector()
    {
        super("MessageSelector");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        onCreate();
        mHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(getApplicationContext(), "Message Selector service started", Toast.LENGTH_LONG).show();
            }
        });

        SharedPreferences prefs = this.getSharedPreferences(getString(R.string.pref_questionnaire), Context.MODE_PRIVATE);
        String anwser = prefs.getString("QuestionnairePersonalityAns", "");

        Log.d("MSS ", anwser);
        try
        {
            JSONObject obj = new JSONObject(anwser);
            String qa1= (String) obj.get("1"); // +n
            String qa2= (String) obj.get("2"); // -n
            String qa3= (String) obj.get("3"); // +e
            String qa4= (String) obj.get("4"); // -e
            String qa5= (String) obj.get("5"); // +o
            String qa6= (String) obj.get("6"); // -o
            String qa7= (String) obj.get("7"); // +a
            String qa8= (String) obj.get("8"); // -a
            String qa9= (String) obj.get("9"); // +c
            String qa10= (String) obj.get("10"); // -c

            Integer nScore = Integer.parseInt(qa1)+ (6-Integer.parseInt(qa2));
            Integer eScore = Integer.parseInt(qa3)+ (6-Integer.parseInt(qa4));
            Integer oScore = Integer.parseInt(qa5)+ (6-Integer.parseInt(qa6));
            Integer aScore = Integer.parseInt(qa7)+ (6-Integer.parseInt(qa8));
            Integer cScore = Integer.parseInt(qa9)+ (6-Integer.parseInt(qa10));

            Map<String, Integer> personalityScore = new HashMap<String, Integer>();


            personalityScore.put("Openness", oScore);
            personalityScore.put("Conscientiousness", cScore);
            personalityScore.put("Extraversion", eScore);
            personalityScore.put("Agreeableness", aScore);
            personalityScore.put("Neuroticism", nScore);

            // random stage to test the methods
            int stageOfChange = Integer.parseInt((String) obj.get("0"));
            // assign user a random condition (either 0 (control TTM) or 1 (tailored personality))
            int condition = 1;
            boolean keepSendingMessages = true;
            String choice;

            // empty message list for user
            Messagelist suitableList = new Messagelist();
            suitableList.clear();
            // list for messages that are already sent to this user (could potentially be used to prevent probability choosing the same category all the time).
            Messagelist messagesAlreadySentList = new Messagelist();
            messagesAlreadySentList.clear();

            Log.d("MSS ", "generating msg list");
            // create a new user
            User user = new User(1, personalityScore, stageOfChange, suitableList, condition);

            // add user to the userlist, do not know if this is useful like this.
            userList.add(user);

            //fill the empty message list for the user with appropriate messages for the user (depending on stage and personality and condition).
            setSuitableMessageList(user);

            // keep sending messages to the user until we do not want to anymore, based on some boolean?
            while(keepSendingMessages)
            {
                Log.d("MSS ", "preparing to display msgs");
                // if there are no more messages to send to the user, refill the list (and therefore start with sending everything again)
                if(user.getMessagelist().isEmpty()) {
                    setSuitableMessageList(user);
                }
                // choose one of these message from this list weighted (i.e. some messages have bigger probability than others based on personality preferences).
                Message messageToUser = pickRandomMessage(user.getMessagelist());

                //add the newest message to the list of all messages already sent to user (could be useful at some point).
                messagesAlreadySentList.add(messageToUser);


                // prints which message is selected for the user. Should be a 'send' to the system later.
                Bundle bundle = intent.getExtras();
                if (bundle != null)
                {
                    Messenger messenger = (Messenger) bundle.get("messenger");
                    android.os.Message msg = android.os.Message.obtain();
                    Bundle b = new Bundle();
                    b.putString("MotivationalMsg", messageToUser.getMessage());
                    msg.setData(b);  //put the data here
                    messenger.send(msg);
                    Log.w("MSS_S", messageToUser.getMessage());
                    Thread.sleep(60000);
                }
                // removes the sent message from the list of possible messages
                user.getMessagelist().remove(messageToUser);
            }

        }
        catch (JSONException e1)
        {
            e1.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public static void setSuitableMessageList(User user) {
        // if user is in stage 1 (precontemplation), and is in tailoring condition, fill list with the relevant messages types
        if (user.getStageOfChange() == 1 && user.condition == 1) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("CR")) {
                    if (user.getPersonality().get("Openness") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Extraversion") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Neuroticism") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("DR")) {
                    if (user.getPersonality().get("Openness") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (user.getPersonality().get("Conscientiousness") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (user.getPersonality().get("Extraversion") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (user.getPersonality().get("Agreeableness") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (user.getPersonality().get("Neuroticism") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (messageListFull.messageList.get(j).getProbabilityNumber() > 0) {
                        user.getMessagelist().add(messageListFull.messageList.get(j));
                    }
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("ER")) {
                    if (user.getPersonality().get("Openness") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (user.getPersonality().get("Neuroticism") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (messageListFull.messageList.get(j).getProbabilityNumber() > 0) {
                        user.getMessagelist().add(messageListFull.messageList.get(j));
                    }
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SOL")) {
                    if (user.getPersonality().get("Openness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Agreeableness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Neuroticism") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SR")) {
                    if (user.getPersonality().get("Openness") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Extraversion") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Agreeableness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Neuroticism") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }
        }
        // for the not tailored condition, just fill the list with the relevant stage messages
        if (user.getStageOfChange() == 1 && user.condition == 0) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("CR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("DR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("ER")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SOL")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }
        }
        // if user is in stage 2, (contemplation)  and is in tailoring condition, fill list with the relevant messages types
        if (user.getStageOfChange() == 2 && user.condition == 1) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("CR")) {
                    if (user.getPersonality().get("Conscientiousness") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Extraversion") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Neuroticism") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("DR")) {
                    if (user.getPersonality().get("Openness") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (user.getPersonality().get("Conscientiousness") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (user.getPersonality().get("Extraversion") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (user.getPersonality().get("Neuroticism") < 7 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (messageListFull.messageList.get(j).getProbabilityNumber() > 0) {
                        user.getMessagelist().add(messageListFull.messageList.get(j));
                    }
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("ER")) {
                    if (user.getPersonality().get("Openness") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (messageListFull.messageList.get(j).getProbabilityNumber() > 0) {
                        user.getMessagelist().add(messageListFull.messageList.get(j));
                    }
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SOL")) {
                    if (user.getPersonality().get("Conscientiousness") > 6 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (user.getPersonality().get("Extraversion") < 7 && messageListFull.messageList.get(j).getProbabilityNumber() >= 20) {
                        messageListFull.messageList.get(j).addProbabilityNumber(-10);
                    }
                    if (messageListFull.messageList.get(j).getProbabilityNumber() > 0) {
                        user.getMessagelist().add(messageListFull.messageList.get(j));
                    }
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }
        }
        // for the not tailored condition, just fill the list with the relevant stage messages
        if (user.getStageOfChange() == 2 && user.condition == 0) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("CR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("DR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("ER")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SOL")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }

        }
        // if user is in stage 3, (preparation) and is in tailoring condition, fill list with the relevant messages types
        if (user.getStageOfChange() == 3 && user.condition == 1) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("SR")) {
                    if (user.getPersonality().get("Openness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Neuroticism") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SEL")) {
                    if (user.getPersonality().get("Agreeableness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("HR")) {
                    if (user.getPersonality().get("Openness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Extraversion") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("CC")) {
                    if (user.getPersonality().get("Openness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Extraversion") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }
        }
        // for the not tailored condition, just fill the list with the relevant stage messages
        if (user.getStageOfChange() == 3 && user.condition == 0) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("SR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SEL")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("HR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("CC")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }
        }
        // if user is in stage 4, (action) and is in tailoring condition, fill list with the relevant messages types
        if (user.getStageOfChange() == 4 && user.condition == 1) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("SEL")) {
                    if (user.getPersonality().get("Neuroticism") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("HR")) {
                    if (user.getPersonality().get("Openness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Neuroticism") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("CC")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("RM")) {
                    if (user.getPersonality().get("Conscientiousness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Neuroticism") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                // if they score 'high' on Agreeableness, already include SC in the possible messages for stage 4
                if (messageListFull.messageList.get(j).getProcessType().equals("SC")) {
                    if (user.getPersonality().get("Conscientiousness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }
        }
        // for the not tailored condition, just fill the list with the relevant stage messages
        if (user.getStageOfChange() == 4 && user.condition == 0) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("SEL")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("HR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("CC")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("RM")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SC")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }

        }
        // if user is in stage 5, (maintenance) and is in tailoring condition, fill list with the relevant messages types
        if (user.getStageOfChange() == 5 && user.condition == 1) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("SEL")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("HR")) {
                    if (user.getPersonality().get("Conscientiousness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Agreeableness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Neuroticism") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("CC")) {
                    if (user.getPersonality().get("Conscientiousness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("RM")) {
                    if (user.getPersonality().get("Conscientiousness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    if (user.getPersonality().get("Neuroticism") < 7) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SC")) {
                    if (user.getPersonality().get("Agreeableness") > 6) {
                        messageListFull.messageList.get(j).addProbabilityNumber(10);
                    }
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }
        }
        // for the not tailored condition, just fill the list with the relevant stage messages
        if (user.getStageOfChange() == 5 && user.condition == 0) {
            for (int j = 0; j < messageListFull.messageList.size(); j++) {
                if (messageListFull.messageList.get(j).getProcessType().equals("SEL")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("HR")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("CC")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("RM")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
                if (messageListFull.messageList.get(j).getProcessType().equals("SC")) {
                    user.getMessagelist().add(messageListFull.messageList.get(j));
                }
            }

        }
    }

    public static Message pickRandomMessage(Messagelist suitableList){
        int totalProbability = 0;
        for(int j = 0; j < suitableList.messageList.size(); j++) {
            totalProbability = totalProbability + suitableList.get(j).probabilityNumber;
            //print the probabilities so you can see the differences in probabilities due to personality scores (if in the right condition).
            //System.out.println("totalprobability so far:" + totalProbability + "and probability of current item: " + suitableList.get(j).probabilityNumber);
        }

        int index = random.nextInt(totalProbability);
        // print the randomly chosen index
        //System.out.println("index:" + index);
        int sum = 0;
        int i=0;
        while(sum < index) {
            sum = sum + suitableList.get(i++).probabilityNumber;
            //see the iteration through the probabilities (and therefore through the messagelist until it arrives at the right index).
            //System.out.println("sum so far:" + sum);
        }

        System.out.println("Message chosen by probability: " + suitableList.get(Math.max(0,i-1)).processType + ": " + suitableList.get(Math.max(0,i-1)).message);
        return suitableList.get(Math.max(0,i-1));
    }
}
