package uni.ut.behaviouralmotivation;

import android.app.Activity;
import android.content.res.AssetManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Here we make a list of all the possible messages, with their parameters
 */
public class Messagelist extends Activity {

    public ArrayList<Message> messageList;

    Messagelist() {

        messageList = new ArrayList<Message>();
        
        // read in a .csv file with the right delimiter instead of hardcoding the messages. Hopefully will make it easier to add or remove messages
        int number = 0;
        /*try {
			InputStream is = getAssets().open("bcMessages.csv");
			Scanner s = new Scanner(is);
        	while(s.hasNextLine()){
        		String line = s.nextLine();   
        		while (line != null) {
        			String[] parts = line.split(";");
        			number++;
            		String message = parts[1];
           			String type = parts[0];
           			Message bcMessage = new Message(number, message, type, false, 20);
           			messageList.add(bcMessage);
           			line = null;
        		}
        	}        			
        s.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
			e.printStackTrace();
		}*/

		/** Make the message objects */
        Message CR1 = new Message(1, "If you exercise, your quality of life will be much higher than it is now.", "CR", false, 20);
        Message CR2 = new Message(2, "Exercise helps lower your risk of heart attack and stoke.", "CR", false, 20);
        Message CR3 = new Message(3, "Exercise helps keep blood sugar, and blood pressure under control. Those who regularly work out are 3 times less likely to develop these problems.", "CR", false, 20);
        Message CR4 = new Message(4, "Exercise will help clear your mind and reduce stress.", "CR", false, 20);
        Message CR5 = new Message(5, "Regular exercise will keep you strong and raise your stamina levels.", "CR", false, 20);

        Message DR1 = new Message(1, "Unless some changes are made in your weight you could risk getting a heart attack,stroke or something else that could effect your life.", "DR", false, 20);
        Message DR2 = new Message(2, "You need to exercise before it takes a toll on your body!", "DR", false, 20);
        Message DR3 = new Message(3, "Start working out before its too late, you're not getting any younger!", "DR", false, 20);
        Message DR4 = new Message(4, "Don't let yourself get old and regret not taking care of your body and your health. Keep at it every day and remember what you're working for.", "DR", false, 20);
        Message DR5 = new Message(5, "It's easier to wake up early in the morning and workout, than it is to look in the mirror and not like what you see.", "DR", false, 20);

        Message ER1 = new Message(1, "You owe it to your family and friends to take care of yourself.", "ER", false, 20);
        Message ER2 = new Message(2, "Take care of yourself so your health doesn't become a burden on other people.", "ER", false, 20);
        Message ER3 = new Message(3, "Your loved ones want you to be around so you should exercise to get healthy.", "ER", false, 20);
        Message ER4 = new Message(4, "Your friends and family are counting on you!", "ER", false, 20);
        Message ER5 = new Message(5, "Remember, you are doing this for your friends and family as much as yourself.", "ER", false, 20);

        Message SOL1 = new Message(1, "The local gym has lots of fun classes you can check out; you'll get fit, and meet new people!", "SOL", false, 20);
        Message SOL2 = new Message(2, "Try some social exercising. Take a yoga or pilates class. Make some friends who tempt you to have good habits this week.", "SOL", false, 20);
        Message SOL3 = new Message(3, "Look online for a good beginner workout.", "SOL", false, 20);
        Message SOL4 = new Message(4, "You're not alone! Tons of people are working to exercise more frequently!", "SOL", false, 20);
        Message SOL5 = new Message(5, "If you want to start exercising, you could always go to a gym; you can preserve your routine regardless of the weather.", "SOL", false, 20);

        Message SR1 = new Message(1, "Think about all of the benefits of becoming healthy.", "SR", false, 20);
        Message SR2 = new Message(2, "Imagine being in the best shape of your life with a long future ahead of you.", "SR", false, 20);
        Message SR3 = new Message(3, "In 10 years, will you be glad you watched television, or upset that you didn't exercise?", "SR", false, 20);
        Message SR4 = new Message(4, "You will be amazed at the changes you will see in yourself after exercising regularly!", "SR", false, 20);
        Message SR5 = new Message(5, "Imagine what you'll look like next year if you start now", "SR", false, 20);

        Message SEL1 = new Message(1, "Today is the day to get moving and get healthy", "SEL", false, 20);
        Message SEL2 = new Message(2, "Decide today to do some exercise.", "SEL", false, 20);
        Message SEL3 = new Message(3, "This will get easier the more you do it. Keep at it!", "SEL", false, 20);
        Message SEL4 = new Message(4, "The hardest thing is always starting, you can do this!", "SEL", false, 20);
        Message SEL5 = new Message(5, "You can and will achieve your fitness goals!", "SEL", false, 20);

        Message HR1 = new Message(1, "I know how hard it is to find time to exercise, but think of how much time you will save by being stronger and more efficient.", "HR", false, 20);
        Message HR2 = new Message(2, "You should exercise more. I know it's hard, but it's worth it to keep going. You'll feel so much better in the long term is you don't give up now.", "HR", false, 20);
        Message HR3 = new Message(3, "I believe in you; you can incorporate exercise into your daily life.", "HR", false, 20);
        Message HR4 = new Message(4, "I'm proud of you for wanting to be healthy, keep at it.", "HR", false, 20);
        Message HR5 = new Message(5, "I'm proud of you for wanting to get in shape.", "HR", false, 20);

        Message CC1 = new Message(1, "Activity doesn't have to be a chore, find something fun to do!", "CC", false, 20);
        Message CC2 = new Message(2, "Find something active yet  fun to do today, so that you'll enjoy your exercise more.", "CC", false, 20);
        Message CC3 = new Message(3, "Don't forget to exercise often, it's more rewarding than watching TV.", "CC", false, 20);
        Message CC4 = new Message(4, "Think of exercise as a way to let go and relax your mind.", "CC", false, 20);
        Message CC5 = new Message(5, "Try a new location to jog in. Keep your mind active while you exercise.", "CC", false, 20);

        Message RM1 = new Message(1, "You are doing a good job, keep up the good lifestyle.", "RM", false, 20);
        Message RM2 = new Message(2, "You're doing great- you should be proud of yourself!", "RM", false, 20);
        Message RM3 = new Message(3, "Congratulations on working towards a healthier you!", "RM", false, 20);
        Message RM4 = new Message(4, "Be proud of yourself for sticking with the plan.", "RM", false, 20);
        Message RM5 = new Message(5, "Don't forget to stop and appreciate your own hard work today.", "RM", false, 20);

        Message SC1 = new Message(1, "Find at least 30 minutes in your schedule today for some exercise.", "SC", false, 20);
        Message SC2 = new Message(2, "Get out there and exercise!", "SC", false, 20);
        Message SC3 = new Message(3, "Always plan your daily activities allowing room for your exercise.", "SC", false, 20);
        Message SC4 = new Message(4, "Schedule your workouts on your calendar.", "SC", false, 20);
        Message SC5 = new Message(5, "Have you worked out yet today? Now's the perfect time! Get up, get going!", "SC", false, 20);

        //* Add the message objects to the list
        messageList.add(CR1);
        messageList.add(CR2);
        messageList.add(CR3);
        messageList.add(CR4);
        messageList.add(CR5);

        messageList.add(DR1);
        messageList.add(DR2);
        messageList.add(DR3);
        messageList.add(DR4);
        messageList.add(DR5);

        messageList.add(ER1);
        messageList.add(ER2);
        messageList.add(ER3);
        messageList.add(ER4);
        messageList.add(ER5);

        messageList.add(SOL1);
        messageList.add(SOL2);
        messageList.add(SOL3);
        messageList.add(SOL4);
        messageList.add(SOL5);

        messageList.add(SR1);
        messageList.add(SR2);
        messageList.add(SR3);
        messageList.add(SR4);
        messageList.add(SR5);

        messageList.add(SEL1);
        messageList.add(SEL2);
        messageList.add(SEL3);
        messageList.add(SEL4);
        messageList.add(SEL5);

        messageList.add(HR1);
        messageList.add(HR2);
        messageList.add(HR3);
        messageList.add(HR4);
        messageList.add(HR5);

        messageList.add(CC1);
        messageList.add(CC2);
        messageList.add(CC3);
        messageList.add(CC4);
        messageList.add(CC5);

        messageList.add(RM1);
        messageList.add(RM2);
        messageList.add(RM3);
        messageList.add(RM4);
        messageList.add(RM5);

        messageList.add(SC1);
        messageList.add(SC2);
        messageList.add(SC3);
        messageList.add(SC4);
        messageList.add(SC5);
        
    }

	public void clear() {
		messageList.clear();
	}

	public void add(Message message) {
		messageList.add(message);
	}
	
	public void remove(Message message) {
		messageList.remove(message);
	}
	public boolean isEmpty() {
		if (messageList.size() == 0) {
			return true;
		} else {
			return false;
		}
		 
	}

	public int size() {
		return messageList.size();
	}

	public Message get(int index) {
		return messageList.get(index);
	}          
}