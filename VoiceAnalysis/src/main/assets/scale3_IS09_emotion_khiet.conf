
///////////////////////////////////////////////////////////////////////////////////////
///////// > openSMILE configuration file for IS09 emotion challenge< //////////////////
/////////                                                            //////////////////
///////// (c) audEERING UG (haftungsbeschr�nkt),                     //////////////////
/////////     All rights reserverd.                                  //////////////////
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
;
; This section is always required in openSMILE configuration files
;   it configures the componentManager and gives a list of all components which are to be loaded
; The order in which the components are listed should match 
;   the order of the data flow for most efficient processing
;
///////////////////////////////////////////////////////////////////////////////////////
[componentInstances:cComponentManager]
 ; this line configures the default data memory:
instance[dataMemory].type=cDataMemory
instance[waveIn].type=cWaveSource
instance[fr1].type=cFramer
instance[pe2].type=cVectorPreemphasis
instance[w1].type=cWindower
instance[fft1].type=cTransformFFT
instance[fftmp1].type=cFFTmagphase
instance[mspec].type=cMelspec
instance[mfcc].type=cMfcc
instance[mzcr].type=cMZcr
instance[acf].type=cAcf
instance[cepstrum].type=cAcf
instance[pitchACF].type=cPitchACF
instance[energy].type=cEnergy
instance[lld].type=cContourSmoother
instance[delta1].type=cDeltaRegression
instance[functL1].type=cFunctionals
;instance[arffsink].type=cArffSink
;instance[libsvmsink].type=cLibsvmSink
printLevelStats=0
nThreads=1

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////   component configuration  ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
; the following sections configure the components listed above
; a help on configuration parameters can be obtained with 
;  SMILExtract -H
; or
;  SMILExtract -H configTypeName (= componentTypeName)
/////////////////////////////////////////////////////////////////////////////////////////////

[waveIn:cWaveSource]
writer.dmLevel=wave
filename=\cm[inputfile(I){test.wav}:name of input file]
buffersize=4000
monoMixdown=1

[fr1:cFramer]
reader.dmLevel=wave
writer.dmLevel=frames
copyInputName = 1
noPostEOIprocessing = 1
frameSize = 0.0250
frameStep = 0.010
frameMode = fixed
frameCenterSpecial = left
buffersize = 1000

[pe2:cVectorPreemphasis]
reader.dmLevel=frames
writer.dmLevel=framespe
copyInputName = 1
processArrayFields = 1
k=0.97
de = 0

[w1:cWindower]
reader.dmLevel=framespe
writer.dmLevel=winframe
copyInputName = 1
processArrayFields = 1
winFunc = ham
gain = 1.0
offset = 0

  // ---- LLD -----

[fft1:cTransformFFT]
reader.dmLevel=winframe
writer.dmLevel=fftc
copyInputName = 1
processArrayFields = 1
inverse = 0

[fftmp1:cFFTmagphase]
reader.dmLevel=fftc
writer.dmLevel=fftmag
copyInputName = 1
processArrayFields = 1
inverse = 0
magnitude = 1
phase = 0

[mspec:cMelspec]
reader.dmLevel=fftmag
writer.dmLevel=mspec1
copyInputName = 1
processArrayFields = 1
htkcompatible = 1
nBands = 26
usePower = 0
lofreq = 0
hifreq = 8000
inverse = 0
specScale = mel

[mfcc:cMfcc]
reader.dmLevel=mspec1
writer.dmLevel=mfcc1
copyInputName = 1
processArrayFields = 1
firstMfcc = 1
lastMfcc =  12
cepLifter = 22.0
htkcompatible = 1


[acf:cAcf]
reader.dmLevel=fftmag
writer.dmLevel=acf
nameAppend = acf
copyInputName = 1
processArrayFields = 1
usePower = 1
cepstrum = 0

[cepstrum:cAcf]
reader.dmLevel=fftmag
writer.dmLevel=cepstrum
nameAppend = acf
copyInputName = 1
processArrayFields = 1
usePower = 1
cepstrum = 1

[pitchACF:cPitchACF]
  ; the pitchACF component must ALWAYS read from acf AND cepstrum in the given order!
reader.dmLevel=acf;cepstrum
writer.dmLevel=pitch
copyInputName = 1
processArrayFields=0
maxPitch = 500
voiceProb = 1
voiceQual = 0
HNR = 0
F0 = 1
F0raw = 0
F0env = 0
voicingCutoff = 0.550000

[energy:cEnergy]
reader.dmLevel=winframe
writer.dmLevel=energy
nameAppend=energy
rms=1
log=0

[mzcr:cMZcr]
reader.dmLevel=frames
writer.dmLevel=mzcr
copyInputName = 1
processArrayFields = 1
zcr = 1
amax = 0
mcr = 0
maxmin = 0
dc = 0

[lld:cContourSmoother]
reader.dmLevel=energy;mfcc1;mzcr;pitch
writer.dmLevel=lld
writer.levelconf.nT=10
;writer.levelconf.noHang=2
writer.levelconf.isRb=0
writer.levelconf.growDyn=1
nameAppend = sma
copyInputName = 1
noPostEOIprocessing = 0
smaWin = 3

// ---- delta regression of LLD ----
[delta1:cDeltaRegression]
reader.dmLevel=lld
writer.dmLevel=lld_de
writer.levelconf.isRb=0
writer.levelconf.growDyn=1
nameAppend = de
copyInputName = 1
noPostEOIprocessing = 0
deltawin=2
blocksize=1

[functL1:cFunctionals]
reader.dmLevel=lld;lld_de
writer.dmLevel=func
copyInputName = 1
 ; frameSize and frameStep = 0 => functionals over complete input
 ; (NOTE: buffersize of lld and lld_de levels must be large enough!!)
frameSize = 0
frameStep = 0
frameMode = full
frameCenterSpecial = left
functionalsEnabled=Extremes;Regression;Moments
Extremes.max = 0
Extremes.min = 0
Extremes.range = 1
Extremes.maxpos = 0
Extremes.minpos = 0
Extremes.amean = 1
Extremes.maxameandist = 0
Extremes.minameandist = 0
 ; Note: the much better way to normalise the times of maxpos and minpos
 ; is 'turn', however for compatibility with old files the default 'frame'
 ; is kept here:
Extremes.norm = frame
Regression.linregc1 = 1
Regression.linregc2 = 1
Regression.linregerrA = 0
Regression.linregerrQ = 0
Regression.qregc1 = 0
Regression.qregc2 = 0
Regression.qregc3 = 0
Regression.qregerrA = 0
Regression.qregerrQ = 0
Regression.centroid = 0
Regression.oldBuggyQerr = 0
Regression.normInputs = 0
Regression.normRegCoeff = 0
Moments.variance = 0
Moments.stddev = 1
Moments.skewness = 1
Moments.kurtosis = 1
Moments.amean = 0

;;;;;;;;; prepare features for standard output module

;; NOTE: no concattenation to levels lld, lld_de and func needed,
;; as data are already saved correctly in these levels
;;\{standard_data_output.conf.inc}

[componentInstances:cComponentManager]
instance[arffsink].type=cArffSink

[arffsink:cArffSink]
reader.dmLevel=func
 ; do not print "frameNumber" attribute to ARFF file
frameIndex = 0
frameTime = 0
 ; name of output file as commandline option
filename=\cm[output(O){ArffOutput.arff}:name of WEKA Arff output file]
 ; name of @relation in the ARFF file
relation=\cm[relation{openSMILE_features}:arff relation attribute, feature set name and/or corpus name]

 ;;;;;configuration of commandline options for target classes in an ARFF file
class[0].name = class
class[0].type = {low,middle,high}  
target[0].all = \cm[class{unknown}:string value for labelA]
 ;;;;
append=\cm[appendstaticarff{1}:set to 0 to disable appending to an existing arff parameter summary file, given by the arffoutput option]
errorOnNoOutput = 0



//////---------------------- END -------------------------///////
