package com.example.meiru.voicerecording;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/**
 * @author Meiru Mu <m.mu@utwente.nl>
 * @description Exertion detection class to detect exertion
 */
public class ExertionDetection extends Activity {
String path=Environment.getExternalStorageDirectory().toString()
        + "/intelirun/Voice";


	//sample rate can be 22050 or 16000
	int sampleFreq = 22050;
	File file;
	String[] genderText = {"female", "male"};
	String gender="";
	SharedPreferences sharedPref;

	Boolean recording;

	/**
	 * @description Sets the gender
	 * @param gender The gender
	 */
	protected void setGender(String gender){
		this.gender=gender;
	  	// sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
	}
   
    /**
	 * @description Constructor
	 */
    public ExertionDetection(){}
    
    
    /**
	 * @description Initialises a thread for recording audio
	 */
    public void startRec(){
            Thread recordThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    recording = true;
                    startRecord();
                }
            });

            recordThread.start();
/*
            startRec.setEnabled(false);
            stopRec.setEnabled(true);
            playBack.setEnabled(true);
*/

    }

    /**
	 * @description Stops the recording of an audio sample
	 */
    public void stopRec() {
            recording = false;
/*
            startRec.setEnabled(true);

            playBack.setEnabled(true);
            stopRec.setEnabled(false);
*/
        }


    public void playBackOn(){
            Thread playRecordThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    recording = false;
                   // playRecord();
                }
            });

            playRecordThread.start();

        }







    /**
	 * @description Starts the recording of an audio sample
	 */
    private void startRecord(){
        String str=System.currentTimeMillis()+"";
        new File(path).mkdirs();
        this.file = new File(path,str+".pcm");

        try {
            //if (!file.exists()) {
                file.createNewFile();
            //}
            OutputStream outputStream = new FileOutputStream(file);
            BufferedOutputStream bufferedOutputStream = new
                    BufferedOutputStream(outputStream);
            DataOutputStream dataOutputStream = new
                    DataOutputStream(bufferedOutputStream);

            int minBufferSize = AudioRecord.getMinBufferSize(sampleFreq,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO,AudioFormat.ENCODING_PCM_16BIT);

            short[] audioData = new short[minBufferSize];

            AudioRecord audioRecord = new
                    AudioRecord(MediaRecorder.AudioSource.MIC,
                    sampleFreq,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    minBufferSize
                    );

            audioRecord.startRecording();

            while (recording){
                int numberOfShort = audioRecord.read(audioData, 0, minBufferSize);
                for(int i=0; i<numberOfShort; i++){
                    dataOutputStream.writeShort(audioData[i]);
                }
            }

        audioRecord.stop();
        dataOutputStream.close();
        audioRecord.release();

        } catch (IOException e){
            Log.d("blaat","Caught IOE exception during speech recording:"+e.getMessage());
        }

    }

    /*private void playRecord(){

        File file = new File(path,"newtest.pcm");

        Log.d("blaat","Recording dir:" + file.getAbsolutePath());

        int shortSizeInBytes = Short.SIZE/Byte.SIZE;
        int bufferSizeInBytes = (int)(file.length()/shortSizeInBytes);
        short[] audioData = new short[bufferSizeInBytes];

        try{
            InputStream inputStream = new FileInputStream(file);
            try{
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                DataInputStream dataInputStream = new DataInputStream(bufferedInputStream);

                int i = 0;
                while(dataInputStream.available() > 0){
                    audioData[i] = dataInputStream.readShort();
                    i++;
                }

                dataInputStream.close();


                AudioTrack audioTrack = new AudioTrack(
                    AudioManager.STREAM_MUSIC,
                    sampleFreq,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    bufferSizeInBytes,
                    AudioTrack.MODE_STREAM);

                audioTrack.play();
                audioTrack.write(audioData,0,bufferSizeInBytes);


            } catch (FileNotFoundException e){
                e.printStackTrace();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }*/


    /**
	 * @description This function copies file fn from assets to apps data/files dir
	 * @param fn Filename of file to copy
	 * @return Nothing
	 */
    private void copyFile(String fn) {
        try {
            InputStream in = getApplicationContext().getAssets().open(fn);
            FileOutputStream out = new FileOutputStream(getApplicationContext().getFilesDir().getAbsolutePath()+"/"+fn);
            int read;
            byte[] buffer = new byte[4096];
            while ((read = in.read(buffer)) > 0) {
                out.write(buffer, 0, read);
            }
            out.close();
            in.close();
            File bla = new File(getApplicationContext().getFilesDir().getAbsolutePath()+"/"+fn);
            //Log.d("blaat", "FN:"+fn +" exec: " + bla.canExecute());

        } catch (IOException e) {
            Log.d("blaat", "Caught copy error"+e.toString());
        }
    }

    static {
        try {
            Log.d("blaat","Loading libopensmile.so");
            System.loadLibrary("opensmile");
        }catch (UnsatisfiedLinkError e) {
            Log.d("blaat","load lib error"+e.getMessage());
        }
    }

    /**
	 * @description Do the feature extraction and classification of the audio sample using Weka.
	 * @return map Map with the audio sample filename and the colour expressing the level of exertion.
	 */
    protected HashMap<String,Integer> fatigueDetection(){
        // TODO: Check if cache dir is writable
       /* File yourDir = new File(path);
        List<String> list=new ArrayList<String>();
        for (File f : yourDir.listFiles()) {
            if (f.isFile()) {
                String filename=f.getName();
                String filenameArray[] = filename.split("\\.");
                String extension = filenameArray[filenameArray.length-1];
                if(extension.trim().equals("pcm"));
                    list.add(f.getName());
            }
        }

        Collections.sort(list);
       File inFile = new  File(path,list.get(list.size()-1));*/
        File inFile=file;
        Log.d("blaat","Sample name: "+inFile.getName());
        Log.d("blaat","Sample size: "+inFile.length() +" bytes");

        // Check whether audio sample is long enough.
        float sampleLength = (float)inFile.length()/(2*sampleFreq);
        if (sampleLength < 6.0) {
            final String promptSmilextract =
                    "Please speak longer than 6 seconds...";

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ExertionDetection.this,promptSmilextract,Toast.LENGTH_LONG).show();
                }
            });
            HashMap map=new HashMap<String, Integer>();
            map.put(inFile.getName(),-1);
            return map;
        }
        Log.d("blaat","Audio sample length: "+sampleLength+"s");
        File cacheDir = new File(path);
        File outDir = new File(cacheDir,"ChoppedAudio");
        if (!outDir.exists()) {
            outDir.mkdir();
        }


        String modelFileName_f = "scale3_60_80_IS09_emotion_khiet_f_all_cv.arff.PolyK.model";
        String modelFileName_m = "scale3_60_80_IS09_emotion_khiet_m_all_cv.arff.PolyK.model";
        String configFileName = "scale3_IS09_emotion_khiet.conf";

        copyFile("SMILExtract");
        File sme = new File(getApplicationContext().getFilesDir().getAbsolutePath()+"/SMILExtract");
        sme.setExecutable(true);
        copyFile(configFileName);
        copyFile(modelFileName_f);
        copyFile(modelFileName_m);

        String pathAssets = getApplicationContext().getFilesDir().getAbsolutePath();
        String pathLib = getApplicationContext().getApplicationInfo().nativeLibraryDir;

        File outFeatureFile = new File(path,"out.arff");
        File configFile = new File(pathAssets+"/"+configFileName);
        File SMILExtractFile = new File(pathAssets+"/SMILExtract");

        FeatureExtraction fe = new FeatureExtraction(configFile,SMILExtractFile,pathLib,outFeatureFile);

        final String promptSmilextract =
                "Speech analysis processing...";

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ExertionDetection.this,promptSmilextract,Toast.LENGTH_LONG).show();
            }
        });
        // Convert our recorded audio from PCM to WAV format and immediately do feature extraction as well.
        PCM2WAV bl = new PCM2WAV(inFile,outDir,sampleFreq,fe);
        bl.chopFE();

        // Select appropriate model depending on user input
        String modelPath;
        if (gender.equals("female")){
            modelPath = pathAssets+"/"+modelFileName_f;
        } else{
            modelPath = pathAssets+"/"+modelFileName_m;
        }

        File modelFile = new File(modelPath);
        // Do fatigue level prediction
        WekaPrediction wp = new WekaPrediction(modelFile,outFeatureFile);
        int predictionClass = wp.doPredict();

 		int colour =Color.WHITE;
        if (predictionClass == 0) {
            colour = Color.GREEN ; //green
        } else if (predictionClass == 1) {
            colour = Color.YELLOW ; //yellow
        } else {
            colour = Color.RED ; //red
        }
        final int finalColour = colour;
       /* runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView fatigue=(TextView)findViewById(R.id.textView_result);
                fatigue.setBackgroundColor(finalColour);
            }
        });*/
        HashMap<String, Integer> map=new HashMap();

        map.put(inFile.getName(), finalColour);
        //delete the features file
        outFeatureFile.delete();
        Log.i("color","mapResult in ED class:"+inFile.getName() + " : " + finalColour);
        //System.out.println("*******************************color:"+finalColour);
        return map;
    }
}