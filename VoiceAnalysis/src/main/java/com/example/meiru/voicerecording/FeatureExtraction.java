package com.example.meiru.voicerecording;

import android.util.Log;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import weka.core.Environment;

/**
 * @author Meiru Mu <m.mu@utwente.nl>
 * @description This class is basically there to run the openSmile executable.
 */
public class FeatureExtraction {

    private File configFile;
    private File SMILExtractFile;
    private String pathLib;
    private File outFeatureFile;

    /**
	 * @description Constructor
	 * @param configFile The config file used for openSmile
	 * @param SMILExtractFile The SMILExtract executable
	 * @param pathLib The location of the necessary libraries needed to run SMILExtract
	 * @param outFeatureFile The file for storing the features extracted by SMILExtract.
	 * @return Nothing.
	 */
    public FeatureExtraction(File configFile, File SMILExtractFile, String pathLib, File outFeatureFile) {
        this.configFile = configFile;
        this.SMILExtractFile = SMILExtractFile;
        this.pathLib = pathLib;
        this.outFeatureFile = outFeatureFile;
    }

    /**
	 * @description Extracts features from a audio fragment
	 * @param inChoppedWavFile The audio file
	 * @return Nothing.
	 */
    public void openSmileExecute(File inChoppedWavFile){
        Log.d("blaat","openSmileExecute called");

		List<String> commands = new ArrayList<String>();
		commands.add(SMILExtractFile.getAbsolutePath());
		commands.add("-C");
		commands.add(configFile.getAbsolutePath());
		commands.add("-I");
		commands.add(inChoppedWavFile.getAbsolutePath());
		commands.add("-nologfile");
		commands.add("1");

		//commands.add("-logfile");
		//commands.add(android.os.Environment.getExternalStorageDirectory().getAbsolutePath()+"/smile.log");
		//commands.add("-l");
		//commands.add("9");
		//commands.add("-appendLogfile");
		//commands.add("1");

		commands.add("-O");
		commands.add(outFeatureFile.getAbsolutePath());
		commands.add("-class");
		commands.add("low");

		ProcessBuilder prb = new ProcessBuilder();
		prb.command(commands);
		prb.environment().put("LD_LIBRARY_PATH", pathLib + ":$LD_LIBRARY_PATH");
		prb.redirectErrorStream(true);
		Process pr = null;
		int exv=0; // Exit value
		String output = "";
		try {
			pr = prb.start();
			BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null) {
				output+=line;
			}
			exv = pr.waitFor();
		} catch(IOException e) {
			Log.d("blaat", "Caught IOE error during feature extraction" + e.getMessage());
		} catch(InterruptedException e) {
			Log.d("blaat", "Caught IE error during feature extraction" + e.getMessage());
		} finally {
			pr.destroy();
		}
	}
}
