package com.example.meiru.voicerecording;

import android.util.Log;

import java.io.File;
import java.io.InputStream;

import weka.core.Instance;
import weka.core.Instances;

import weka.classifiers.Classifier;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * @author Meiru Mu <m.mu@utwente.nl>
 * @description This class does the exertion detection using Weka.
 */
public class WekaPrediction {
    private File model;
    private File features;

    /**
	 * @description Constructor
	 * @param model The model used to do the prediction
	 * @param features The features from each piece of audio
	 */
    public WekaPrediction(File model, File features) {
        this.model = model;
        this.features = features;
    }
    
    /**
	 * @description Does the prediction and returns the level of exertion
	 * @return The exertion level as an integer 
	 * <p><ul>
	 * <li> 0 Low exertion 
	 * <li> 1 Medium exertion 
	 * <li> 2 High exertion 
	 * </ul>
	 */    
    public int doPredict() {
        // Load model from file
        Classifier cls = null;
        try {
            cls = (Classifier) weka.core.SerializationHelper.read(model.getAbsolutePath());
        } catch(Exception e) {
            Log.i("blaat", "Could not serialise model file :(");
            Log.i("blaat",e.toString());
        }

        // Load test data from file
        Instances data = null;
        try {

            DataSource source = new DataSource(features.getAbsolutePath());

            data = source.getDataSet();

            // set class attribute
            data.setClassIndex(data.numAttributes() - 1); // Class index is usually last column

        } catch(Exception e) {
            Log.i("blaat", "Could not open input arff file :(");
        }

        // Do predictions
        double lows = 0;
        double middles = 0;
        double highs = 0;
        for (int i = 0; i < data.numInstances(); i++) {
            Instance inst = data.instance(i);
            try {
                double actualClassValue = inst.classValue();
                String actual = inst.classAttribute().value((int)actualClassValue);

                double pred = cls.classifyInstance(inst);
                Log.i("blaat","Pred value is"+pred);
                String result = inst.classAttribute().value((int)pred);
                Log.i("blaat","Predicted is "+result);

                // Count number of high classifications
                if (result.equals("high")) {
                    highs++;
                } else if (result.equals("low")) {
                    lows++;
                } else {
                    middles++;
                }

            } catch(Exception e) {
                Log.i("blaat", "Classification oops...");
            }
        }

        // Majority vote, perhaps add unresolved option ??
        if ((highs > middles) && (highs > lows)) {
            Log.i("blaat", "high: " + highs + "/" + data.numInstances());
            return 2;
        }
        else if ((middles >= highs) && (middles > lows)){
            Log.i("blaat", "middle: " + middles + "/" + data.numInstances());
            return 1;

        } else {
            Log.i("blaat", "low: " + lows + "/" + data.numInstances());
            return 0;
        }
    }
}
