package com.example.meiru.voicerecording;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import weka.core.SystemInfo;
import weka.core.converters.Saver;

/**
 * @author Meiru Mu <m.mu@utwente.nl>
 * @description This class is converts a PCM audio sample to overlapping pieces of WAV 
 * audio samples. Features are extracted immediately from each WAV piece. This (difficult)
 * conversion is needed as SMILExtract cannot handle raw PCM audio.
 */  
public class PCM2WAV {

    private long tc; // Centre piece duration expressed in seconds
    private long to; // Overlap piece expressed in seconds
    private int wvChannels;
    private int wvBitsPerSample;
    private File inFile;
    private File outDir;
    private int wvSampleRate;
    private FeatureExtraction fe;

    
    /**
	 * @description Constructor
	 * @param inFile Input PCM audio file
	 * @param outDir Location to store the output
	 * @param wvSampleRate Sample reate of the input audio file
	 * @param FeatureExtraction Feature extractor
	 */
    public PCM2WAV(File inFile, File outDir, int wvSampleRate,FeatureExtraction fe) {
        tc = 0; // Centre piece duration expressed in seconds
        to = 2; // Overlap piece expressed in seconds
        wvChannels = 1;
        wvBitsPerSample = 16;
        this.inFile = inFile;
        this.outDir = outDir;
        this.wvSampleRate = wvSampleRate;
        this.fe = fe;
    }

    /**
	 * @description 'Chops' the input audio file and extract features from each piece.
	 */
    public void chopFE() {
        Log.d("blaat", "ChopFE called");
        int wvByteRate = wvSampleRate*wvChannels*wvBitsPerSample/8;
            int bc = (int)tc*wvByteRate; // Centre piece expressed in bytes
            int bo = (int)to*wvByteRate; // Overlap piece expressed in bytes

               FileInputStream is = null;

            try {
                is = new FileInputStream(inFile);
                //Log.d("blaat","BO in bytes "+bo);
                byte[] buffer = new byte[bc + bo];
                short[] sbuffer = new short[(bc+bo)/2];
                byte[] buffero = new byte[bo]; // Buffer for overlap
                short[] sbuffero = new short[bo/2];
                is.read(buffero); // Initial piece for overlap

                for (long i = bo; i <= inFile.length()-bc-bo; i=i+bc+bo) {
                    float ts = (i-bo)/wvByteRate;
                    File outFILE = new File(Environment.getExternalStorageDirectory(),"out.wav");
                    DataOutputStream os = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outFILE),bc+2*bo));
                    is.read(buffer);
                    writeWAVheader(os,bc+2*bo,wvSampleRate,wvChannels,wvBitsPerSample);

                    // WAV data is in little endian format !!
                    long start = System.currentTimeMillis();
                    ByteBuffer.wrap(buffero).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(sbuffero);
                    ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(sbuffer);
                    for (int j=0; j < sbuffero.length; j++) {
                        os.writeShort(sbuffero[j]);
                    }
                    for (int j=0; j < sbuffer.length; j++) {
                        os.writeShort(sbuffer[j]);
                    }

                    long extime = System.currentTimeMillis()-start;
                    Log.d("blaat", "Took "+extime + "ms to run."); //about 30ms for each 4s segment

                    os.flush();
                    os.close();

                    // Save overlap piece for next round
                    java.lang.System.arraycopy(buffer,bc,buffero,0,bo);

                    // Perform feature extraction
                    fe.openSmileExecute(outFILE);

                }

                is.close();
            } catch (IOException e) {
                Log.d("blaat","Exception in PCM2WAV");
            }

        }

        /**
		 * @description Write a WAV/RIFF header to a file
		 * @param os Were to write the header to
		 * @param wvDataSize Size of the audio part
		 * @param wvSampleRate Sample rate of the audio file
		 * @param wvChannels Number of channels in the audio file
		 * @param wvBitsPerSample Number of bits per sample
		 */
        private static void writeWAVheader(DataOutputStream os,int wvDataSize,int wvSampleRate, int wvChannels,int wvBitsPerSample) {
            // Credits: http://stackoverflow.com/questions/9179536/writing-pcm-recorded-data-into-a-wav-file-java-android
            //          http://soundfile.sapp.org/doc/WaveFormat/
            int wvByteRate = wvSampleRate*wvChannels*wvBitsPerSample/8;
            int wvBlockAlign = wvChannels*wvBitsPerSample/8;

            try {
                os.writeBytes("RIFF");
                os.write(intToByteArray((int)36 + wvDataSize), 0, 4);  // 04 - how big is the rest of this file?
                os.writeBytes("WAVE");                                 // 08 - WAVE
                os.writeBytes("fmt ");                                 // 12 - fmt
                os.write(intToByteArray((int)16), 0, 4);  // 16 - size of this chunk, 16 for PCM
                os.write(shortToByteArray((short)1), 0, 2);     // 20 - what is the audio format? 1 for PCM = Pulse Code Modulation
                os.write(shortToByteArray((short)wvChannels), 0, 2);   // 22 - mono or stereo? 1 or 2?  (or 5 or ???)
                os.write(intToByteArray((int)wvSampleRate), 0, 4);     // 24 - samples per second (numbers per second)
                os.write(intToByteArray((int)wvByteRate), 0, 4);       // 28 - bytes per second
                os.write(shortToByteArray((short)wvBlockAlign), 0, 2); // 32 - # of bytes in one sample, for all channels
                os.write(shortToByteArray((short)wvBitsPerSample), 0, 2);  // 34 - how many bits in a sample(number)?  usually 16 or 24
                os.writeBytes("data");                                 // 36 - data
                os.write(intToByteArray((int)wvDataSize), 0, 4);       // 40 - how big is this data chunk
            } catch (IOException e) {
                Log.d("blaat","Exception in writeWAVheader");
            }
        }

		/**
		 * @description Convert an int to byte array (little endian)
		 * @param i Integer
		 * @return b A byte array
		 */
        private static byte[] intToByteArray(int i) {
            byte[] b = new byte[4];
            b[0] = (byte) (i & 0x00FF);
            b[1] = (byte) ((i >> 8) & 0x000000FF);
            b[2] = (byte) ((i >> 16) & 0x000000FF);
            b[3] = (byte) ((i >> 24) & 0x000000FF);
            return b;
        }

        /**
		 * @description Convert a short to a byte array (little endian)
		 * @param data Short
		 * @return A byte array
		 */
        private static byte[] shortToByteArray(short data) {
            return new byte[]{(byte)(data & 0xff),(byte)((data >>> 8) & 0xff)};
        }

}


