package com.askcs.android.affectbutton;

public interface AffectListener {
  
  public void onAffectChanged( Affect affect );
  
}
