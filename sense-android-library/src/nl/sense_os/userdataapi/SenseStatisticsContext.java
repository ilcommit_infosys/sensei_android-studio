package nl.sense_os.userdataapi;

/**
 * Created by tatsuya on 22/03/16.
 */
public class SenseStatisticsContext {
    public static final String DOMAIN = "domain";
    public static final String GROUP = "group";
    public static final String USER = "user";

    private SenseStatisticsContext() { }
}
